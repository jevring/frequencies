/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.scoundrel.ui;

import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.hooks.SequencerVisualizer;
import net.jevring.frequencies.v2.input.KeyTimings;
import net.jevring.frequencies.v2.input.sequencer.Sequencer;
import net.jevring.frequencies.v2.ui.*;
import net.jevring.frequencies.v2.util.ValueFormatter;
import net.jevring.scoundrel.voices.DrumVoice;

import javax.swing.*;
import java.awt.*;

import static net.jevring.frequencies.v2.ui.GridBadLayoutUtils.gbc;

/**
 * A drum sequencer for scoundrel.
 *
 * @author markus@jevring.net
 * @created 2021-03-19 23:42
 */
public class JScoundrelSequencer extends JPanel implements SequencerVisualizer {
	private final JControlledTextIconButton startSequencerButton;
	private final JSequencerPad[][] sequencerStepPads;

	public JScoundrelSequencer(Controls controls, DrumVoice[] voices, KeyTimings keyTimings) {
		sequencerStepPads = new JSequencerPad[16][voices.length];
		startSequencerButton = new JControlledTextIconButton('▶', controls.getBooleanControl("sequencer-active"), true);
		// todo: have a little box with 4 numbered boxes in it that lets us choose a pattern for just that voice. 
		//  Should activate at the end of the sequence (this will definitely be too small to activate without a mouse).
		//  This hard about this one! This might be too much.
		//  With the mutes, it's already perfectly possible to perform with this. 

		JCheckBox midiClockIn = new JControlledCheckBox("MIDI Clock IN", controls.getBooleanControl("receive-midi-clock-active"));
		midiClockIn.setHorizontalTextPosition(SwingConstants.CENTER);
		midiClockIn.setVerticalTextPosition(SwingConstants.BOTTOM);

		JKnob tempoKnob = new JKnob("Tempo", controls.getControl("tempo"), ValueFormatter.INTEGER);

		setLayout(new GridBagLayout());

		// _todo: why did these things all suddenly acquire borders?!?!?!
		// It was because the ipadx and ipady in the gbc. That's what it's been this whole time!
		// I'm keeping this comment here to remind myself if it ever happens again.
		// Padding is now set to 0, which seems to work for thief as well

		for (int instrument = 0; instrument < voices.length; instrument++) {
			DrumVoice voice = voices[instrument];

			int row = voices.length - instrument; // invert the voice order so that the kick is at the bottom
			add(new JLabel(voice.getName()), gbc(0, row));
			String controlPrefix = voice.getControlPrefix();

			add(new JInstantPad(keyTimings, voice.getInstructionNote()), gbc(1, row));
			add(new JSequencerPad(controls.getBooleanControl(controlPrefix + "-sequencer-mute")), gbc(2, row));
			for (int sequenceStep = 0; sequenceStep < 16; sequenceStep++) {
				JSequencerPad sequencerStepPad = new JSequencerPad(controls.getBooleanControl(controlPrefix + "-sequencer-enabled-" + sequenceStep));
				add(sequencerStepPad, gbc(3 + sequenceStep, row));
				sequencerStepPads[sequenceStep][instrument] = sequencerStepPad;
			}
		}

		add(tempoKnob, gbc(20, 3, 1, 2));
		add(midiClockIn, gbc(20, 5, 2, 1));
		add(new JControlledTextIconButton('⏹', controls.getBooleanControl("sequencer-active"), false), gbc(20, 6));
		add(startSequencerButton, gbc(20, 7));


		JLabel hitLabel = new JLabel("Hit");
		hitLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(hitLabel, gbc(1, 8));
		JLabel muteLabel = new JLabel("Mute");
		muteLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(muteLabel, gbc(2, 8));
		for (int i = 0; i < 16; i++) {
			JLabel counter = new JLabel(String.valueOf(i + 1));
			counter.setHorizontalAlignment(SwingConstants.CENTER);
			//counter.setHorizontalTextPosition(SwingConstants.CENTER);
			add(counter, gbc(3 + i, 8));
		}
	}

	@Override
	public void visualizedSequencerStep(int step, boolean on) {
		for (JSequencerPad pad : sequencerStepPads[step]) {
			pad.on(on);
		}
	}

	@Override
	public void visualizedSequencerBeat(Sequencer sequencer) {
		startSequencerButton.setHighlightOn(!startSequencerButton.isHighlightOn());
	}
}
