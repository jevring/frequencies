/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.scoundrel;

import net.jevring.frequencies.v2.configuration.Configuration;
import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.hooks.SequencerVisualizer;
import net.jevring.frequencies.v2.input.midi.MidiManager;
import net.jevring.frequencies.v2.input.midi.MidiReceiver;
import net.jevring.frequencies.v2.output.Device;
import net.jevring.frequencies.v2.ui.*;
import net.jevring.frequencies.v2.util.ValueFormatter;
import net.jevring.scoundrel.ui.JScoundrelSequencer;

import javax.swing.*;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;

/**
 * The full UI for {@link Scoundrel}.
 *
 * @author markus@jevring.net
 * @created 2021-03-08
 */
public class ScoundrelUI extends JPanel {
	private final JMiniWaveformDisplay miniWaveformDisplay = new JMiniWaveformDisplay();
	private final JScoundrelSequencer sequencer;
	private final Configuration configuration;
	private final Scoundrel scoundrel;
	private final Controls controls;
	private volatile JFrame window;

	public ScoundrelUI(Scoundrel scoundrel) {
		this.configuration = scoundrel.getConfiguration();
		this.scoundrel = scoundrel;
		this.controls = scoundrel.getControls();
		this.sequencer = new JScoundrelSequencer(controls, scoundrel.getVoices(), scoundrel.getKeyTimings());
		createUI();
	}

	public JFrame displayUI(Consumer<Device> restartWithNewDevice) {
		this.window = SwingUtils.showWindowFor("Scoundrel v1.0-SNAPSHOT", this, 1250, 900);
		configureMenu(window, restartWithNewDevice);
		return window;
	}

	private void createUI() {
		SwingUtilities.invokeLater(() -> {
			// todo: with a non-standard width here, the waveform oscillator background is not correctly centered
			setLayout(new FlowLayout());
			JPanel kick = new JPanel(new GridLayout(4, 2));
			kick.setBorder(BorderFactory.createTitledBorder("Kick"));
			int width = 75;
			kick.add(new JKnob("Level", controls.getControl("kick-level"), ValueFormatter.PURE, width));
			kick.add(new JVariableOscillatorKnob(controls.getControl("kick-variable-waveform")));
			kick.add(new JKnob("Volume hold", controls.getControl("kick-volume-envelope-hold"), ValueFormatter.MILLISECONDS, width));
			kick.add(new JKnob("Volume decay", controls.getControl("kick-volume-envelope-decay"), ValueFormatter.MILLISECONDS, width));
			kick.add(new JKnob("Pitch decay", controls.getControl("kick-pitch-envelope-decay"), ValueFormatter.MILLISECONDS, width));
			kick.add(new JKnob("Pitch depth", controls.getControl("kick-pitch-envelope-modulation-depth"), ValueFormatter.PURE, width));
			kick.add(new JKnob("Noise decay", controls.getControl("kick-noise-envelope-decay"), ValueFormatter.MILLISECONDS, width));
			add(kick);

			JPanel snare = new JPanel(new GridLayout(4, 2));
			snare.setBorder(BorderFactory.createTitledBorder("Snare"));
			snare.add(new JKnob("Level", controls.getControl("snare-level"), ValueFormatter.PURE, width));
			snare.add(new JVariableOscillatorKnob(controls.getControl("snare-variable-waveform")));
			snare.add(new JKnob("Volume decay", controls.getControl("snare-volume-envelope-decay"), ValueFormatter.MILLISECONDS, width));
			snare.add(new JKnob("Pitch decay", controls.getControl("snare-pitch-envelope-decay"), ValueFormatter.MILLISECONDS, width));
			snare.add(new JKnob("Pitch depth", controls.getControl("snare-pitch-envelope-modulation-depth"), ValueFormatter.PURE, width));
			snare.add(new JKnob("Noise decay", controls.getControl("snare-noise-envelope-decay"), ValueFormatter.MILLISECONDS, width));
			snare.add(new JKnob("Filter Hz", controls.getControl("snare-noise-filter-cutoff-frequency"), ValueFormatter.PURE, width));
			//snare.add(new JKnob("Filter Q", controls.getControl("snare-noise-q-resonance-emphasis"), ValueFormatter.PURE, width));
			//snare.add(new JControlledComboBox(scoundrel.getControls().getDiscreteControl("snare-noise-filter"), 240));
			//snare.add(new JKnob("Noise volume", controls.getControl("snare-noise-volume"), ValueFormatter.PERCENT, width));
			//snare.add(new JKnob("Waveform volume", controls.getControl("snare-waveform-volume"), ValueFormatter.PERCENT, width));
			add(snare);

			JPanel simpleTom = new JPanel(new GridLayout(4, 2));
			simpleTom.setBorder(BorderFactory.createTitledBorder("Simple Tom"));
			simpleTom.add(new JKnob("Level", controls.getControl("simple-tom-level"), ValueFormatter.PURE, width));
			simpleTom.add(new JVariableOscillatorKnob(controls.getControl("simple-tom-variable-waveform")));
			simpleTom.add(new JKnob("Volume decay", controls.getControl("simple-tom-volume-envelope-decay"), ValueFormatter.MILLISECONDS, width));
			simpleTom.add(new JKnob("Pitch decay", controls.getControl("simple-tom-pitch-envelope-decay"), ValueFormatter.MILLISECONDS, width));
			simpleTom.add(new JKnob("Pitch depth", controls.getControl("simple-tom-pitch-envelope-modulation-depth"), ValueFormatter.PURE, width));
			//simpleTom.add(new JKnob("Filter Q", controls.getControl("simpleTom-noise-q-resonance-emphasis"), ValueFormatter.PURE, width));
			//simpleTom.add(new JControlledComboBox(scoundrel.getControls().getDiscreteControl("simpleTom-noise-filter"), 240));
			//simpleTom.add(new JKnob("Noise volume", controls.getControl("simpleTom-noise-volume"), ValueFormatter.PERCENT, width));
			//simpleTom.add(new JKnob("Waveform volume", controls.getControl("simpleTom-waveform-volume"), ValueFormatter.PERCENT, width));
			add(simpleTom);

			JPanel laserTom = new JPanel(new GridLayout(4, 3));
			laserTom.setBorder(BorderFactory.createTitledBorder("Laser Tom"));
			laserTom.add(new JKnob("Level", controls.getControl("laser-tom-level"), ValueFormatter.PURE, width));
			laserTom.add(new JVariableOscillatorKnob(controls.getControl("laser-tom-osc-1-variable-waveform")));
			laserTom.add(new JVariableOscillatorKnob(controls.getControl("laser-tom-osc-2-variable-waveform")));
			laserTom.add(new JKnob("Volume decay", controls.getControl("laser-tom-volume-envelope-decay"), ValueFormatter.MILLISECONDS, width));
			laserTom.add(new JKnob("Pitch decay", controls.getControl("laser-tom-pitch-envelope-decay"), ValueFormatter.MILLISECONDS, width));
			laserTom.add(new JKnob("Pitch depth", controls.getControl("laser-tom-pitch-envelope-modulation-depth"), ValueFormatter.PURE, width));
			laserTom.add(new JKnob("Filter Hz", controls.getControl("laser-tom-filter-cutoff-frequency"), ValueFormatter.PURE, width));
			laserTom.add(new JKnob("Filter Q", controls.getControl("laser-tom-q-resonance-emphasis"), ValueFormatter.PURE, width));
			//laserTom.add(new JControlledComboBox(scoundrel.getControls().getDiscreteControl("laser-tom-filter"), 240));
			laserTom.add(new JKnob("Osc1 volume", controls.getControl("laser-tom-osc-1-volume"), ValueFormatter.PERCENT, width));
			laserTom.add(new JKnob("Osc2 volume", controls.getControl("laser-tom-osc-2-volume"), ValueFormatter.PERCENT, width));
			add(laserTom);


			JPanel openHat = new JPanel(new GridLayout(4, 1));
			openHat.setBorder(BorderFactory.createTitledBorder("Open hat"));
			openHat.add(new JKnob("Level", controls.getControl("open-hat-level"), ValueFormatter.PURE, width));
			openHat.add(new JKnob("Volume decay", controls.getControl("open-hat-volume-envelope-decay"), ValueFormatter.MILLISECONDS, width));
			openHat.add(new JKnob("Filter Hz", controls.getControl("open-hat-filter-cutoff-frequency"), ValueFormatter.PURE, width));
			openHat.add(new JKnob("Filter Q", controls.getControl("open-hat-q-resonance-emphasis"), ValueFormatter.PURE, width));
			add(openHat).setPreferredSize(new Dimension(112, 365));

			JPanel closedHat = new JPanel(new GridLayout(4, 1));
			closedHat.setBorder(BorderFactory.createTitledBorder("Closed hat"));
			closedHat.add(new JKnob("Level", controls.getControl("closed-hat-level"), ValueFormatter.PURE, width));
			closedHat.add(new JKnob("Volume decay", controls.getControl("closed-hat-volume-envelope-decay"), ValueFormatter.MILLISECONDS, width));
			closedHat.add(new JKnob("Filter Hz", controls.getControl("closed-hat-filter-cutoff-frequency"), ValueFormatter.PURE, width));
			closedHat.add(new JKnob("Filter Q", controls.getControl("closed-hat-q-resonance-emphasis"), ValueFormatter.PURE, width));
			add(closedHat).setPreferredSize(new Dimension(112, 365));

			JPanel clave = new JPanel(new GridLayout(4, 1));
			clave.setBorder(BorderFactory.createTitledBorder("Clave"));
			clave.add(new JKnob("Level", controls.getControl("clave-level"), ValueFormatter.PURE, width));
			clave.add(new JKnob("Volume decay", controls.getControl("clave-volume-envelope-decay"), ValueFormatter.MILLISECONDS, width));
			add(clave).setPreferredSize(new Dimension(113, 365));

			add(new JPanel()).setPreferredSize(new Dimension(21, 365));

			add(sequencer);
		});
	}

	// region Menu
	private void configureMenu(JFrame window, Consumer<Device> restartWithNewDevice) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				JMenu fileMenu = createFileMenu();
				JMenu midiMenu = createMidiMenu(window);
				JMenu outputMenu = createOutputMenu(restartWithNewDevice);

				JMenuBar mainMenu = new JMenuBar();
				mainMenu.add(fileMenu);
				mainMenu.add(midiMenu);
				mainMenu.add(outputMenu);

				window.setJMenuBar(mainMenu);
			}
		});
	}


	private JMenu createOutputMenu(Consumer<Device> restartWithNewDevice) {
		JMenu outputMenu = styleMenuItem(new JMenu("Outputs"));
		List<Device> supportedSoundDevices = Device.supportedSoundDevices(Scoundrel.AUDIO_FORMAT);
		for (Device supportedSoundDevice : supportedSoundDevices) {
			outputMenu.add(styleMenuItem(new JMenuItem(supportedSoundDevice.describe()))).addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					restartWithNewDevice.accept(supportedSoundDevice);
				}
			});
		}
		return outputMenu;
	}

	private JMenu createFileMenu() {
		JMenuItem resetSequencerMenuItem = styleMenuItem(new JMenuItem("Reset sequencer", KeyEvent.VK_R));
		resetSequencerMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				configuration.resetSequencer();
			}
		});

		JMenu fileMenu = styleMenuItem(new JMenu("File"));
		fileMenu.add(resetSequencerMenuItem);
		return fileMenu;
	}

	private <T extends JMenuItem> T styleMenuItem(T menuItem) {
		// todo: if we change the style, this has to be redone. How can we do that.
		menuItem.setFont(new Font(FontSupport.PREFERRED_FONT, Font.PLAIN, 12));
		menuItem.setForeground(Skins.currentSkin().foreground());
		menuItem.setBackground(Skins.currentSkin().background());
		return menuItem;
	}

	private JMenu createMidiMenu(JFrame window) {
		MidiManager midiManager = scoundrel.getMidiManager();
		JMenu midiDeviceMenu = styleMenuItem(new JMenu("Midi device"));
		midiDeviceMenu.addMenuListener(new MenuListener() {
			@Override
			public void menuSelected(MenuEvent e) {
				midiDeviceMenu.removeAll();

				for (String compatibleDevice : midiManager.getMidiDevices().compatibleDevices()) {
					JCheckBoxMenuItem menuItem = new JCheckBoxMenuItem(compatibleDevice, midiManager.isCurrentDevice(compatibleDevice));
					midiDeviceMenu.add(styleMenuItem(menuItem)).addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							if (midiManager.isCurrentDevice(compatibleDevice)) {
								midiManager.disableMidiDevice(compatibleDevice);
							} else {
								midiManager.enableMidiDevice(compatibleDevice);
							}
						}
					});
				}
			}

			@Override
			public void menuDeselected(MenuEvent e) {
			}

			@Override
			public void menuCanceled(MenuEvent e) {
			}
		});

		JMenu midiMappingsMenu = styleMenuItem(new JMenu("Midi mappings"));
		midiMappingsMenu.add(styleMenuItem(new JMenuItem("Map controls", KeyEvent.VK_C))).addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						MidiMapperGlassPane midiMapperGlassPane =
								new MidiMapperGlassPane(ScoundrelUI.this, scoundrel.getMidiManager().getMidiMappings());
						window.setGlassPane(midiMapperGlassPane);
						midiMapperGlassPane.setVisible(true);
					}
				});
			}
		});
		midiMappingsMenu.add(styleMenuItem(new JMenuItem("Save midi config", KeyEvent.VK_S))).addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				midiManager.writeToDefaultFile();
			}
		});
		midiMappingsMenu.add(styleMenuItem(new JMenuItem("Save midi config as", KeyEvent.VK_A))).addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser(configuration.ensureConfigurationDirectoryExists());
				int response = fileChooser.showSaveDialog(window);
				if (response == JFileChooser.APPROVE_OPTION) {
					midiManager.writeToFile(fileChooser.getSelectedFile());
				}
			}
		});

		MidiReceiver midiReceiver = midiManager.getReceiver();
		JMenu midiChannelsMenu = styleMenuItem(new JMenu("Midi channels (notes)"));
		midiChannelsMenu.addMenuListener(new MenuListener() {
			@Override
			public void menuSelected(MenuEvent e) {
				midiChannelsMenu.removeAll();

				midiChannelsMenu.add(styleMenuItem(new JCheckBoxMenuItem("All", midiReceiver.listensOnAllChannels()))).addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						midiReceiver.listenOnAllChannels();
						// This menu item can't really be unchecked. 
						// The other menu items will have to be checked instead
					}
				});
				midiChannelsMenu.add(styleMenuItem(new JCheckBoxMenuItem("None", midiReceiver.listensOnNoChannels()))).addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						midiReceiver.listenOnNoChannels();
						// This menu item can't really be unchecked. 
						// The other menu items will have to be checked instead
					}
				});
				for (int i = 1; i <= 16; i++) {
					final int channel = i;
					JCheckBoxMenuItem checkBoxForChannel = new JCheckBoxMenuItem("Channel " + channel, midiReceiver.listensToNotesOnChannel(channel));
					midiChannelsMenu.add(styleMenuItem(checkBoxForChannel)).addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							boolean currentlyListensOnThisChannel = midiReceiver.listensToNotesOnChannel(channel);
							midiReceiver.listenToNotesOnChannel(channel, !currentlyListensOnThisChannel);
						}
					});
				}
			}

			@Override
			public void menuDeselected(MenuEvent e) {
			}

			@Override
			public void menuCanceled(MenuEvent e) {
			}
		});

		JMenu midiMenu = styleMenuItem(new JMenu("Midi"));
		midiMenu.add(midiDeviceMenu);
		midiMenu.add(midiMappingsMenu);
		midiMenu.add(midiChannelsMenu);
		return midiMenu;
	}

	//endregion

	//region Visualization
	public void visualizeLoopTime(double percentageOfMax) {
		String labelTest = String.format(Locale.US, "Loop: %03.1f%%", percentageOfMax);
		//SwingUtilities.invokeLater(() -> loopTimeLabel.setText(labelTest));
	}

	public void visualizeNewWaveformChunk(double[] samples) {
		miniWaveformDisplay.newChunk(samples);
	}

	public SequencerVisualizer getSequenceVisualizer() {
		return sequencer;
	}

	//endregion
}
