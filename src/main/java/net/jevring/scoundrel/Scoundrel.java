/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.scoundrel;

import net.jevring.frequencies.v2.configuration.Configuration;
import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.control.DiscreteControlListener;
import net.jevring.frequencies.v2.control.curves.Exponential;
import net.jevring.frequencies.v2.control.curves.Linear;
import net.jevring.frequencies.v2.effects.EffectsChain;
import net.jevring.frequencies.v2.engine.MainLoopPullingEngine;
import net.jevring.frequencies.v2.engine.MasterVolume;
import net.jevring.frequencies.v2.engine.Mono;
import net.jevring.frequencies.v2.envelopes.Envelopes;
import net.jevring.frequencies.v2.filters.Filters;
import net.jevring.frequencies.v2.hooks.SequencerVisualizer;
import net.jevring.frequencies.v2.hooks.Visualizer;
import net.jevring.frequencies.v2.input.InstructionInput;
import net.jevring.frequencies.v2.input.KeyTimings;
import net.jevring.frequencies.v2.input.midi.MidiManager;
import net.jevring.frequencies.v2.input.tempo.MainLoopTempoSource;
import net.jevring.frequencies.v2.input.tempo.MidiClockTempoSource;
import net.jevring.frequencies.v2.modulation.matrix.ModulationMatrix;
import net.jevring.frequencies.v2.output.Device;
import net.jevring.frequencies.v2.ui.Skins;
import net.jevring.scoundrel.input.DrumInput;
import net.jevring.scoundrel.input.DrumSequencer;
import net.jevring.scoundrel.input.DrumVoicePolyphonicSequencerStep;
import net.jevring.scoundrel.input.DrumVoiceStep;
import net.jevring.scoundrel.voices.*;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.LineUnavailableException;
import javax.swing.*;
import java.io.File;
import java.util.function.Consumer;

/**
 * Scoundrel is a drum synthesizer and sequencer.
 *
 * @author markus@jevring.net
 * @created 2020-04-12 18:04
 */
public class Scoundrel {
	private static final int VOICES = 7;
	private static final float SAMPLE_SIZE_IN_BITS = Short.SIZE;
	private static final float SAMPLE_RATE = 44100f;
	private static final int MONO = 1;
	public static final AudioFormat AUDIO_FORMAT = new AudioFormat(SAMPLE_RATE, (int) SAMPLE_SIZE_IN_BITS, MONO, true, true);
	private final KeyTimings keyTimings = new KeyTimings();
	private final Filters filters = new Filters(SAMPLE_RATE);
	private final Controls controls = new Controls();
	private final Envelopes envelopes = new Envelopes();
	private final MidiManager midiManager;
	private final MainLoopPullingEngine engine;
	private final Configuration configuration;
	private final MainLoopTempoSource mainLoopTempoSource;
	private final MidiClockTempoSource midiClockTempoSource;
	private final DrumSequencer drumSequencer;
	private final DrumVoice[] voices;
	
	// todo: ratchets for every sequencer step
	
	// todo: support velocity settings for each step (hard to record on just the screen, though)

	public Scoundrel() {
		createControls();
		midiClockTempoSource = new MidiClockTempoSource();
		mainLoopTempoSource = new MainLoopTempoSource(controls);
		File dotConfigDirectory = new File(new File(System.getProperty("user.home")), ".config/scoundrel");
		midiManager = new MidiManager(controls, keyTimings, midiClockTempoSource, dotConfigDirectory);
		midiManager.initializeMidi();
		configuration = new Configuration(new ModulationMatrix(controls, VOICES), controls, dotConfigDirectory);


		// todo: these voices should have a default frequency, and then possibly a "tone" knob to move them. 
		//  That way they can always be played using the same midi note, and we can map the whole drum kit to an octave 
		this.voices = new DrumVoice[VOICES];
		voices[KickVoice.VOICE_INDEX] = new KickVoice(controls, SAMPLE_RATE, filters);
		voices[SnareVoice.VOICE_INDEX] = new SnareVoice(controls, SAMPLE_RATE, filters);
		voices[SimpleTomVoice.VOICE_INDEX] = new SimpleTomVoice(controls, SAMPLE_RATE);
		voices[LaserTomVoice.VOICE_INDEX] = new LaserTomVoice(controls, SAMPLE_RATE, filters);
		voices[OpenHatVoice.VOICE_INDEX] = new OpenHatVoice(controls, SAMPLE_RATE, filters);
		voices[ClosedHatVoice.VOICE_INDEX] = new ClosedHatVoice(controls, SAMPLE_RATE, filters);
		voices[ClaveVoice.VOICE_INDEX] = new ClaveVoice(controls, SAMPLE_RATE);
		// todo: want rim shot and hand clap and finger snap as well

		controls.createBooleanControl("sequencer-active", false);

		controls.createBooleanControl("kick-sequencer-mute", false);
		controls.createBooleanControl("snare-sequencer-mute", false);
		controls.createBooleanControl("simple-tom-sequencer-mute", false);
		controls.createBooleanControl("laser-tom-sequencer-mute", false);
		controls.createBooleanControl("open-hat-sequencer-mute", false);
		controls.createBooleanControl("closed-hat-sequencer-mute", false);
		controls.createBooleanControl("clave-sequencer-mute", false);


		DrumVoicePolyphonicSequencerStep[] sequence = new DrumVoicePolyphonicSequencerStep[16];
		for (int sequenceStep = 0; sequenceStep < sequence.length; sequenceStep++) {
			DrumVoiceStep[] voicesForStep = new DrumVoiceStep[voices.length];
			for (DrumVoice drumVoice : voices) {
				voicesForStep[drumVoice.getVoiceIndex()] =
						new DrumVoiceStep(controls.createBooleanControl(drumVoice.getControlPrefix() + "-sequencer-enabled-" + sequenceStep, false),
						                  controls.getBooleanControl(drumVoice.getControlPrefix() + "-sequencer-mute"),
						                  drumVoice.getInstructionNote());
			}
			sequence[sequenceStep] = new DrumVoicePolyphonicSequencerStep(voicesForStep);
		}
		drumSequencer = new DrumSequencer(sequence, keyTimings, controls, voices.length, SequencerVisualizer.noop());

		InstructionInput instructionInput = new DrumInput(keyTimings, voices);
		engine = new MainLoopPullingEngine(new Mono(), instructionInput, EffectsChain.empty(), new MasterVolume(controls), SAMPLE_RATE, voices);

		controls.getBooleanControl("receive-midi-clock-active").addListener(new DiscreteControlListener<>() {
			@Override
			public void valueChanged(Boolean value, Object source) {
				selectTempoSource(value);
			}
		});
	}

	public void startEngine(Device device, SequencerVisualizer sequencerVisualizer, Visualizer visualizer) throws LineUnavailableException {
		engine.start(device.open());
		engine.setVisualizer(visualizer);
		drumSequencer.setSequencerVisualizer(sequencerVisualizer);
		mainLoopTempoSource.start();
	}

	public void stopEngine() {
		mainLoopTempoSource.stop();
		engine.stop();
	}

	private void createControls() {
		controls.createControl("tempo", 10, 750, 138, new Linear(), true);
		controls.createBooleanControl("receive-midi-clock-active", false);
		// See TempoTools.normalize() for a description of the rate
		controls.createControl("rate", 2, 24, 6, new Linear(), true);

		// This needs to be here because it's a "magic" control used when receiving this MIDI instruction
		controls.createControl("pitch-bend", -1, 1, 0, new Linear());

		controls.createBooleanControl("legato", false);
		controls.createControl("glide-length", 0, 1, 0, new Exponential());
		controls.createControl("overdrive-volume", 0, 10, 0.8, new Exponential());
		controls.createControl("balance", -1, 1, 0, new Linear());

		controls.createControl("clave-level", 0, 10, 0.8, new Exponential());
		controls.createControl("clave-volume-envelope-decay", 0, 300, 200, new Exponential());

		controls.createControl("open-hat-level", 0, 10, 0.8, new Exponential());
		controls.createControl("open-hat-volume-envelope-decay", 0, 2000, 1000, new Exponential());
		controls.createControl("open-hat-filter-cutoff-frequency", 20, 20_000, 4_000, new Exponential());
		controls.createControl("open-hat-q-resonance-emphasis", 0, 1, 0.25, new Exponential());
		controls.createDiscreteControl("open-hat-filter", filters.all(), "HighPassJuceIIRFilter");

		controls.createControl("closed-hat-level", 0, 10, 0.8, new Exponential());
		controls.createControl("closed-hat-volume-envelope-decay", 0, 2000, 200, new Exponential());
		controls.createControl("closed-hat-filter-cutoff-frequency", 20, 20_000, 2_000, new Exponential());
		controls.createControl("closed-hat-q-resonance-emphasis", 0, 1, 0.25, new Exponential());
		controls.createDiscreteControl("closed-hat-filter", filters.all(), "HighPassJuceIIRFilter");

		controls.createControl("laser-tom-volume-envelope-decay", 0, 600, 400, new Exponential());
		controls.createControl("laser-tom-pitch-envelope-decay", 0, 600, 200, new Exponential());

		controls.createControl("laser-tom-level", 0, 10, 0.8, new Exponential());
		controls.createControl("laser-tom-osc-1-variable-waveform", 0, 3, 2, new Linear());
		controls.createControl("laser-tom-osc-2-variable-waveform", 0, 3, 2, new Linear());
		controls.createControl("laser-tom-pitch-envelope-modulation-depth", 0, 0.5, 0.25, new Linear());
		controls.createControl("laser-tom-filter-envelope-depth", -1, 1, 0.6, new Linear());
		controls.createControl("laser-tom-filter-cutoff-frequency", 20, 20_000, 4000, new Exponential());
		controls.createControl("laser-tom-q-resonance-emphasis", 0, 1, 0.25, new Exponential());
		controls.createDiscreteControl("laser-tom-filter", filters.all(), "DiodeLadderFilter");

		controls.createControl("laser-tom-osc-1-volume", 0, 1, 0.5, new Linear());
		controls.createControl("laser-tom-osc-2-volume", 0, 1, 0.30, new Linear());

		controls.createControl("simple-tom-volume-envelope-decay", 0, 600, 400, new Exponential());
		controls.createControl("simple-tom-pitch-envelope-decay", 0, 40, 20, new Exponential());

		controls.createControl("simple-tom-level", 0, 10, 0.8, new Exponential());
		controls.createControl("simple-tom-octave-offset", -4, 4, 0, new Linear(), true);
		controls.createControl("simple-tom-detune-semi-tones", -12, 12, 0, new Linear());
		controls.createControl("simple-tom-phase-shift", -0.5, 0.5, 0, new Linear());
		controls.createControl("simple-tom-wave-shape", -1, 1, 0, new Linear());
		controls.createControl("simple-tom-quantization-steps", 1, 16, 16, new Linear());
		controls.createControl("simple-tom-variable-waveform", 0, 3, 2, new Linear());
		controls.createControl("simple-tom-unison-detune-semi-tones", 0, 2, 1, new Exponential());
		controls.createControl("simple-tom-unison-voices", 1, 16, 1, new Exponential(), true);

		controls.createControl("simple-tom-pitch-envelope-modulation-depth", 0, 0.5, 0.25, new Linear());

		controls.createControl("snare-volume-envelope-decay", 0, 500, 244, new Exponential());
		controls.createControl("snare-noise-envelope-decay", 0, 600, 234, new Exponential());
		controls.createControl("snare-pitch-envelope-decay", 0, 40, 20, new Exponential());

		controls.createControl("snare-level", 0, 10, 0.8, new Exponential());
		controls.createControl("snare-octave-offset", -4, 4, 0, new Linear(), true);
		controls.createControl("snare-detune-semi-tones", -12, 12, 0, new Linear());
		controls.createControl("snare-phase-shift", -0.5, 0.5, 0, new Linear());
		controls.createControl("snare-wave-shape", -1, 1, 0, new Linear());
		controls.createControl("snare-quantization-steps", 1, 16, 16, new Linear());
		controls.createControl("snare-variable-waveform", 0, 3, 2, new Linear());
		controls.createControl("snare-unison-detune-semi-tones", 0, 2, 1, new Exponential());
		controls.createControl("snare-unison-voices", 1, 16, 1, new Exponential(), true);

		controls.createControl("snare-pitch-envelope-modulation-depth", 0, 0.5, 0.25, new Linear());

		controls.createControl("snare-noise-filter-cutoff-frequency", 20, 20_000, 13_000, new Exponential());
		controls.createControl("snare-noise-q-resonance-emphasis", 0, 1, 0.0, new Exponential());
		controls.createDiscreteControl("snare-noise-filter", filters.all(), "DiodeLadderFilter");

		controls.createControl("snare-waveform-volume", 0, 1, 0.5, new Linear());
		controls.createControl("snare-noise-volume", 0, 1, 0.65, new Linear());

		controls.createControl("kick-level", 0, 10, 0.8, new Exponential());
		controls.createControl("kick-volume-envelope-hold", 0, 1_000, 20, new Exponential());
		controls.createControl("kick-volume-envelope-decay", 0, 1_000, 500, new Exponential());
		controls.createControl("kick-pitch-envelope-decay", 0, 1_000, 200, new Exponential());
		controls.createControl("kick-noise-envelope-decay", 0, 30, 20, new Exponential());
		controls.createControl("kick-pitch-envelope-modulation-depth", 0, 0.75, 0.37, new Linear());
		controls.createControl("kick-variable-waveform", 0, 3, 2, new Linear());
		controls.createControl("kick-noise-filter-envelope-depth", -1, 1, 0.3, new Linear());
		controls.createControl("kick-noise-filter-cutoff-frequency", 20, 20_000, 20, new Exponential());
		controls.createControl("kick-noise-q-resonance-emphasis", 0, 1, 0.10, new Exponential());
		controls.createDiscreteControl("kick-noise-filter", filters.all(), "DiodeLadderFilter");
		controls.createControl("kick-waveform-volume", 0, 1, 0.5, new Linear());
		controls.createControl("kick-noise-volume", 0, 1, 0.5, new Linear());

		controls.createBooleanControl("velocity-sensitive-keys", false);
		controls.createBooleanControl("clip", true);

	}

	public float getSampleRate() {
		return SAMPLE_RATE;
	}

	public Controls getControls() {
		return controls;
	}

	public Envelopes getEnvelopes() {
		return envelopes;
	}

	public KeyTimings getKeyTimings() {
		return keyTimings;
	}

	public MidiManager getMidiManager() {
		return midiManager;
	}

	public Configuration getConfiguration() {
		return configuration;
	}

	public DrumVoice[] getVoices() {
		return voices;
	}

	private void selectTempoSource(boolean midiSyncOn) {
		if (midiSyncOn) {

			midiClockTempoSource.addSequencer(drumSequencer);

			mainLoopTempoSource.removeSequencer(drumSequencer);

		} else {

			mainLoopTempoSource.addSequencer(drumSequencer);

			midiClockTempoSource.removeSequencer(drumSequencer);

		}
	}


	public static void main(String[] args) throws LineUnavailableException {
		Scoundrel scoundrel = new Scoundrel();
		String headless = System.getProperty("headless");
		if (headless != null && !"false".equalsIgnoreCase(headless)) {
			scoundrel.startEngine(Device.defaultDevice(AUDIO_FORMAT), SequencerVisualizer.noop(), Visualizer.noop());
		} else {
			ScoundrelUI ui = new ScoundrelUI(scoundrel);
			scoundrel.startEngine(Device.defaultDevice(AUDIO_FORMAT), ui.getSequenceVisualizer(), new ScoundrelUIVisualizer(ui));
			JFrame window = ui.displayUI(new Consumer<>() {
				@Override
				public void accept(Device device) {
					try {
						scoundrel.stopEngine();
						scoundrel.startEngine(device, ui.getSequenceVisualizer(), new ScoundrelUIVisualizer(ui));
					} catch (LineUnavailableException e) {
						e.printStackTrace();
					}
				}
			});
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					Skins.setSkin(Skins.defaultSkin(), window);
				}
			});
		}
	}
}
