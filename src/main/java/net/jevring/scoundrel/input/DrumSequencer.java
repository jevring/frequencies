/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.scoundrel.input;

import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.control.DiscreteControlListener;
import net.jevring.frequencies.v2.hooks.SequencerVisualizer;
import net.jevring.frequencies.v2.input.KeyTimings;
import net.jevring.frequencies.v2.input.sequencer.AbstractPolyphonicKeySequencer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A sequencer for multiple concurrent drum tracks.
 *
 * @author markus@jevring.net
 * @created 2021-03-21 00:11
 */
public class DrumSequencer extends AbstractPolyphonicKeySequencer<DrumVoicePolyphonicSequencerStep, SequencerVisualizer> {
	/**
	 * Similarly to {@link AbstractPolyphonicKeySequencer.Voice#currentlyPlayingNotes} this should ideally only contain a single thing, but because we can change the tempo and
	 * the sequence length, we occasionally mess it up.
	 */
	private final List<Integer> currentlyHighlightedSteps = new ArrayList<>();

	public DrumSequencer(DrumVoicePolyphonicSequencerStep[] sequence,
	                     KeyTimings keyTimings,
	                     Controls controls,
	                     int voices,
	                     SequencerVisualizer sequenceVisualizer) {
		super(sequence, keyTimings, controls, voices, sequenceVisualizer);
		controls.getBooleanControl("sequencer-active").addListener(new DiscreteControlListener<>() {
			@Override
			public void valueChanged(Boolean active, Object source) {
				setActive(active);
			}
		});
	}

	@Override
	protected void nextCycle() {
		// nothing for us to do here
	}

	@Override
	protected void visualizeStep(int step) {
		if (currentlyHighlightedSteps.size() > 1) {
			System.out.println("There were currently " + currentlyHighlightedSteps.size() + " steps highlighted: " + currentlyHighlightedSteps);
		}
		Iterator<Integer> iterator = currentlyHighlightedSteps.iterator();
		while (iterator.hasNext()) {
			Integer currentlyHighlightedStep = iterator.next();
			iterator.remove();
			sequencerVisualizer.visualizedSequencerStep(currentlyHighlightedStep, false);
		}
		currentlyHighlightedSteps.add(step);
		sequencerVisualizer.visualizedSequencerStep(step, true);
	}

	@Override
	protected void visualizeBeat() {
		// todo: maybe we should skip this here and only call it on step%4==0 in visualizeStep?
		sequencerVisualizer.visualizedSequencerBeat(this);
	}

	@Override
	protected void setActive(boolean active) {
		super.setActive(active);
		if (sequencerVisualizer != null) {
			// Normally we don't do a null check for the sequencer visualizer, because once we're running, it's never supposed to be null.
			// But when we configure the controls, we haven't yet had time to set it, so we have to check.
			for (int step = 0; step < 16; step++) {
				// Doing this for all allows us to potentially make cool shutdown effects.
				// It also ensures that everything is properly turned off
				sequencerVisualizer.visualizedSequencerStep(step, false);
			}
		}
	}
}
