/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.scoundrel.input;

import net.jevring.frequencies.v2.envelopes.Phase;
import net.jevring.frequencies.v2.input.Instruction;
import net.jevring.frequencies.v2.input.InstructionInput;
import net.jevring.frequencies.v2.input.KeyTiming;
import net.jevring.frequencies.v2.input.KeyTimings;
import net.jevring.scoundrel.voices.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Input for a drum machine where each voice is a specific drum.
 *
 * @author markus@jevring.net
 * @created 2021-03-08 12:48
 */
public class DrumInput implements InstructionInput {
	private final KeyTimings keyTimings;
	private final DrumVoice[] voices;
	private final Instruction[] previousInstruction;

	public DrumInput(KeyTimings keyTimings, DrumVoice[] voices) {
		this.keyTimings = keyTimings;
		this.voices = voices;
		this.previousInstruction = new Instruction[voices.length];
	}

	@Override
	public List<Instruction> inputInstructions() {
		List<Instruction> instructions = new ArrayList<>();
		for (int i = 0; i < voices.length; i++) {
			// Make sure that there's always something
			instructions.add(null);
		}
		List<KeyTiming> keys = keyTimings.keys();
		for (KeyTiming key : keys) {
			// todo: should we do high/mid/low toms?
			
			// It is a lot easier to just pass the frequencies in here.
			// However, if we want a tone setting in each voice, we'll have to re-create the instruction
			if (key.key() == KickVoice.NOTE) {
				int voice = 0;
				instructions.set(voice, keyTimingToInstruction(key, 42, voice));
			} else if (key.key() == SnareVoice.NOTE) {
				int voice = 1;
				instructions.set(voice, keyTimingToInstruction(key, 82.41, voice));
			} else if (key.key() == SimpleTomVoice.NOTE) {
				int voice = 2;
				instructions.set(voice, keyTimingToInstruction(key, 130.81, voice));
			} else if (key.key() == LaserTomVoice.NOTE) {
				int voice = 3;
				instructions.set(voice, keyTimingToInstruction(key, 130.81, voice));
			} else if (key.key() == OpenHatVoice.NOTE) {
				int voice = 4;
				instructions.set(voice, keyTimingToInstruction(key, 0, voice));
			} else if (key.key() == ClosedHatVoice.NOTE) {
				int voice = 5;
				instructions.set(voice, keyTimingToInstruction(key, 0, voice));
			} else if (key.key() == ClaveVoice.NOTE) {
				int voice = 6;
				instructions.set(voice, keyTimingToInstruction(key, 1318.51, voice));
			}
		}
		return instructions;

	}

	private Instruction keyTimingToInstruction(KeyTiming keyTiming, double frequency, int voice) {
		int key = keyTiming.key();
		long age = keyTiming.nanosecondsActivated() + keyTiming.nanosecondsDeactivated();
		Phase volumeEnvelopePhase = voices[voice].getVolumeEnvelope().phase(keyTiming.nanosecondsActivated(), keyTiming.nanosecondsDeactivated());
		if (volumeEnvelopePhase != Phase.IDLE) {
			boolean newInstruction;
			if (previousInstruction[voice] == null ||
			    previousInstruction[voice].getKey() != key ||
			    previousInstruction[voice].getTimeDown() != keyTiming.lastTimeDown()) {
				newInstruction = previousInstruction[voice] == null || previousInstruction[voice].getTimeDown() != keyTiming.lastTimeDown();
			} else {
				newInstruction = false;
			}
			Instruction instruction = new Instruction(key,
			                                          age,
			                                          frequency,
			                                          newInstruction,
			                                          volumeEnvelopePhase,
			                                          keyTiming.nanosecondsActivated(),
			                                          keyTiming.nanosecondsDeactivated(),
			                                          keyTiming.velocity(),
			                                          keyTiming.lastTimeDown());
			previousInstruction[voice] = instruction;
			return instruction;
		} else {
			return null;
		}
	}
}
