/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.scoundrel.voices;

import net.jevring.frequencies.v2.control.BooleanControl;
import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.envelopes.AttackHoldDecayEnvelope;
import net.jevring.frequencies.v2.envelopes.DecayEnvelope;
import net.jevring.frequencies.v2.envelopes.Envelope;
import net.jevring.frequencies.v2.envelopes.ExponentialDecayEnvelope;
import net.jevring.frequencies.v2.filters.ControlledFilter;
import net.jevring.frequencies.v2.filters.Filters;
import net.jevring.frequencies.v2.input.Instruction;
import net.jevring.frequencies.v2.modular.*;
import net.jevring.frequencies.v2.oscillators.Oscillator;
import net.jevring.frequencies.v2.waveforms.QuantizedWaveform;
import net.jevring.frequencies.v2.waveforms.VariableWaveForm;

/**
 * A kick with inspiration from the 909 and 808 from various places. Both can be mimicked reasonably well, but things can also
 * be taken in different directions.
 * <ul>
 *     <li><a href="https://www.musicradar.com/how-to/how-to-recreate-classic-analogue-drum-sounds-in-your-daw-and-with-hardware">https://www.musicradar.com/how-to/how-to-recreate-classic-analogue-drum-sounds-in-your-daw-and-with-hardware</a></li>
 *     <li><a href="https://www.youtube.com/watch?v=A_AWH3SgM84">https://www.youtube.com/watch?v=A_AWH3SgM84</a></li>
 *     <li><a href="https://web.archive.org/web/20160403123708/http://www.soundonsound.com/sos/Feb02/articles/synthsecrets0202.asp">https://web.archive.org/web/20160403123708/http://www.soundonsound.com/sos/Feb02/articles/synthsecrets0202.asp</a></li>
 *     <li><a href="https://www.polynominal.com/site/studio/gear/drum/roland-tr909/roland-tr909-service-manual.pdf>https://www.polynominal.com/site/studio/gear/drum/roland-tr909/roland-tr909-service-manual.pdf</a></li>
 *     <li><a href="http://www.network-909.de/bassdrum.htm">http://www.network-909.de/bassdrum.htm</a></li>
 * </ul>
 *
 * @author markus@jevring.net
 * @created 2021-03-09 20:26
 */
public class KickVoice implements DrumVoice {
	public static final int NOTE = 60; // C4
	public static final int VOICE_INDEX = 0;
	/**
	 * Volume envelope. Controlled 'decay' potentiometer. Controls the basic triangle wave, and thus the length of the sound
	 * ENV1/EG1 in the schematics.
	 */
	private final AttackHoldDecayEnvelope volumeEnvelope = new AttackHoldDecayEnvelope();
	/**
	 * Applies to the low-pass-filtered noise generator.
	 * ENV2/EG2 in the schematics.
	 */
	private final DecayEnvelope noiseEnvelope = new ExponentialDecayEnvelope();
	/**
	 * Controls the pitch of the triangle wave. Controlled 'tune' potentiometer.
	 * ENV3/EG3 in the schematics.
	 */
	private final DecayEnvelope pitchEnvelope = new ExponentialDecayEnvelope();

	private final Source chain;

	public KickVoice(Controls controls, double sampleRate, Filters filters) {
		volumeEnvelope.setAttackInMillis(0);
		controls.getControl("kick-volume-envelope-hold").addRawLongListener(volumeEnvelope::setHoldInMillis);
		controls.getControl("kick-volume-envelope-decay").addRawLongListener(volumeEnvelope::setDecayInMillis);
		controls.getControl("kick-pitch-envelope-decay").addRawLongListener(pitchEnvelope::setDecayInMillis);
		controls.getControl("kick-noise-envelope-decay").addRawLongListener(noiseEnvelope::setDecayInMillis);
		EnvelopeModule volumeEnvelopeModule = new EnvelopeModule(volumeEnvelope, sampleRate);
		EnvelopeModule noiseEnvelopeModule = new EnvelopeModule(noiseEnvelope, sampleRate);
		EnvelopeModule pitchEnvelopeModule = new EnvelopeModule(pitchEnvelope, sampleRate);

		// This oscillator is much more complicated than we need, but it's easier to just re-use the same oscillator, rather than have custom ones
		Oscillator oscillator = new Oscillator(sampleRate);
		// NOTE: Quantized in the outside and variable on the inside behaves better than the other way around.
		oscillator.setWaveform(new QuantizedWaveform(new VariableWaveForm()));
		// It's this large FM range coupled with the envelope that gives us the click and the punch in the kick at the start
		oscillator.setFrequencyModulationModulationRange(96d);
		OscillatorModule oscillatorModule = new OscillatorModule(oscillator);
		oscillatorModule.getFrequencyModulation()
		                .connect(new AttenuverterModule(controls.getControl("kick-pitch-envelope-modulation-depth"), pitchEnvelopeModule));
		controls.getControl("kick-variable-waveform").addRawDoubleListener(oscillator::setWaveformVariation);
		controls.getControl("kick-variable-waveform").set(0, 1, 3, this);

		NoiseModule noiseModule = new NoiseModule();

		FilterModule filterModule = new FilterModule(new ControlledFilter("kick-noise", controls, filters));
		filterModule.setInput(noiseModule);
		filterModule.getCutoffFrequencyModulation()
		            .connect(new AttenuverterModule(controls.getControl("kick-noise-filter-envelope-depth"), pitchEnvelopeModule));

		ScalingModule applyVolumeEnvelope = new ScalingModule(BooleanControl.alwaysTrue());
		applyVolumeEnvelope.setInput(oscillatorModule);
		applyVolumeEnvelope.setScaleInput(volumeEnvelopeModule);

		ScalingModule applyNoiseEnvelope = new ScalingModule(BooleanControl.alwaysTrue());
		applyNoiseEnvelope.setInput(filterModule);
		applyNoiseEnvelope.setScaleInput(noiseEnvelopeModule);

		MixerModule lastMixer = new MixerModule();
		lastMixer.add(applyVolumeEnvelope, controls.getControl("kick-waveform-volume"));
		lastMixer.add(applyNoiseEnvelope, controls.getControl("kick-noise-volume"));

		AttenuverterModule applyLevel = new AttenuverterModule(controls.getControl("kick-level"), lastMixer);

		this.chain = applyLevel;

	}

	@Override
	public double[] samples(Instruction instruction, int samplesToGenerate) {
		return chain.generateSamples(samplesToGenerate, instruction);
	}

	@Override
	public void next() {
		chain.next();
	}

	@Override
	public Envelope getVolumeEnvelope() {
		return volumeEnvelope;
	}

	@Override
	public String getName() {
		return "Kick";
	}

	@Override
	public String getControlPrefix() {
		return "kick";
	}

	@Override
	public int getVoiceIndex() {
		return VOICE_INDEX;
	}

	@Override
	public int getInstructionNote() {
		return NOTE;
	}
}
