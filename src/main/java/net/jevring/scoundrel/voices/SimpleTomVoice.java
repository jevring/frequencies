/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.scoundrel.voices;

import net.jevring.frequencies.v2.control.BooleanControl;
import net.jevring.frequencies.v2.control.ControlListener;
import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.envelopes.DecayEnvelope;
import net.jevring.frequencies.v2.envelopes.Envelope;
import net.jevring.frequencies.v2.envelopes.ExponentialDecayEnvelope;
import net.jevring.frequencies.v2.input.Instruction;
import net.jevring.frequencies.v2.modular.*;
import net.jevring.frequencies.v2.oscillators.ControlledOscillator;
import net.jevring.frequencies.v2.oscillators.Oscillator;

/**
 * A simple tom, kind of like the 808
 * <ul>
 *     <li><a href="https://www.musicradar.com/how-to/how-to-recreate-classic-analogue-drum-sounds-in-your-daw-and-with-hardware">https://www.musicradar.com/how-to/how-to-recreate-classic-analogue-drum-sounds-in-your-daw-and-with-hardware</a></li>
 * </ul>
 *
 * @author markus@jevring.net
 * @created 2021-03-16
 */
public class SimpleTomVoice implements DrumVoice {
	public static final int NOTE = 64; // E4
	public static final int VOICE_INDEX = 2;
	private final DecayEnvelope volumeEnvelope = new ExponentialDecayEnvelope();
	private final DecayEnvelope pitchEnvelope = new ExponentialDecayEnvelope();

	private final Source chain;

	public SimpleTomVoice(Controls controls, double sampleRate) {
		// todo: low=F1, mid=c3, and high=E3 (less pitch depth)
		controls.getControl("simple-tom-volume-envelope-decay").addListener(new ControlListener() {
			@Override
			public void valueChanged(double min, double newValue, double max, Object source) {
				volumeEnvelope.setDecayInMillis((long) newValue);
			}
		});
		controls.getControl("simple-tom-pitch-envelope-decay").addListener(new ControlListener() {
			@Override
			public void valueChanged(double min, double newValue, double max, Object source) {
				pitchEnvelope.setDecayInMillis((long) newValue);
			}
		});
		EnvelopeModule volumeEnvelopeModule = new EnvelopeModule(volumeEnvelope, sampleRate);
		EnvelopeModule pitchEnvelopeModule = new EnvelopeModule(pitchEnvelope, sampleRate);

		// This oscillator is much more complicated than we need, but it's easier to just re-use the same oscillator, rather than have custom ones
		Oscillator oscillator = new ControlledOscillator("simple-tom", controls, sampleRate, true);
		oscillator.setFrequencyModulationModulationRange(96d);
		OscillatorModule oscillatorModule = new OscillatorModule(oscillator);
		oscillatorModule.getFrequencyModulation()
		                .connect(new AttenuverterModule(controls.getControl("simple-tom-pitch-envelope-modulation-depth"), pitchEnvelopeModule));
		controls.getControl("simple-tom-variable-waveform").set(0, 0, 3, this);


		ScalingModule applyVolumeEnvelope = new ScalingModule(BooleanControl.alwaysTrue());
		applyVolumeEnvelope.setInput(oscillatorModule);
		applyVolumeEnvelope.setScaleInput(volumeEnvelopeModule);

		AttenuverterModule applyLevel = new AttenuverterModule(controls.getControl("simple-tom-level"), applyVolumeEnvelope);

		this.chain = applyLevel;

	}

	@Override
	public double[] samples(Instruction instruction, int samplesToGenerate) {
		return chain.generateSamples(samplesToGenerate, instruction);
	}

	@Override
	public void next() {
		chain.next();
	}

	@Override
	public Envelope getVolumeEnvelope() {
		return volumeEnvelope;
	}

	@Override
	public String getName() {
		return "Tom";
	}

	@Override
	public String getControlPrefix() {
		return "simple-tom";
	}

	@Override
	public int getVoiceIndex() {
		return VOICE_INDEX;
	}

	@Override
	public int getInstructionNote() {
		return NOTE;
	}
}
