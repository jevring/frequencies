/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.scoundrel.voices;

import net.jevring.frequencies.v2.control.BooleanControl;
import net.jevring.frequencies.v2.control.ControlListener;
import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.envelopes.DecayEnvelope;
import net.jevring.frequencies.v2.envelopes.Envelope;
import net.jevring.frequencies.v2.envelopes.ExponentialDecayEnvelope;
import net.jevring.frequencies.v2.filters.ControlledFilter;
import net.jevring.frequencies.v2.filters.Filters;
import net.jevring.frequencies.v2.input.Instruction;
import net.jevring.frequencies.v2.modular.*;
import net.jevring.frequencies.v2.oscillators.ControlledOscillator;
import net.jevring.frequencies.v2.oscillators.Oscillator;

/**
 * A snare with inspiration from the 909 and 808 from various places. Both can be mimicked reasonably well, but things can also
 * be taken in different directions.
 * <ul>
 *     <li><a href="https://www.musicradar.com/how-to/how-to-recreate-classic-analogue-drum-sounds-in-your-daw-and-with-hardware">https://www.musicradar.com/how-to/how-to-recreate-classic-analogue-drum-sounds-in-your-daw-and-with-hardware</a></li>
 *     <li><a href="https://www.polynominal.com/site/studio/gear/drum/roland-tr909/roland-tr909-service-manual.pdf>https://www.polynominal.com/site/studio/gear/drum/roland-tr909/roland-tr909-service-manual.pdf</a></li>
 * </ul>
 *
 * @author markus@jevring.net
 * @created 2021-03-14
 */
public class SnareVoice implements DrumVoice {
	public static final int NOTE = 62; // D4
	public static final int VOICE_INDEX = 1;
	private final DecayEnvelope volumeEnvelope = new ExponentialDecayEnvelope();
	private final DecayEnvelope noiseEnvelope = new ExponentialDecayEnvelope();
	private final DecayEnvelope pitchEnvelope = new ExponentialDecayEnvelope();

	private final Source chain;

	public SnareVoice(Controls controls, double sampleRate, Filters filters) {
		// todo: should be played in E2 (160Hz, 100-200Hz range)
		controls.getControl("snare-volume-envelope-decay").addListener(new ControlListener() {
			@Override
			public void valueChanged(double min, double newValue, double max, Object source) {
				volumeEnvelope.setDecayInMillis((long) newValue);
			}
		});
		controls.getControl("snare-pitch-envelope-decay").addListener(new ControlListener() {
			@Override
			public void valueChanged(double min, double newValue, double max, Object source) {
				pitchEnvelope.setDecayInMillis((long) newValue);
			}
		});
		controls.getControl("snare-noise-envelope-decay").addListener(new ControlListener() {
			@Override
			public void valueChanged(double min, double newValue, double max, Object source) {
				noiseEnvelope.setDecayInMillis((long) newValue);
			}
		});
		EnvelopeModule volumeEnvelopeModule = new EnvelopeModule(volumeEnvelope, sampleRate);
		EnvelopeModule noiseEnvelopeModule = new EnvelopeModule(noiseEnvelope, sampleRate);
		EnvelopeModule pitchEnvelopeModule = new EnvelopeModule(pitchEnvelope, sampleRate);

		// This oscillator is much more complicated than we need, but it's easier to just re-use the same oscillator, rather than have custom ones
		Oscillator oscillator = new ControlledOscillator("snare", controls, sampleRate, true);
		oscillator.setFrequencyModulationModulationRange(96d);
		OscillatorModule oscillatorModule = new OscillatorModule(oscillator);
		oscillatorModule.getFrequencyModulation()
		                .connect(new AttenuverterModule(controls.getControl("snare-pitch-envelope-modulation-depth"), pitchEnvelopeModule));
		controls.getControl("snare-variable-waveform").set(0, 0, 3, this);

		NoiseModule noiseModule = new NoiseModule();

		FilterModule filterModule = new FilterModule(new ControlledFilter("snare-noise", controls, filters));
		filterModule.setInput(noiseModule);

		ScalingModule applyVolumeEnvelope = new ScalingModule(BooleanControl.alwaysTrue());
		applyVolumeEnvelope.setInput(oscillatorModule);
		applyVolumeEnvelope.setScaleInput(volumeEnvelopeModule);

		ScalingModule applyNoiseEnvelope = new ScalingModule(BooleanControl.alwaysTrue());
		applyNoiseEnvelope.setInput(filterModule);
		applyNoiseEnvelope.setScaleInput(noiseEnvelopeModule);

		MixerModule lastMixer = new MixerModule();
		lastMixer.add(applyVolumeEnvelope, controls.getControl("snare-waveform-volume"));
		lastMixer.add(applyNoiseEnvelope, controls.getControl("snare-noise-volume"));

		AttenuverterModule applyLevel = new AttenuverterModule(controls.getControl("snare-level"), lastMixer);

		this.chain = applyLevel;

	}

	@Override
	public double[] samples(Instruction instruction, int samplesToGenerate) {
		return chain.generateSamples(samplesToGenerate, instruction);
	}

	@Override
	public void next() {
		chain.next();
	}

	@Override
	public Envelope getVolumeEnvelope() {
		if (volumeEnvelope.getDecayInMillis() > noiseEnvelope.getDecayInMillis()) {
			return volumeEnvelope;
		} else {
			return noiseEnvelope;
		}
	}

	@Override
	public String getName() {
		return "Snare";
	}

	@Override
	public String getControlPrefix() {
		return "snare";
	}

	@Override
	public int getVoiceIndex() {
		return VOICE_INDEX;
	}

	@Override
	public int getInstructionNote() {
		return NOTE;
	}
}
