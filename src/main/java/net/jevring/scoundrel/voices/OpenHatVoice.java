/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.scoundrel.voices;

import net.jevring.frequencies.v2.control.BooleanControl;
import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.envelopes.DecayEnvelope;
import net.jevring.frequencies.v2.envelopes.Envelope;
import net.jevring.frequencies.v2.envelopes.ExponentialDecayEnvelope;
import net.jevring.frequencies.v2.filters.ControlledFilter;
import net.jevring.frequencies.v2.filters.Filters;
import net.jevring.frequencies.v2.input.Instruction;
import net.jevring.frequencies.v2.modular.*;

/**
 * An open hi-hat.
 * <ul>
 *     <li><a href="https://www.musicradar.com/how-to/how-to-recreate-classic-analogue-drum-sounds-in-your-daw-and-with-hardware">https://www.musicradar.com/how-to/how-to-recreate-classic-analogue-drum-sounds-in-your-daw-and-with-hardware</a></li>
 *     <li><a href="https://www.polynominal.com/site/studio/gear/drum/roland-tr909/roland-tr909-service-manual.pdf>https://www.polynominal.com/site/studio/gear/drum/roland-tr909/roland-tr909-service-manual.pdf</a></li>
 * </ul>
 *
 * @author markus@jevring.net
 * @created 2021-03-17
 */
public class OpenHatVoice implements DrumVoice {
	public static final int NOTE = 67; // G4
	public static final int VOICE_INDEX = 4;
	private final DecayEnvelope volumeEnvelope = new ExponentialDecayEnvelope();

	private final Source chain;

	public OpenHatVoice(Controls controls, double sampleRate, Filters filters) {
		controls.getControl("open-hat-volume-envelope-decay").addRawLongListener(volumeEnvelope::setDecayInMillis);
		EnvelopeModule volumeEnvelopeModule = new EnvelopeModule(volumeEnvelope, sampleRate);

		NoiseModule noiseModule = new NoiseModule();

		FilterModule filterModule = new FilterModule(new ControlledFilter("open-hat", controls, filters));
		filterModule.setInput(noiseModule);

		ScalingModule applyNoiseEnvelope = new ScalingModule(BooleanControl.alwaysTrue());
		applyNoiseEnvelope.setInput(filterModule);
		applyNoiseEnvelope.setScaleInput(volumeEnvelopeModule);

		AttenuverterModule applyLevel = new AttenuverterModule(controls.getControl("open-hat-level"), applyNoiseEnvelope);

		this.chain = applyLevel;

	}

	@Override
	public double[] samples(Instruction instruction, int samplesToGenerate) {
		return chain.generateSamples(samplesToGenerate, instruction);
	}

	@Override
	public void next() {
		chain.next();
	}

	@Override
	public Envelope getVolumeEnvelope() {
		return volumeEnvelope;
	}

	@Override
	public String getName() {
		return "Open hat";
	}

	@Override
	public String getControlPrefix() {
		return "open-hat";
	}

	@Override
	public int getVoiceIndex() {
		return VOICE_INDEX;
	}

	@Override
	public int getInstructionNote() {
		return NOTE;
	}
}
