/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.scoundrel.voices;

import net.jevring.frequencies.v2.control.BooleanControl;
import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.envelopes.DecayEnvelope;
import net.jevring.frequencies.v2.envelopes.Envelope;
import net.jevring.frequencies.v2.envelopes.ExponentialDecayEnvelope;
import net.jevring.frequencies.v2.filters.ControlledFilter;
import net.jevring.frequencies.v2.filters.Filters;
import net.jevring.frequencies.v2.input.Instruction;
import net.jevring.frequencies.v2.modular.*;
import net.jevring.frequencies.v2.oscillators.Oscillator;
import net.jevring.frequencies.v2.waveforms.QuantizedWaveform;
import net.jevring.frequencies.v2.waveforms.VariableWaveForm;

/**
 * A weirder tom. I tried to get the natural sound of the 909, but I got something different.
 * <ul>
 *     <li><a href="https://www.musicradar.com/how-to/how-to-recreate-classic-analogue-drum-sounds-in-your-daw-and-with-hardware">https://www.musicradar.com/how-to/how-to-recreate-classic-analogue-drum-sounds-in-your-daw-and-with-hardware</a></li>
 *     <li><a href="https://www.polynominal.com/site/studio/gear/drum/roland-tr909/roland-tr909-service-manual.pdf>https://www.polynominal.com/site/studio/gear/drum/roland-tr909/roland-tr909-service-manual.pdf</a></li>
 * </ul>
 *
 * @author markus@jevring.net
 * @created 2021-03-16
 */
public class LaserTomVoice implements DrumVoice {
	public static final int NOTE = 65; // F4
	public static final int VOICE_INDEX = 3;
	private final DecayEnvelope volumeEnvelope = new ExponentialDecayEnvelope();
	private final DecayEnvelope pitchEnvelope = new ExponentialDecayEnvelope();

	private final Source chain;

	public LaserTomVoice(Controls controls, double sampleRate, Filters filters) {
		// todo: low=F1, mid=c3, and high=E3 (less pitch depth)
		controls.getControl("laser-tom-volume-envelope-decay").addRawLongListener(volumeEnvelope::setDecayInMillis);
		controls.getControl("laser-tom-pitch-envelope-decay").addRawLongListener(pitchEnvelope::setDecayInMillis);
		EnvelopeModule volumeEnvelopeModule = new EnvelopeModule(volumeEnvelope, sampleRate);
		EnvelopeModule pitchEnvelopeModule = new EnvelopeModule(pitchEnvelope, sampleRate);

		// This oscillator is much more complicated than we need, but it's easier to just re-use the same oscillator, rather than have custom ones
		Oscillator osc1 = new Oscillator(sampleRate);
		osc1.setFrequencyModulationModulationRange(96d);
		// NOTE: Quantized in the outside and variable on the inside behaves better than the other way around.
		osc1.setWaveform(new QuantizedWaveform(new VariableWaveForm()));
		OscillatorModule osc1Module = new OscillatorModule(osc1);
		osc1Module.getFrequencyModulation()
		          .connect(new AttenuverterModule(controls.getControl("laser-tom-pitch-envelope-modulation-depth"), pitchEnvelopeModule));
		controls.getControl("laser-tom-osc-1-variable-waveform").addRawDoubleListener(osc1::setWaveformVariation);
		controls.getControl("laser-tom-osc-1-variable-waveform").set(0, 1, 3, this);

		Oscillator osc2 = new Oscillator(sampleRate);
		osc2.setFrequencyModulationModulationRange(96d);
		osc2.setSemitoneOffset(12d);
		// NOTE: Quantized in the outside and variable on the inside behaves better than the other way around.
		osc2.setWaveform(new QuantizedWaveform(new VariableWaveForm()));
		OscillatorModule osc2Module = new OscillatorModule(osc2);
		osc2Module.getFrequencyModulation()
		          .connect(new AttenuverterModule(controls.getControl("laser-tom-pitch-envelope-modulation-depth"), pitchEnvelopeModule));
		controls.getControl("laser-tom-osc-2-variable-waveform").addRawDoubleListener(osc2::setWaveformVariation);
		controls.getControl("laser-tom-osc-2-variable-waveform").set(0, 1, 3, this);


		MixerModule oscillatorMixer = new MixerModule();
		oscillatorMixer.add(osc1Module, controls.getControl("laser-tom-osc-1-volume"));
		oscillatorMixer.add(osc2Module, controls.getControl("laser-tom-osc-2-volume"));

		FilterModule filterModule = new FilterModule(new ControlledFilter("laser-tom", controls, filters));
		filterModule.setInput(oscillatorMixer);

		ScalingModule applyPitchEnvelopeToFilter = new ScalingModule(BooleanControl.alwaysTrue());
		applyPitchEnvelopeToFilter.setInput(filterModule);
		applyPitchEnvelopeToFilter.setScaleInput(pitchEnvelopeModule);

		ScalingModule applyVolumeEnvelope = new ScalingModule(BooleanControl.alwaysTrue());
		applyVolumeEnvelope.setInput(filterModule);
		applyVolumeEnvelope.setScaleInput(volumeEnvelopeModule);

		AttenuverterModule applyLevel = new AttenuverterModule(controls.getControl("laser-tom-level"), applyVolumeEnvelope);

		this.chain = applyLevel;

	}

	@Override
	public double[] samples(Instruction instruction, int samplesToGenerate) {
		return chain.generateSamples(samplesToGenerate, instruction);
	}

	@Override
	public void next() {
		chain.next();
	}

	@Override
	public Envelope getVolumeEnvelope() {
		return volumeEnvelope;
	}

	@Override
	public String getName() {
		return "Laser tom";
	}

	@Override
	public String getControlPrefix() {
		return "laser-tom";
	}

	@Override
	public int getVoiceIndex() {
		return VOICE_INDEX;
	}

	@Override
	public int getInstructionNote() {
		return NOTE;
	}
}
