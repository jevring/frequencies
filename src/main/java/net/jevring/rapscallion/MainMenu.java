/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.rapscallion;

import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.control.DiscreteControl;
import net.jevring.frequencies.v2.control.ListenerRegistration;
import net.jevring.frequencies.v2.modulation.matrix.ModulationMatrix;
import net.jevring.frequencies.v2.util.ValueFormatter;

import java.util.ArrayList;
import java.util.List;

/**
 * The dynamic menu that powers {@link Rapscallion}.
 *
 * @author markus@jevring.net
 * @created 2021-11-27 13:18
 */
public class MainMenu {
	private final List<MainMenuItemChangedListener> menuChangedListeners = new ArrayList<>();
	private final List<MainMenuItem> mainMenu;
	private final Controls controls;
	private volatile MainMenuItem currentMainMenuItem;

	public MainMenu(Controls controls, ModulationMatrix modulationMatrix) {
		this.controls = controls;
		ModulationMatrixSubMenu modulationMatrixSubMenu = new ModulationMatrixSubMenu(modulationMatrix);
		// todo: the variable waveform rendering will need to be different. Possibly the icons
		mainMenu = List.of(new MainMenuItem("Sequencer",
		                                    List.of(createSubMenuItem("tempo", ValueFormatter.INTEGER, "BPM"),
		                                            createSubMenuItem("euclidean-sequencer-outer-steps", ValueFormatter.INTEGER, "Outer ring steps"),
		                                            createSubMenuItem("euclidean-sequencer-inner-steps", ValueFormatter.INTEGER, "Inner ring steps"),
		                                            createSubMenuItem("euclidean-sequencer-rotation", ValueFormatter.INTEGER, "Inner ring rotation steps"))),
		                   new MainMenuItem("Music",
		                                    List.of(createSubMenuItem("input-scale", null, "Scale"),
		                                            createSubMenuItem("euclidean-sequencer-note", ValueFormatter.MIDI_NOTE_TO_NAME, "Mid note"),
		                                            createSubMenuItem("euclidean-sequencer-note-variance", ValueFormatter.INTEGER, "Note variance"))),
		                   new MainMenuItem("Filter",
		                                    List.of(createSubMenuItem("filter", null, "Type"),
		                                            createSubMenuItem("q-resonance-emphasis", ValueFormatter.PERCENT, "Resonance"),
		                                            createSubMenuItem("filter-cutoff-frequency", ValueFormatter.FREQUENCY, "Cutoff"))),
		                   new MainMenuItem("OSC1 - 1",
		                                    List.of(createSubMenuItem("primary-oscillator-variable-waveform", ValueFormatter.PURE, "Waveform"),
		                                            createSubMenuItem("primary-oscillator-octave-offset", ValueFormatter.INTEGER, "Octave"),
		                                            createSubMenuItem("primary-oscillator-detune-semi-tones", ValueFormatter.SEMITONES, "Detune"),
		                                            createSubMenuItem("primary-oscillator-wave-shape", ValueFormatter.PURE, "Shape"))),
		                   new MainMenuItem("OSC1 - 2",
		                                    List.of(createSubMenuItem("primary-oscillator-unison-voices", ValueFormatter.INTEGER, "Unison voices"),
		                                            createSubMenuItem("primary-oscillator-unison-detune-semi-tones", ValueFormatter.SEMITONES, "Unison detune"),
		                                            createSubMenuItem("primary-oscillator-quantization-steps",
		                                                              ValueFormatter.INTEGER_16_OFF,
		                                                              "Quantization steps"))),
		                   new MainMenuItem("OSC2 - 1",
		                                    List.of(createSubMenuItem("secondary-oscillator-variable-waveform", ValueFormatter.PURE, "Waveform"),
		                                            createSubMenuItem("secondary-oscillator-octave-offset", ValueFormatter.INTEGER, "Octave"),
		                                            createSubMenuItem("secondary-oscillator-detune-semi-tones", ValueFormatter.SEMITONES, "Detune"),
		                                            createSubMenuItem("secondary-oscillator-wave-shape", ValueFormatter.PURE, "Shape"))),
		                   new MainMenuItem("OSC2 - 2",
		                                    List.of(createSubMenuItem("secondary-oscillator-unison-voices", ValueFormatter.INTEGER, "Unison voices"),
		                                            createSubMenuItem("secondary-oscillator-unison-detune-semi-tones",
		                                                              ValueFormatter.SEMITONES,
		                                                              "Unison detune"),
		                                            createSubMenuItem("secondary-oscillator-quantization-steps",
		                                                              ValueFormatter.INTEGER_16_OFF,
		                                                              "Quantization steps"))),
		                   new MainMenuItem("Amp envelope",
		                                    List.of(createSubMenuItem("volume-envelope-attack", ValueFormatter.MILLISECONDS, "Attack"),
		                                            createSubMenuItem("volume-envelope-decay", ValueFormatter.MILLISECONDS, "Decay"))),
		                   new MainMenuItem("Filter envelope",
		                                    List.of(createSubMenuItem("filter-envelope-attack", ValueFormatter.MILLISECONDS, "Attack"),
		                                            createSubMenuItem("filter-envelope-decay", ValueFormatter.MILLISECONDS, "Decay"),
		                                            createSubMenuItem("filter-envelope-depth", ValueFormatter.PURE, "Depth"))),
		                   new MainMenuItem("Modulation envelope",
		                                    List.of(createSubMenuItem("modulation-envelope-attack", ValueFormatter.MILLISECONDS, "Attack"),
		                                            createSubMenuItem("modulation-envelope-decay", ValueFormatter.MILLISECONDS, "Decay"))),
		                   new MainMenuItem("Mixer",
		                                    List.of(createSubMenuItem("noise-oscillator-volume", ValueFormatter.PERCENT, "Noise"),
		                                            createSubMenuItem("primary-oscillator-volume", ValueFormatter.PERCENT, "OSC1"),
		                                            createSubMenuItem("secondary-oscillator-volume", ValueFormatter.PERCENT, "OSC2"),
		                                            createSubMenuItem("overdrive-volume", ValueFormatter.PURE, "Volume"))),
		                   new MainMenuItem("Delay",
		                                    List.of(createSubMenuItem("delay-dry-wet-mix", ValueFormatter.PURE, "Dry/wet"),
		                                            createSubMenuItem("delay-in-milliseconds", ValueFormatter.MILLISECONDS, "Delay"),
		                                            createSubMenuItem("delay-decay", ValueFormatter.PURE, "Decay"))),
		                   new MainMenuItem("Reverb",
		                                    List.of(createSubMenuItem("reverb-dry-wet-mix", ValueFormatter.PURE, "Dry/wet"),
		                                            createSubMenuItem("reverb-length", ValueFormatter.PURE, "Length"))),
		                   new MainMenuItem("Mod matrix",
		                                    List.of(modulationMatrixSubMenu.source(),
		                                            modulationMatrixSubMenu.depth(),
		                                            modulationMatrixSubMenu.target(),
		                                            modulationMatrixSubMenu.slot())),
		                   new MainMenuItem("LFO1",
		                                    List.of(createSubMenuItem("lfo1-frequency", ValueFormatter.FREQUENCY, "Frequency"),
		                                            createSubMenuItem("lfo1-variable-waveform", ValueFormatter.PURE, "Waveform"),
		                                            createSubMenuItem("lfo1-wave-shape", ValueFormatter.PURE, "Shape"),
		                                            createSubMenuItem("lfo1-quantization-steps", ValueFormatter.INTEGER_16_OFF, "Quantization steps"))),
		                   new MainMenuItem("LFO2",
		                                    List.of(createSubMenuItem("lfo2-frequency", ValueFormatter.FREQUENCY, "Frequency"),
		                                            createSubMenuItem("lfo2-variable-waveform", ValueFormatter.PURE, "Waveform"),
		                                            createSubMenuItem("lfo2-wave-shape", ValueFormatter.PURE, "Shape"),
		                                            createSubMenuItem("lfo2-quantization-steps", ValueFormatter.INTEGER_16_OFF, "Quantization steps"))),
		                   new MainMenuItem("LFO3",
		                                    List.of(createSubMenuItem("lfo3-frequency", ValueFormatter.FREQUENCY, "Frequency"),
		                                            createSubMenuItem("lfo3-variable-waveform", ValueFormatter.PURE, "Waveform"),
		                                            createSubMenuItem("lfo3-wave-shape", ValueFormatter.PURE, "Shape"),
		                                            createSubMenuItem("lfo3-quantization-steps", ValueFormatter.INTEGER_16_OFF, "Quantization steps"))),
		                   new MainMenuItem("Sample and hold",
		                                    List.of(createSubMenuItem("sample-and-hold-frequency", ValueFormatter.FREQUENCY, "Frequency"),
		                                            createSubMenuItem("sample-and-hold-slew", ValueFormatter.PERCENT, "Slew"))),
		                   // todo: we'll need a special custom menu for the presets.
		                   new MainMenuItem("Presets", List.of()));
		currentMainMenuItem = mainMenu.get(0);
	}

	private SubMenuItem createSubMenuItem(String controlName, ValueFormatter valueFormatter, String name) {
		Control control = controls.getControl(controlName);
		if (control != null) {
			return new SubMenuItem(new ControlLink(control), new ControlSubMenuItemRenderer(valueFormatter, control, name));
		} else {
			DiscreteControl discreteControl = controls.getDiscreteControl(controlName);
			if (discreteControl != null) {
				return new SubMenuItem(new DiscreteControlLink(discreteControl), new DiscreteControlSubMenuItemRenderer(discreteControl, name));
			} else {
				throw new IllegalArgumentException("Unknown control " + controlName);
			}
		}
	}

	public int getNumberOfMainMenuScreens() {
		return mainMenu.size();
	}

	public ListenerRegistration addMainMenuItemChangedListener(MainMenuItemChangedListener listener) {
		menuChangedListeners.add(listener);
		listener.mainMenuItemChanged(currentMainMenuItem);
		return new ListenerRegistration() {
			@Override
			public void unregister() {
				menuChangedListeners.remove(listener);
			}
		};
	}

	public void setMainMenu(int newValue) {
		MainMenuItem newMainMenuItem = mainMenu.get(newValue);
		if (newMainMenuItem != currentMainMenuItem) {
			currentMainMenuItem = newMainMenuItem;
			for (MainMenuItemChangedListener menuChangedListener : menuChangedListeners) {
				menuChangedListener.mainMenuItemChanged(currentMainMenuItem);
			}
		}
	}

	public void setSubMenuValue(int subMenuIndex, double min, double newValue, double max, Object source) {
		// todo: this will be easier when they're endless encoders, but for now, we should change the value of the knob to represent the value of the linked control.
		//  We should be careful to not end up in a circular update thing
		List<SubMenuItem> subMenuItems = currentMainMenuItem.subMenuItems();
		if (subMenuItems.size() > subMenuIndex) {
			subMenuItems.get(subMenuIndex).controlListener().valueChanged(min, newValue, max, source);
		} else {
			System.out.printf("Sub menu control with index %d doesn't exist in main menu %s which only has %d items. Skipping%n",
			                  subMenuIndex,
			                  currentMainMenuItem.name(),
			                  subMenuItems.size());
		}
	}
}
