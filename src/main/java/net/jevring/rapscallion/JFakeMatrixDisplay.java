/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.rapscallion;

import net.jevring.frequencies.v2.ui.GridBadLayoutUtils;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * This is a mock-up of what the matrix display will *do*,
 * but not what it will look like, other than in terms of layout
 *
 * @author markus@jevring.net
 * @created 2021-11-27 12:31
 */
public class JFakeMatrixDisplay extends JPanel implements MainMenuItemChangedListener {
	private final JControlDisplay[] jControlDisplays = new JControlDisplay[4];
	private final JLabel mainMenuName;

	public JFakeMatrixDisplay() {
		mainMenuName = new JLabel();
		mainMenuName.setFont(new Font("Calibri", Font.PLAIN, 8));
		JControlDisplay subMenu0 = new JControlDisplay();
		JControlDisplay subMenu1 = new JControlDisplay();
		JControlDisplay subMenu2 = new JControlDisplay();
		JControlDisplay subMenu3 = new JControlDisplay();

		jControlDisplays[0] = subMenu0;
		jControlDisplays[1] = subMenu1;
		jControlDisplays[2] = subMenu2;
		jControlDisplays[3] = subMenu3;

		if (false) {
			// This gives us exact position
			setLayout(null);
			add(mainMenuName).setBounds(0, 0, 150, 30);
			add(subMenu0).setBounds(0, 30, 60, 30);
			add(subMenu1).setBounds(60, 30, 60, 30);
			add(subMenu2).setBounds(120, 30, 60, 30);
			add(subMenu3).setBounds(180, 30, 60, 30);
		} else {
			// This gives us dynamic size, but things move around
			setLayout(new GridBagLayout());
			add(mainMenuName, GridBadLayoutUtils.gbc(0, 0, 4, 1));
			add(subMenu0, GridBadLayoutUtils.gbc(0, 1));
			add(subMenu1, GridBadLayoutUtils.gbc(1, 1));
			add(subMenu2, GridBadLayoutUtils.gbc(2, 1));
			add(subMenu3, GridBadLayoutUtils.gbc(3, 1));
		}
	}

	@Override
	public void mainMenuItemChanged(MainMenuItem newMainMenuItem) {
		mainMenuName.setText(newMainMenuItem.name());

		List<SubMenuItem> subMenuItems = newMainMenuItem.subMenuItems();
		for (int i = 0; i < 4; i++) {
			if (subMenuItems.size() > i) {
				SubMenuItem subMenuItem = subMenuItems.get(i);
				jControlDisplays[i].switchSubMenuItem(subMenuItem);
			} else {
				jControlDisplays[i].switchSubMenuItem(null);
			}
		}
	}
/*
	@Override
	public void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
		g.drawRect(0,0,256,64);
		g.setColor(Color.WHITE);
		g.drawString("Hello!", 0,0);
	}
 */
}
