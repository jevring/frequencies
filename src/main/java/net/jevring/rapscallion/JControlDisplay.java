/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.rapscallion;

import net.jevring.frequencies.v2.control.ListenerRegistration;

import javax.swing.*;
import java.awt.*;

/**
 * todo: javadoc for this class
 *
 * @author markus@jevring.net
 * @created 2021-11-27 16:12
 */
public class JControlDisplay extends JPanel {
	private final JLabel valueLabel;
	private final JLabel nameLabel;

	private volatile ListenerRegistration listenerRegistration;

	public JControlDisplay() {
		valueLabel = new JLabel("");
		valueLabel.setFont(new Font("Calibri", Font.PLAIN, 8));
		nameLabel = new JLabel("");
		nameLabel.setFont(new Font("Calibri", Font.PLAIN, 8));
		setLayout(new GridLayout(2, 1));
		add(valueLabel);
		add(nameLabel);
	}

	public void switchSubMenuItem(SubMenuItem subMenuItem) {
		if (listenerRegistration != null) {
			listenerRegistration.unregister();
			listenerRegistration = null;
		}

		if (subMenuItem != null) {
			SubMenuItemRenderer subMenuItemRenderer = subMenuItem.subMenuItemRenderer();
			nameLabel.setText(subMenuItemRenderer.name());
			listenerRegistration = subMenuItemRenderer.addValueListener(valueLabel::setText);
		} else {
			nameLabel.setText("N/A");
			valueLabel.setText("N/A");
		}
	}
}
