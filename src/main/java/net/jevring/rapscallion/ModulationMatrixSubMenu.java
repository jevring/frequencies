/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.rapscallion;

import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.control.ControlListener;
import net.jevring.frequencies.v2.math.Interpolation;
import net.jevring.frequencies.v2.modulation.matrix.ModulationMatrix;
import net.jevring.frequencies.v2.modulation.matrix.ModulationMatrixRow;
import net.jevring.frequencies.v2.modulation.matrix.ModulationSource;
import net.jevring.frequencies.v2.modulation.matrix.ModulationTarget;
import net.jevring.frequencies.v2.ui.JModulationMatrix;
import net.jevring.frequencies.v2.util.ValueFormatter;

/**
 * Rapscallion version of {@link JModulationMatrix}.
 *
 * @author markus@jevring.net
 * @created 2021-11-28 14:12
 */
public class ModulationMatrixSubMenu {
	private static final int MAX_SLOTS = 6;
	private final ValueListenerList sourceValueListeners = new ValueListenerList();
	private final ValueListenerList depthValueListeners = new ValueListenerList();
	private final ValueListenerList targetValueListeners = new ValueListenerList();
	private final ValueListenerList slotValueListeners = new ValueListenerList();
	private final ModulationMatrix modulationMatrix;
	private final Slot[] slots = new Slot[MAX_SLOTS];
	private volatile int slot = 0;

	public ModulationMatrixSubMenu(ModulationMatrix modulationMatrix) {
		this.modulationMatrix = modulationMatrix;
		for (int i = 0; i < MAX_SLOTS; i++) {
			slots[i] = new Slot();
		}
	}

	public void modulationMatrixUpdated() {
		// todo: call this when a preset it loaded
		int rowsAdded = 0;
		for (ModulationMatrixRow modulationMatrixRow : modulationMatrix.getModulationConfig(false)) {
			slots[rowsAdded++] = new Slot(modulationMatrixRow.source(), modulationMatrixRow.target(), modulationMatrixRow.depth());
		}
		for (int i = rowsAdded; i < MAX_SLOTS; i++) {
			slots[i] = new Slot();
		}
	}

	// todo: we have to hide used couplings already, just like in thief

	public SubMenuItem source() {
		return new SubMenuItem(new ControlListener() {
			@Override
			public void valueChanged(double min, double newValue, double max, Object source) {

				// do the null check here to allow it to be emptied
				ModulationSource modulationSource = null;
				if (newValue > 0) {
					ModulationSource[] modulationSources = ModulationSource.values();
					int sourceIndex = (int) Interpolation.linear(min, max, newValue, 0, modulationSources.length - 1);
					modulationSource = modulationSources[sourceIndex];
				}

				if (slots[slot].updateSourceIfNecessary(modulationSource)) {
					sourceValueListeners.valueChanged(slots[slot].sourceValueIfAvailable());
					depthValueListeners.valueChanged(slots[slot].depthValueIfAvailable());
				}
			}
		}, new NamedValueListenerRenderer(() -> slots[slot].sourceValueIfAvailable(), sourceValueListeners, "Source"));
	}

	public SubMenuItem depth() {
		return new SubMenuItem(new ControlListener() {
			@Override
			public void valueChanged(double min, double newValue, double max, Object source) {
				slots[slot].updateDepth(min, newValue, max, source);
				depthValueListeners.valueChanged(slots[slot].depthValueIfAvailable());
			}
		}, new NamedValueListenerRenderer(() -> slots[slot].depthValueIfAvailable(), depthValueListeners, "Depth"));

	}

	public SubMenuItem target() {
		return new SubMenuItem(new ControlListener() {
			@Override
			public void valueChanged(double min, double newValue, double max, Object source) {
				ModulationTarget modulationTarget = null;
				// do the null check here to allow it to be emptied
				if (newValue > 0) {
					ModulationTarget[] modulationTargets = ModulationTarget.values();
					int sourceIndex = (int) Interpolation.linear(min, max, newValue, 0, modulationTargets.length - 1);
					modulationTarget = modulationTargets[sourceIndex];
				}

				if (slots[slot].updateTargetIfNecessary(modulationTarget)) {
					targetValueListeners.valueChanged(slots[slot].targetValueIfAvailable());
					depthValueListeners.valueChanged(slots[slot].depthValueIfAvailable());
				}
			}
		}, new NamedValueListenerRenderer(() -> slots[slot].targetValueIfAvailable(), targetValueListeners, "Target"));
	}

	public SubMenuItem slot() {
		return new SubMenuItem(new ControlListener() {
			@Override
			public void valueChanged(double min, double newValue, double max, Object source) {
				slot = (int) Interpolation.linear(min, max, newValue, 0, MAX_SLOTS - 1);
				slotValueListeners.valueChanged(String.valueOf(slot));
				// todo: these things can be null, in which case we should set something else.
				//  This whole thing needs to get better
				sourceValueListeners.valueChanged(slots[slot].sourceValueIfAvailable());
				targetValueListeners.valueChanged(slots[slot].targetValueIfAvailable());
				depthValueListeners.valueChanged(slots[slot].depthValueIfAvailable());
			}
		}, new NamedValueListenerRenderer(() -> String.valueOf(slot), slotValueListeners, "Slot"));
	}

	/**
	 * This assumes that there is only ever a single thread that updates this at a time.
	 * This is a valid assumption when using swing, as we're on the event dispatch thread,
	 * and it's likely a valid assumption when we're running on GPIO polling or whatever
	 * it ends up being. If this is violated, we can end up with dangling modulation slots,
	 * if for example target and source are updated at the same time.
	 * To ensure this, the two relevant methods are synchronized. Not strictly speaking
	 * necessary if the assumptions are correct, but uncontended locks are pretty fast, so
	 * it's likely ok.
	 */
	private final class Slot {
		private ModulationSource source;
		private ModulationTarget target;
		private Control depth;

		public Slot(ModulationSource source, ModulationTarget target, Control depth) {
			this.source = source;
			this.target = target;
			this.depth = depth;
		}

		public Slot() {
		}

		public synchronized boolean updateSourceIfNecessary(ModulationSource source) {
			if (this.source != source) {
				removeDepthIfApplicable();
				this.source = source;
				setDepthIfApplicable();
				return true;
			} else {
				return false;
			}
		}

		public synchronized boolean updateTargetIfNecessary(ModulationTarget target) {
			if (this.target != target) {
				removeDepthIfApplicable();
				this.target = target;
				setDepthIfApplicable();
				return true;
			} else {
				return false;
			}
		}

		private void setDepthIfApplicable() {
			if (this.source != null && this.target != null) {
				depth = modulationMatrix.add(this.source, this.target);
			} else {
				depth = null;
			}
		}

		private void removeDepthIfApplicable() {
			if (this.source != null && this.target != null) {
				modulationMatrix.remove(this.source, this.target);
				// set it to 0 so that we get a reset value if we configure this matrix pairing again
				depth.setRaw(0);
				depth = null;
			}
		}

		public void updateDepth(double myMin, double myValue, double myMax, Object source) {
			if (depth != null) {
				depth.set(myMin, myValue, myMax, source);
			}
		}

		public String depthValueIfAvailable() {
			if (depth == null) {
				return "";
			} else {
				return ValueFormatter.PURE.format(depth.getCurrentValue());
			}
		}
		
		// todo: we should have some shorter names for source and target here

		public String sourceValueIfAvailable() {
			if (source == null) {
				return "";
			} else {
				return String.valueOf(source);
			}
		}

		public String targetValueIfAvailable() {
			if (target == null) {
				return "";
			} else {
				return String.valueOf(target);
			}
		}
	}
}
