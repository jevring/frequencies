/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.rapscallion;

import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.hooks.EuclideanSequencerVisualizer;
import net.jevring.frequencies.v2.input.sequencer.Sequencer;
import net.jevring.frequencies.v2.ui.JControlledTextIconButton;
import net.jevring.frequencies.v2.ui.JKnob;

import javax.swing.*;

/**
 * The full UI for {@link Rapscallion}.
 *
 * @author markus@jevring.net
 * @created 2020-02-23 14:25
 */
public class RapscallionUI extends JPanel implements EuclideanSequencerVisualizer {
	private final Controls controls;
	private final Rapscallion rapscallion;

	public RapscallionUI(Rapscallion rapscallion) {
		this.controls = rapscallion.getControls();
		this.rapscallion = rapscallion;
		createUI();
	}

	private void createUI() {

		SwingUtilities.invokeLater(() -> {
			//region Layout
			JFakeMatrixDisplay fakeMatrixDisplay = new JFakeMatrixDisplay();
			rapscallion.getMenu().addMainMenuItemChangedListener(fakeMatrixDisplay);
			setLayout(null);
			add(new JKnob("Menu", controls.getControl("main-menu"))).setBounds(0, 0, 50, 85);
			add(new JControlledTextIconButton('▶', controls.getBooleanControl("euclidean-sequencer-active"), true)).setBounds(0, 90, 50, 50);
			add(new JControlledTextIconButton('⏹', controls.getBooleanControl("euclidean-sequencer-active"), false)).setBounds(0, 135, 50, 50);
			add(fakeMatrixDisplay).setBounds(60, 0, 250, 85);
			add(new JKnob("Sub1", controls.getControl("sub-menu-0"))).setBounds(60, 90, 50, 85);
			add(new JKnob("Sub2", controls.getControl("sub-menu-1"))).setBounds(120, 90, 50, 85);
			add(new JKnob("Sub3", controls.getControl("sub-menu-2"))).setBounds(180, 90, 50, 85);
			add(new JKnob("Sub4", controls.getControl("sub-menu-3"))).setBounds(240, 90, 50, 85);
			//endregion
		});
	}

	//region Visualization
	@Override
	public void visualizedSequencerStep(int step, boolean on, boolean[] steps) {
	}

	@Override
	public void visualizedSequencerStep(int step, boolean on) {
	}

	@Override
	public void visualizedSequencerBeat(Sequencer sequencer) {
	}
	//endregion
}
