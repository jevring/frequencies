/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.ui;

import javax.swing.*;
import java.awt.*;

/**
 * Visualizes waveforms chunk by chunk.
 *
 * @author markus@jevring.net
 * @created 2020-05-03 11:52
 */
public class JWaveformDisplay extends JComponent implements JOscilloscope {
	private final int maxXAxis = 1200;
	private final int maxYAxis = 100;
	private final int yOffset = 150;
	private final int xOffset = 5;

	private volatile Skin skin = Skins.BLUE_RED;
	private volatile double[] data = new double[maxXAxis];
	private volatile int lastChunkEnd = 0;

	public JWaveformDisplay() {
		Dimension size = new Dimension(1250, 300);
		setMinimumSize(size);
		setMaximumSize(size);
		setPreferredSize(size);
		setSize(size);
	}

	public void newChunk(double[] chunk) {
		int p = lastChunkEnd;
		for (int i = 0; i < chunk.length; i++) {
			double v = chunk[i];
			data[p++ % data.length] = v;
		}
		lastChunkEnd = p;
		repaint();
	}

	@Override
	protected void paintComponent(Graphics g) {
		g.setColor(skin.foreground());
		g.drawLine(xOffset, -135 + yOffset, xOffset, 135 + yOffset); // y-axis
		g.drawLine(xOffset, 0 + yOffset, maxXAxis + xOffset, 0 + yOffset); // x-axis

		if (data != null) {
			Polygon p = new Polygon();

			int x = xOffset;
			for (int i = 0; i < data.length; i++) {
				int scaledValue = (int) (data[i] * maxYAxis);
				p.addPoint(x, yOffset - scaledValue);
				x++;
				if (x >= maxXAxis) {
					break;
				}
			}
			g.setColor(skin.modulationRange());
			g.drawPolyline(p.xpoints, p.ypoints, p.npoints);
			g.setColor(skin.modulationValue());
			for (int i = 0; i < p.npoints; i++) {
				// we want to draw the dots AFTER the polyline
				g.drawRect(p.xpoints[i], p.ypoints[i], 1, 1);
			}
		}
	}
}
