/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.ui;

import javax.swing.*;
import java.awt.*;

/**
 * A light like an LED.
 *
 * @author markus@jevring.net
 * @created 2020-05-08 15:58
 */
public class JLight extends JComponent implements JSkinnable {
	private volatile Skin skin = Skins.currentSkin();
	private volatile boolean on;
	private volatile Color color;

	public JLight() {
		Dimension size = new Dimension(12, 12);
		setMaximumSize(size);
		setMinimumSize(size);
		setPreferredSize(size);
		setSize(size);
	}

	@Override
	protected void paintComponent(Graphics g) {
		if (isOpaque()) {
			g.setColor(skin.background());
			g.fillRect(0, 0, getWidth(), getHeight());
		}
		g.setColor(Color.darkGray);
		int xCenter = getWidth() / 2;
		int yCenter = getHeight() / 2;
		g.fillOval(xCenter - 6, yCenter - 6, 12, 12);
		if (on) {
			g.setColor(color);
			g.fillOval(xCenter - 4, yCenter - 4, 8, 8);
		}
	}

	@Override
	public void setSkin(Skin skin) {
		this.skin = skin;
		setOpaque(skin.opaque());
	}

	public void on(Color color) {
		this.color = color;
		this.on = true;
		repaint();
	}

	public void off() {
		this.on = false;
		repaint();
	}
}
