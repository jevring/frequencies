/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.ui;

import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.util.ValueFormatter;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

import static net.jevring.frequencies.v2.ui.GridBadLayoutUtils.gbc;

/**
 * UI For sound-generating oscillators
 *
 * @author markus@jevring.net
 * @created 2020-05-30 20:52
 */
public class JOscillatorPanel extends JPanel {
	public JOscillatorPanel(Controls controls, String oscillatorControlPrefix) {
		JKnob octaveOffsetKnob = new JKnob("Octave", controls.getControl(oscillatorControlPrefix + "-octave-offset"), ValueFormatter.INTEGER);
		JKnob waveformKnob = new JVariableOscillatorKnob(controls.getControl(oscillatorControlPrefix + "-variable-waveform"));
		JKnob quantizationSteps = new JKnob("Steps", controls.getControl(oscillatorControlPrefix + "-quantization-steps"), ValueFormatter.INTEGER_16_OFF);

		JKnob detuneKnob = new JKnob("Detune", controls.getControl(oscillatorControlPrefix + "-detune-semi-tones"), ValueFormatter.SEMITONES);
		JKnob waveShapeKnob = new JKnob("Shape", controls.getControl(oscillatorControlPrefix + "-wave-shape"));
		JKnob phaseShiftKnob = new JKnob("Phase", controls.getControl(oscillatorControlPrefix + "-phase-shift"));
		JKnob unisonVoicesKnob = new JKnob("Voices", controls.getControl(oscillatorControlPrefix + "-unison-voices"));
		JLabel unisonLabel = new JLabel("Unison", SwingConstants.CENTER);
		JKnob unisonDetuneKnob = new JKnob("Detune", controls.getControl(oscillatorControlPrefix + "-unison-detune-semi-tones"), ValueFormatter.SEMITONES);

		detuneKnob.setAlignmentX(CENTER_ALIGNMENT);
		setLayout(new GridBagLayout());
		setBorder(new TitledBorder(null, titleOf(oscillatorControlPrefix), TitledBorder.LEFT, TitledBorder.TOP));
		//@formatter:off
		add(octaveOffsetKnob, gbc(0, 0, 1, 1, 1, 1));
		add(waveformKnob,     gbc(1, 0, 1, 1, 1, 1));
		add(quantizationSteps,gbc(2, 0, 1, 1, 1, 1));
		add(detuneKnob,       gbc(0, 2, 1, 1, 1, 1));
		add(waveShapeKnob,    gbc(1, 2, 1, 1, 1, 1));
		add(phaseShiftKnob,   gbc(2, 2, 1, 1, 1, 1));
		add(unisonVoicesKnob, gbc(0, 3, 1, 1, 1, 1));
		add(unisonLabel,      gbc(1, 3, 1, 1, 1, 1));
		add(unisonDetuneKnob, gbc(2, 3, 1, 1, 1, 1));
		//@formatter:on
	}

	private String titleOf(String oscillatorControlPrefix) {
		// This isn't super elegant, but with currently only two choices, it doesn't have to be
		if ("primary-oscillator".equals(oscillatorControlPrefix)) {
			return "Primary oscillator";
		} else {
			return "Secondary oscillator";
		}
	}

}
