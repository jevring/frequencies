/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.ui;

import net.jevring.frequencies.v2.input.midi.MidiMappings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * Higlighting glass pane that lets us find the control names of the selected controls.
 *
 * @author markus@jevring.net
 * @created 2020-04-13 15:55
 */
public class MidiMapperGlassPane extends JComponent {
	private final List<JComponent> components = new ArrayList<>();
	private final MidiMappings midiMappings;
	private Component selected;

	public MidiMapperGlassPane(JComponent container, MidiMappings midiMappings) {
		this.midiMappings = midiMappings;
		enumerate(container, components);
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Point pointRelativeToTheUI = SwingUtilities.convertPoint(MidiMapperGlassPane.this, e.getPoint(), container);
				Component c = SwingUtilities.getDeepestComponentAt(container, (int) pointRelativeToTheUI.getX(), (int) pointRelativeToTheUI.getY());
				while (c != null) {
					if (c instanceof JControlChangeMappable jccm) {
						if (selected == c) {
							selected = null;
							midiMappings.stopListeningForNewMidiMapping();
						} else {
							selected = c;
							midiMappings.listenForNewMidiMapping(jccm.getControl());
						}
						repaint();
						// it's important that we return here so that we don't call through to "selected = null;"
						return;
					}
					c = c.getParent();
				}
				selected = null;
				midiMappings.stopListeningForNewMidiMapping();
				setVisible(false);
				repaint();
			}
		});
	}

	private <T extends Component & JControlChangeMappable> T getJControlChangeMappableComponentOrNull(JComponent container, Point point) {
		Point pointRelativeToTheUI = SwingUtilities.convertPoint(MidiMapperGlassPane.this, point, container);
		Component c = SwingUtilities.getDeepestComponentAt(container, (int) pointRelativeToTheUI.getX(), (int) pointRelativeToTheUI.getY());
		while (c != null) {
			if (c instanceof JControlChangeMappable jccm) {
				return (T) jccm;
			}
			c = c.getParent();
		}
		return null;
	}

	@Override
	public void setVisible(boolean visible) {
		if (!visible) {
			midiMappings.stopListeningForNewMidiMapping();
		}
		super.setVisible(visible);
	}

	private void enumerate(JComponent c, java.util.List<JComponent> components) {
		if (c instanceof JControlChangeMappable) {
			components.add(c);
		}
		for (int i = 0; i < c.getComponentCount(); i++) {
			Component component = c.getComponent(i);
			if (component instanceof JComponent jComponent) {
				enumerate(jComponent, components);
			}
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		for (JComponent component : components) {
			if (component instanceof JControlChangeMappable jccm) {
				Point point = SwingUtilities.convertPoint(component, 0, 0, getRootPane());
				int x = (int) point.getX();
				int y = (int) point.getY();
				g.setColor(new Color(255, 175, 175, 127));
				g.fillRect(x, y, component.getWidth(), component.getHeight());

				if (component == selected) {
					Graphics2D g2 = (Graphics2D) g;
					g2.setStroke(new BasicStroke(3));
					g.setColor(Color.RED);
					g.drawRect(x, y, component.getWidth(), component.getHeight());
				}
			}
		}
	}
}
