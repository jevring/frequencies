/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.ui;

import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.util.ValueFormatter;

import javax.swing.*;
import javax.swing.border.TitledBorder;

/**
 * UI for the mixer.
 *
 * @author markus@jevring.net
 * @created 2021-02-27 21:40
 */
public class JMixerPanel extends JPanel {
	public JMixerPanel(Controls controls) {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setBorder(new TitledBorder(null, "Mixer", TitledBorder.LEFT, TitledBorder.TOP));
		setAlignmentY(JComponent.TOP_ALIGNMENT);

		JKnob noiseOscillatorVolume = new JKnob("Noise", controls.getControl("noise-oscillator-volume"), ValueFormatter.PERCENT, 70);
		JKnob primaryOscillatorVolume = new JKnob("Primary", controls.getControl("primary-oscillator-volume"), ValueFormatter.PERCENT, 70);
		JKnob secondaryOscillatorVolume = new JKnob("Secondary", controls.getControl("secondary-oscillator-volume"), ValueFormatter.PERCENT, 70);
		JKnob ringModulatorVolume = new JKnob("Ring mod", controls.getControl("ring-modulator-volume"), ValueFormatter.PERCENT, 70);
		JCheckBox clip = new JCheckBox("Clip");
		Bindings.bind(clip, controls.getBooleanControl("clip"));

		JKnob overdriveKnob = new JKnob("Overdrive", controls.getControl("overdrive-volume"), ValueFormatter.PURE, 70);
		JKnob balanceKnob = new JKnob("Balance", controls.getControl("balance"), ValueFormatter.PURE, 70);

		noiseOscillatorVolume.setAlignmentX(LEFT_ALIGNMENT);
		primaryOscillatorVolume.setAlignmentX(LEFT_ALIGNMENT);
		secondaryOscillatorVolume.setAlignmentX(LEFT_ALIGNMENT);
		ringModulatorVolume.setAlignmentX(LEFT_ALIGNMENT);
		overdriveKnob.setAlignmentX(LEFT_ALIGNMENT);
		clip.setAlignmentX(LEFT_ALIGNMENT);
		balanceKnob.setAlignmentX(LEFT_ALIGNMENT);

		add(noiseOscillatorVolume);
		add(primaryOscillatorVolume);
		add(secondaryOscillatorVolume);
		add(ringModulatorVolume);
		add(overdriveKnob);
		add(clip);
		add(balanceKnob);
	}
}
