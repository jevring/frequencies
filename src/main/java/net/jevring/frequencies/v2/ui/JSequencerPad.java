/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.ui;

import net.jevring.frequencies.v2.control.BooleanControl;
import net.jevring.frequencies.v2.control.DiscreteControlListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;

/**
 * A pad that can be activated or deactivated, and can also be highlighted in both those states.
 *
 * @author markus@jevring.net
 * @created 2021-03-13 13:55
 */
public class JSequencerPad extends JComponent {
	private final Color deactivated = Color.LIGHT_GRAY;
	private final Color activated = Color.RED;
	private final Color hit = Color.GREEN;
	private final BooleanControl control;
	private volatile boolean on = false;

	public JSequencerPad(BooleanControl control) {
		this.control = control;
		setBorder(BorderFactory.createLineBorder(Color.WHITE)); // todo: this border should be aligned with the skin color
		Dimension size = new Dimension(60, 60);
		setSize(size);
		setPreferredSize(size);
		setMinimumSize(size);
		setMaximumSize(size);
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				if ((e.getModifiersEx() & MouseEvent.BUTTON1_DOWN_MASK) == MouseEvent.BUTTON1_DOWN_MASK) {
					control.set(!control.get(), JSequencerPad.this);
					repaint();
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				control.set(!control.get(), JSequencerPad.this);
				repaint();
			}
		});
		control.addListener(new DiscreteControlListener<>() {
			@Override
			public void valueChanged(Boolean value, Object source) {
				if (source != JSequencerPad.this) {
					repaint();
				}
			}
		});
	}

	@Override
	protected void paintComponent(Graphics g) {
		g.setColor(deactivated);
		g.fillRect(0, 0, 60, 60);
		if (control.get()) {
			g.setColor(activated);
			g.fillRect(1, 1, 58, 58);
		}
		if (on) {
			RadialGradientPaint radialGradientPaint =
					new RadialGradientPaint(new Point2D.Float(30, 30), 20, new float[]{0.5f, 1.0f}, new Color[]{hit, control.get() ? activated : deactivated});
			((Graphics2D) g).setPaint(radialGradientPaint);
			g.fillOval(0, 0, 60, 60);
		}
	}

	/**
	 * Visualize this step
	 *
	 * @param on whether the step should be highlighted or not
	 */
	public void on(boolean on) {
		this.on = on;
		repaint();
	}
}
