/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.ui;

import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.util.ValueFormatter;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

import static net.jevring.frequencies.v2.ui.GridBadLayoutUtils.gbc;

/**
 * A panel for an LFO control.
 *
 * @author markus@jevring.net
 * @created 2020-04-15 20:14
 */
public class LFOPanel extends JPanel {
	public LFOPanel(Controls controls, String prefix) {
		setBorder(new TitledBorder(null, prefix.toUpperCase(), TitledBorder.LEFT, TitledBorder.TOP));
		setAlignmentY(JComponent.TOP_ALIGNMENT);
		JKnob quantizationSteps = new JKnob("Steps", controls.getControl(prefix + "-quantization-steps"), ValueFormatter.INTEGER_16_OFF);
		JKnob waveformKnob = new JVariableOscillatorKnob(controls.getControl(prefix + "-variable-waveform"));
		JKnob frequency = new JKnob("Frequency", controls.getControl(prefix + "-frequency"), ValueFormatter.LOW_FREQUENCY);
		JKnob waveShape = new JKnob("Shape", controls.getControl(prefix + "-wave-shape"));
		JCheckBox keyReset = new JCheckBox("Retrigger");
		Bindings.bind(keyReset, controls.getBooleanControl(prefix + "-key-reset"));

		setLayout(new GridBagLayout());
		// @formatter:off
		add(frequency,         gbc(0, 0, 1, 1, 1, 1));
		add(waveformKnob,      gbc(1, 0, 1, 1, 1, 1));
		add(quantizationSteps, gbc(0, 1, 1, 1, 1, 1));
		add(waveShape,         gbc(1, 1, 1, 1, 1, 1));
		add(keyReset,          gbc(0, 2, 2, 1, 1, 1));
		// @formatter:on
	}
}
