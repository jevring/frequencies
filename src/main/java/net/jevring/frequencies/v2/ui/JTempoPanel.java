/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.ui;

import net.jevring.frequencies.v2.control.BooleanControl;
import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.input.arpeggiator.Arpeggiator;
import net.jevring.frequencies.v2.input.euclidean.EuclideanSequencer;
import net.jevring.frequencies.v2.input.sequencer.Sequencer;
import net.jevring.frequencies.v2.input.sequencer.StepSequencer;
import net.jevring.frequencies.v2.util.ValueFormatter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static net.jevring.frequencies.v2.ui.GridBadLayoutUtils.gbc;

/**
 * Integrated sequencer with step, euclidean, and arpeggiator all in one.
 *
 * @author markus@jevring.net
 * @created 2021-02-25 22:02
 */
public class JTempoPanel extends JPanel {
	private final JLight[] sequencerLeds = new JLight[16];
	private final Controls controls;
	private final JControlledTextIconButton stepSequencerStart;
	private final JControlledTextIconButton arpeggiatorStart;
	private final JControlledTextIconButton euclideanSequencerStart;
	private final JEuclideanStepVisualizer euclideanStepVisualizer;

	public JTempoPanel(Controls controls) {
		this.controls = controls;
		setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));

		//@formatter:off
		JPanel tempoPanel = (JPanel) add(new JPanel(new GridBagLayout()));
		tempoPanel.setBorder(BorderFactory.createTitledBorder("Tempo"));
		JCheckBox midiClockIn = new JControlledCheckBox("MIDI Clock IN", bc("receive-midi-clock-active"));
		midiClockIn.setHorizontalTextPosition(SwingConstants.CENTER);
		midiClockIn.setVerticalTextPosition(SwingConstants.BOTTOM);
		JKnob swingKnob = new JKnob("Swing", c("swing"), ValueFormatter.PERCENT);
		swingKnob.setDisabled(true);
		JKnob tempoKnob = new JKnob("Tempo", c("tempo"), ValueFormatter.INTEGER);
		JKnob rateKnob = new JKnob("Rate", c("rate"), ValueFormatter.PPQN);
		tempoPanel.add(tempoKnob,   gbc(0, 1, 1, 1));
		tempoPanel.add(rateKnob,    gbc(0, 2, 1, 1));
		//tempoPanel.add(swingKnob,   gbc(1, 1, 1, 1)); // maybe we can add swing back later, if we really want it. For now it's not needed
		tempoPanel.add(midiClockIn, gbc(1, 2, 1, 1));

		JPanel arpeggiatorPanel = (JPanel) add(new JPanel(new GridBagLayout()));
		arpeggiatorPanel.setBorder(BorderFactory.createTitledBorder("Arpeggiator"));
		arpeggiatorStart = new JControlledTextIconButton('▶', bc("arpeggiator-active"), true);
		JTextIconButton arpeggiatorStop = new JControlledTextIconButton('⏹', bc("arpeggiator-active"), false);
		JKnob arpeggiatorGateKnob = new JKnob("Gate", c("arpeggiator-gate"), ValueFormatter.PERCENT);
		JKnob arpeggiatorOctavesKnob = new JKnob("Octaves", c("arpeggiator-octaves"), ValueFormatter.INTEGER);
		arpeggiatorPanel.add(arpeggiatorOctavesKnob, gbc(0, 0, 1, 1));
		arpeggiatorPanel.add(arpeggiatorGateKnob,    gbc(0, 1, 1, 2));
		arpeggiatorPanel.add(arpMode(),              gbc(1, 0, 1, 1));
		arpeggiatorPanel.add(arpeggiatorStop,        gbc(1, 1, 1, 1));
		arpeggiatorPanel.add(arpeggiatorStart,       gbc(1, 2, 1, 1));


		JPanel euclideanSequencerPanel = (JPanel) add(new JPanel(new GridBagLayout()));
		euclideanSequencerPanel.setBorder(BorderFactory.createTitledBorder("Euclidean sequencer"));
		euclideanSequencerStart = new JControlledTextIconButton('▶', bc("euclidean-sequencer-active"), true);
		JTextIconButton euclideanSequencerStop = new JControlledTextIconButton('⏹', bc("euclidean-sequencer-active"), false);
		JKnob euclideanNoteKnob = new JKnob("Note", c("euclidean-sequencer-note"), ValueFormatter.MIDI_NOTE_TO_NAME);
		JKnob euclideanGateKnob = new JKnob("Gate", c("euclidean-sequencer-gate"), ValueFormatter.PERCENT);
		JKnob euclideanOuterSteps = new JKnob("Outer", c("euclidean-sequencer-outer-steps"), ValueFormatter.INTEGER);
		JKnob euclideanInnerSteps = new JKnob("Inner", c("euclidean-sequencer-inner-steps"), ValueFormatter.INTEGER);
		JKnob euclideanRotation = new JKnob("Rotation", c("euclidean-sequencer-rotation"), ValueFormatter.INTEGER);
		JKnob euclideanNoteVariance = new JKnob("Variance", c("euclidean-sequencer-note-variance"), ValueFormatter.INTEGER);
		
		euclideanStepVisualizer = new JEuclideanStepVisualizer();
		
		euclideanSequencerPanel.add(euclideanOuterSteps,     gbc(0, 0, 1, 1));
		euclideanSequencerPanel.add(euclideanInnerSteps,     gbc(1, 0, 1, 1));
		euclideanSequencerPanel.add(euclideanRotation,       gbc(2, 0, 1, 1));
		euclideanSequencerPanel.add(euclideanStepVisualizer, gbc(3, 0, 1, 1));
		euclideanSequencerPanel.add(euclideanNoteKnob,       gbc(0, 1, 1, 2));
		euclideanSequencerPanel.add(euclideanNoteVariance,   gbc(1, 1, 1, 2));
		euclideanSequencerPanel.add(euclideanGateKnob,       gbc(2, 1, 1, 2));
		euclideanSequencerPanel.add(euclideanSequencerStop,  gbc(3, 1, 1, 1));
		euclideanSequencerPanel.add(euclideanSequencerStart, gbc(3, 2, 1, 1));

		JPanel stepSequencerPanel = (JPanel) add(new JPanel(new GridBagLayout()));
		stepSequencerPanel.setBorder(BorderFactory.createTitledBorder("Step sequencer"));

		for (int step = 0; step < 16; step++) {
			JKnob note = new JKnob("Note", c("sequencer-note-" + step), ValueFormatter.MIDI_NOTE_TO_NAME);
			JKnob gate = new JKnob("Gate", c("sequencer-gate-" + step), ValueFormatter.PERCENT);
			JLight led = new JLight();
			JLabel stepLabel = new JLabel(String.valueOf(step + 1));

			led.setAlignmentX(CENTER_ALIGNMENT);
			stepLabel.setAlignmentX(CENTER_ALIGNMENT);
			stepLabel.setHorizontalAlignment(JLabel.CENTER);

			stepSequencerPanel.add(note,      gbc(step, 0, 1, 1));
			stepSequencerPanel.add(gate,      gbc(step, 1, 1, 2));
			stepSequencerPanel.add(led,       gbc(step, 3, 1, 1));
			stepSequencerPanel.add(stepLabel, gbc(step, 4, 1, 1));
			
			sequencerLeds[step] = led;

			// todo: fit the ties in somewhere
			//add(ico('⁀'),  gbc(4+(i*2) + 1, 3, 1, 1));
			// this doesn't work, because it doesn't allow freely places things in addition to the things in the layout.
			// If we want to do that, we're going to have to just paint them over, which is not ideal
			//ico('⁀').setBounds(310 + (i * 30), 240, 10, 10);
		}

		stepSequencerStart = new JControlledTextIconButton('▶', bc("sequencer-active"), true);
		JTextIconButton stepSequencerStop = new JControlledTextIconButton('⏹', bc("sequencer-active"), false);
		JTextIconButton stepSequencerRecord = new JControlledTextIconButton('⏺', bc("sequencer-listen"), true);
		stepSequencerStop.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				bc("sequencer-listen").set(false, this);
			}
		});
		stepSequencerStart.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				bc("sequencer-listen").set(false, this);
			}
		});
		JKnob sequencerSteps = new JKnob("Steps", c("sequencer-steps"), ValueFormatter.INTEGER);
		stepSequencerPanel.add(sequencerSteps,      gbc(17, 0, 1, 1));
		stepSequencerPanel.add(stepSequencerRecord, gbc(17, 1, 1, 1));
		stepSequencerPanel.add(stepSequencerStop,   gbc(17, 2, 1, 1));
		stepSequencerPanel.add(stepSequencerStart,  gbc(17, 3, 1, 2));

		//@formatter:on

		euclideanSequencerStop.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				euclideanSequencerStart.setHighlightOn(false);
			}
		});
		arpeggiatorStop.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				arpeggiatorStart.setHighlightOn(false);
			}
		});
		stepSequencerStop.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				stepSequencerStart.setHighlightOn(false);
			}
		});

	}

	private JComboBox<String> arpMode() {
		// If we could deal with real objects in the combo boxes, we could have an object whose toString() method
		// returned the character that we wanted, instead of this whole rendering thing...
		JComboBox<String> modeBox = new JControlledComboBox(controls.getDiscreteControl("arpeggiator-mode"), 20);
		modeBox.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 18)); // this is needed for the icons
		ListCellRenderer<? super String> originalRenderer = modeBox.getRenderer();
		modeBox.setRenderer(new ListCellRenderer<>() {
			@Override
			public Component getListCellRendererComponent(JList<? extends String> list, String value, int index, boolean isSelected, boolean cellHasFocus) {
				String renderedValue = switch (value) {
					case "UP" -> "⇈";
					case "DOWN" -> "⇊";
					case "UP_AND_DOWN" -> "⇅";
					case "RANDOM" -> "☈";
					default -> value;
				};
				return originalRenderer.getListCellRendererComponent(list, renderedValue, index, isSelected, cellHasFocus);
			}
		});
		return modeBox;
	}

	private Control c(String key) {
		return controls.getControl(key);
	}

	private BooleanControl bc(String key) {
		return controls.getBooleanControl(key);
	}

	/**
	 * Visualize the step sequencer steps.
	 *
	 * @param step which step this is about
	 * @param on   whether the step is currently active
	 */
	public void step(int step, boolean on) {
		if (step >= 0 && step < sequencerLeds.length) {
			JLight led = sequencerLeds[step];
			if (on) {
				led.on(Color.RED);
			} else {
				led.off();
			}
		}
	}

	public void beat(Sequencer sequencer) {
		if (sequencer instanceof EuclideanSequencer) {
			euclideanSequencerStart.setHighlightOn(!euclideanSequencerStart.isHighlightOn());
		} else if (sequencer instanceof Arpeggiator) {
			arpeggiatorStart.setHighlightOn(!arpeggiatorStart.isHighlightOn());
		} else if (sequencer instanceof StepSequencer) {
			stepSequencerStart.setHighlightOn(!stepSequencerStart.isHighlightOn());
		}
	}

	public void step(int step, boolean on, boolean[] steps) {
		euclideanStepVisualizer.update(step, on, steps);
	}

	private static final class JEuclideanStepVisualizer extends JPanel {
		private final int diameterOfCircleOfDots = 40;
		private int currentStepOn;
		private boolean[] steps;
		private int outerSteps;

		private void update(int step, boolean on, boolean[] steps) {
			this.outerSteps = steps.length;
			this.steps = steps;
			if (on) {
				this.currentStepOn = step;
			} else {
				this.currentStepOn = -1;
			}
			repaint();
		}

		@Override
		protected void paintComponent(Graphics g) {
			// Without calling super, we'll get other components' content painted in ours. Why?
			// It's also possible to avoid this by just painting the background ourselves
			//g.setColor(getBackground());
			//g.fillRect(0, 0, getWidth(), getHeight());
			super.paintComponent(g);


			Graphics2D g2 = (Graphics2D) g;
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g.setFont(new Font(FontSupport.PREFERRED_FONT, Font.PLAIN, 12));
			FontMetrics fontMetrics = g.getFontMetrics();
			int stringHeight = fontMetrics.getHeight();

			double degreesPerOuter = 360d / outerSteps;
			double radiansPerDegree = Math.PI / 180d;

			int width = getWidth();
			int xOffset = (width - diameterOfCircleOfDots) / 2;
			int yOffset = 6 + stringHeight;

			for (int i = 0; i < outerSteps; i++) {
				// do -90 degrees here to start at 12 o'clock, rather than at 3 o'clock
				double theta = ((degreesPerOuter * i) - 90) * radiansPerDegree;
				int indicatorDiameter = diameterOfCircleOfDots / 7;
				int indicatorRadius = indicatorDiameter / 2;
				int radius = (diameterOfCircleOfDots / 2) - indicatorRadius;
				int indicatorX = (int) (radius + (radius * Math.cos(theta)));
				int indicatorY = (int) (radius + (radius * Math.sin(theta)));
				if (steps[i]) {
					g.setColor(Color.RED);
				} else {
					g.setColor(Color.PINK);
				}
				if (i == currentStepOn) {
					g.setColor(Color.GREEN);
				}
				g.fillOval(xOffset + indicatorX, yOffset + indicatorY, indicatorDiameter, indicatorDiameter);
			}
		}
	}
}
