/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.ui;

import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.control.ControlListener;
import net.jevring.frequencies.v2.math.Clamp;
import net.jevring.frequencies.v2.math.Interpolation;
import net.jevring.frequencies.v2.modulation.SidedDepth;
import net.jevring.frequencies.v2.util.ValueFormatter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Locale;
import java.util.Objects;

/**
 * A knob to twist connected to a {@link Control}.
 *
 * @author markus@jevring.net
 * @created 2020-02-21 21:04
 */
public class JKnob extends JPanel implements JControlChangeMappable, JModulationDisplay, JSkinnable {
	private static final boolean DOT_INDICATOR = false;
	private static final int INDICATOR_DEGREE_OFFSET_AT_START = 135;
	/**
	 * The higher this value, the more sensitive the knob gets.
	 */
	private static final double SENSITIVITY = 250d;
	private static final int DIAMETER = 40;
	private final Control control;
	private final String label;
	private final ValueFormatter valueFormat;

	private volatile int mouseClickedLocation = -1;
	private volatile double valueAtMouseClick = -1;

	private volatile SidedDepth modulationDepth;
	private volatile double modulationValue;

	private volatile Skin skin = Skins.defaultSkin();
	private volatile boolean disabled = false;

	public JKnob(String label, Control control) {
		this(label, control, ValueFormatter.PURE);
	}

	public JKnob(String label, Control control, ValueFormatter valueFormat) {
		this(label, control, valueFormat, 50);
	}

	public JKnob(String label, Control control, ValueFormatter valueFormat, int width) {
		this.valueFormat = valueFormat;
		setDoubleBuffered(true);


		int height = 85;
		if (label == null) {
			height = 65;
		}
		setAllSizes(width, height);
		this.label = label;
		this.control = Objects.requireNonNull(control);
		this.control.addListener(new ControlListener() {
			@Override
			public void valueChanged(double min, double newValue, double max, Object source) {
				repaint();
			}
		});

		ToolTipManager.sharedInstance().setInitialDelay(150);
		updateTooltip();
		addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					requestFocus();
					mouseClickedLocation = e.getY();
					valueAtMouseClick = control.getCurrentValue();
				} else {
					mouseClickedLocation = -1;
					valueAtMouseClick = -1;
				}
			}
		});
		
		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				if (mouseClickedLocation != -1) {
					int distance = mouseClickedLocation - e.getY();
					double domainChange;
					if (distance > 0) {
						domainChange = Interpolation.linear(0, SENSITIVITY, distance, 0, control.getMax());
					} else {
						domainChange = Interpolation.linear(SENSITIVITY, 0, distance, control.getMax(), 0);
					}
					double newValue = valueAtMouseClick + domainChange;
					double domainValue = Clamp.clamp(newValue, control.getMin(), control.getMax());
					control.set(control.getMin(), domainValue, control.getMax(), JKnob.this);
					updateTooltip();
					repaint();
				}
			}
		});
		addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_UP) {
					increment();
				} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					decrement();
				}
			}
		});
		addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				if (e.getWheelRotation() < 0) {
					// mouse wheel went "up", away from the user
					increment();
				} else {
					decrement();
				}
			}
		});
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
		updateTooltip();
		repaint();
	}

	public void setAllSizes(int width, int height) {
		Dimension size = new Dimension(width, height);
		setMinimumSize(size);
		setPreferredSize(size);
		setMaximumSize(size);
		setSize(size);
	}

	private void increment() {
		if (control.isStepped()) {
			control.setRaw(control.getCurrentValue() + 1);
		} else {
			control.setRaw(BigDecimal.valueOf(control.getCurrentValue()).add(new BigDecimal("0.1")).setScale(1, RoundingMode.HALF_UP).doubleValue());
		}
		updateTooltip();
		repaint();
	}

	private void decrement() {
		if (control.isStepped()) {
			control.setRaw(control.getCurrentValue() - 1);
		} else {
			control.setRaw(BigDecimal.valueOf(control.getCurrentValue()).subtract(new BigDecimal("0.1")).setScale(1, RoundingMode.HALF_UP).doubleValue());
		}
		updateTooltip();
		repaint();
	}

	private void updateTooltip() {
		if (disabled) {
			setToolTipText("This control is disabled. I'm working on fixing it.");
		} else {
			setToolTipText(String.format(Locale.US,
			                             "%s: Min: %.2f, Value: %.2f, Max: %.2f, Curve: %s",
			                             this.label == null ? "" : this.label,
			                             control.getMin(),
			                             control.getCurrentValue(),
			                             control.getMax(),
			                             control.getCurve().getClass().getSimpleName()));
		}
	}

	@Override
	public void setSkin(Skin skin) {
		this.skin = skin;
		repaint();
	}

	@Override
	public void currentModulation(SidedDepth depth, double value) {
		if (Objects.equals(depth, this.modulationDepth) && value == this.modulationValue) {
			// This won't happen when there's modulation going on, but it'll happen all the time when there is NO modulation going on
			// This tiny check here makes the CPU usage on my machine go from ~15% to ~3%,
			// and the gpu usage, which was inexplicably at 50% (though probably not really), go down to ~5%
			return;
		}
		this.modulationDepth = depth;
		this.modulationValue = value;
		repaint();
	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setFont(new Font(FontSupport.PREFERRED_FONT, Font.PLAIN, 12));
		FontMetrics fontMetrics = g.getFontMetrics();
		int stringHeight = fontMetrics.getHeight();

		if (isOpaque()) {
			// Fill the background
			g.setColor(skin.background());
			g.fillRect(0, 0, getWidth(), getHeight());
		}
		int width = getWidth();
		int xOffset = (width - DIAMETER) / 2;
		int yOffset;
		if (label != null) {
			yOffset = 6 + stringHeight;
		} else {
			yOffset = 6;
		}

		if (modulationDepth != null) {
			// static
			double maxDegreesCoverageForModulationRange = 270d;

			// Draw the modulation range (green)
			int arcStartInDegrees = (int) Interpolation.linear(control.getMin(), control.getMax(), control.getCurrentValue(), 235d, -45);
			double negativeRangeProportion = modulationDepth.negative() / 2d;
			double positiveRangeProportion = modulationDepth.positive() / 2d;
			int negativeRange = (int) (maxDegreesCoverageForModulationRange * negativeRangeProportion);
			int positiveRange = (int) (maxDegreesCoverageForModulationRange * positiveRangeProportion);

			g.setColor(skin.modulationRange());
			int rangeXOffset = xOffset - 6;
			int rangeYOffset = yOffset - 6;
			g.fillArc(rangeXOffset, rangeYOffset, DIAMETER + 12, DIAMETER + 12, arcStartInDegrees, -negativeRange); // negative
			g.fillArc(rangeXOffset, rangeYOffset, DIAMETER + 12, DIAMETER + 12, arcStartInDegrees, -positiveRange); // positive

			if (modulationValue != 0) {
				// Draw the current modulation (yellow)
				g.setColor(skin.modulationValue());
				if (modulationValue < 0) {
					double activeProportion = modulationValue / modulationDepth.negative();
					int value = (int) (negativeRange * activeProportion);
					g.fillArc(rangeXOffset + 3, rangeYOffset + 3, DIAMETER + 6, DIAMETER + 6, arcStartInDegrees, -value);
				} else {
					double activeProportion = modulationValue / modulationDepth.positive();
					int value = (int) (positiveRange * activeProportion);
					g.fillArc(rangeXOffset + 3, rangeYOffset + 3, DIAMETER + 6, DIAMETER + 6, arcStartInDegrees, -value);
				}
			}
		}

		// Draw main knob
		if (disabled) {
			g.setColor(Color.GRAY);
		} else {
			g.setColor(skin.knobBackground());
		}
		g.fillOval(xOffset, yOffset, DIAMETER, DIAMETER);


		// Draw the indicator
		// the arc only covers 3/4 of the circle
		double currentValueRotationInDegrees = Interpolation.linear(control.getMin(), control.getMax(), control.getCurrentValue(), 0, 270d);
		double radiansPerDegree = Math.PI / 180d;
		double theta = (currentValueRotationInDegrees + INDICATOR_DEGREE_OFFSET_AT_START) * radiansPerDegree;
		if (DOT_INDICATOR) {
			int indicatorDiameter = DIAMETER / 7;
			int indicatorRadius = indicatorDiameter / 2;
			int radius = (DIAMETER / 2) - indicatorRadius;
			int indicatorX = (int) (radius + (radius * Math.cos(theta)));
			int indicatorY = (int) (radius + (radius * Math.sin(theta)));
			g.setColor(skin.knobForeground());
			g.fillOval(xOffset + indicatorX, yOffset + indicatorY, indicatorDiameter, indicatorDiameter);
		} else {
			double radius = DIAMETER / 2d;
			double endX = xOffset + (radius + (radius * Math.cos(theta)));
			double endY = yOffset + (radius + (radius * Math.sin(theta)));

			double centerX = xOffset + radius;
			double centerY = yOffset + radius;

			double lengthX = endX - centerX;
			double lengthY = endY - centerY;

			double segmentStartX = centerX + (lengthX * 0.6);
			double segmentStartY = centerY + (lengthY * 0.6);

			double segmentEndX = centerX + (lengthX * 0.9);
			double segmentEndY = centerY + (lengthY * 0.9);

			g.setColor(skin.knobForeground());
			g2.setStroke(new BasicStroke(3f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
			g.drawLine((int) Math.round(segmentStartX), (int) Math.round(segmentStartY), (int) Math.round(segmentEndX), (int) Math.round(segmentEndY));
		}

		int mid = DIAMETER / 2;
		g.setColor(skin.text());

		if (valueFormat != null) {
			String value = valueFormat.format(control.getCurrentValue());
			int valueWidth = fontMetrics.stringWidth(value);
			int valueX = mid - (valueWidth / 2);
			g.drawString(value, xOffset + valueX, yOffset + DIAMETER + stringHeight);
		}

		if (label != null) {
			int labelWidth = fontMetrics.stringWidth(label);
			int labelX = mid - (labelWidth / 2);
			g.drawString(label, xOffset + labelX, stringHeight);
		}
	}

	@Override
	public String getControl() {
		return control.getKey();
	}

}
