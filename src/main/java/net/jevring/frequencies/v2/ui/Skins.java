/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.ui;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import javax.swing.plaf.metal.DefaultMetalTheme;
import javax.swing.plaf.metal.MetalLookAndFeel;
import java.awt.*;

/**
 * Entrypoint for managing and setting skins
 *
 * @author markus@jevring.net
 * @created 2020-04-26 14:24
 */
public class Skins {
	private static final Color windows10LightThemeTaskBar = new Color(219, 218, 235);
	private static final Color k9sSelectedPod = new Color(135, 255, 175);

	private static final Color winampCenterBlueGrey = new Color(55, 55, 87);
	private static final Color winampEdgeBlueGrey = new Color(22, 22, 34);
	private static final Color winampButtonGrey = new Color(189, 206, 214);
	private static final Color winampGold = new Color(236, 206, 122);

	private static final Color beige = new Color(204, 201, 179);
	private static final Color intellijIdeaLight = new Color(242, 242, 242);

	// todo: come up with some skins that actually look nice, and don't look like garbage
	public static final Skin DARK = new Skin("Dark", Color.gray, Color.WHITE, Color.WHITE, Color.darkGray, Color.white, Color.green, Color.yellow, true);
	public static final Skin DARK_OPAQUE =
			new Skin("Dark (frikkin' laser beams!)", Color.gray, Color.WHITE, Color.WHITE, Color.darkGray, Color.white, Color.green, Color.yellow, false);
	public static final Skin LIGHT = new Skin("Light", Color.white, Color.BLACK, Color.BLACK, Color.darkGray, Color.white, Color.green, Color.yellow, true);
	public static final Skin BLUE_RED = new Skin("Light", Color.white, Color.BLACK, Color.BLACK, Color.darkGray, Color.white, Color.blue, Color.red, false);

	private static volatile Skin currentSkin = defaultSkin();

	public static Skin defaultSkin() {
		return LIGHT;
	}

	public static Skin currentSkin() {
		return currentSkin;
	}

	public static java.util.List<Skin> skins() {
		return java.util.List.of(DARK, DARK_OPAQUE, LIGHT);
	}

	public static void setSkin(Skin skin, Component c) {

		try {
			MetalLookAndFeel.setCurrentTheme(new SkinnedMetalTheme(skin));
			UIManager.setLookAndFeel(new MetalLookAndFeel());
			SwingUtilities.updateComponentTreeUI(c);
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		setSkin0(skin, c);
	}

	private static void setSkin0(Skin skin, Component c) {
		currentSkin = skin;
		if (c instanceof JComponent jComponent && !(c instanceof JMenuBar)) {
			jComponent.setOpaque(skin.opaque());
		}
		if (c instanceof JSkinnable jSkinnable) {
			jSkinnable.setSkin(skin);
		} else {
			c.setBackground(skin.background());
			c.setForeground(skin.foreground());
			if (c instanceof JPanel jPanel) {
				Border border = jPanel.getBorder();
				if (border instanceof TitledBorder titledBorder) {
					titledBorder.setTitleColor(skin.text());
					if (titledBorder.getBorder() instanceof LineBorder lineBorder) {
						// todo: this is super hacky. Can we make it better?
						titledBorder.setBorder(new BorderUIResource.LineBorderUIResource(skin.foreground(), lineBorder.getThickness()));
					}
				}
			} else if (c instanceof JComboBox<?> comboBox) {
				Component editorComponent = comboBox.getEditor().getEditorComponent();
				if (editorComponent instanceof JComponent jEditorComponent) {
					jEditorComponent.setOpaque(skin.opaque());
				}
				// If we switch skins too much, we'll end up with a nasty nested renderer,
				// but we have to do this so that we can chain renderers elsewhere.
				// Ideally we shouldn't be changing skins that often
				// todo: why doesn't this work with wildcards?
				ListCellRenderer originalRenderer = comboBox.getRenderer();
				comboBox.setRenderer(new BasicComboBoxRenderer.UIResource() {
					@Override
					public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
						Component listCellRendererComponent = originalRenderer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
						if (listCellRendererComponent instanceof JComponent jListCellRendererComponent) {
							jListCellRendererComponent.setOpaque(skin.opaque());
						}
						return listCellRendererComponent;
					}
				});
			}
		}
		if (c instanceof Container container) {
			for (int i = 0; i < container.getComponentCount(); i++) {
				Component component = container.getComponent(i);
				setSkin0(skin, component);
			}
		}
	}

	/**
	 * We could use this class to skin the whole thing, but then we'd have to figure out all the choices
	 * they've made in how colors are grouped, and work with that. It can be done, and it probably should
	 * be done this way, but for now let's settle on a mix between a theme and a skin.
	 */
	private static final class SkinnedMetalTheme extends DefaultMetalTheme {
		private final FontUIResource font = new FontUIResource(FontSupport.PREFERRED_FONT, Font.PLAIN, 12);
		private final Skin skin;

		private SkinnedMetalTheme(Skin skin) {
			this.skin = skin;
		}

		@Override
		public ColorUIResource getControlInfo() {
			// Arrows on dropdowns
			// As much as we'd have liked this to work, it didn't: https://stackoverflow.com/questions/3008447/how-can-i-change-the-arrow-style-in-a-jcombobox/3008587#3008587
			return new ColorUIResource(skin.foreground());
		}

		@Override
		public FontUIResource getControlTextFont() {
			return font;
		}

		@Override
		public FontUIResource getSystemTextFont() {
			return font;
		}

		@Override
		public FontUIResource getUserTextFont() {
			return font;
		}

		@Override
		public FontUIResource getMenuTextFont() {
			return font;
		}

		@Override
		public FontUIResource getWindowTitleFont() {
			return font;
		}

		@Override
		public FontUIResource getSubTextFont() {
			return font;
		}
	}
}
