/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.ui;

import net.jevring.frequencies.v2.control.DiscreteControl;
import net.jevring.frequencies.v2.control.DiscreteControlListener;

import javax.swing.*;
import java.awt.*;

/**
 * A {@link JComboBox} married to a {@link DiscreteControl}
 *
 * @author markus@jevring.net
 * @created 2020-04-27 22:00
 */
public class JControlledComboBox extends JComboBox<String> implements JControlChangeMappable {
	private final DiscreteControl discreteControl;
	
	public JControlledComboBox(DiscreteControl control, int width) {
		super(control.getAllowedValues().toArray(new String[0]));
		this.discreteControl = control;
		addItemListener(e -> control.set((String) e.getItem(), this));
		setSelectedIndex(0);
		Dimension size = new Dimension(width, 20);
		setMaximumSize(size);
		setMinimumSize(size);
		setPreferredSize(size);
		setSize(size);
		control.addListener(new DiscreteControlListener<>() {
			@Override
			public void valueChanged(String value, Object source) {
				if (source != this) {
					JControlledComboBox.this.setSelectedItem(value);
				}
			}
		});
	}

	@Override
	public String getControl() {
		return discreteControl.getKey();
	}
}
