/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.ui;

import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.control.DiscreteControl;
import net.jevring.frequencies.v2.control.DiscreteControlListener;
import net.jevring.frequencies.v2.envelopes.Envelope;
import net.jevring.frequencies.v2.envelopes.Envelopes;
import net.jevring.frequencies.v2.envelopes.LoopingEnvelope;
import net.jevring.frequencies.v2.util.ValueFormatter;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

import static net.jevring.frequencies.v2.ui.GridBadLayoutUtils.gbc;

/**
 * Controls for an envelope.
 *
 * @author markus@jevring.net
 * @created 2020-04-18 16:42
 */
public class JEnvelopePanel extends JPanel {
	public JEnvelopePanel(Controls controls, Envelopes envelopes, String prefix) {
		DiscreteControl envelopeType = controls.getDiscreteControl(prefix + "-envelope-type");
		JControlledComboBox envelopeSelector = new JControlledComboBox(envelopeType, 180);
		JKnob volumeAttackKnob = new JKnob("Attack", controls.getControl(prefix + "-envelope-attack"), ValueFormatter.MILLISECONDS);
		JKnob volumeDecayKnob = new JKnob("Decay", controls.getControl(prefix + "-envelope-decay"), ValueFormatter.MILLISECONDS);
		JKnob volumeSustainKnob = new JKnob("Sustain", controls.getControl(prefix + "-envelope-sustain"), ValueFormatter.PERCENT);
		JKnob volumeReleaseKnob = new JKnob("Release", controls.getControl(prefix + "-envelope-release"), ValueFormatter.MILLISECONDS);
		JCheckBox loop = new JCheckBox("Loop", false);
		Bindings.bind(loop, controls.getBooleanControl(prefix + "-envelope-loop"));
		envelopeType.mapping(envelopes::create).addListener(new DiscreteControlListener<Envelope>() {
			@Override
			public void valueChanged(Envelope envelope, Object source) {
				if (envelope instanceof LoopingEnvelope) {
					loop.setEnabled(true);
					loop.setToolTipText(null);
				} else {
					loop.setEnabled(false);
					loop.setToolTipText("Not supported for this envelope");
				}
			}
		});

		setLayout(new GridBagLayout());
		String title = prefix + " envelope";
		title = Character.toUpperCase(title.charAt(0)) + title.substring(1);
		setBorder(new TitledBorder(null, title, TitledBorder.LEFT, TitledBorder.TOP));
		setAlignmentX(LEFT_ALIGNMENT);
		//@formatter:off
		add(envelopeSelector,       gbc(0, 0, 3, 1, 1, 2));
		add(loop,                   gbc(3, 0, 1, 1, 1, 2));
		add(volumeAttackKnob,       gbc(0, 1, 1, 1, 1, 2));
		add(volumeDecayKnob,        gbc(1, 1, 1, 1, 1, 2));
		add(volumeSustainKnob,      gbc(2, 1, 1, 1, 1, 2));
		add(volumeReleaseKnob,      gbc(3, 1, 1, 1, 1, 2));
		//@formatter:on
	}
}
