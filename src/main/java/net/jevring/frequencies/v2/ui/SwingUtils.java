/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.ui;

import javax.swing.*;
import java.awt.*;

/**
 * Swing utilities that are not the normal swing utilities =)
 *
 * @author markus@jevring.net
 * @created 2019-12-08 20:39
 */
public class SwingUtils {
	public static JFrame showWindowFor(String name, JComponent contentPane, final int width, final int height) {
		JFrame frame = new JFrame(name);
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {

				frame.setContentPane(contentPane);
				frame.pack();
				frame.setVisible(true);
				frame.setResizable(false);
				// todo: try to position this in the middle
				//Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
				Dimension size = new Dimension(width, height);
				int x = 300;
				int y = 100;
				frame.setLocation(x, y);
				frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
				frame.setPreferredSize(size);
				frame.setSize(size);
			}
		});
		return frame;
	}

	public static JFrame showWindowFor(String name, JComponent contentPane, boolean fixedSize) {
		JFrame frame = new JFrame(name);
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {

				frame.setContentPane(contentPane);
				frame.pack();
				frame.setVisible(true);
				frame.setResizable(false);
				Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
				Dimension size;
				if (fixedSize) {
					size = new Dimension(1332, 875);
				} else {
					size = frame.getSize();
				}
				int x = (int) ((screenSize.getWidth() / 2) - (size.getWidth() / 2));
				int y = (int) ((screenSize.getHeight() / 2) - (size.getHeight() / 2));
				if (fixedSize) {
					x = 300;
					y = 100;
				}
				frame.setLocation(x, y);
				frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
				if (fixedSize) {
					frame.setPreferredSize(size);
					frame.setSize(size);
				}
			}
		});
		return frame;
	}
}
