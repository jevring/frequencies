/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.ui;

import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.util.ValueFormatter;

import javax.swing.*;
import javax.swing.border.TitledBorder;

/**
 * A panel for sample and hold control.
 *
 * @author markus@jevring.net
 */
public class JBitCrusherPanel extends JPanel {
	public JBitCrusherPanel(Controls controls) {
		setBorder(new TitledBorder(null, "Bit crusher", TitledBorder.LEFT, TitledBorder.TOP));
		setAlignmentY(JComponent.TOP_ALIGNMENT);
		JKnob downsampling = new JKnob("Downsampling", controls.getControl("bit-crusher-downsampling"), ValueFormatter.INTEGER, 75);
		JKnob resolution = new JKnob("Resolution", controls.getControl("bit-crusher-resolution"), ValueFormatter.INTEGER, 75);

		setLayout(null);
		add(downsampling);
		add(resolution);
		downsampling.setBounds(5, 46, 75, 85);
		resolution.setBounds(100, 46, 75, 85);
	}
}
