/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.ui;

import net.jevring.frequencies.v2.input.KeyTimings;

import javax.swing.text.JTextComponent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashSet;
import java.util.Set;

/**
 * @author markus@jevring.net
 * @created 2020-02-10 21:24
 */
public class KeyboardInput implements KeyListener {
	private final Set<Integer> keysDown = new HashSet<>();
	private final KeyTimings keyTimings;

	public KeyboardInput(KeyTimings keyTimings) {
		this.keyTimings = keyTimings;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// NOOP
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int key = toMidi(e);
		if (key == -1 || e.getComponent() instanceof JTextComponent) {
			return;
		}
		if (keysDown.add(key)) {
			if (key >= 0) {
				keyTimings.down(key, 127);
			}
		}
		e.consume();
	}

	@Override
	public void keyReleased(KeyEvent e) {
		int key = toMidi(e);
		if (key == -1 || e.getComponent() instanceof JTextComponent) {
			return;
		}
		keysDown.remove(key);
		if (key >= 0) {
			keyTimings.up(key);
		}
		e.consume();
	}

	private int toMidi(KeyEvent e) {
		return switch (e.getKeyCode()) {
			case KeyEvent.VK_A -> 60;
			case KeyEvent.VK_W -> 61;
			case KeyEvent.VK_S -> 62;
			case KeyEvent.VK_E -> 63;
			case KeyEvent.VK_D -> 64;
			case KeyEvent.VK_F -> 65;
			case KeyEvent.VK_T -> 66;
			case KeyEvent.VK_G -> 67;
			case KeyEvent.VK_Y -> 68;
			case KeyEvent.VK_H -> 69;
			case KeyEvent.VK_U -> 70;
			case KeyEvent.VK_J -> 71;
			default -> -1; // only activate on some keys
		};
	}
}
