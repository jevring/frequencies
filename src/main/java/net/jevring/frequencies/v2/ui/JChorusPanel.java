/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.ui;

import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.util.ValueFormatter;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

import static net.jevring.frequencies.v2.ui.GridBadLayoutUtils.gbc;

/**
 * Controls for the chorus effect.
 *
 * @author markus@jevring.net
 * @created 2021-02-13 13:50
 */
public class JChorusPanel extends JPanel {
	public JChorusPanel(Controls controls) {
		super(new GridBagLayout());
		setBorder(new TitledBorder("Chorus"));

		JKnob dryWetMix = new JKnob("Dry/wet mix", controls.getControl("chorus-dry-wet-mix"), ValueFormatter.PERCENT, 60);
		JKnob d1 = new JKnob("Delay1", controls.getControl("chorus-delay-in-milliseconds-1"), ValueFormatter.LOW_MILLISECONDS);
		JKnob d2 = new JKnob("Delay2", controls.getControl("chorus-delay-in-milliseconds-2"), ValueFormatter.LOW_MILLISECONDS);
		JKnob r1 = new JKnob("rate1", controls.getControl("chorus-rate-1"), ValueFormatter.LOW_FREQUENCY);
		JKnob r2 = new JKnob("rate2", controls.getControl("chorus-rate-2"), ValueFormatter.LOW_FREQUENCY);
		JKnob depth1 = new JKnob("depth1", controls.getControl("chorus-depth-1"), ValueFormatter.LOW_MILLISECONDS);
		JKnob depth2 = new JKnob("depth2", controls.getControl("chorus-depth-2"), ValueFormatter.LOW_MILLISECONDS);

		// @formatter:off
		add(dryWetMix, gbc(0, 1, 1, 1, 1, 1));
		add(d1,        gbc(1, 1, 1, 1, 1, 1));
		add(d2,        gbc(2, 1, 1, 1, 1, 1));
		add(r1,        gbc(1, 2, 1, 1, 1, 1));
		add(r2,        gbc(2, 2, 1, 1, 1, 1));
		add(depth1,    gbc(1, 3, 1, 1, 1, 1));
		add(depth2,    gbc(2, 3, 1, 1, 1, 1));
		// @formatter:on

	}
}
