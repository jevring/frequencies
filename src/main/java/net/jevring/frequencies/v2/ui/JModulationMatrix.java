/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.ui;

import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.modulation.matrix.ModulationMatrix;
import net.jevring.frequencies.v2.modulation.matrix.ModulationMatrixRow;
import net.jevring.frequencies.v2.modulation.matrix.ModulationSource;
import net.jevring.frequencies.v2.modulation.matrix.ModulationTarget;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * UI for the {@link net.jevring.frequencies.v2.modulation.matrix.ModulationMatrix}.
 *
 * @author markus@jevring.net
 * @created 2020-04-11 18:11
 */
public class JModulationMatrix extends JPanel {
	private static final int rowHeight = 25;
	private static final int rows = 13;
	private final ModulationMatrix modulationMatrix;

	public JModulationMatrix(ModulationMatrix modulationMatrix) {
		this.modulationMatrix = modulationMatrix;
		setBorder(new EtchedBorder());
		BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
		setLayout(layout);
		setBorder(new TitledBorder("Modulation matrix"));
		Dimension size = new Dimension(575, 390);
		setMaximumSize(size);
		setMinimumSize(size);
		setPreferredSize(size);
		setSize(size);
	}

	public void modulationMatrixUpdated() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				removeAll();
				addAll();
				Skins.setSkin(Skins.currentSkin(), JModulationMatrix.this);
			}
		});
	}

	private void addAll() {
		int rowsAdded = 0;
		for (ModulationMatrixRow modulationMatrixRow : modulationMatrix.getModulationConfig(false)) {
			add(new JModulationMatrixRow(modulationMatrixRow.source(), modulationMatrixRow.target(), modulationMatrixRow.depth()));
			rowsAdded++;
		}
		for (int i = rowsAdded; i < rows; i++) {
			add(new JModulationMatrixRow(false));
		}
	}

	private final class JModulationMatrixRow extends JPanel {
		private final JComboBox<ModulationSource> sources = new JComboBox<>(ModulationSource.values());
		private final JComboBox<ModulationTarget> targets = new JComboBox<>(ModulationTarget.values());
		private final JSmallKnob knob = new JSmallKnob();
		private final JButton deleteButton = new JButton("Delete");
		private ModulationSource previousSource;
		private ModulationTarget previousTarget;

		public JModulationMatrixRow(ModulationSource source, ModulationTarget target, Control depth) {
			this(true);
			sources.setSelectedItem(source);
			targets.setSelectedItem(target);
			knob.attachControl(depth);
			previousSource = source;
			previousTarget = target;
			registerHooks();
		}

		public JModulationMatrixRow(boolean skipHooks) {
			sources.setSelectedItem(null);
			targets.setSelectedItem(null);
			setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
			add(sources);
			add(knob);
			add(targets);
			add(deleteButton);
			deleteButton.setMargin(new Insets(0, 2, 0, 2));
			deleteButton.setSize(new Dimension(56, rowHeight));
			deleteButton.setMaximumSize(deleteButton.getSize());
			deleteButton.setMinimumSize(deleteButton.getSize());
			deleteButton.setPreferredSize(deleteButton.getSize());

			Dimension size = new Dimension(600, rowHeight);
			setMaximumSize(size);
			setMinimumSize(size);
			setPreferredSize(size);
			setSize(size);
			sources.setSize(new Dimension(175, rowHeight));
			targets.setSize(new Dimension(175, rowHeight));

			if (!skipHooks) {
				registerHooks();
			}
		}

		private void registerHooks() {
			deleteButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					modulationMatrix.remove((ModulationSource) sources.getSelectedItem(), (ModulationTarget) targets.getSelectedItem());
					sources.setSelectedItem(null);
					targets.setSelectedItem(null);
				}
			});
			sources.addActionListener(this::potentiallyUpdateModulationMatrix);
			targets.addActionListener(this::potentiallyUpdateModulationMatrix);

			// todo: These iterations might not be super efficient. That said, there are only like 10 values in each, so it probably doesn't matter.
			//  Maybe we can clean this up later, but for now it works.
			sources.addPopupMenuListener(new PopupMenuListener() {
				@Override
				public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
					sources.removeAllItems();
					// add the list items that are not forbidden based on target selection and modulation matrix content
					List<ModulationMatrixRow> existingRows = modulationMatrix.getModulationConfig(true);
					ModulationTarget modulationTarget = (ModulationTarget) targets.getSelectedItem();
					nextSource:
					for (ModulationSource modulationSource : ModulationSource.values()) {
						if (modulationTarget != null) {
							for (ModulationMatrixRow existingRow : existingRows) {
								if (existingRow.source() == modulationSource && existingRow.target() == modulationTarget) {
									//System.out.println("Skipping existing row: " + existingRow);
									continue nextSource;
								}
							}
						}
						sources.addItem(modulationSource);
					}
					// todo. this works, but we have to ensure that the size of the thing doesn't fluctuate too much
				}

				@Override
				public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
				}

				@Override
				public void popupMenuCanceled(PopupMenuEvent e) {
				}
			});

			targets.addPopupMenuListener(new PopupMenuListener() {
				@Override
				public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
					targets.removeAllItems();
					// add the list items that are not forbidden based on source selection and modulation matrix content
					List<ModulationMatrixRow> existingRows = modulationMatrix.getModulationConfig(true);
					ModulationSource modulationSource = (ModulationSource) sources.getSelectedItem();
					nextSource:
					for (ModulationTarget modulationTarget : ModulationTarget.values()) {
						if (modulationSource != null) {
							for (ModulationMatrixRow existingRow : existingRows) {
								if (existingRow.source() == modulationSource && existingRow.target() == modulationTarget) {
									//System.out.println("Skipping existing row: " + existingRow);
									continue nextSource;
								}
							}
						}
						targets.addItem(modulationTarget);
					}
					// todo. this works, but we have to ensure that the size of the thing doesn't fluctuate too much
				}

				@Override
				public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
				}

				@Override
				public void popupMenuCanceled(PopupMenuEvent e) {
				}
			});
		}

		private void potentiallyUpdateModulationMatrix(ActionEvent ignored) {
			ModulationSource source = (ModulationSource) sources.getSelectedItem();
			ModulationTarget target = (ModulationTarget) targets.getSelectedItem();
			if (source != null && target != null) {
				modulationMatrix.remove(previousSource, previousTarget);
				Control control = modulationMatrix.add(source, target);
				knob.attachControl(control);
				previousSource = source;
				previousTarget = target;
			}
		}
	}
}
