/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.ui;

import javax.swing.*;
import java.awt.*;

/**
 * Visualizes waveforms chunk by chunk.
 *
 * @author markus@jevring.net
 * @created 2020-05-03 11:52
 */
public class JMiniWaveformDisplay extends JComponent implements JOscilloscope, JSkinnable {
	private volatile Skin skin = Skins.BLUE_RED;
	private volatile Polygon polygon = null;

	@Override
	public void newChunk(double[] chunk) {

		// We can get width and height here because we know the component is not resizable. 
		// If it was, this might not work in paintComponent(), but for now it's fine
		int width = getWidth();
		int height = getHeight();

		int middleByHeight = height / 2;

		Polygon p = new Polygon();

		int x = 5;
		int maxXAxis = width - 10;
		int scale = middleByHeight - 10;
		
		int peakIndex = -1;
		double peak = Double.MIN_VALUE;
		for (int i = 0; i < chunk.length; i++) {
			double v = chunk[i];
			if (v > peak) {
				peak = v;
				peakIndex = i;
			}
		}
		peakIndex = Math.max(0, peakIndex - 10);

		for (int i = peakIndex; i < chunk.length; i += 2) {
			double datum = chunk[i];
			int scaledValue = (int) (datum * scale);
			p.addPoint(x, middleByHeight - scaledValue);
			x++;
			if (x >= maxXAxis) {
				break;
			}
		}
		this.polygon = p;
		repaint();
	}

	@Override
	protected void paintComponent(Graphics g) {
		int width = getWidth();
		int height = getHeight();

		int middleByHeight = height / 2;

		// Draw the axes
		if (isOpaque()) {
			g.setColor(skin.background());
			g.fillRect(0, 0, width, height);
		}
		g.setColor(skin.foreground());
		g.drawLine(5, 5, 5, height - 5); // y-axis
		g.drawLine(5, middleByHeight, width - 10, middleByHeight); // x-axis

		if (polygon != null) {
			g.setColor(Color.green);
			g.drawPolyline(polygon.xpoints, polygon.ypoints, polygon.npoints);
			g.setColor(Color.red);
			for (int i = 0; i < polygon.npoints; i++) {
				// we want to draw the dots AFTER the polyline
				g.drawRect(polygon.xpoints[i], polygon.ypoints[i], 1, 1);
			}
		}
	}

	@Override
	public void setSkin(Skin skin) {
		this.skin = skin;
		repaint();
	}
}
