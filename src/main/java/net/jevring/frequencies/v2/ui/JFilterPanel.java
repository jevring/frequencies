/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.ui;

import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.util.ValueFormatter;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

import static net.jevring.frequencies.v2.ui.GridBadLayoutUtils.gbc;

/**
 * UI for the filter.
 *
 * @author markus@jevring.net
 * @created 2021-02-27 21:42
 */
public class JFilterPanel extends JPanel {
	public JFilterPanel(Controls controls) {
		JControlledComboBox filterSelector = new JControlledComboBox(controls.getDiscreteControl("filter"), 240);
		JKnob cutoffFrequencyKnob = new JKnob("Cutoff", controls.getControl("filter-cutoff-frequency"), ValueFormatter.FREQUENCY);
		JKnob resonanceKnob = new JKnob("Q", controls.getControl("q-resonance-emphasis"));
		JKnob filterEnvelopeDepth = new JKnob("Env depth", controls.getControl("filter-envelope-depth"));

		setLayout(new GridBagLayout());
		setBorder(new TitledBorder(null, "Filter", TitledBorder.LEFT, TitledBorder.TOP));
		setAlignmentY(JComponent.TOP_ALIGNMENT);
		add(filterSelector, gbc(0, 0, 3, 1, 1, 1));
		add(filterEnvelopeDepth, gbc(0, 1, 1, 1, 1, 1));
		add(cutoffFrequencyKnob, gbc(1, 1, 1, 1, 1, 1));
		add(resonanceKnob, gbc(2, 1, 1, 1, 1, 1));
		setAlignmentX(LEFT_ALIGNMENT);
		Dimension filterPanelSize = new Dimension(250, 135);
		setMaximumSize(filterPanelSize);
		setMinimumSize(filterPanelSize);
		setPreferredSize(filterPanelSize);
		setSize(filterPanelSize);
	}
}
