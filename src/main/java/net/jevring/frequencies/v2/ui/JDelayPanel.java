/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.ui;

import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.util.ValueFormatter;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

import static net.jevring.frequencies.v2.ui.GridBadLayoutUtils.gbc;

/**
 * UI for {@link net.jevring.frequencies.v2.effects.delay.DelayAndEcho}.
 *
 * @author markus@jevring.net
 * @created 2021-02-27 21:37
 */
public class JDelayPanel extends JPanel {
	public JDelayPanel(Controls controls) {
		super(new GridBagLayout());
		setBorder(new TitledBorder("Delay and echo"));

		JKnob dryWetMix = new JKnob("Dry/wet mix", controls.getControl("delay-dry-wet-mix"), ValueFormatter.PERCENT, 60);
		JKnob time = new JKnob("Time", controls.getControl("delay-in-milliseconds"), ValueFormatter.MILLISECONDS);
		JKnob decay = new JKnob("Decay", controls.getControl("delay-decay"));

		// @formatter:off
		add(dryWetMix, gbc(0, 0, 1, 1, 1, 1));
		add(time,      gbc(1, 0, 1, 1, 1, 1));
		add(decay,     gbc(2, 0, 1, 1, 1, 1));
		// @formatter:on
	}
}
