/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.ui;

import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.control.ControlListener;
import net.jevring.frequencies.v2.control.curves.Linear;
import net.jevring.frequencies.v2.math.Clamp;
import net.jevring.frequencies.v2.math.Interpolation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.Locale;
import java.util.Objects;

/**
 * A smaller knob than {@link JKnob} with fewer features.
 *
 * @author markus@jevring.net
 * @created 2020-04-11 19:12
 */
public class JSmallKnob extends JPanel implements JControlChangeMappable, JSkinnable {
	private static final boolean DOT_INDICATOR = false;
	/**
	 * The higher this value, the more sensitive the knob gets.
	 */
	private static final double SENSITIVITY = 50d;
	private static final int DIAMETER = 25;
	private volatile int mouseClickedLocation = -1;
	private volatile double valueAtMouseClick = -1;
	private volatile Control control = new Control("noop", -1, 0, 1, new Linear(), false); // this is just a dummy control until we set a real one
	private volatile Skin skin = Skins.defaultSkin();

	public JSmallKnob() {
		Dimension size = new Dimension(DIAMETER + 2, DIAMETER + 2);
		setMinimumSize(size);
		setPreferredSize(size);
		setMaximumSize(size);
		setSize(size);

		ToolTipManager.sharedInstance().setInitialDelay(150);
		updateTooltip();
		addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					mouseClickedLocation = e.getY();
					valueAtMouseClick = control.getCurrentValue();
				} else {
					mouseClickedLocation = -1;
					valueAtMouseClick = -1;
				}
			}
		});
		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				if (mouseClickedLocation != -1) {
					int distance = mouseClickedLocation - e.getY();
					double domainChange;
					if (distance > 0) {
						domainChange = Interpolation.linear(0, SENSITIVITY, distance, 0, control.getMax());
					} else {
						domainChange = Interpolation.linear(SENSITIVITY, 0, distance, control.getMax(), 0);
					}
					double newValue = valueAtMouseClick + domainChange;
					double domainValue = Clamp.clamp(newValue, control.getMin(), control.getMax());
					updateTooltip();
					control.set(control.getMin(), domainValue, control.getMax(), JSmallKnob.this);
					repaint();
				}
			}
		});
	}

	private void updateTooltip() {
		setToolTipText(String.format(Locale.US, "%.2f", control.getCurrentValue()));
	}

	public void attachControl(Control control) {
		Objects.requireNonNull(control);
		this.control = control;
		// todo: this should start using the ListenerRegistration to unregister
		this.control.addListener(new ControlListener() {
			@Override
			public void valueChanged(double min, double newValue, double max, Object source) {
				repaint();
			}
		});
		repaint();
	}

	@Override
	public void setSkin(Skin skin) {
		this.skin = skin;
		repaint();
	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		if (isOpaque()) {
			g.setColor(skin.background());
			g.fillRect(0, 0, getWidth(), getHeight());
		}
		int width = getWidth();
		// Draw main knob
		g.setColor(skin.knobBackground());
		int xOffset = (int) Math.round((width - DIAMETER) / 2d);
		int yOffset = 0;
		g.fillOval(xOffset, 0, DIAMETER, DIAMETER);

		// Draw the indicator
		// the arc only covers 3/4 of the circle
		double degrees = Interpolation.linear(control.getMin(), control.getMax(), control.getCurrentValue(), 0, 270d) + 135;
		double radiansPerDegree = Math.PI / 180d;
		double theta = degrees * radiansPerDegree;
		if (DOT_INDICATOR) {
			double indicatorDiameter = DIAMETER / 5d;
			double indicatorRadius = indicatorDiameter / 2d;
			double radius = (DIAMETER / 2d) - indicatorRadius;
			int indicatorX = (int) (radius + (radius * Math.cos(theta)));
			int indicatorY = (int) (radius + (radius * Math.sin(theta)));
			g.setColor(skin.knobForeground());
			g.fillOval(xOffset + indicatorX, indicatorY, (int) Math.round(indicatorDiameter), (int) Math.round(indicatorDiameter));
		} else {
			double radius = DIAMETER / 2d;
			double endX = xOffset + (radius + (radius * Math.cos(theta)));
			double endY = yOffset + (radius + (radius * Math.sin(theta)));

			double centerX = xOffset + radius;
			double centerY = yOffset + radius;

			double lengthX = endX - centerX;
			double lengthY = endY - centerY;

			double segmentStartX = centerX + (lengthX * 0.6);
			double segmentStartY = centerY + (lengthY * 0.6);

			double segmentEndX = centerX + (lengthX * 0.9);
			double segmentEndY = centerY + (lengthY * 0.9);

			g.setColor(skin.knobForeground());
			g2.setStroke(new BasicStroke(3f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
			g.drawLine((int) Math.round(segmentStartX), (int) Math.round(segmentStartY), (int) Math.round(segmentEndX), (int) Math.round(segmentEndY));
		}


	}

	@Override
	public String getControl() {
		return control.getKey();
	}

}
