/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.ui;

import javax.swing.*;
import java.awt.*;

/**
 * A button that only renders a single character in monospace.
 *
 * @author markus@jevring.net
 * @created 2020-06-07 23:51
 */
public class JTextIconButton extends JButton implements JSkinnable {
	private volatile boolean highlightOn = false;
	private volatile Skin skin = Skins.defaultSkin();

	public JTextIconButton(char character) {
		super(String.valueOf(character));
		setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
	}

	@Override
	public void setSkin(Skin skin) {
		// We implement skinnable to avoid changing the font, since we'll need a special font to render these icons
		this.skin = skin;
		setBackground(skin.background());
		setForeground(skin.foreground());
	}

	public boolean isHighlightOn() {
		return highlightOn;
	}

	public void setHighlightOn(boolean highlightOn) {
		this.highlightOn = highlightOn;
		if (this.highlightOn) {
			setForeground(Color.RED);
		} else {
			// todo: do we need a highlight color in the skin?
			setForeground(skin.foreground());
		}
		repaint();
	}
}
