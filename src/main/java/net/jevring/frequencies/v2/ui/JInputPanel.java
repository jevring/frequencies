/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.ui;

import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.control.DiscreteControl;
import net.jevring.frequencies.v2.control.DiscreteControlListener;
import net.jevring.frequencies.v2.input.InputMode;
import net.jevring.frequencies.v2.util.ValueFormatter;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import static net.jevring.frequencies.v2.ui.GridBadLayoutUtils.gbc;

/**
 * UI for the input configuration.
 *
 * @author markus@jevring.net
 * @created 2021-02-27 21:43
 */
public class JInputPanel extends JPanel {
	public JInputPanel(Controls controls) {
		JCheckBox inputModeSelector = new JCheckBox("Polyphonic");
		DiscreteControl inputModeControl = controls.getDiscreteControl("input-mode");
		inputModeControl.addListener(new DiscreteControlListener<>() {
			@Override
			public void valueChanged(String value, Object source) {
				if (source != inputModeSelector) {
					inputModeSelector.setSelected(InputMode.POLYPHONIC.name().equals(value));
				}
			}
		});
		inputModeSelector.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				inputModeControl.set(e.getStateChange() == ItemEvent.SELECTED ? InputMode.POLYPHONIC.name() : InputMode.MONOPHONIC.name(), inputModeSelector);
			}
		});

		JControlledComboBox scale = new JControlledComboBox(controls.getDiscreteControl("input-scale"), 100);
		JKnob scaleRootNote = new JKnob("Root note", controls.getControl("input-scale-root-note"), ValueFormatter.NOTE_TO_NAME);

		JKnob glideLengthKnob = new JKnob("Glide", controls.getControl("glide-length"), ValueFormatter.SECONDS);
		JCheckBox velocitySensitivity = new JCheckBox("Velocity");
		Bindings.bind(velocitySensitivity, controls.getBooleanControl("velocity-sensitive-keys"));

		JCheckBox legato = new JCheckBox("Legato");
		Bindings.bind(legato, controls.getBooleanControl("legato"));


		glideLengthKnob.setAlignmentX(LEFT_ALIGNMENT);
		velocitySensitivity.setAlignmentX(LEFT_ALIGNMENT);

		setLayout(new GridBagLayout());
		setBorder(new TitledBorder(null, "Input", TitledBorder.LEFT, TitledBorder.TOP));
		setAlignmentX(LEFT_ALIGNMENT);
		//@formatter:off
		add(glideLengthKnob,                  gbc(0, 0, 1, 3, 1, 2));
		add(inputModeSelector,                gbc(1, 0, 1, 1, 1, 2));
		add(velocitySensitivity,              gbc(1, 1, 1, 1, 1, 2));
		add(legato,                           gbc(1, 2, 1, 1, 1, 2));
		add(scaleRootNote,                    gbc(0, 3, 1, 1, 1, 2));
		add(scale,                            gbc(1, 3, 1, 1, 1, 2));

		//@formatter:on
	}
}
