/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.oscillators;

import net.jevring.frequencies.v2.math.Clamp;
import net.jevring.frequencies.v2.util.SignalEnergyAdder;
import net.jevring.frequencies.v2.waveforms.Waveform;

/**
 * Tracks the state for an oscillator.
 *
 * @author markus@jevring.net
 * @created 2020-02-20 23:04
 */
public class Oscillator {
	/**
	 * This is the highest possible midi CC number. At max value it should be turned completely off.
	 */
	public static final int QUANTIZATION_DISABLED = 16;
	private static final int PER_OSCILLATOR_UNISON_VOICES = 32;
	private static final double OCTAVE_MODULATION_RANGE = 4d;
	private static final double DETUNE_MODULATION_RANGE = 12d;
	private static final double QUANTIZATION_STEPS_MODULATION_RANGE = 16d;
	private static final double WAVEFORM_VARIATION_MODULATION_RANGE = 3d;
	private final PerOscillatorUnisonVoice[] voices = new PerOscillatorUnisonVoice[PER_OSCILLATOR_UNISON_VOICES];
	private final double sampleRate;
	private volatile Waveform waveform;

	private volatile double octaveOffset = 0;
	private volatile double semitoneOffset = 0;
	/**
	 * A value between -0.5 and +0.5
	 */
	private volatile double phaseShift = 0;
	/**
	 * A waveform-specific value between -1 and +1.
	 */
	private volatile double waveShape = 0;
	/**
	 * Into how many steps are quantizing the values of this waveform. Disabled if value is {@link #QUANTIZATION_DISABLED}
	 */
	private volatile int quantizationSteps = QUANTIZATION_DISABLED;

	private volatile double waveformVariation = 2;

	private volatile int perOscillatorUnisonVoices = 1;
	private volatile double perOscillatorUnisonDetuneSemiTones = 1d;
	/**
	 * This range is variable because it's really hard to come up with one size that fits all. It doesn't have a knob (yet?), but it can be changed.
	 */
	private volatile double frequencyModulationModulationRange = 2d;
	/**
	 * This range is variable because it's really hard to come up with one size that fits all. It doesn't have a knob (yet?), but it can be changed.
	 */
	private volatile double pitchBendModulationRange = 2d;

	public Oscillator(double sampleRate) {
		this.sampleRate = sampleRate;
		// GAAAAAAAAAAAAAAAH! For 2 hours I sat here with Arrays.fill(voices, new PerVoiceOscillator()); and wondered why it sounded like shit! =(
		// I'm a huge fucking idiot
		for (int i = 0; i < PER_OSCILLATOR_UNISON_VOICES; i++) {
			double phaseOffset = Math.random() * 0.1;
			if (i == 16) {
				// The middle oscillator should always have a 0 phase offset because it needs to align between
				// the primary and secondary oscillators. When we're doing unison this doesn't really matter, 
				// but when we just have two clean waveforms, it does.
				// Any small offset introduces an amount of buzzing
				phaseOffset = 0;
			}
			voices[i] = new PerOscillatorUnisonVoice(phaseOffset);
		}
	}

	public Waveform getWaveform() {
		return waveform;
	}

	public void setWaveform(Waveform waveform) {
		this.waveform = waveform;
		reset();
	}

	public void setOctaveOffset(double octaveOffset) {
		this.octaveOffset = octaveOffset;
	}

	public void setSemitoneOffset(double semitoneOffset) {
		this.semitoneOffset = semitoneOffset;
	}

	public void setPhaseShift(double phaseShift) {
		this.phaseShift = phaseShift;
	}

	public void setWaveShape(double waveShape) {
		this.waveShape = waveShape;
	}

	public void setQuantizationSteps(int quantizationSteps) {
		this.quantizationSteps = quantizationSteps;
	}

	public void setWaveformVariation(double waveformVariation) {
		this.waveformVariation = waveformVariation;
	}

	public void setPerOscillatorUnisonVoices(int perOscillatorUnisonVoices) {
		this.perOscillatorUnisonVoices = Math.min(perOscillatorUnisonVoices, PER_OSCILLATOR_UNISON_VOICES);
	}

	public void setPerOscillatorUnisonDetuneSemiTones(double perOscillatorUnisonDetuneSemiTones) {
		this.perOscillatorUnisonDetuneSemiTones = perOscillatorUnisonDetuneSemiTones;
	}

	public void setFrequencyModulationModulationRange(double frequencyModulationModulationRange) {
		this.frequencyModulationModulationRange = frequencyModulationModulationRange;
	}

	public void setPitchBendModulationRange(double pitchBendModulationRange) {
		this.pitchBendModulationRange = pitchBendModulationRange;
	}

	public double[] generateSamples(int samplesToGenerate,
	                                double frequency,
	                                double[] frequencyModulation,
	                                double[] waveShapeModulation,
	                                double[] pitchBendModulation,
	                                double[] phaseShiftModulation,
	                                double[] octaveOffsetModulation,
	                                double[] semitoneOffsetModulation,
	                                double[] quantizationStepModulation,
	                                double[] waveformVariationModulation) {
		// NOTE: We're NOT going to do oscillator sync. If we do, the oscillator will be the only component with multiple outputs, which, 
		// while solvable, generates too many headaches to be worth it now. Not to mention that the concept becomes really weird when 
		// we have per-oscillator unison

		// We explicitly want this for the integer division behavior, as otherwise we end up with too many voices
		@SuppressWarnings("IntegerDivisionInFloatingPointContext")
		double uv = perOscillatorUnisonVoices / 2;
		if (perOscillatorUnisonVoices % 2 == 0) {
			uv -= 0.5;
		}
		SignalEnergyAdder output = new SignalEnergyAdder(perOscillatorUnisonVoices, samplesToGenerate);
		for (double d = -uv; d <= uv; d++) {
			// Introduce stereo spread in unison. Figure out how to do that.=
			// To do that EVERYTHING has to support stereo processing, which might be more than we care to do at the moment.
			// We need to have a mix for the unison frequencies as well. Do they decrease in volume as we move out? or increase? or is that shape configurable?
			// ADAM SZABO - How to Emulate the Super Saw
			// https://pdfs.semanticscholar.org/1852/250068e864215dd7f12755cf00636868a251.pdf
			// We should have a dial for how evenly things are spread our. At 1 it should be completely even, and at 0 it should be very clustered in the middle, or something
			// This is unlikely to make any difference, as any number of voices above 3-5 sound almost exactly the same. 
			// There's no difference between 7 and 15 from what I can hear.
			// After much experimentation with high-pass filters, as specified by szabo, there isn't a significantly large difference to warrant
			// either converting our saws to the super saw waveform or messing with it in any significant way. Yes, there is a difference when you 
			// apply a key-tracking high-pass filter to the saw to emulate the shape found in the paper, and yes it sounds ever so slightly different, 
			// but not objectively better, so I'm just going to give it a rest. It's not worth doing given the added complexity.

			// I tried modulation for the unison spread, and it doesn't really sound that interesting, so I'm skipping it.

			int voiceIndex = (int) ((PER_OSCILLATOR_UNISON_VOICES / 2) + Math.round(d));
			PerOscillatorUnisonVoice voice = voices[voiceIndex];
			double unisonFrequency = frequency * Math.pow(2, (d * (perOscillatorUnisonDetuneSemiTones / (double) perOscillatorUnisonVoices)) / 12d);
			double[] perOscillatorVoiceSamples = generateSamples(voice,
			                                                     samplesToGenerate,
			                                                     unisonFrequency,
			                                                     frequencyModulation,
			                                                     waveShapeModulation,
			                                                     pitchBendModulation,
			                                                     phaseShiftModulation,
			                                                     octaveOffsetModulation,
			                                                     semitoneOffsetModulation,
			                                                     quantizationStepModulation,
			                                                     waveformVariationModulation);
			output.add(perOscillatorVoiceSamples);
		}
		return output.getOutput();
	}

	private double[] generateSamples(PerOscillatorUnisonVoice voice,
	                                 int samplesToGenerate,
	                                 double frequency,
	                                 double[] frequencyModulation,
	                                 double[] waveShapeModulation,
	                                 double[] pitchBendModulation,
	                                 double[] phaseShiftModulation,
	                                 double[] octaveOffsetModulation,
	                                 double[] semitoneOffsetModulation,
	                                 double[] quantizationStepModulation,
	                                 double[] waveformVariationModulation) {
		double[] samples = new double[samplesToGenerate];
		if (frequency == 0) {
			return samples;
		}
		for (int i = 0; i < samplesToGenerate; i++) {
			double octaveMultiplier = Math.pow(2, octaveOffset + (octaveOffsetModulation[i] * OCTAVE_MODULATION_RANGE));
			double semitoneMultiplier = Math.pow(2, (semitoneOffset + (semitoneOffsetModulation[i] * DETUNE_MODULATION_RANGE)) / 12d);
			double frequencyModulationMultiplier = Math.pow(2, (frequencyModulation[i] * frequencyModulationModulationRange) / 12d);
			double pitchBendMultiplier = Math.pow(2, (pitchBendModulation[i] * pitchBendModulationRange) / 12d);
			double frequencyMultiplier = octaveMultiplier * semitoneMultiplier * frequencyModulationMultiplier * pitchBendMultiplier;

			double samplesPerCycle = sampleRate / (frequency * frequencyMultiplier);
			// A value between 0 and 1. 
			// 0 at the start of the wave form (wave form is at y=0) 
			// 1 at the end of the wave form (the wave having gone through one whole period and arrived back at y=0).
			// This is the same for all wave forms, no matter if they are Sine or sawtooth or what
			double stepSizeInPercent = 1d / samplesPerCycle;

			voice.cycleProgress += stepSizeInPercent;
			if (voice.cycleProgress > 1d) {
				// We've gone past the end of the cycle. Reset the cycle
				voice.oddCycle = !voice.oddCycle;
				voice.cycleProgress = voice.cycleProgress % 1d;
			}


			int modulatedQuantizationSteps =
					(int) Clamp.clamp(quantizationSteps + (quantizationStepModulation[i] * QUANTIZATION_STEPS_MODULATION_RANGE), 1, QUANTIZATION_DISABLED);
			double modulatedWaveformVariation = Clamp.clamp(waveformVariation + (waveformVariationModulation[i] * WAVEFORM_VARIATION_MODULATION_RANGE),
			                                                0,
			                                                WAVEFORM_VARIATION_MODULATION_RANGE);

			double phaseShiftedCycleProgress = (voice.cycleProgress + voice.phaseOffset + phaseShift + phaseShiftModulation[i]) % 1d;
			double modulatedWaveShape = waveShape + waveShapeModulation[i];
			samples[i] =
					waveform.valueAt(phaseShiftedCycleProgress, modulatedWaveShape, voice.oddCycle, modulatedQuantizationSteps, modulatedWaveformVariation);
		}
		return samples;
	}

	public void reset() {
		for (PerOscillatorUnisonVoice voice : voices) {
			voice.cycleProgress = 0d;
			voice.oddCycle = false;
		}
	}

	private static final class PerOscillatorUnisonVoice {
		/**
		 * We assume that we'll only be modifying the cycle progress from a single thread.
		 */
		private double cycleProgress = 0d;
		/**
		 * We assume that we'll only be modifying the cycle alternation.
		 */
		private boolean oddCycle = false;

		/**
		 * This should be different per voice, but we don't really care what the value is.
		 * This value will change with each reboot of the synth. The value itself doesn't matter.
		 * It should just be different from the other voices.
		 * It should be in the range of -0.5 to +0.5
		 * Without this, the waveforms tend to sync up more. And while that can sound cool, it might not be what the user wants.
		 * <p>
		 * In the future we might use a control to determine if this is what we want. I.e. synced phase starts.
		 */
		private final double phaseOffset;

		private PerOscillatorUnisonVoice(double phaseOffset) {
			this.phaseOffset = phaseOffset;
		}
	}
}
