/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.oscillators;

import net.jevring.frequencies.v2.control.ControlListener;
import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.waveforms.QuantizedWaveform;
import net.jevring.frequencies.v2.waveforms.VariableWaveForm;

/**
 * A wrapper around an oscillator and the controls needed to control it.
 *
 * @author markus@jevring.net
 * @created 2020-05-05 19:30
 */
public class ControlledOscillator extends Oscillator {

	/**
	 * This expects the controls to be "pure" in the sense that they have exactly the same range
	 * as the expected values they'll be controlling. This saves us from interpolating. This isn't
	 * always the case, but it happens to be for these controls.
	 */
	public ControlledOscillator(String oscillatorControlPrefix, Controls controls, double sampleRate, boolean generatesSound) {
		super(sampleRate);

		// NOTE: Quantized in the outside and variable on the inside behaves better than the other way around.
		setWaveform(new QuantizedWaveform(new VariableWaveForm()));

		controls.getControl(oscillatorControlPrefix + "-wave-shape").addListener(new ControlListener() {
			@Override
			public void valueChanged(double min, double waveShape, double max, Object source) {
				setWaveShape(waveShape);
			}
		});
		controls.getControl(oscillatorControlPrefix + "-quantization-steps").addListener(new ControlListener() {
			@Override
			public void valueChanged(double min, double value, double max, Object source) {
				setQuantizationSteps((int) value);
			}
		});
		controls.getControl(oscillatorControlPrefix + "-variable-waveform").addListener(new ControlListener() {
			@Override
			public void valueChanged(double min, double value, double max, Object source) {
				setWaveformVariation(value);
			}
		});
		if (generatesSound) {
			controls.getControl(oscillatorControlPrefix + "-phase-shift").addListener(new ControlListener() {
				@Override
				public void valueChanged(double min, double phaseShift, double max, Object source) {
					setPhaseShift(phaseShift);
				}
			});
			controls.getControl(oscillatorControlPrefix + "-octave-offset").addListener(new ControlListener() {
				@Override
				public void valueChanged(double min, double octaveOffset, double max, Object source) {
					setOctaveOffset(octaveOffset);
				}
			});
			controls.getControl(oscillatorControlPrefix + "-detune-semi-tones").addListener(new ControlListener() {
				@Override
				public void valueChanged(double min, double detuneSemitones, double max, Object source) {
					setSemitoneOffset(detuneSemitones);
				}
			});
			controls.getControl(oscillatorControlPrefix + "-unison-detune-semi-tones").addListener(new ControlListener() {
				@Override
				public void valueChanged(double min, double detuneSemitones, double max, Object source) {
					setPerOscillatorUnisonDetuneSemiTones(detuneSemitones);
				}
			});
			controls.getControl(oscillatorControlPrefix + "-unison-voices").addListener(new ControlListener() {
				@Override
				public void valueChanged(double min, double newValue, double max, Object source) {
					setPerOscillatorUnisonVoices((int) newValue);
				}
			});
		}
	}
}
