/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.configuration;

import net.jevring.frequencies.v2.control.BooleanControl;
import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.control.DiscreteControl;
import net.jevring.frequencies.v2.modulation.matrix.ModulationMatrix;
import net.jevring.frequencies.v2.modulation.matrix.ModulationSource;
import net.jevring.frequencies.v2.modulation.matrix.ModulationTarget;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Randomizes all controls with the exception of input controls like velocity sensitivity.
 *
 * @author markus@jevring.net
 * @created 2021-02-06 15:06
 */
public class DefaultRandomizer implements Randomizer {
	private static final Set<String> controlsExemptFromRandomization =
			Set.of("balance",
			       "clip",
			       "overdrive-volume",
			       "velocity-sensitive-keys",
			       "glide-length",
			       "input-mode",
			       // when dry-wet mix ends up being set to like 0.1, we get almost no sound, which is confusing
			       "delay-dry-wet-mix",
			       "pitch-bend",
			       "volume-envelope-loop",
			       "filter-envelope-loop",
			       "legato",
			       "euclidean-sequencer-active",
			       "sequencer-active",
			       "arpeggiator-active",
			       "sequencer-listen",
			       "primary-oscillator-phase-shift",
			       "secondary-oscillator-phase-shift");
	private static final Set<ModulationTarget> modulationTargetsExemptFromRandomization =
			EnumSet.of(ModulationTarget.PRIMARY_OSCILLATOR_PHASE_SHIFT, ModulationTarget.SECONDARY_OSCILLATOR_PHASE_SHIFT);

	@Override
	public void randomizeSequencer(Controls controls) {
		for (Control control : controls.getControls().values()) {
			if (control.getKey().contains("sequencer-")) {
				control.set(0, Math.random(), 1, this);
			}
		}
		for (DiscreteControl discreteControl : controls.getDiscreteControls().values()) {
			if (discreteControl.getKey().contains("sequencer-")) {
				int item = (int) Math.round(Math.random() * (discreteControl.getAllowedValues().size() - 1));
				discreteControl.set(discreteControl.getAllowedValues().get(item), this);
			}
		}
		Random random = new Random();
		for (BooleanControl booleanControl : controls.getBooleanControls().values()) {
			if (controlsExemptFromRandomization.contains(booleanControl.getKey())) {
				continue;
			}
			if (booleanControl.getKey().contains("sequencer-")) {
				booleanControl.set(random.nextBoolean(), this);
			}
		}
	}

	@Override
	public void randomizeSynthesizer(Controls controls, ModulationMatrix modulationMatrix) {
		int modulationMatrixSlots = (int) (Math.random() * 10d);
		ModulationSource[] modulationSources = ModulationSource.values();
		List<ModulationTarget> modulationTargets =
				Arrays.stream(ModulationTarget.values()).filter(Predicate.not(modulationTargetsExemptFromRandomization::contains)).collect(Collectors.toList());
		for (int i = 0; i < modulationMatrixSlots; i++) {
			modulationMatrix.add(modulationSources[(int) (Math.random() * (modulationSources.length - 1))],
			                     modulationTargets.get((int) (Math.random() * (modulationTargets.size() - 1))));
		}
		for (Control control : controls.getControls().values()) {
			if (!control.getKey().contains("sequencer-") && !controlsExemptFromRandomization.contains(control.getKey())) {
				control.set(0, Math.random(), 1, this);
			}
		}
		for (DiscreteControl discreteControl : controls.getDiscreteControls().values()) {
			if (!discreteControl.getKey().contains("sequencer-") && !controlsExemptFromRandomization.contains(discreteControl.getKey())) {
				discreteControl.set(discreteControl.getAllowedValues().get((int) (Math.random() * (discreteControl.getAllowedValues().size() - 1))), this);
			}
		}
		Random random = new Random();
		for (BooleanControl booleanControl : controls.getBooleanControls().values()) {
			if (!booleanControl.getKey().contains("sequencer-") && !controlsExemptFromRandomization.contains(booleanControl.getKey())) {
				booleanControl.set(random.nextBoolean(), this);
			}
		}
	}
}
