/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.configuration;

import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.control.DiscreteControl;
import net.jevring.frequencies.v2.modulation.matrix.ModulationMatrix;
import net.jevring.frequencies.v2.modulation.matrix.ModulationSource;
import net.jevring.frequencies.v2.modulation.matrix.ModulationTarget;

import java.util.List;
import java.util.Random;

/**
 * Randomizes a limited set of controls in a way that is less likely to yield chaos and garbage compared to {@link DefaultRandomizer}.
 *
 * @author markus@jevring.net
 * @created 2021-02-06 17:11
 */
public class NiceRandomizer implements Randomizer {


	@Override
	public void randomizeSequencer(Controls controls) {
		Random random = new Random();
		controls.getControl("sequencer-steps").setRaw(16);
		for (Control control : controls.getControls().values()) {
			if (control.getKey().startsWith("sequencer-note-")) {
				control.set(0, 36 + random.nextInt(8), 127, this);
			} else if (control.getKey().startsWith("sequencer-gate-")) {
				control.set(0, 0.6, 0.9, this);
			}
		}
	}

	@Override
	public void randomizeSynthesizer(Controls controls, ModulationMatrix modulationMatrix) {
		Random random = new Random();
		DiscreteControl filter = controls.getDiscreteControl("filter");
		int randomFilter = random.nextInt(3);
		if (randomFilter < 1) {
			filter.set("LowPassJuceIIRFilter", this);
			controls.getControl("filter-cutoff-frequency").set(0, random.nextDouble(), 1, this);
		} else if (randomFilter < 2) {
			filter.set("HighPassJuceIIRFilter", this);
			controls.getControl("filter-cutoff-frequency").set(0, random.nextDouble() * 0.5, 1, this);
		} else {
			filter.set("BandPassRBJFilter", this);
			controls.getControl("filter-cutoff-frequency").set(0, random.nextDouble() * 0.5, 1, this);
		}
		controls.getControl("q-resonance-emphasis").set(0, 0.1 + random.nextDouble(), 1, this);

		controls.getControl("primary-oscillator-octave-offset").set(-4, random(-2, -1, 0, 1, 2), 4, this);
		controls.getControl("secondary-oscillator-octave-offset").set(-4, random(-2, -1, 0, 1, 2), 4, this);

		controls.getControl("primary-oscillator-detune-semi-tones").set(-12, random(-7, -5, -3, 0, 3, 5, 7), 12, this);
		controls.getControl("secondary-oscillator-detune-semi-tones").set(-12, random(-7, -5, -3, 0, 3, 5, 7), 12, this);

		controls.getControl("primary-oscillator-variable-waveform").set(0, random.nextDouble(), 1, this);
		controls.getControl("secondary-oscillator-variable-waveform").set(0, random.nextDouble(), 1, this);

		controls.getControl("primary-oscillator-quantization-steps").set(0, random.nextDouble(), 1, this);
		controls.getControl("secondary-oscillator-quantization-steps").set(0, random.nextDouble(), 1, this);

		controls.getControl("primary-oscillator-unison-voices").setRaw(random(1, 3, 5, 7));
		controls.getControl("primary-oscillator-unison-detune-semi-tones").setRaw(random(0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1));
		controls.getControl("secondary-oscillator-unison-voices").setRaw(random(1, 3, 5, 7));
		controls.getControl("secondary-oscillator-unison-detune-semi-tones").setRaw(random(0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1));

		controls.getControl("primary-oscillator-wave-shape").setRaw(random.nextDouble() * -1);
		controls.getControl("secondary-oscillator-wave-shape").setRaw(random.nextDouble() * -1);

		// max is 2 here so that the highest we can go is 50%
		controls.getControl("noise-oscillator-volume").set(0, random.nextDouble(), 2, this);
		// We want to limit the ADR times to about 1.5s at max. However since they're using an exponential curve, 
		// we can't just multiply with 0.15 to keep it down. We have to find where that limit actually is, and go with that
		double envelopeTimeRange = 0.4;
		controls.getControl("volume-envelope-attack").set(0, random.nextDouble() * envelopeTimeRange, 1, this);
		controls.getControl("volume-envelope-decay").set(0, random.nextDouble() * envelopeTimeRange, 1, this);
		controls.getControl("volume-envelope-sustain").set(0, 0.6, 1, this);
		controls.getControl("volume-envelope-release").set(0, random.nextDouble() * envelopeTimeRange, 1, this);
		randomize(controls.getDiscreteControl("volume-envelope-type"));

		controls.getControl("filter-envelope-attack").set(0, random.nextDouble() * envelopeTimeRange, 1, this);
		controls.getControl("filter-envelope-decay").set(0, random.nextDouble() * envelopeTimeRange, 1, this);
		controls.getControl("filter-envelope-sustain").set(0, 0.6, 1, this);
		controls.getControl("filter-envelope-release").set(0, random.nextDouble() * envelopeTimeRange, 1, this);
		randomize(controls.getDiscreteControl("filter-envelope-type"));

		controls.getControl("modulation-envelope-attack").set(0, random.nextDouble() * envelopeTimeRange, 1, this);
		controls.getControl("modulation-envelope-decay").set(0, random.nextDouble() * envelopeTimeRange, 1, this);
		controls.getControl("modulation-envelope-sustain").set(0, 0.6, 1, this);
		controls.getControl("modulation-envelope-release").set(0, random.nextDouble() * envelopeTimeRange, 1, this);
		randomize(controls.getDiscreteControl("modulation-envelope-type"));


		int modulationMatrixSlots = (int) (Math.random() * 10d);
		List<ModulationSource> modulationSources = List.of(ModulationSource.LFO1,
		                                                   ModulationSource.LFO2,
		                                                   ModulationSource.LFO3,
		                                                   ModulationSource.SAMPLE_AND_HOLD,
		                                                   ModulationSource.AMPLIFIER_ENVELOPE,
		                                                   ModulationSource.FILTER_ENVELOPE,
		                                                   ModulationSource.MODULATION_ENVELOPE);
		List<ModulationTarget> modulationTargets = List.of(ModulationTarget.PRIMARY_OSCILLATOR_OCTAVE,
		                                                   ModulationTarget.PRIMARY_OSCILLATOR_DETUNE,
		                                                   ModulationTarget.PRIMARY_OSCILLATOR_WAVE_SHAPE,
		                                                   ModulationTarget.PRIMARY_OSCILLATOR_WAVEFORM_VARIATION,
		                                                   ModulationTarget.PRIMARY_OSCILLATOR_QUANTIZATION_STEPS,
		                                                   ModulationTarget.SECONDARY_OSCILLATOR_OCTAVE,
		                                                   ModulationTarget.SECONDARY_OSCILLATOR_DETUNE,
		                                                   ModulationTarget.SECONDARY_OSCILLATOR_WAVE_SHAPE,
		                                                   ModulationTarget.SECONDARY_OSCILLATOR_WAVEFORM_VARIATION,
		                                                   ModulationTarget.SECONDARY_OSCILLATOR_QUANTIZATION_STEPS,
		                                                   ModulationTarget.FILTER_CUTOFF_FREQUENCY,
		                                                   ModulationTarget.FILTER_RESONANCE);

		for (int i = 0; i < modulationMatrixSlots; i++) {
			ModulationSource modulationSource = modulationSources.get((int) (Math.random() * (modulationSources.size() - 1)));


			if (modulationSource == ModulationSource.SAMPLE_AND_HOLD) {
				controls.getControl("sample-and-hold-frequency").set(0, random.nextDouble(), 1, this);
				controls.getControl("sample-and-hold-slew").set(0, random.nextDouble(), 1, this);
			} else if (modulationSource == ModulationSource.LFO1) {
				controls.getControl("lfo1-variable-waveform").set(0, random.nextDouble(), 1, this);
				controls.getControl("lfo1-frequency").set(0, random.nextDouble(), 1, this);
			} else if (modulationSource == ModulationSource.LFO2) {
				controls.getControl("lfo2-variable-waveform").set(0, random.nextDouble(), 1, this);
				controls.getControl("lfo2-frequency").set(0, random.nextDouble(), 1, this);
			} else if (modulationSource == ModulationSource.LFO3) {
				controls.getControl("lfo3-variable-waveform").set(0, random.nextDouble(), 1, this);
				controls.getControl("lfo3-frequency").set(0, random.nextDouble(), 1, this);
			}

			Control depth = modulationMatrix.add(modulationSource, modulationTargets.get((int) (Math.random() * (modulationTargets.size() - 1))));
			depth.set(0, random.nextDouble(), 1, this);
		}
	}

	private int random(int... values) {
		return values[(int) (Math.random() * values.length)];
	}

	private double random(double... values) {
		return values[(int) (Math.random() * values.length)];
	}

	private void randomize(DiscreteControl discreteControl) {
		discreteControl.set(discreteControl.getAllowedValues().get((int) (Math.random() * (discreteControl.getAllowedValues().size() - 1))), this);
	}
}
