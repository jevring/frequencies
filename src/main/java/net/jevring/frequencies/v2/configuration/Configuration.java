/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.configuration;

import net.jevring.frequencies.v2.control.BooleanControl;
import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.control.DiscreteControl;
import net.jevring.frequencies.v2.modulation.matrix.ModulationMatrix;
import net.jevring.frequencies.v2.modulation.matrix.ModulationMatrixRow;
import net.jevring.frequencies.v2.modulation.matrix.ModulationSource;
import net.jevring.frequencies.v2.modulation.matrix.ModulationTarget;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Responsible for loading and saving all configuration.
 *
 * @author markus@jevring.net
 * @created 2020-04-11 17:06
 */
public class Configuration {
	private final Randomizer randomizer = new NiceRandomizer();


	private final File configurationsDirectory;

	private final ModulationMatrix modulationMatrix;
	private final Controls controls;

	public Configuration(ModulationMatrix modulationMatrix, Controls controls, File dotConfigDirectory) {
		this.configurationsDirectory = new File(dotConfigDirectory, "configurations");
		this.modulationMatrix = modulationMatrix;
		this.controls = controls;
	}

	public File ensureConfigurationDirectoryExists() {
		if (!configurationsDirectory.exists()) {
			if (!configurationsDirectory.mkdirs()) {
				System.err.println("Couldn't create dir " + configurationsDirectory.getAbsolutePath());
			}
		}
		return configurationsDirectory;
	}

	public void save(File output) {
		try (ConfigurationWriter writer = new ConfigurationWriter("3", output)) {
			// NOTE: The modulation matrix HAS to come first, as otherwise there will be no controls created that we can restore
			for (ModulationMatrixRow modulationMatrixRow : modulationMatrix.getModulationConfig(false)) {
				writer.add("modulation-matrix", modulationMatrixRow.source().name() + "," + modulationMatrixRow.target().name());
			}
			for (Map.Entry<String, DiscreteControl> entry : new TreeMap<>(controls.getDiscreteControls()).entrySet()) {
				DiscreteControl control = entry.getValue();
				String value = control.get();
				writer.add("discrete-controls", String.format(Locale.US, "%s,%s", entry.getKey(), value));
			}
			for (Map.Entry<String, BooleanControl> entry : new TreeMap<>(controls.getBooleanControls()).entrySet()) {
				BooleanControl control = entry.getValue();
				boolean value = control.get();
				writer.add("discrete-controls", String.format(Locale.US, "%s,%b", entry.getKey(), value));
			}
			for (Map.Entry<String, Control> entry : new TreeMap<>(controls.getControls()).entrySet()) {
				Control control = entry.getValue();
				writer.add("controls", String.format(Locale.US, "%s,%f", entry.getKey(), control.getCurrentValue()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void reset() {
		resetSynthesizer();
		resetSequencer();
	}

	public void resetSequencer() {
		controls.resetSequencer(this);
	}

	public void resetSynthesizer() {
		// Start with a clean slate
		modulationMatrix.clear();
		controls.resetSynthesizer(this);
	}

	public void load(InputStream inputStream) {
		reset();

		justLoad(inputStream);
	}

	private void justLoad(InputStream inputStream) {

		try (InputStream is = inputStream) {
			ConfigurationReader.read(is, new StoredConfigurationCallback() {
				private final Map<String, Object> values = new HashMap<>();
				private final Map<String, List<String>> modulationMatrixValues = new HashMap<>();

				@Override
				public void handle(String version, String section, String line) {
					try {
						String[] parts = line.split(",");
						String key = compatibilityMapper(parts[0]);
						// these are actual config lines
						if ("controls".equals(section)) {
							double value;
							if (parts.length == 5) {
								value = Double.parseDouble(parts[3]);
							} else if (parts.length == 2) {
								value = Double.parseDouble(parts[1]);
							} else {
								System.out.println("Skipping unknown format: " + line);
								return;
							}
							values.put(key, value);
						} else if ("discrete-controls".equals(section)) {
							String value = compatibilityMapper(parts[1]);
							if (!"3".equals(version) && key.endsWith("-waveform")) {
								String oscillator = key.substring(0, key.length() - 9);
								//noinspection EnhancedSwitchMigration
								switch (value) {
									case "3StepUp": {
										values.put(oscillator + "-quantization-steps", 1);
										values.put(oscillator + "-variable-waveform", 2);
										break;
									}
									case "4StepUp": {
										values.put(oscillator + "-quantization-steps", 2);
										values.put(oscillator + "-variable-waveform", 2);
										break;
									}
									case "8StepUp": {
										values.put(oscillator + "-quantization-steps", 4);
										values.put(oscillator + "-variable-waveform", 2);
										break;
									}
									case "3Pyramid": {
										values.put(oscillator + "-quantization-steps", 1);
										values.put(oscillator + "-variable-waveform", 1);
										break;
									}
									case "5Pyramid": {
										values.put(oscillator + "-quantization-steps", 3);
										values.put(oscillator + "-variable-waveform", 1);
										break;
									}
									case "9Pyramid": {
										values.put(oscillator + "-quantization-steps", 5);
										values.put(oscillator + "-variable-waveform", 1);
										break;
									}
									case "Saw": {
										values.put(oscillator + "-quantization-steps", 16);
										values.put(oscillator + "-variable-waveform", 2);
										break;
									}
									case "Square": {
										values.put(oscillator + "-quantization-steps", 16);
										values.put(oscillator + "-variable-waveform", 3);
										break;
									}
									case "Triangle": {
										values.put(oscillator + "-quantization-steps", 16);
										values.put(oscillator + "-variable-waveform", 1);
										break;
									}
									case "Sine": {
										values.put(oscillator + "-quantization-steps", 16);
										values.put(oscillator + "-variable-waveform", 0);
										break;
									}
								}
								// We're done processing this
								return;
							}
							values.put(key, value);
						} else if ("modulation-matrix".equals(section)) {
							modulationMatrixValues.computeIfAbsent(parts[0], ignored -> new ArrayList<>()).add(parts[1]);
						} else {
							System.err.printf("UNKNOWN: version: %s, section: %s, setting: %s%n", version, section, line);
						}
					} catch (IllegalArgumentException e) {
						// A single invalid line shouldn't break the rest
						e.printStackTrace();
					}
				}

				@Override
				public void loadComplete() {
					// Rectify some preset values 
					if ((double) values.getOrDefault("delay-in-milliseconds", 0d) == 0d) {
						// no delay, override old defaults with no defaults
						values.put("delay-decay", 0.25);
						values.put("delay-dry-wet-mix", 1);
					}
					
					
					/*
					The order needs to be:
					1. Modulation matrix (to prepare all the related controls)
					2. Discreet controls (to set up filters and envelopes so that they can receive settings)
					3. Controls (normal settings and modifications to the modulation matrix and the discreet controls)
					4. Boolean controls (activating all other settings)
					 */

					for (ModulationSource modulationSource : ModulationSource.values()) {
						for (ModulationTarget modulationTarget : ModulationTarget.values()) {
							List<String> modulationTargets = modulationMatrixValues.get(modulationSource.name());
							if (modulationTargets != null && modulationTargets.contains(modulationTarget.name())) {
								modulationMatrix.add(modulationSource, modulationTarget);
							}
						}
					}

					for (Map.Entry<String, DiscreteControl> entry : controls.getDiscreteControls().entrySet()) {
						String key = entry.getKey();
						DiscreteControl control = entry.getValue();
						Object value = values.get(key);
						if (value instanceof String s) {
							// instanceof implies not null
							try {
								control.set(s, this);
							} catch (RuntimeException e) {
								// A single invalid line shouldn't break the rest
								e.printStackTrace();
							}
						}
					}

					for (Map.Entry<String, Control> entry : controls.getControls().entrySet()) {
						String key = entry.getKey();
						Control control = entry.getValue();
						Object value = values.get(key);
						if (value instanceof Double d) {
							// instanceof implies not null
							control.setRaw(d);
						}
					}

					for (Map.Entry<String, BooleanControl> entry : controls.getBooleanControls().entrySet()) {
						String key = entry.getKey();
						if (key.endsWith("active") || key.endsWith("listen")) {
							// we never want to enter an active mode from just a load.
							continue;
						}
						BooleanControl control = entry.getValue();
						Object value = values.get(key);
						if (value instanceof String s) {
							// instanceof implies not null
							try {
								control.set(Boolean.parseBoolean(s), this);
							} catch (RuntimeException e) {
								// A single invalid line shouldn't break the rest
								e.printStackTrace();
							}
						}
					}

				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void randomize() {
		resetSynthesizer();
		resetSequencer();
		randomizer.randomize(controls, modulationMatrix);
	}

	public void randomizeSequencer() {
		resetSequencer();
		randomizer.randomizeSequencer(controls);
	}

	public void randomizeSynthesizer() {
		resetSynthesizer();
		randomizer.randomizeSynthesizer(controls, modulationMatrix);
	}

	/**
	 * Maps old values from saves files that are not available anymore, to new ones that are.
	 */
	private String compatibilityMapper(String input) {
		return switch (input) {
			case "Krajeski4PoleLadderFilter" -> "Stilson4PoleLadderFilter";
			case "Open303Sawtooth", "Open303MoogSawtooth", "Open303MoogSaw", "SawtoothUp", "SawtoothDown", "SawUp", "SawDown" -> "Saw";
			case "Open303Square", "Pulse" -> "Square";
			case "Sinus" -> "Sine";
			case "filter-envelope-to-filter-cutoff-frequency-depth" -> "filter-envelope-depth";
			default -> input;
		};
	}
}
