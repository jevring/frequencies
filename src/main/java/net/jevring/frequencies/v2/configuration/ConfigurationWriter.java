/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.configuration;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Writes configuration file. Is not re-usable. Must be created anew for each configuration file. Best used inside a try-with-resources block.
 *
 * @author markus@jevring.net
 * @created 2020-06-21 15:59
 */
public class ConfigurationWriter implements AutoCloseable {
	/**
	 * This is a linked has map, because order is important.
	 */
	private final Map<String, List<String>> data = new LinkedHashMap<>();
	private boolean closed = false;
	private final String version;
	private final File file;


	public ConfigurationWriter(String version, File file) {
		this.version = version;
		this.file = file;
	}

	public void add(String section, String setting) {
		data.computeIfAbsent(section, ignored -> new ArrayList<>()).add(setting);
	}

	@Override
	public void close() throws Exception {
		if (closed) {
			throw new IllegalStateException("Already closed");
		}
		this.closed = true;

		try (PrintWriter out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file)))) {
			out.println("# Created at " + ZonedDateTime.now());
			out.println("# Created by <Your name and email and blog and soundcloud etc here>");
			out.println("[version]");
			out.println(version);

			out.println();


			for (Map.Entry<String, List<String>> entry : data.entrySet()) {
				String section = entry.getKey();
				List<String> settings = entry.getValue();
				out.print("[");
				out.print(section);
				out.print("]");
				out.println();
				settings.stream().sorted().forEach(out::println);
				out.println();
			}
			out.flush();
		}
	}
}
