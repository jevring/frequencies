/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.configuration;

import java.io.*;

/**
 * Reads a stored configuration from an input stream.
 *
 * @author markus@jevring.net
 * @created 2020-06-21 15:44
 */
public class ConfigurationReader {
	/**
	 * Reads configuration from the stream. This does <b>not</b> close the stream. The caller must do this.
	 */
	public static void read(InputStream inputStream, StoredConfigurationCallback callback) throws IOException {
		String section = null;
		String version = null;
		BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
		String line;
		while ((line = in.readLine()) != null) {
			if (line.isBlank() || line.startsWith("#")) {
				continue;
			} else if (line.startsWith("[") && line.endsWith("]")) {
				section = line.substring(1, line.length() - 1);
			} else if ("version".equals(section)) {
				if (version != null) {
					throw new IllegalArgumentException("Cannot have version specified more than once");
				}
				version = line;
			} else {
				callback.handle(version, section, line);
			}
		}
		callback.loadComplete();
	}

	public static void read(File file, StoredConfigurationCallback callback) throws IOException {
		try (FileInputStream fis = new FileInputStream(file)) {
			read(fis, callback);
		}
	}
}
