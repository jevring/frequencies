/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.math;

/**
 * Linear interpolation
 *
 * @author markus@jevring.net
 * @created 2020-02-08 17:24
 */
public class Interpolation {
	public static double linear(double sourceStart, double sourceEnd, double sourceValue, double targetStart, double targetEnd) {
		double sourceRange = sourceEnd - sourceStart;
		double valueInSourceRange = sourceValue - sourceStart;
		double proportion = valueInSourceRange / sourceRange;
		double targetRange = targetEnd - targetStart;
		double targetChange = proportion * targetRange;
		return targetStart + targetChange;
	}

	public static double exponential(double sourceStart, double sourceEnd, double sourceValue, double targetStart, double targetEnd) {
		// todo: look at matching this closer to the Moog dials that have 0-10% of the range in the first 75% of the knob range. For example in the grandmother filter cutoff.
		double sourceRange = sourceEnd - sourceStart;
		double valueInSourceRange = sourceValue - sourceStart;
		double proportion = valueInSourceRange / sourceRange;
		double targetRange = targetEnd - targetStart;
		double targetChange = targetRange * Math.pow(proportion, 2);
		return targetStart + targetChange;
	}

	public static double logarithmic(double sourceStart, double sourceEnd, double sourceValue, double targetStart, double targetEnd) {
		// 0.01 .. 10 is the input value we want to the log10 output range we want, which is -2 .. 1
		double in = linear(sourceStart, sourceEnd, sourceValue, 0.01, 10);
		double logIn = Math.log10(in);
		// the input range is now the log output range. Interpolate that to the target output
		return linear(-2d, 1, logIn, targetStart, targetEnd);
	}
}
