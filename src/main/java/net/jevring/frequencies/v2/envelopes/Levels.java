/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.envelopes;

/**
 * The output levels of an envelope. Is also used to cycle in the previous state.
 *
 * @author markus@jevring.net
 * @created 2020-03-11 22:15
 */
final class Levels {
	private final double originalReleaseStartingPoint;
	private final int samplesProcessed;
	private final double lastSample;

	@Deprecated
	Levels(double originalReleaseStartingPoint, int samplesProcessed, double[] output) {
		this(originalReleaseStartingPoint, samplesProcessed, output[output.length - 1]);
	}

	Levels(double originalReleaseStartingPoint, int samplesProcessed, double lastSample) {
		this.originalReleaseStartingPoint = originalReleaseStartingPoint;
		this.samplesProcessed = samplesProcessed;
		this.lastSample = lastSample;
	}

	double getOriginalReleaseStartingPoint() {
		return originalReleaseStartingPoint;
	}

	int getSamplesProcessed() {
		return samplesProcessed;
	}

	double lastSample() {
		return lastSample;
	}
}
