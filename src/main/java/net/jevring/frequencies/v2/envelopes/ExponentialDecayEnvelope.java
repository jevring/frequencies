/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.envelopes;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Decay-only envelope with an exponential curve.
 *
 * @author markus@jevring.net
 * @created 2020-02-29 19:05
 */
public class ExponentialDecayEnvelope implements DecayEnvelope, LoopingEnvelope, Envelope {
	private static final double ZEROISH = 0.01;
	private static final double LN_OF_ZEROISH = Math.log(ZEROISH);
	private volatile long decayInNanos = 200;
	private volatile boolean loop = false;
	private Levels previous;

	public long getDecayInMillis() {
		return TimeUnit.NANOSECONDS.toMillis(decayInNanos);
	}

	public void setDecayInMillis(long decay) {
		this.decayInNanos = TimeUnit.MILLISECONDS.toNanos(decay);
	}

	public boolean isLoop() {
		return loop;
	}

	public void setLoop(boolean loop) {
		this.loop = loop;
	}

	@Override
	public double[] levels(long nanosecondsActivated, long nanosecondsDeactivated, int samplesToGenerate, double sampleRate) {
		// These things are all static, and only depend on the decay time, not the invocation
		double nanosPerSecond = TimeUnit.SECONDS.toNanos(1);
		double decayInSeconds = decayInNanos / nanosPerSecond;
		int samplesInDecay = (int) (decayInSeconds * sampleRate);
		double decayFactor = Math.exp(LN_OF_ZEROISH / samplesInDecay);

		// These things are dynamic and depend on the invocation
		int samplesProcessed;
		if (previous != null) {
			samplesProcessed = previous.getSamplesProcessed();
		} else {
			samplesProcessed = 0;
		}

		double[] output = new double[samplesToGenerate];
		if (previous != null) {
			// This is not strictly speaking true. We should start with the last value at output[-1] ideally, but this is a reasonable
			// approximation, and we're only "wasting" one sample to do this.
			output[0] = previous.lastSample();
		} else {
			output[0] = 1d;
		}

		int totalSamplesProcessed = samplesProcessed;
		for (int i = 1; i < samplesToGenerate; i++) {
			if (loop && totalSamplesProcessed >= samplesInDecay) {
				// We've reached the end of the envelope, and we want to repeat it.
				// Reset the envelope progress and generate remaining samples again from the start
				totalSamplesProcessed = 0;
				output[i] = 1d;
			} else {
				totalSamplesProcessed++;
				output[i] = output[i - 1] * decayFactor;
			}
		}
		previous = new Levels(Double.NaN, totalSamplesProcessed, output);
		return output;
	}

	public static void main(String[] args) {
		ExponentialDecayEnvelope envelope = new ExponentialDecayEnvelope();
		envelope.setDecayInMillis(250);
		envelope.setLoop(true);
		long oneSecondInNanos = TimeUnit.SECONDS.toNanos(1);

		int chunkSize = 441;
		double sampleRate = 44100d;
		long chunkInNanos = (long) ((1d / 100d) * oneSecondInNanos);
		int chunk = 0;
		long nanosecondsActivated = 0;
		long nanosecondsDeactivated = 0;
		for (int i = 0; i <= 44100; i += chunkSize) {
			if (chunk < 66) {
				nanosecondsActivated += chunkInNanos;
			} else {
				nanosecondsDeactivated += chunkInNanos;
			}
			double[] envelopeValues = envelope.levels(nanosecondsActivated, nanosecondsDeactivated, chunkSize, sampleRate);

			for (double d : envelopeValues) {
				System.out.printf(Locale.US, "%f%n", d);
			}
			chunk++;
		}
	}


	@Override
	public Phase phase(long nanosecondsActivated, long nanosecondsDeactivated) {
		if (nanosecondsActivated + nanosecondsDeactivated < decayInNanos || (loop && nanosecondsDeactivated == 0)) {
			return Phase.DECAY;
		} else {
			return Phase.IDLE;
		}
	}

	@Override
	public void reset() {
		previous = null;
	}

	@Override
	public String toString() {
		return "ExponentialDecay";
	}
}
