/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.envelopes;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * This class represents an Attack-Decay-Sustain-Release envelope. All value are measured in milliseconds.
 *
 * @author markus@jevring.net
 * @created 2019-12-08 20:17
 */
public class LinearADSREnvelope implements AttackEnvelope, DecayEnvelope, SustainEnvelope, ReleaseEnvelope, ResetToZeroEnvelope, Envelope {
	/*
	ADR are measured in _time_, and S is measured in _level_: https://www.soundonsound.com/techniques/modifiers-controllers
	This means that A and D happen when you hit a key, and S is the level at which it's sustained while the key is held, and the R is how it fades out.
	 */
	private long attack = nanos(250);
	private long decay = nanos(250);
	private double sustain = 0.5;
	private long release = nanos(250);
	
	private boolean resetToZero = false;

	private EnvelopeState previous = new EnvelopeState(1, 1 - sustain, sustain, 0, 0);

	private static long nanos(long timeInMillis) {
		return TimeUnit.MILLISECONDS.toNanos(timeInMillis);
	}

	private static long millis(long timeInNanos) {
		return TimeUnit.NANOSECONDS.toMillis(timeInNanos);
	}

	public long getAttackInMillis() {
		return millis(attack);
	}

	public void setAttackInMillis(long attack) {
		this.attack = nanos(attack);
	}

	public long getDecayInMillis() {
		return millis(decay);
	}

	public void setDecayInMillis(long decay) {
		this.decay = nanos(decay);
	}

	public double getSustainLevel() {
		return sustain;
	}

	public void setSustainLevel(double sustain) {
		this.sustain = sustain;
	}

	public long getReleaseInMillis() {
		return millis(release);
	}

	public void setReleaseInMillis(long release) {
		this.release = nanos(release);
	}

	public boolean isResetToZero() {
		return resetToZero;
	}

	public void setResetToZero(boolean resetToZero) {
		this.resetToZero = resetToZero;
	}

	public Phase phase(long nanosecondsActivated, long nanosecondsDeactivated) {
		if (nanosecondsDeactivated > release) {
			return Phase.IDLE;
		} else if (nanosecondsDeactivated > 0) {
			return Phase.RELEASE;
		} else if (nanosecondsActivated < attack) {
			return Phase.ATTACK;
		} else if (nanosecondsActivated < attack + decay) {
			return Phase.DECAY;
		} else {
			// key is being held after both attack and decay, so we're in sustain
			return Phase.SUSTAIN;
		}
	}
	
	/*
	Judging from this discussion, having envelopes "reset to 0" is either bad, or completely expected.
	https://gearspace.com/board/electronic-music-instruments-and-electronic-music-production/1249300-minilogue-xd-egs-still-reset-0-a.html
	It also produces two completely different sounds. 
	Initially I wanted to avoid the "click" that comes with resetting to 0 on occasion by fading the envelope from wherever it was down to 0
	over, say, 1ms, i.e. ~44 samples, before starting it back up again. I got this idea from this video:
	https://www.youtube.com/watch?v=A_AWH3SgM84&list=PLF4FDP9_Sf6KV56xWfyeFCW1IzgIvJP8Y&index=36
	
	However, it seems that these two things actually present us with a better option: just make "reset to 0" optional.
	We have legato as an option, so we may as well have "reset to 0" as an option.
	Legato and skipping "reset to 0" are NOT the same, despite on the surface being very similar. 
	Legato means that the envelope is NOT retriggered.
	Skipping "reset to 0" means that the envelope IS retriggered, but the *starting point* is wherever the envelope was before. 
	
	todo: should this be per-envelope? Would we want the amp envelope to behave like this, but not the filter envelope?
	 */

	@Override
	public double[] levels(long nanosecondsActivated, long nanosecondsDeactivated, int samplesToGenerate, double sampleRate) {

		double releaseRange = previous.releaseRange();
		double attackRange = previous.attackRange();
		double decayRange = previous.decayRange();

		double nanosPerSecond = TimeUnit.SECONDS.toNanos(1);
		int samplesProcessed = previous.samplesProcessed();

		double[] output = new double[samplesToGenerate];
		if (nanosecondsDeactivated > 0) {
			// We only deal in release at this point

			// handle the cases where the key was released "early", before the sustain had been reached
			double startingPointForRelease = previous.lastSample();
			if (Double.isNaN(previous.releaseRange())) {
				// This is the first time we've seen the key released. 
				// Or sustain is set to 0, in which case the outcome is no sound anyway, since we go from 0 to 0

				// If we don't track the ORIGINAL starting point, the starting point gets smaller and smaller with every 
				// invocation, which means that the diff per step gets smaller and smaller, 
				// and we have a Xeno's paradox thing where we have an exponential fall-off and never reach 0
				releaseRange = startingPointForRelease;
			} else {
				// We've seen the key released before, so we're doing to just the previous value to get started
				releaseRange = previous.releaseRange();
			}
			double releaseInSeconds = release / nanosPerSecond;
			double samplesInRelease = releaseInSeconds * sampleRate;
			// Release goes from $sustain (or wherever the user released) to 0 in $samplesInRelease steps. 
			// Note that this is a positive number, so it will have to be subtracted when used.
			double releaseTick = releaseRange / samplesInRelease;

			// This is not strictly speaking true. We should start with the last value at output[-1] ideally, but this is a reasonable
			// approximation, and we're only "wasting" one sample to do this.
			output[0] = startingPointForRelease;

			for (int i = 1; i < samplesToGenerate; i++) {
				double v = output[i - 1] - releaseTick;
				if (v <= 0) {
					break;
				}
				output[i] = v;
			}
		} else {
			// We are somewhere BEFORE the release, so we don't have to deal with release at all. We only go as far as sustain

			// The reason we transition from nanoseconds to samples here is that we want to be able to change the state of the envelope
			// in the middle of a chunk. If we just relied on nanoseconds here, each chunk would be guaranteed to be in the same phase.
			// This doesn't matter when the phases are long, but when we have, say, a 5ms attack, then that would be extended to ~10-15ms
			// because that's approximately how big the chunks are

			// Starting at output[0] is not, strictly speaking, true. We should start with the last value at output[-1] ideally, but this is a reasonable
			// approximation, and we're only "wasting" one sample to do this.
			if (samplesProcessed == 0) {
				// This is the very start of the envelope. Choose the correct starting point, 
				// making sure we shortcut to the correct positions if their settings are 0, 
				// since we'd otherwise have 0 samples in which to reach that point
				if (resetToZero) {
					if (attack == 0) {
						if (decay == 0) {
							output[0] = sustain;
						} else {
							output[0] = 1d;
						}
					} else {
						output[0] = 0d;
					}
				} else {
					if (attack > 0) {
						// We're starting in attack. Start rising from wherever we were, up to 1, meaning we have 1-last to go.
						// If we're starting in attack, decay will happen normally.
						// For example if we were at 0.2, then the attack range will be 1-0.2=0.8, meaning that we'll need to go 0.8 up to reach 1, 
						// which is always the final target for the attack
						attackRange = 1d - previous.lastSample();
						output[0] = previous.lastSample();
					} else if (decayRange > 0) {
						// If we have ANY decay we start at 1. Always. That's always where the decay starts from. 
						// If we have 0 attack, this should be an abrupot jump
						decayRange = 1d - sustain;
						output[0] = 1d;
					} else {
						// Both attack and decay are 0, start at sustain
						output[0] = sustain;
					}
				}
			} else {
				output[0] = previous.lastSample();
			}

			double attackInSeconds = attack / nanosPerSecond;
			double samplesInAttack = attackInSeconds * sampleRate;
			// since attack goes from 0 to 1 (i.e. full range is 1) in $samplesInAttack steps
			double attackTick = attackRange / samplesInAttack;

			double decayInSeconds = decay / nanosPerSecond;
			double samplesInDecay = decayInSeconds * sampleRate;
			// since decay goes from 1 to $sustain in $samplesInDecay steps. Note that this is a positive number, so it will have to be subtracted when used.
			double decayTick = decayRange / samplesInDecay;


			for (int i = 1; i < samplesToGenerate; i++) {
				int totalSamplesProcessed = samplesProcessed + i;
				if (totalSamplesProcessed <= samplesInAttack) {
					output[i] = output[i - 1] + attackTick;
				} else if (totalSamplesProcessed <= (samplesInAttack + samplesInDecay)) {
					output[i] = output[i - 1] - decayTick;
				} else {
					output[i] = sustain;
				}
			}
		}
		previous = new EnvelopeState(attackRange, decayRange, releaseRange, samplesProcessed + samplesToGenerate, output[output.length - 1]);
		return output;
	}

	@Override
	public void reset() {
		if (resetToZero) {
			this.previous = new EnvelopeState(1, 1 - sustain, Double.NaN, 0, 0);
		} else {
			this.previous = new EnvelopeState(1, 1 - sustain, Double.NaN, 0, this.previous.lastSample());
		}
	}

	public static void main(String[] args) {
		LinearADSREnvelope envelope = new LinearADSREnvelope();
		long oneSecondInNanos = TimeUnit.SECONDS.toNanos(1);

		int chunkSize = 441;
		double sampleRate = 44100d;
		long chunkInNanos = (long) ((1d / 100d) * oneSecondInNanos);
		int chunk = 0;
		long nanosecondsActivated = 0;
		long nanosecondsDeactivated = 0;
		boolean reset = false;
		for (int i = 0; i <= 44100; i += chunkSize) {
			if (chunk == 80) {
				envelope.setResetToZero(true);
				envelope.reset();
				nanosecondsActivated = 0;
				nanosecondsDeactivated = 0;
				reset = true;
			} else if (reset && chunk > 80) {
				nanosecondsActivated += chunkInNanos;
			}
			if (!reset) {
				if (chunk < 66) {
					nanosecondsActivated += chunkInNanos;
				} else {
					nanosecondsDeactivated += chunkInNanos;
				}
			}
			double[] envelopeValues = envelope.levels(nanosecondsActivated, nanosecondsDeactivated, chunkSize, sampleRate);

			for (double d : envelopeValues) {
				System.out.printf(Locale.US, "%f%n", d);
			}
			chunk++;
		}
	}

	@Override
	public String toString() {
		return "LinearADSR";
	}
}
