/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.envelopes;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Attack-decay envelope with linear attack and exponential decay. No sustain. Can loop.
 *
 * @author markus@jevring.net
 * @created 2020-06-25 19:12
 */
public class AttackDecayEnvelope implements AttackEnvelope, DecayEnvelope, LoopingEnvelope, ResetToZeroEnvelope, Envelope {
	private static final double ZEROISH = 0.01;
	private static final double LN_OF_ZEROISH = Math.log(ZEROISH);
	private volatile long decayInNanos = 200;
	private volatile long attackInNanos = 0;
	private volatile boolean loop = false;

	private EnvelopeState previous = new EnvelopeState(1, 1, Double.NaN, 0, 0);

	private boolean resetToZero = false;

	public long getAttackInMillis() {
		return TimeUnit.NANOSECONDS.toMillis(attackInNanos);
	}

	public void setAttackInMillis(long attackInMillis) {
		this.attackInNanos = TimeUnit.MILLISECONDS.toNanos(attackInMillis);
	}

	public long getDecayInMillis() {
		return TimeUnit.NANOSECONDS.toMillis(decayInNanos);
	}

	public void setDecayInMillis(long decay) {
		this.decayInNanos = TimeUnit.MILLISECONDS.toNanos(decay);
	}

	public boolean isLoop() {
		return loop;
	}

	public void setLoop(boolean loop) {
		this.loop = loop;
	}

	public boolean isResetToZero() {
		return resetToZero;
	}

	public void setResetToZero(boolean resetToZero) {
		this.resetToZero = resetToZero;
	}

	@Override
	public double[] levels(long nanosecondsActivated, long nanosecondsDeactivated, int samplesToGenerate, double sampleRate) {
		double attackRange = previous.attackRange();
		double decayRange = previous.decayRange();

		// These things are all static, and only depend on the decay time, not the invocation
		double nanosPerSecond = TimeUnit.SECONDS.toNanos(1);

		double attackInSeconds = attackInNanos / nanosPerSecond;
		double samplesInAttack = attackInSeconds * sampleRate;
		// since attack goes from 0 to 1 (i.e. full range is 1) in $samplesInAttack steps
		double attackTick = attackRange / samplesInAttack;

		double decayInSeconds = decayInNanos / nanosPerSecond;
		double samplesInDecay = decayInSeconds * sampleRate;
		double decayFactor = Math.exp(LN_OF_ZEROISH / samplesInDecay);

		// These things are dynamic and depend on the invocation
		int samplesProcessed = previous.samplesProcessed();

		double[] output = new double[samplesToGenerate];
		
		// The reason we transition from nanoseconds to samples here is that we want to be able to change the state of the envelope
		// in the middle of a chunk. If we just relied on nanoseconds here, each chunk would be guaranteed to be in the same phase.
		// This doesn't matter when the phases are long, but when we have, say, a 5ms attack, then that would be extended to ~10-15ms
		// because that's approximately how big the chunks are

		// Starting at output[0] is not, strictly speaking, true. We should start with the last value at output[-1] ideally, but this is a reasonable
		// approximation, and we're only "wasting" one sample to do this.
		if (samplesProcessed == 0) {
			// This is the very start of the envelope. Choose the correct starting point, 
			// making sure we shortcut to the correct positions if their settings are 0, 
			// since we'd otherwise have 0 samples in which to reach that point
			if (resetToZero) {
				if (attackInNanos == 0) {
					output[0] = 1d;
				} else {
					output[0] = 0d;
				}
			} else {
				if (attackInNanos > 0) {
					// We're starting in attack. Start rising from wherever we were, up to 1, meaning we have 1-last to go.
					// If we're starting in attack, decay will happen normally.
					// For example if we were at 0.2, then the attack range will be 1-0.2=0.8, meaning that we'll need to go 0.8 up to reach 1, 
					// which is always the final target for the attack
					attackRange = 1d - previous.lastSample();
					output[0] = previous.lastSample();
				} else if (decayRange > 0) {
					// If we have ANY decay we start at 1. Always. That's always where the decay starts from. 
					// If we have 0 attack, this should be an abrupot jump
					decayRange = 1d;
					output[0] = 1d;
				}
			}
		} else {
			output[0] = previous.lastSample();
		}


		int totalSamplesInEnvelope = (int) (samplesInAttack + samplesInDecay);
		int totalSamplesProcessed = samplesProcessed;
		for (int i = 1; i < samplesToGenerate; i++) {
			if (loop && totalSamplesProcessed >= totalSamplesInEnvelope) {
				// We've reached the end of the envelope, and we want to repeat it.
				// Reset the envelope progress and generate remaining samples again from the start
				totalSamplesProcessed = 0;
				if (attackInNanos == 0) {
					output[i] = 1d;
				} else {
					output[i] = 0;
				}
			} else {
				totalSamplesProcessed++;
				if (totalSamplesProcessed <= samplesInAttack) {
					output[i] = output[i - 1] + attackTick;
				} else if (totalSamplesProcessed <= (samplesInAttack + samplesInDecay)) {
					output[i] = output[i - 1] * decayFactor;
				}
			}
		}
		previous = new EnvelopeState(attackRange, decayRange, Double.NaN, samplesProcessed + samplesToGenerate, output[output.length - 1]);
		return output;
	}


	@Override
	public Phase phase(long nanosecondsActivated, long nanosecondsDeactivated) {
		if (nanosecondsActivated + nanosecondsDeactivated < (attackInNanos + decayInNanos) || (loop && nanosecondsDeactivated == 0)) {
			// The fact that we don't distinguish between decay and attack doesn't really matter, because the only phase we *actually* use is IDLE.
			return Phase.DECAY;
		} else {
			return Phase.IDLE;
		}
	}

	@Override
	public void reset() {
		if (resetToZero) {
			this.previous = new EnvelopeState(1, 1, Double.NaN, 0, 0);
		} else {
			this.previous = new EnvelopeState(1, 1, Double.NaN, 0, this.previous.lastSample());
		}
	}

	@Override
	public String toString() {
		return "AttackDecay";
	}

	public static void main(String[] args) {
		AttackDecayEnvelope envelope = new AttackDecayEnvelope();
		envelope.setAttackInMillis(100);
		envelope.setDecayInMillis(0);
		envelope.setLoop(false);
		long oneSecondInNanos = TimeUnit.SECONDS.toNanos(1);

		int chunkSize = 441;
		double sampleRate = 44100d;
		long chunkInNanos = (long) ((1d / 100d) * oneSecondInNanos);
		int chunk = 0;
		long nanosecondsActivated = 0;
		long nanosecondsDeactivated = 0;
		for (int i = 0; i <= 44100; i += chunkSize) {
			if (chunk < 66) {
				nanosecondsActivated += chunkInNanos;
			} else {
				nanosecondsDeactivated += chunkInNanos;
			}
			double[] envelopeValues = envelope.levels(nanosecondsActivated, nanosecondsDeactivated, chunkSize, sampleRate);

			for (double d : envelopeValues) {
				System.out.printf(Locale.US, "%f%n", d);
			}
			chunk++;
		}
	}
}
