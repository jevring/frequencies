/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.envelopes;

import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.control.DiscreteControlListener;
import net.jevring.frequencies.v2.input.InputMode;

/**
 * An envelope and its controls.
 *
 * @author markus@jevring.net
 * @created 2020-03-11 21:50
 */
public class ControlledEnvelope implements Envelope {
	private volatile Envelope envelope = new LinearADSREnvelope();

	public ControlledEnvelope(Controls controls, Envelopes envelopes, String controlPrefix) {
		controls.getDiscreteControl(controlPrefix + "-envelope-type").mapping(envelopes::create).addListener(new DiscreteControlListener<>() {
			@Override
			public void valueChanged(Envelope newEnvelope, Object source) {
				copyEnvelopeSettings(envelope, newEnvelope);
				envelope = newEnvelope;
			}
		});
		controls.getControl(controlPrefix + "-envelope-attack").addListener((min, newValue, max, source) -> setAttackIfPossible((long) newValue));
		controls.getControl(controlPrefix + "-envelope-decay").addListener((min, newValue, max, source) -> setDecayIfPossible((long) newValue));
		controls.getControl(controlPrefix + "-envelope-sustain").addListener((min, newValue, max, source) -> setSustainIfPossible(newValue));
		controls.getControl(controlPrefix + "-envelope-release").addListener((min, newValue, max, source) -> setReleaseIfPossible((long) newValue));
		controls.getBooleanControl(controlPrefix + "-envelope-loop").addListener((loop, source) -> setLoopIfPossible(loop));
		controls.getDiscreteControl("input-mode")
		        .mapping(InputMode::valueOf)
		        .addListener((inputMode, source) -> setResetToZeroIfPossible(inputMode == InputMode.POLYPHONIC));
	}

	@Override
	public double[] levels(long nanosecondsActivated, long nanosecondsDeactivated, int samplesToGenerate, double sampleRate) {
		return envelope.levels(nanosecondsActivated, nanosecondsDeactivated, samplesToGenerate, sampleRate);
	}

	@Override
	public Phase phase(long nanosecondsActivated, long nanosecondsDeactivated) {
		return envelope.phase(nanosecondsActivated, nanosecondsDeactivated);
	}

	@Override
	public void reset() {
		envelope.reset();
	}

	private void setResetToZeroIfPossible(boolean resetToZero) {
		if (envelope instanceof ResetToZeroEnvelope resetToZeroEnvelope) {
			resetToZeroEnvelope.setResetToZero(resetToZero);
		}
	}

	private void setLoopIfPossible(boolean loop) {
		if (envelope instanceof LoopingEnvelope loopingEnvelope) {
			loopingEnvelope.setLoop(loop);
		}
	}

	private void setReleaseIfPossible(long releaseInMilliseconds) {
		if (envelope instanceof ReleaseEnvelope releaseEnvelope) {
			releaseEnvelope.setReleaseInMillis(releaseInMilliseconds);
		}
	}

	private void setSustainIfPossible(double sustain) {
		if (envelope instanceof SustainEnvelope sustainEnvelope) {
			sustainEnvelope.setSustainLevel(sustain);
		}
	}

	private void setDecayIfPossible(long decayTimeInMillis) {
		if (envelope instanceof DecayEnvelope decayEnvelope) {
			decayEnvelope.setDecayInMillis(decayTimeInMillis);
		}
	}

	private void setAttackIfPossible(long attackTimeInMilliseconds) {
		if (envelope instanceof AttackEnvelope attackEnvelope) {
			attackEnvelope.setAttackInMillis(attackTimeInMilliseconds);
		}
	}

	private void copyEnvelopeSettings(Envelope existingEnvelope, Envelope newEnvelope) {
		if (existingEnvelope instanceof ResetToZeroEnvelope existingResetToZeroEnvelope && newEnvelope instanceof ResetToZeroEnvelope newResetToZeroEnvelope) {
			newResetToZeroEnvelope.setResetToZero(existingResetToZeroEnvelope.isResetToZero());
		}
		if (existingEnvelope instanceof LoopingEnvelope existingLoopEnvelope && newEnvelope instanceof LoopingEnvelope newLoopEnvelope) {
			newLoopEnvelope.setLoop(existingLoopEnvelope.isLoop());
		}
		if (existingEnvelope instanceof AttackEnvelope existingAttackEnvelope && newEnvelope instanceof AttackEnvelope newAttackEnvelope) {
			newAttackEnvelope.setAttackInMillis(existingAttackEnvelope.getAttackInMillis());
		}
		if (existingEnvelope instanceof DecayEnvelope existingDecayEnvelope && newEnvelope instanceof DecayEnvelope newDecayEnvelope) {
			newDecayEnvelope.setDecayInMillis(existingDecayEnvelope.getDecayInMillis());
		}
		if (existingEnvelope instanceof SustainEnvelope existingSustainEnvelope && newEnvelope instanceof SustainEnvelope newSustainEnvelope) {
			newSustainEnvelope.setSustainLevel(existingSustainEnvelope.getSustainLevel());
		}
		if (existingEnvelope instanceof ReleaseEnvelope existingReleaseEnvelope && newEnvelope instanceof ReleaseEnvelope newReleaseEnvelope) {
			newReleaseEnvelope.setReleaseInMillis(existingReleaseEnvelope.getReleaseInMillis());
		}
	}
}
