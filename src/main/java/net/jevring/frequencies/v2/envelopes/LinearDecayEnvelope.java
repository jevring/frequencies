/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.envelopes;

import java.util.concurrent.TimeUnit;

/**
 * Decay-only envelope with a linear curve.
 *
 * @author markus@jevring.net
 * @created 2020-02-29 19:05
 */
public class LinearDecayEnvelope implements DecayEnvelope, Envelope {
	private volatile long decayInNanos = 200;
	private Levels previous;

	public long getDecayInMillis() {
		return TimeUnit.NANOSECONDS.toMillis(decayInNanos);
	}

	public void setDecayInMillis(long decay) {
		this.decayInNanos = TimeUnit.MILLISECONDS.toNanos(decay);
	}

	@Override
	public double[] levels(long nanosecondsActivated, long nanosecondsDeactivated, int samplesToGenerate, double sampleRate) {

		// NOTE: We can't just generate the entire envelope and then just look it up, because the key can be released at any time
		double originalReleaseStartingPoint = 0;
		double nanosPerSecond = TimeUnit.SECONDS.toNanos(1);
		int samplesProcessed = 0;
		if (previous != null) {
			samplesProcessed = previous.getSamplesProcessed();
		}

		double[] output = new double[samplesToGenerate];
		double decayInSeconds = decayInNanos / nanosPerSecond;
		double samplesInDecay = decayInSeconds * sampleRate;
		double decayTick = 1d / samplesInDecay;

		if (previous == null) {
			output[0] = 1d;
		} else {
			// This is not strictly speaking true. We should start with the last value at output[-1] ideally, but this is a reasonable
			// approximation, and we're only "wasting" one sample to do this.
			output[0] = previous.lastSample();
		}

		for (int i = 1; i < samplesToGenerate; i++) {
			output[i] = output[i - 1] - decayTick;
		}
		previous = new Levels(originalReleaseStartingPoint, samplesProcessed + samplesToGenerate, output);
		return output;
	}

	@Override
	public Phase phase(long nanosecondsActivated, long nanosecondsDeactivated) {
		if (nanosecondsActivated + nanosecondsDeactivated < decayInNanos) {
			return Phase.DECAY;
		} else {
			return Phase.IDLE;
		}
	}

	@Override
	public void reset() {
		previous = null;
	}

	@Override
	public String toString() {
		return "LinearDecay";
	}
}
