/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.waveforms;

import net.jevring.frequencies.v2.math.Interpolation;

import java.util.Locale;

/**
 * A triangle waveform
 *
 * @author markus@jevring.net
 * @created 2019-12-08 11:16
 */
public class TriangleWaveform implements Waveform {
	private final SawtoothWaveform saw = new SawtoothWaveform(true);
	private final WaveShaper waveShaper = new WaveShaper();

	@Override
	public double valueAt(double cycleProgress, double shape, boolean oddCycle, int quantizationSteps, double waveformInterpolation) {
		double triangle = baseValueAt(cycleProgress);
		if (shape <= -0.1) {
			return korgPrologueishWaveShape(shape, triangle);
		} else if (shape > 0.1) {
			return waveShaper.shapeSample(triangle, shape);
		} else {
			return triangle;
		}
	}

	/**
	 * Page 25 of 81 in http://cdn.korg.com/us/support/download/files/9317b94261e53e700b0145502850edc2.pdf
	 */
	private double korgPrologueishWaveShape(double shape, double triangle) {
		shape = Interpolation.linear(-0.1, -1, shape, 1, 0.25);
		// This isn't really correct, judging by the illustrations in the pdf.
		// That might just be illustrations, though, and not the actual waveform being produced.
		// It might actually be something like the "SNICster" detailed here: http://ijfritz.byethost4.com/sy_cir10.htm?i=1
		// Judging from the waveforms. However I'm not sure how to turn that into working code.
		if (triangle > shape) {
			double diff = triangle - shape;
			return shape - (diff * (1 + shape * 2));
		} else if (triangle < -shape) {
			double diff = triangle + shape; // -1 + 0.9 = -0.1
			return -shape - (diff * (1 + shape * 2));
		} else {
			return triangle;
		}
	}

	private double baseValueAt(double cycleProgress) {
		return 2.0 * Math.abs(saw.sawUpFromZero((cycleProgress + 0.25) % 1)) - 1;
	}

	@Override
	public String toString() {
		return "Triangle";
	}

	public static void main(String[] args) {
		TriangleWaveform w = new TriangleWaveform();
		for (int i = 1; i <= 10; i++) {
			double shape = -i / 10d;
			for (double d = 0; d <= 1; d += 0.01) {
				System.out.printf(Locale.US, "%.3f%n", w.valueAt(d, shape, i % 2 != 0, 0, 0));
			}
		}
	}
}
