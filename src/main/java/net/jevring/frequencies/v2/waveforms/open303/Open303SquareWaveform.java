/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.waveforms.open303;

import net.jevring.frequencies.v2.math.Clamp;

/**
 * From https://sourceforge.net/projects/open303/
 *
 * @author markus@jevring.net
 */
public class Open303SquareWaveform extends InterpolatedWaveform {
	// initialize internal 'back-panel' parameters
	private final double tanhShaperFactor = dB2amp(36.9);
	private final double tanhShaperOffset = 4.37;
	private final double squarePhaseShift = 180.0;


	public Open303SquareWaveform() {
		// generate the saw-wave:
		int N = tableLength;
		double k = 0.5;
		int N1 = (int) Clamp.clamp(Math.round(k * (N - 1)), 1, N - 1);
		int N2 = N - N1;
		double s1 = 1.0 / (N1 - 1);
		double s2 = 1.0 / N2;
		for (int n = 0; n < N1; n++) {
			prototypeTable[n] = s1 * n;
		}
		for (int n = N1; n < N; n++) {
			prototypeTable[n] = -1.0 + s2 * (n - N1);
		}

		// switch polarity and apply tanh-shaping with dc-offset:
		for (int n = 0; n < N; n++) {
			prototypeTable[n] = -Math.tanh(tanhShaperFactor * prototypeTable[n] + tanhShaperOffset);
		}

		// do a circular shift to phase-align with the saw-wave, when both waveforms are mixed:
		int nShift = (int) Math.round(N * squarePhaseShift / 360.0);
		circularShift(prototypeTable, N, nShift);
	}

	private static double dB2amp(double dB) {
		return Math.exp(dB * 0.11512925464970228420089957273422);
		//return pow(10.0, (0.05*dB)); // naive, inefficient version
	}

	void circularShift(double[] buffer, int length, int numPositions) {
		int na = Math.abs(numPositions);
		while (na > length) {
			na -= length;
		}
		double[] tmp = new double[na];
		if (numPositions < 0) {
			System.arraycopy(buffer, 0, tmp, 0, na);
			System.arraycopy(buffer, na, buffer, 0, length - na);
			System.arraycopy(tmp, 0, buffer, length - na, na);
		} else if (numPositions > 0) {
			System.arraycopy(buffer, length - na, tmp, 0, na);
			System.arraycopy(buffer, 0, buffer, na, length - na);
			System.arraycopy(tmp, 0, buffer, 0, na);
		}
	}

	@Override
	public String toString() {
		return "Open303Sawtooth";
	}
}
