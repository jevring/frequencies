/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.waveforms;

import net.jevring.frequencies.v2.oscillators.Oscillator;

/**
 * A wrapper for quantizing the provided waveform into {@code quantizationSteps} steps.
 *
 * @author markus@jevring.net
 * @created 2021-01-31 21:07
 */
public class QuantizedWaveform implements Waveform {
	private final Waveform waveform;

	public QuantizedWaveform(Waveform waveform) {
		this.waveform = waveform;
	}

	@Override
	public double valueAt(double cycleProgress, double shape, boolean oddCycle, int quantizationSteps, double waveformInterpolation) {
		double v = waveform.valueAt(cycleProgress, shape, oddCycle, quantizationSteps, waveformInterpolation);
		if (quantizationSteps == Oscillator.QUANTIZATION_DISABLED) {
			return v;
		} else {
			double d = 1d / (double) quantizationSteps;
			return d * Math.floor((v / d) + 0.5);
		}
	}
}
