/*
 * Copyright 2020 Markus Jevring                                              
 *                                                                            
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *                                                                            
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 *                                                                            
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 *                                                                            
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.waveforms.open303;

import net.jevring.frequencies.v2.math.Clamp;

/**
 * From https://sourceforge.net/projects/open303/
 *
 * @author markus@jevring.net
 */
public class Open303SawtoothWaveform extends InterpolatedWaveform {


	public Open303SawtoothWaveform() {
		// generate the saw-wave:
		int N = tableLength;
		double k = 0.5;
		int N1 = (int) Clamp.clamp(Math.round(k * (N - 1)), 1, N - 1);
		int N2 = N - N1;
		double s1 = 1.0 / (N1 - 1);
		double s2 = 1.0 / N2;
		for (int n = 0; n < N1; n++) {
			prototypeTable[n] = s1 * n;
		}
		for (int n = N1; n < N; n++) {
			prototypeTable[n] = -1.0 + s2 * (n - N1);
		}
	}


	@Override
	public String toString() {
		return "Open303Sawtooth";
	}
}
