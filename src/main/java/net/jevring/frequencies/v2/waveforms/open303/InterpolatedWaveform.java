/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.waveforms.open303;

import net.jevring.frequencies.v2.waveforms.Waveform;
import net.jevring.frequencies.v2.waveforms.WaveShaper;

/**
 * Generate a waveform table and then to lookups.
 * Not nearly as nice as the mip-mapped stuff from open303, but it'll do.
 *
 * @author markus@jevring.net
 * @created 2020-03-25 19:18
 */
abstract class InterpolatedWaveform implements Waveform {
	private final WaveShaper waveShaper = new WaveShaper();
	protected static final int tableLength = 2048;
	protected final double[] prototypeTable = new double[tableLength];

	@Override
	public double valueAt(double cycleProgress, double shape, boolean oddCycle, int quantizationSteps, double waveformInterpolation) {
		return waveShaper.shapeSample(baseValueAt(cycleProgress), shape);
	}

	private double baseValueAt(double cycleProgress) {
		double indexAndRemainder = tableLength * cycleProgress;
		int index = (int) indexAndRemainder;
		if (index == tableLength) {
			return prototypeTable[prototypeTable.length - 1];
		} else if (index == 0) {
			return prototypeTable[0];
		} else {
			double remainder = indexAndRemainder - index;
			//return prototypeTable[index];
			//return prototypeTable[index - 1] + (remainder * prototypeTable[index]);
			// return ((1d - remainder) * prototypeTable[index - 1]) + (remainder * prototypeTable[index]);
			return (remainder * prototypeTable[index - 1]) + ((1d - remainder) * prototypeTable[index]);
		}
	}
}
