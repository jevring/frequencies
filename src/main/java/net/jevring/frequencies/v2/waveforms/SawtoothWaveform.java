/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.waveforms;

import java.util.Locale;

/**
 * @author markus@jevring.net
 * @created 2019-12-08 11:16
 */
public class SawtoothWaveform implements Waveform {
	private final WaveShaper waveShaper = new WaveShaper();
	private final boolean up;

	public SawtoothWaveform(boolean up) {
		this.up = up;
	}

	@Override
	public double valueAt(double cycleProgress, double shape, boolean oddCycle, int quantizationSteps, double waveformInterpolation) {
		return valueAtUp(cycleProgress, shape, oddCycle) * (up ? 1 : -1);
	}

	private double valueAtUp(double cycleProgress, double shape, boolean oddCycle) {
		if (shape > 0.1) {
			return waveShaper.shapeSample(sawUpFromZero(cycleProgress), shape);
		} else if (shape < -0.1) {
			return korgPrologueWaveShape(cycleProgress, Math.abs(shape), oddCycle);
		} else {
			return sawUpFromZero(cycleProgress);
		}
	}

	/**
	 * Page 25 of 81 in http://cdn.korg.com/us/support/download/files/9317b94261e53e700b0145502850edc2.pdf
	 */
	private double korgPrologueWaveShape(double cycleProgress, double shape, boolean oddCycle) {
		if (oddCycle) {
			// This is the "first" cycle, and subsequently every other. 
			// This is the normal saw up.
			return sawUpFromZero(cycleProgress);
		} else {
			// This is the "second" cycle, and subsequently every other. 
			// This is where we use the shape to decide how much of it is the inverse saw.

			// This conversion happens "around" the middle of the even cycles
			// Thus a value of 0 would mean no inversion, and a value of 1 would mean that the whole cycle is inverted.
			double shapeSizeAroundCenter = shape / 2d;
			if (cycleProgress < 0.5 + shapeSizeAroundCenter && cycleProgress > 0.5 - shapeSizeAroundCenter) {
				return sawDownFromZero(cycleProgress);
			} else {
				return sawUpFromZero(cycleProgress);
			}
		}
	}

	double sawUpFromZero(double cycleProgress) {
		return 2.0 * (cycleProgress - Math.floor(cycleProgress + 0.5d));
	}

	private double sawUpFromMinusOne(double cycleProgress) {
		return -1 + (cycleProgress * 2.0);
	}

	private double sawDownFromZero(double cycleProgress) {
		return -1 * sawUpFromZero(cycleProgress);
	}

	@Override
	public String toString() {
		return "Sawtooth" + (up ? "Up" : "Down");
	}

	public static void main(String[] args) {
		SawtoothWaveform w = new SawtoothWaveform(true);
		for (int i = 1; i < 6; i++) {
			for (double d = 0; d <= 1; d += 0.01) {
				System.out.printf(Locale.US, "%.3f%n", w.valueAt(d, 0.5, i % 2 != 0, 0, 0));
			}
		}
	}
}
