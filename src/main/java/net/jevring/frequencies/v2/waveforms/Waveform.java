/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.waveforms;

/**
 * Produces values according to a certain waveform for a given place in the cycle.
 *
 * @author markus@jevring.net
 * @created 2019-12-08 11:13
 */
public interface Waveform {
	/**
	 * The value for this wave form at {@code cycleProgress}, which is a value between 0, which is the start of the period, and 1, which is the end of the period.
	 * A period here is the shape of one full wave form.
	 *
	 * @param cycleProgress         a value between 0 and 1
	 * @param shape                 This is a waveform-specific shape measure. The value provided must lie in the range from -1 to +1.
	 *                              How this is interpreted and used, if at all, is up to the waveform itself.
	 *                              If this value is modulated, that modulation must be included in this value, as waveforms are stateless.
	 * @param oddCycle              Every other cycle is an off cycle, and this tells the waveform which we're in
	 * @param quantizationSteps     The number of steps to break the waveform down into. This is like a bit crusher in a sense.
	 *                              This is used only by the quantized waveform
	 * @param waveformInterpolation used for variable waveforms to move between the waveform. Ignored for all others. A value between 0 and 3 inclusive.
	 * @return a value between -1 and 1.
	 */
	public double valueAt(double cycleProgress, double shape, boolean oddCycle, int quantizationSteps, double waveformInterpolation);
}
