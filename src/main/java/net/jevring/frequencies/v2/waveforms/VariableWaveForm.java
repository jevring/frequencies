/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.waveforms;

import net.jevring.frequencies.v2.math.Clamp;

/**
 * A variable waveform that changes shape, mixing between two waveforms in the following order.
 * <ol start="0">
 *     <li>Sine</li>
 *     <li>Triangle</li>
 *     <li>Saw</li>
 *     <li>Square</li>
 * </ol>
 * <p>
 * The wave shape parameter will affect the two relevant waveforms in the same way, since we don't want to have
 * multiple settings for this. This might be odd when we mix saw and square, but we'll find out.
 *
 * @author markus@jevring.net
 * @created 2021-02-01 18:35
 */
public class VariableWaveForm implements Waveform {
	// We shift the triangle and the sine here so that the waveforms line up nicely when transforming them.
	// We do this here because for the other uses of the waveforms, like when they're LFOs, we want them 
	// to start at the same point, which is 0.
	private static final Waveform sine = new PhaseShiftedWaveform(new SineWaveform(), -0.5);
	private static final Waveform triangle = new PhaseShiftedWaveform(new TriangleWaveform(), -0.5);
	private static final Waveform saw = new SawtoothWaveform(false);
	private static final Waveform square = new PulseWaveform();
	private static final Waveform[] waveforms = new Waveform[]{sine, triangle, saw, square};
	/* 
	// While this works, it looks and sounds much better when we have a quantized waveform wrapping the variable waveform.
	// This is because the intersections of the waveforms align, rather than being slightly different
	private static final Waveform[] quantizedWaveforms =
			new Waveform[]{new QuantizedWaveform(sine), new QuantizedWaveform(triangle), new QuantizedWaveform(saw), new QuantizedWaveform(square)};
			
	 */

	@Override
	public double valueAt(double cycleProgress, double shape, boolean oddCycle, int quantizationSteps, double waveformInterpolation) {
		// Mix is a value from 0 to 3 (array indexed)
		// 0.00 = 100% Sine
		// 1.00 = 100% Triangle
		// 2.00 = 100% Saw
		// 3.00 = 100% square
		// With values between those scaling between the waveforms.
		// For example 0.5 would be 50% sine and 50% triangle
		double clampedWaveformInterpolation = Clamp.clamp(waveformInterpolation, 0d, 3d);
		int i = (int) Math.floor(clampedWaveformInterpolation);
		Waveform w1 = waveforms[i];
		// We could get rid of this branch by introducing a zero waveform at the "end" of the array and simply hitting that when we're
		// all the way at the end, but that's probably not worth it. If this becomes a performance bottleneck, we can consider it.
		// Even if it does, with this branch we skip a lot of math which we would otherwise carry out on the value 0, to no effect.
		if (i == 3) {
			return w1.valueAt(cycleProgress, shape, oddCycle, quantizationSteps, clampedWaveformInterpolation);
		} else {
			Waveform w2 = waveforms[i + 1];
			double v1 = w1.valueAt(cycleProgress, shape, oddCycle, quantizationSteps, clampedWaveformInterpolation);
			double v2 = w2.valueAt(cycleProgress, shape, oddCycle, quantizationSteps, clampedWaveformInterpolation);

			double r2 = clampedWaveformInterpolation - i;
			double r1 = 1d - r2;

			return (v1 * r1) + (v2 * r2);

		}
	}

	@Override
	public String toString() {
		return "Variable";
	}
}
