/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.waveforms.open303;

/**
 * From https://sourceforge.net/projects/open303/
 *
 * @author markus@jevring.net
 */
public class Open303MoogSawtoothWaveform extends InterpolatedWaveform {


	public Open303MoogSawtoothWaveform() {
		// the sawUp part:
		int i;
		for (i = 0; i < (tableLength / 2); i++) {
			prototypeTable[i] = (double) (2 * i) / (double) (tableLength);
		}

		for (i = (tableLength / 2); i < (tableLength); i++) {
			prototypeTable[i] = (double) (2 * i) / (double) (tableLength) - 2.0;
		}

		// the triangle part:
		for (i = 0; i < (tableLength / 2); i++) {
			prototypeTable[i] += 1 - (double) (4 * i) / (double) (tableLength);
		}

		for (i = (tableLength / 2); i < tableLength; i++) {
			prototypeTable[i] += -1 + (double) (4 * i) / (double) (tableLength);
		}

		removeDC();
		normalize();
	}

	private void removeDC() {
		// calculate DC-offset (= average value of the table):
		double dcOffset = 0.0;
		int i;
		for (i = 0; i < tableLength; i++) {
			dcOffset += prototypeTable[i];
		}
		dcOffset = dcOffset / tableLength;

		// remove DC-Offset:
		for (i = 0; i < tableLength; i++) {
			prototypeTable[i] -= dcOffset;
		}
	}

	private void normalize() {
		// find maximum:
		double max = 0.0;
		int i;
		for (i = 0; i < tableLength; i++) {
			if (Math.abs(prototypeTable[i]) > max) {
				max = Math.abs(prototypeTable[i]);
			}
		}

		// normalize to amplitude 1.0:
		double scale = 1.0 / max;
		for (i = 0; i < tableLength; i++) {
			prototypeTable[i] *= scale;
		}
	}


	@Override
	public String toString() {
		return "Open303MoogSawtooth";
	}
}
