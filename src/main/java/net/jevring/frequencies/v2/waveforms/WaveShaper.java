/*
 * Copyright 2020 Markus Jevring                                              
 *                                                                            
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *                                                                            
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 *                                                                            
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 *                                                                            
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.waveforms;

import net.jevring.frequencies.v2.math.Interpolation;

/**
 * Shapes non-pulse waveforms. A number > 0 makes the waveform "narrower". A number < 0 makes the waveform "wider".
 *
 * @author markus@jevring.net
 * @created 2020-05-04 23:19
 */
public class WaveShaper {
	public double shapeSample(double input, double shape) {
		// More notes: http://www.carbon111.com/waveshaping1.html
		if (shape > 0.1) {
			// turn the shape into narrow peaks
			// http://floss.booktype.pro/csound/e-waveshaping/
			double v = Math.pow(Math.abs(input), Interpolation.linear(0.1, 1, shape, 1, 50));
			if (input < 0) {
				// negative values should still be negative
				v = -v;
			}
			return v;
		} else if (shape < -0.1) {
			// apply tanh() to the value
			// we start from 2.5 as that's much closer to what a normal waveform looks like. at least in the case of a sawtooth
			return Math.tanh(input * Interpolation.linear(0.1, 1, Math.abs(shape), 2.5, 10));
		} else {
			// else leave it untouched. Make a little deadband here where it would otherwise sound a bit weird.
			return input;
		}
	}
}
