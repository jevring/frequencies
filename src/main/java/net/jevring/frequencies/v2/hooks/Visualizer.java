/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.hooks;

import net.jevring.frequencies.v2.input.Instruction;

import java.util.List;

/**
 * This is a class for all the various visualization hooks. By default it has empty implementations of
 * all method bodies. This means that you can choose what visualizations you choose to hook, and which
 * you choose to disregard, without affecting the synthesizer itself.
 *
 * <p>It's important that all these visualizations are asynchronous. They <b>must not</b> delay the
 * audio engine loop. If they do, you can end up with sound artefacts.</p>
 *
 * @author markus@jevring.net
 * @created 2020-05-03 11:36
 */
public interface Visualizer {
	public static Visualizer noop() {
		return new Visualizer() {
			@Override
			public void visualizeModulation(int samplesToGenerate, List<Instruction> instructions) {
			}

			@Override
			public void visualizeLoopTime(double percentageOfMax) {
			}

			@Override
			public void visualizeWaveformChunk(double[] samples) {
			}
		};
	}

	public void visualizeModulation(int samplesToGenerate, List<Instruction> instructions);

	public void visualizeLoopTime(double percentageOfMax);

	/**
	 * Called with the pre-amplification samples in the range -1 to +1. This is also called with the pre-balance
	 * samples, which means that it'll effectively be mono.
	 *
	 * @param samples the samples from the latest chunk.
	 */
	public void visualizeWaveformChunk(double[] samples);
}
