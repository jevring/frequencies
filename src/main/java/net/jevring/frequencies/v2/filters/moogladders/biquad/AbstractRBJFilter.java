package net.jevring.frequencies.v2.filters.moogladders.biquad;

/**
 * https://github.com/ddiakopoulos/MoogLadders/blob/master/src/Filters.h
 *
 * @author markus@jevring.net
 * @created 2020-02-22 00:39
 */
public abstract class AbstractRBJFilter extends AbstractBiQuadFilter {
	protected double omega;
	protected double cosOmega;
	protected double sinOmega;

	protected double Q;
	protected double alpha;
	protected double A;

	protected double[] a = new double[3];
	protected double[] b = new double[3];

	public AbstractRBJFilter(double sampleRate) {
		super(sampleRate);
		Q = 1;
		A = 1;
		setCutoffFrequency(1000);
	}

	private void updateCoefficients() {
		cosOmega = Math.cos(omega);
		sinOmega = Math.sin(omega);

		typeDependentUpdateCoefficient();

		// Normalize filter coefficients
		double factor = 1.0f / a[0];

		double[] aNorm = new double[2];
		double[] bNorm = new double[3];

		aNorm[0] = a[1] * factor;
		aNorm[1] = a[2] * factor;

		bNorm[0] = b[0] * factor;
		bNorm[1] = b[1] * factor;
		bNorm[2] = b[2] * factor;

		setBiquadCoefs(bNorm, aNorm);
	}

	protected abstract void typeDependentUpdateCoefficient();

	/**
	 * In Hertz, 0 to Nyquist
	 *
	 * @param c
	 */
	@Override
	public void setCutoffFrequency(double c) {
		super.setCutoffFrequency(c);
		omega = hzToRad(c) / sampleRate;
		updateCoefficients();
	}

	/**
	 * Arbitrary, from 0.01f to ~20
	 *
	 * @param q
	 */
	@Override
	public void setResonance(double q) {
		super.setResonance(q);
		this.Q = q;
		updateCoefficients();
	}

	@Override
	public double getMinResonance() {
		return 0.01;
	}

	@Override
	public double getMaxResonance() {
		return 20d;
	}

	@Override
	public double getResonance() {
		return Q;
	}

	protected double hzToRad(double f) {
		return Math.PI * 2d * f;
	}
}
