package net.jevring.frequencies.v2.filters.chatgpt;

import net.jevring.frequencies.v2.filters.CutoffFrequencyFilter;
import net.jevring.frequencies.v2.filters.Filter;
import net.jevring.frequencies.v2.filters.QFilter;

import java.util.Arrays;

/**
 * I asked ChatGPT to write a resonant low-pass filter. This is what it wrote.
 * Interestingly, it relies on the sample rate, but there isn't actually a field for the sample rate,
 * so I have added that manually.
 * 
 * <p>Results:<br>
 * <ul>
 *     <li>Seems to simply kill the synthesizer if the cut-off frequency goes above 16kHz or something.</li>
 *     <li>Resonance has very little effect in the range 0-1. Anything higher than 1 does nothing.</li>
 *     <li>Looking at the graphs and listening to the sound, it is a low-pass filter, but it's not a very good one.</li>
 *     <li>Practically all the other low-pass filters in this repository are better</li>
 * </ul>
 * 
 * 
 * </p>
 */
public class ChatGPTResonantLowPassFilter implements Filter, CutoffFrequencyFilter, QFilter {
	private final double sampleRate;

	// Define filter parameters
	private double cutoff;   // cutoff frequency in Hz
	private double resonance; // resonance (Q) value


	// Create arrays for filter state
	private double[] b = new double[3]; // feedforward coefficients
	private double[] a = new double[3]; // feedback coefficients
	private double[] x = new double[3]; // input history
	private double[] y = new double[3]; // output history

	// Default constructor
	public ChatGPTResonantLowPassFilter(double sampleRate) {
		this(sampleRate, 1000.0, 0.5); // set default parameters
	}

	// Constructor
	public ChatGPTResonantLowPassFilter(double sampleRate, double cutoff, double resonance) {
		this.sampleRate = sampleRate;
		// Set filter parameters
		this.cutoff = cutoff;
		this.resonance = resonance;

		// Calculate filter coefficients
		calculateCoefficients();
	}

	@Override
	public double[] apply(double[] input) {
		// Create an array for the output samples
		double[] output = new double[input.length];

		// Apply filter to each sample in the input array
		for (int i = 0; i < input.length; i++) {
			// Update input and output history
			x[2] = x[1];
			x[1] = x[0];
			x[0] = input[i];
			y[2] = y[1];
			y[1] = y[0];

			// Calculate next output sample
			y[0] = b[0] * x[0] + b[1] * x[1] + b[2] * x[2] - a[0] * y[0] - a[1] * y[1];

			// Store the output sample in the output array
			output[i] = y[0];
		}

		return output;
	}

	// Method to calculate filter coefficients
	private void calculateCoefficients() {
		double k = Math.tan(Math.PI * cutoff / sampleRate);
		double norm = 1.0 / (1.0 + k / resonance + k * k);
		b[0] = k * k * norm;
		b[1] = 2.0 * b[0];
		b[2] = b[0];
		a[0] = 2.0 * (k * k - 1.0) * norm;
		a[1] = (1.0 - k / resonance + k * k) * norm;
	}

	// Setter methods for filter parameters

	@Override
	public void setCutoffFrequency(double cutoffFrequency) {
		this.cutoff = cutoffFrequency;
		calculateCoefficients();
	}

	@Override
	public double getCutoffFrequency() {
		return cutoff;
	}

	public void setResonance(double resonance) {
		this.resonance = resonance;
		calculateCoefficients();
	}

	@Override
	public double getResonance() {
		return this.resonance;
	}

	@Override
	public void reset() {
		Arrays.fill(x, 0d);
		Arrays.fill(y, 0d);
	}

	@Override
	public double getMaxResonance() {
		return 1;
	}
}
