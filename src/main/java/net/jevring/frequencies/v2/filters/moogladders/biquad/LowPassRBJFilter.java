package net.jevring.frequencies.v2.filters.moogladders.biquad;

/**
 * https://github.com/ddiakopoulos/MoogLadders/blob/master/src/Filters.h
 *
 * @author markus@jevring.net
 * @created 2020-02-22 01:24
 */
public class LowPassRBJFilter extends AbstractRBJFilter {

	public LowPassRBJFilter(double sampleRate) {
		super(sampleRate);
	}

	@Override
	protected void typeDependentUpdateCoefficient() {
		alpha = sinOmega / (2.0 * Q);
		b[0] = (1 - cosOmega) / 2;
		b[1] = 1 - cosOmega;
		b[2] = b[0];
		a[0] = 1 + alpha;
		a[1] = -2 * cosOmega;
		a[2] = 1 - alpha;
	}
}
