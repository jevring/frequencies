/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.filters.wikipedia;

import net.jevring.frequencies.v2.filters.CutoffFrequencyFilter;

/**
 * https://en.wikipedia.org/wiki/Low-pass_filter
 *
 * @author markus@jevring.net
 * @created 2020-02-15 15:35
 */
public class WikipediaRCLowPassFilter implements CutoffFrequencyFilter {
	private static final double TWO_PI = 2d * Math.PI;
	/**
	 * The length in seconds of a sample. This is 1/SAMPLE_RATE
	 */
	private final double dt;

	private volatile double cutoffFrequency;

	/**
	 * @param sampleRate the sample rate of the input stream. Almost always 44100
	 */
	public WikipediaRCLowPassFilter(double sampleRate) {
		this.dt = 1d / sampleRate;
		setCutoffFrequency(sampleRate / 2d);
	}

	public double getCutoffFrequency() {
		return cutoffFrequency;
	}

	/**
	 * Sets the cutoff frequency. Should ideally not be > sample rate / 2
	 */
	public void setCutoffFrequency(double cutoffFrequency) {
		this.cutoffFrequency = cutoffFrequency;
	}

	@Override
	public double[] apply(double[] input) {
		// R = resistance of the resistor in the filter
		// C = capacitance of the capacitor in the filter
		double RC = 1d / (cutoffFrequency * TWO_PI);

		double alpha = dt / (RC + dt);

		double[] output = new double[input.length];

		output[0] = alpha * input[0];
		for (int i = 1; i < input.length; i++) {
			// These should be equivalent according to wikipedia
			// output[i] = (alpha * input[i]) + ((1d - alpha) * output[i - 1]);
			output[i] = output[i - 1] + (alpha * (input[i] - output[i - 1]));
		}
		return output;
	}

	@Override
	public String toString() {
		return "WikipediaRCLowPassFilter";
	}
}
