// See: http://www.willpirkle.com/forum/licensing-and-book-code/licensing-and-using-book-code/
// The license is "You may also use the code from the FX and Synth books without licensing or fees. 
// The code is for you to develop your own plugins for your own use or for commercial use."
package net.jevring.frequencies.v2.filters.moogladders.ladder;

import net.jevring.frequencies.v2.filters.ModulatedFilter;
import net.jevring.frequencies.v2.math.Clamp;
import net.jevring.frequencies.v2.math.Interpolation;

/**
 * https://github.com/ddiakopoulos/MoogLadders/blob/master/src/OberheimVariationModel.h
 *
 * @author markus@jevring.net
 * @created 2020-02-21 23:42
 */
public class Oberheim4PoleLadderFilter extends AbstractLadderFilter implements ModulatedFilter {
	private final VAOnePole lpf1;
	private final VAOnePole lpf2;
	private final VAOnePole lpf3;
	private final VAOnePole lpf4;
	private double K;
	private double gamma;
	private double alpha0;
	private double Q;
	private double saturation;
	private double[] oberheimCoefs = new double[5];

	public Oberheim4PoleLadderFilter(double sampleRate) {
		super(sampleRate);
		lpf1 = new VAOnePole();
		lpf2 = new VAOnePole();
		lpf3 = new VAOnePole();
		lpf4 = new VAOnePole();

		saturation = 1.0;
		Q = 3.0;
	}

	@Override
	public void reset() {
		lpf1.reset();
		lpf2.reset();
		lpf3.reset();
		lpf4.reset();
	}

	@Override
	public double[] apply(double[] input, double[] frequencyModulation, double[] resonanceModulation) {
		double initialCutoffFrequency = this.cutoffFrequency;
		double initialResonance = this.resonance;
		double[] samples = new double[input.length];
		System.arraycopy(input, 0, samples, 0, input.length);
		for (int s = 0; s < input.length; ++s) {
			setCutoffFrequency(Clamp.clamp(initialCutoffFrequency + (frequencyModulation[s] * CUTOFF_FREQUENCY_MODULATION_RANGE), 20, 20_000));
			setResonance(initialResonance + Interpolation.linear(0, 1, resonanceModulation[s], getMinResonance(), getMaxResonance()));
			loop(samples, s);
		}
		setCutoffFrequency(initialCutoffFrequency);
		setResonance(initialResonance);
		return samples;
	}

	@Override
	public double[] apply(double[] input) {
		double[] samples = new double[input.length];
		System.arraycopy(input, 0, samples, 0, input.length);
		for (int s = 0; s < input.length; ++s) {
			loop(samples, s);
		}
		return samples;
	}

	private void loop(double[] samples, int s) {
		double in = samples[s];

		double sigma = lpf1.getFeedbackOutput() + lpf2.getFeedbackOutput() + lpf3.getFeedbackOutput() + lpf4.getFeedbackOutput();

		in *= 1.0 + K;

		// calculate input to first filter
		double u = (in - K * sigma) * alpha0;

		u = Math.tanh(saturation * u);

		double stage1 = lpf1.tick(u);
		double stage2 = lpf2.tick(stage1);
		double stage3 = lpf3.tick(stage2);
		double stage4 = lpf4.tick(stage3);

		// Oberheim variations
		samples[s] = oberheimCoefs[0] * u + oberheimCoefs[1] * stage1 + oberheimCoefs[2] * stage2 + oberheimCoefs[3] * stage3 + oberheimCoefs[4] * stage4;
	}

	@Override
	public void setCutoffFrequency(double c) {
		cutoffFrequency = c;

		// prewarp for BZT
		double wd = 2.0 * Math.PI * cutoffFrequency;
		double T = 1.0 / sampleRate;
		double wa = (2.0 / T) * Math.tan(wd * T / 2.0);
		double g = wa * T / 2.0;

		// Feedforward coeff
		double G = g / (1.0 + g);

		lpf1.setAlpha(G);
		lpf2.setAlpha(G);
		lpf3.setAlpha(G);
		lpf4.setAlpha(G);

		lpf1.setBeta(G * G * G / (1.0 + g));
		lpf2.setBeta(G * G / (1.0 + g));
		lpf3.setBeta(G / (1.0 + g));
		lpf4.setBeta(1.0 / (1.0 + g));

		gamma = G * G * G * G;
		alpha0 = 1.0 / (1.0 + K * gamma);

		// Oberheim variations / lpf4
		oberheimCoefs[0] = 0.0;
		oberheimCoefs[1] = 0.0;
		oberheimCoefs[2] = 0.0;
		oberheimCoefs[3] = 0.0;
		oberheimCoefs[4] = 1.0;
	}

	@Override
	public void setResonance(double r) {
		// this maps resonance = 1->10 to K = 0 -> 4
		K = (4.0) * (r - 1.0) / (10.0 - 1.0);
	}

	@Override
	public double getMaxResonance() {
		return 10d;
	}

	@Override
	public double getMinResonance() {
		return 1d;
	}

	private static final class VAOnePole {
		private double alpha;
		private double beta;
		private double gamma;
		private double delta;
		private double epsilon;
		private double a0;
		private double feedback;
		private double z1;

		private VAOnePole() {
			reset();
		}

		void reset() {
			alpha = 1.0;
			beta = 0.0;
			gamma = 1.0;
			delta = 0.0;
			epsilon = 0.0;
			a0 = 1.0;
			feedback = 0.0;
			z1 = 0.0;
		}

		double tick(double s) {
			s = s * gamma + feedback + epsilon * getFeedbackOutput();
			double vn = (a0 * s - z1) * alpha;
			double out = vn + z1;
			z1 = vn + out;
			return out;
		}

		double getFeedbackOutput() {
			return beta * (z1 + feedback * delta);
		}

		void setAlpha(double a) {
			alpha = a;
		}

		void setBeta(double b) {
			beta = b;
		}
	}


}
