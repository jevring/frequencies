// Based on an implementation by Magnus Jonsson
// https://github.com/magnusjonsson/microtracker (unlicense)

package net.jevring.frequencies.v2.filters.moogladders.ladder;

/**
 * https://github.com/ddiakopoulos/MoogLadders/blob/master/src/MicrotrackerModel.h
 *
 * @author markus@jevring.net
 * @created 2020-02-21 22:45
 */
public class Microtracker4PoleLadderFilter extends AbstractLadderFilter {
	private double p0, p1, p2, p3, p32, p33, p34;

	public Microtracker4PoleLadderFilter(double sampleRate) {
		super(sampleRate);
	}

	@Override
	public void setCutoffFrequency(double c) {
		cutoffFrequency = min(c * 2d * Math.PI / sampleRate, 1d);
	}

	@Override
	public void reset() {
		p0 = 0;
		p1 = 0;
		p2 = 0;
		p3 = 0;
		p32 = 0;
		p33 = 0;
		p34 = 0;
	}

	@Override
	public double getMaxResonance() {
		// no idea what this should be. no docs
		return 20d;
	}

	@Override
	public double[] apply(double[] input) {
		double[] samples = new double[input.length];
		System.arraycopy(input, 0, samples, 0, input.length);
		double k = resonance * 4;
		for (int s = 0; s < input.length; ++s) {
			// Coefficients optimized using differential evolution
			// to make feedback gain 4.0 correspond closely to the
			// border of instability, for all values of omega.
			double out = p3 * 0.360891 + p32 * 0.417290 + p33 * 0.177896 + p34 * 0.0439725;

			p34 = p33;
			p33 = p32;
			p32 = p3;

			p0 += (fastTanh(samples[s] - k * out) - fastTanh(p0)) * cutoffFrequency;
			p1 += (fastTanh(p0) - fastTanh(p1)) * cutoffFrequency;
			p2 += (fastTanh(p1) - fastTanh(p2)) * cutoffFrequency;
			p3 += (fastTanh(p2) - fastTanh(p3)) * cutoffFrequency;

			samples[s] = out;
		}
		return samples;
	}
}
