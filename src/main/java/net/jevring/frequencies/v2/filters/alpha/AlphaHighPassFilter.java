/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.filters.alpha;

/**
 * High-pass filter that accepts an alpha value directly.
 *
 * @author markus@jevring.net
 * @created 2020-02-15 12:08
 */
public class AlphaHighPassFilter implements AlphaFilter {
	private volatile double alpha = 0.5;

	@Override
	public double[] apply(double[] input) {
		double[] output = new double[input.length];

		output[0] = input[0];
		for (int i = 1; i < input.length; i++) {
			// These should be equivalent according to wikipedia
			//output[i] = (alpha * output[i - 1]) + (alpha * (input[i] - input[i - 1]));
			output[i] = alpha * (output[i - 1] + input[i] - input[i - 1]);
		}
		return output;
	}

	public double getAlpha() {
		return alpha;
	}

	@Override
	public void setAlpha(double alpha) {
		if (alpha < 0 || alpha > 1) {
			throw new IllegalArgumentException("Alpha must be between 0 and 1");
		}
		this.alpha = alpha;
	}

	@Override
	public String toString() {
		return "AlphaHighPass";
	}
}
