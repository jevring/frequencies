package net.jevring.frequencies.v2.filters.moogladders.ladder;

import java.util.Arrays;

/**
 * https://github.com/ddiakopoulos/MoogLadders/blob/master/src/ImprovedModel.h
 * <p>
 * This model is based on a reference implementation of an algorithm developed by
 * Stefano D'Angelo and Vesa Valimaki, presented in a paper published at ICASSP in 2013.
 * This improved model is based on a circuit analysis and compared against a reference
 * Ngspice simulation. In the paper, it is noted that this particular model is
 * more accurate in preserving the self-oscillating nature of the real filter.
 * <p>References: "An Improved Virtual Analog Model of the Moog Ladder Filter"
 * <p>Original Implementation: D'Angelo, Valimaki
 *
 * @author markus@jevring.net
 * @created 2020-02-21 22:45
 */
public class DAngeloValimaki4PoleLadderFilter extends AbstractLadderFilter {
	/**
	 * Thermal voltage (26 milliwats at room temperature)
	 */
	private static final double VT = 0.312;

	private double[] V = new double[4];
	private double[] dV = new double[4];
	private double[] tV = new double[4];

	private double g;
	private double drive = 1d;

	public DAngeloValimaki4PoleLadderFilter(double sampleRate) {
		super(sampleRate);
		setCutoffFrequency(1000d);
		setResonance(0.1); // [0,4]
	}

	@Override
	public void setCutoffFrequency(double c) {
		cutoffFrequency = c;
		double x = (Math.PI * cutoffFrequency) / sampleRate;
		g = 4.0 * Math.PI * VT * cutoffFrequency * (1.0 - x) / (1.0 + x);
	}

	@Override
	public double getMaxResonance() {
		// no idea what this should be. no docs
		return 20d;
	}

	@Override
	public void reset() {
		Arrays.fill(V, 0d);
		Arrays.fill(dV, 0d);
		Arrays.fill(tV, 0d);
	}

	@Override
	public double[] apply(double[] input) {
		double[] samples = new double[input.length];
		System.arraycopy(input, 0, samples, 0, input.length);
		double dV0, dV1, dV2, dV3;

		for (int i = 0; i < input.length; i++) {
			dV0 = -g * (Math.tanh((drive * samples[i] + resonance * V[3]) / (2.0 * VT)) + tV[0]);
			V[0] += (dV0 + dV[0]) / (2.0 * sampleRate);
			dV[0] = dV0;
			tV[0] = Math.tanh(V[0] / (2.0 * VT));

			dV1 = g * (tV[0] - tV[1]);
			V[1] += (dV1 + dV[1]) / (2.0 * sampleRate);
			dV[1] = dV1;
			tV[1] = Math.tanh(V[1] / (2.0 * VT));

			dV2 = g * (tV[1] - tV[2]);
			V[2] += (dV2 + dV[2]) / (2.0 * sampleRate);
			dV[2] = dV2;
			tV[2] = Math.tanh(V[2] / (2.0 * VT));

			dV3 = g * (tV[2] - tV[3]);
			V[3] += (dV3 + dV[3]) / (2.0 * sampleRate);
			dV[3] = dV3;
			tV[3] = Math.tanh(V[3] / (2.0 * VT));

			samples[i] = V[3];
		}
		return samples;
	}
}
