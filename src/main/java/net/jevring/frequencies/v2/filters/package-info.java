/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * When I first read <a href="https://www.dsprelated.com/freebooks/filters/Simplest_Lowpass_Filter_I.html">this document</a> I didn't understand a goddamn thing.
 * Now, however, I know perfectly well what it does, and I also learned, just now, that the amount of state you keep between invocations is equivalent to the "order"
 * of the filter.  This (likely) also means that the main loop itself will have to take that many pieces of state into account to fully be of order N.
 * It's still surprising that they're not dividing by two in that page, but still.
 *
 * <p>I seem to be picking up more and more information, though. I'm still pretty sure I don't know what the fuck I'm doing, but occasionally I come across
 * sources that actually explain things, like for example <a href="https://www.vicanek.de/articles/BiquadFits.pdf">this paper about the coefficients, and how
 * they are needed, but don't have any semantic meaning</a>. I had originally assumed that the people who wrote filter source code simply had been to lazy to
 * give these coefficients real names, but no, they actually have no real names! That was an eye-opener! The downside is that now I know that I'm less likely
 * to understand what it all means, but at least I can resign myself to understand that at least the names mean nothing.</p>
 * <p>Eventually patterns started to emerge. I'm fairly sure that most filters I've seen are 2nd order biquad filters. Not because I know what that <b>means</b>
 * necessarily, but I see patterns in the stuff that I read, and those patterns are often connected to the keywords. I have, <b>mostly</b>, come to terms with
 * the fact that I will likely never know how these filters work. I understand the oscillators and the envelopes and stuff, but the filters elude me. I can only
 * hope to be able to use others' work correctly. I reached a similar conclusion in <a href="https://bitbucket.org/jevring/threedee/src/default/">threedee</a>,
 * my 3d rendering engine, when it came to things like perspective projection and stuff. I could use it, but it lay just outside my capability to comprehend.</p>
 *
 * <p>What I'm really missing is a recipe that takes me from like {transfer functions and other related things} to {actual code in any language
 * (that isn't matlab or numpy)}</p>
 *
 * <p><b>BiQuad filters</b>
 * Aha, wikipedia strikes again with details that <a href="https://en.wikipedia.org/wiki/Digital_biquad_filter">the b, b1, and b2 coefficients determine the
 * zeroes and the a1 and a2 coefficients determine the poles</a>. I still don't understand zeroes or poles, but this is yet another clue!
 * </p>
 *
 *
 *
 * <p>A note on licenses: I try to use as permissive a license as possible on all my stuff. It usually ends up being some variant of the BSD license. Some
 * people, however, use GPL, and even worse, GPL 3. Due to my (admittedly limited) understanding of these licenses, I'm basically not even allowed to look
 * at this code, lest I get inspired by it, and implement something sufficiently similar that someone can say "that looks like mine, now your shit has to be GPL
 * as well!". I want to avoid that, so I have to disregard a lot of "open source" code that would have otherwise been helpful. =(</p>
 *
 * <p>I'll use this file to keep track of some links of useful things related to filters.
 * <ul>
 *     <li>It all started <a href="https://en.wikipedia.org/wiki/Low-pass_filter">here, of course, on the wikipedia page for low-pass filters</a>.
 *     The simplest implementation of a low-pass filter I could find that was actually code and not just circuit diagrams and math.</li>
 *     <li>I found out that the Moog Grandmother uses something called a ladder filter. I'd seem the schematics of them before, but armed with this word,
 *     "ladder filter", I could google more things and find <a href="https://github.com/ddiakopoulos/MoogLadders">this collection of C++ implementation of Moog Ladder filters</a>
 *     that I fully intend to port to Java. Thanks to the amazing authors for unlicensing most of the code!</li>
 *     <li><a href="https://www.youtube.com/watch?v=se31yZHNLLI">This video</a> got me a decent intro to filters and taught me about Q/resonance/emphasis.</li>
 *     <li><a href="http://shepazu.github.io/Audio-EQ-Cookbook/audio-eq-cookbook.html">This actually looks like a really good explanation of what a biquad
 *     filter <b>means</b></a> and how that translates into code. (<a href="https://dsp.stackexchange.com/questions/8693/how-does-a-low-pass-filter-programmatically-work">The first answer
 *     here on the DSP stack exchange also contains the same information</a>). (Yes, <a href="https://en.wikipedia.org/wiki/Digital_biquad_filter">this too,
 *     because wikipedia is awesome</a>) It's got the magic piece of math that takes sampled data in and gives the equation for it.
 *     It aligns pretty well with one of the few useful tidbits I got from the JUCE code that is actually ISC licensed, which is
 *     <a href="https://github.com/WeAreROLI/JUCE/blob/master/modules/juce_audio_basics/utilities/juce_IIRFilter.cpp">their IIRFilter class.</a></li>
 *     <li>Then there's this. <a href="https://github.com/berndporr/iir1">A list of implementations of various 2nd order filters such as Butterworth and
 *     Chebyshev</a>, but it's really hard to understand.</li>
 *     <li>A friend of mine who's an electrical engineer also gave me this which <a href="https://www.mikroe.com/ebooks/digital-filter-design/introduction-iir-filter">
 *     is a book on digital filter design</a>, which would likely be very useful if I understood more of it</li>
 *     <li>A well-documented and well-written piece of C++ code for a TB-303 emulator, the <a href="https://sourceforge.net/projects/open303/">Open303</a>.
 *     It's not updated anymore, but it looks great. MIT Licensed.</li>
 *     <li><a href="https://www.youtube.com/watch?v=Vrpr0ohAWxI">Drum synthesis with the Moog Sub-Phatty</a></li>
 * </ul>
 * </p>
 *
 * @author markus@jevring.net
 * @created 2020-02-17 22:41
 */
package net.jevring.frequencies.v2.filters;