/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */


package net.jevring.frequencies.v2.filters;

import net.jevring.frequencies.v2.math.Clamp;
import net.jevring.frequencies.v2.math.Interpolation;

import java.util.Arrays;

/**
 * This is a port to Java of the diode ladder filter originally written by Dominique Wurtz linked here:
 * <p>
 * https://pastebin.com/THe5JG5f
 * https://www.kvraudio.com/forum/viewtopic.php?t=346155
 *
 * @author markus@jevring.net
 * @created 2020-02-22 20:20
 */
public class DiodeLadderFilter implements CutoffFrequencyFilter, QFilter, ModulatedFilter {
	private final double sampleRate;
	private volatile double k;
	private volatile double A;
	/**
	 * filter memory (4 integrators plus 1st order HPF)
	 */
	private final double[] z = new double[5];
	private volatile double ah;
	private volatile double bh;
	private volatile double cutoffFrequency;
	private volatile double fc;
	private volatile double q;

	public DiodeLadderFilter(double sampleRate) {
		this.sampleRate = sampleRate;
		setResonance(0d);
		setCutoffFrequency(sampleRate / 2d);
	}

	@Override
	public void reset() {
		//if (k < 17) {
			Arrays.fill(z, 0d);
		//}
	}

	@Override
	public void setCutoffFrequency(double cutoffFrequency) {
		this.cutoffFrequency = Clamp.clamp(cutoffFrequency, 20, 20_000);
		double nyquistFrequency = sampleRate / 2d;
		// fc normalized cutoff frequency in the range [0..1] => 0 HZ .. Nyquist
		fc = Clamp.clamp(this.cutoffFrequency / nyquistFrequency, 0d, 1d);

		// not to be confused with the field lower-case k
		double K = fc * Math.PI;
		ah = (K - 2d) / (K + 2d);
		bh = 2d / (K + 2d);
	}

	@Override
	public double getCutoffFrequency() {
		return this.cutoffFrequency;
	}

	/**
	 * @param q resonance in the range [0..1]
	 */
	@Override
	public void setResonance(double q) {
		this.q = q;
		k = 20d * q;
		A = 1 + 0.5 * k; // resonance gain compensation 
	}

	@Override
	public double getMaxResonance() {
		return 1d;
	}

	@Override
	public double getResonance() {
		return this.q;
	}

	@Override
	public double[] apply(double[] input, double[] frequencyModulation, double[] resonanceModulation) {
		double initialCutoffFrequency = this.cutoffFrequency;
		double initialResonance = this.q;
		double[] output = new double[input.length];
		for (int i = 0; i < input.length; i++) {
			setCutoffFrequency(initialCutoffFrequency + (frequencyModulation[i] * CUTOFF_FREQUENCY_MODULATION_RANGE));
			setResonance(initialResonance + Interpolation.linear(0, 1, resonanceModulation[i], getMinResonance(), getMaxResonance()));
			loop(input[i], output, i);
		}
		setCutoffFrequency(initialCutoffFrequency);
		setResonance(initialResonance);
		return output;
	}

	@Override
	public double[] apply(double[] input) {
		double[] output = new double[input.length];
		for (int i = 0; i < input.length; i++) {
			loop(input[i], output, i);
		}
		return output;
	}

	private void loop(double x1, double[] output, int i) {
		double x = x1;

		double a = Math.PI * fc; // PI is Nyquist frequency
		// a = 2 * tan(0.5*a); // dewarping, not required with 2x oversampling
		a = 2 * Math.tan(0.5 * a); // MJ: we're not oversampling, so I guess we have to do this
		double ainv = 1d / a;
		double a2 = a * a;
		double b = 2d * a + 1d;
		double b2 = b * b;
		double c = 1d / (2d * a2 * a2 - 4d * a2 * b2 + b2 * b2);
		double g0 = 2d * a2 * a2 * c;
		double g = g0 * bh;

		// current state
		double s0 = (a2 * a * z[0] + a2 * b * z[1] + z[2] * (b2 - 2 * a2) * a + z[3] * (b2 - 3 * a2) * b) * c;
		double s = bh * s0 - z[4];

		// solve feedback loop (linear)
		double y5 = (g * x + s) / (1 + g * k);

		// input clipping
		double y0 = clip(x - k * y5);
		y5 = g * y0 + s;

		// compute integrator outputs
		double y4 = g0 * y0 + s0;
		double y3 = (b * y4 - z[3]) * ainv;
		double y2 = (b * y3 - a * y4 - z[2]) * ainv;
		double y1 = (b * y2 - a * y3 - z[1]) * ainv;

		// update filter state
		z[0] += 4 * a * (y0 - y1 + y2);
		z[1] += 2 * a * (y1 - 2 * y2 + y3);
		z[2] += 2 * a * (y2 - 2 * y3 + y4);
		z[3] += 2 * a * (y3 - 2 * y4);
		z[4] = bh * y4 + ah * y5;

		output[i] = A * y4;
	}

	private static double clip(double d) {
		return d / (1 + Math.abs(d));
	}

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}
}
