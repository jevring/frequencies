package net.jevring.frequencies.v2.filters.moogladders.biquad;

/**
 * https://github.com/ddiakopoulos/MoogLadders/blob/master/src/Filters.h
 *
 * @author markus@jevring.net
 * @created 2020-02-22 01:24
 */
public class AllPassRBJFilter extends AbstractRBJFilter {

	public AllPassRBJFilter(double sampleRate) {
		super(sampleRate);
	}

	@Override
	protected void typeDependentUpdateCoefficient() {
		alpha = sinOmega / (2.0 * Q);
		b[0] = 1 - alpha;
		b[1] = -2 * cosOmega;
		b[2] = 1 + alpha;
		a[0] = b[2];
		a[1] = b[1];
		a[2] = b[0];
	}
}
