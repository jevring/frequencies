package net.jevring.frequencies.v2.filters.moogladders.ladder;

//@formatter:off
import net.jevring.frequencies.v2.filters.ModulatedFilter;
import net.jevring.frequencies.v2.math.Interpolation;import java.util.Arrays; /**
 * https://github.com/ddiakopoulos/MoogLadders/blob/master/src/RKSimulationModel.h
 * 
 * Imitates a Moog resonant filter by Runge-Kutte numerical integration of
 * a differential equation approximately describing the dynamics of the circuit.
 *
 * Useful references:
 * 	* Tim Stilson
 * 	"Analyzing the Moog VCF with Considerations for Digital Implementation"
 * 		Sections 1 and 2 are a reasonably good introduction but the 
 * 		model they use is highly idealized.
 * 	* Timothy E. Stinchcombe
 * 	"Analysis of the Moog Transistor Ladder and Derivative Filters"
 * 		Long, but a very thorough description of how the filter works including
 * 		its nonlinearities
 * 	* Antti Huovilainen
 * 	"Non-linear digital implementation of the moog ladder filter"
 * 		Comes close to giving a differential equation for a reasonably realistic
 * 		model of the filter
 * The differential equations are:
 * 	y1' = k * (S(x - r * y4) - S(y1))
 * 	y2' = k * (S(y1) - S(y2))
 * 	y3' = k * (S(y2) - S(y3))
 * 	y4' = k * (S(y3) - S(y4))
 * where k controls the cutoff frequency, r is feedback (<= 4 for stability), and S(x) is a saturation function.
 *
 * @author markus@jevring.net
 * @created 2020-02-21 22:45
 */
//@formatter:on
public class RKSimulation4PoleLadderFilter extends AbstractLadderFilter implements ModulatedFilter {
	private final double[] state = new double[4];
	private final double saturation;
	private final double saturationInv;
	private final int oversampleFactor;
	private final double stepSize;

	public RKSimulation4PoleLadderFilter(double sampleRate) {
		super(sampleRate);
		saturation = 3.0;
		saturationInv = 1.0 / saturation;

		oversampleFactor = 1;

		stepSize = 1.0 / (oversampleFactor * sampleRate);
	}

	@Override
	public void reset() {
		Arrays.fill(state, 0);
	}

	@Override
	public void setResonance(double q) {
		// 0 to 10
		super.setResonance(q);
	}

	@Override
	public double getMaxResonance() {
		return 10d;
	}

	@Override
	public void setCutoffFrequency(double c) {
		cutoffFrequency = transformCutoffFrequency(c);
	}

	private double transformCutoffFrequency(double c) {
		return 2.0 * Math.PI * c;
	}

	@Override
	public double[] apply(double[] input, double[] frequencyModulation, double[] resonanceModulation) {
		double[] samples = new double[input.length];
		System.arraycopy(input, 0, samples, 0, input.length);

		for (int s = 0; s < input.length; ++s) {
			double cutoffFrequency = this.cutoffFrequency + transformCutoffFrequency((frequencyModulation[s] * CUTOFF_FREQUENCY_MODULATION_RANGE));
			double resonance = this.resonance + Interpolation.linear(0, 1, resonanceModulation[s], getMinResonance(), getMaxResonance());
			for (int j = 0; j < oversampleFactor; j++) {
				rungekutteSolver(samples[s], state, cutoffFrequency, resonance);
			}

			samples[s] = state[3];
		}
		return samples;
	}

	@Override
	public double[] apply(double[] input) {
		double[] samples = new double[input.length];
		System.arraycopy(input, 0, samples, 0, input.length);

		for (int s = 0; s < input.length; ++s) {
			for (int j = 0; j < oversampleFactor; j++) {
				rungekutteSolver(samples[s], state, cutoffFrequency, resonance);
			}

			samples[s] = state[3];
		}
		return samples;
	}

	private void calculateDerivatives(double input, double[] dstate, double[] state, double cutoffFrequency, double resonance) {
		double satstate0 = clip(state[0], saturation, saturationInv);
		double satstate1 = clip(state[1], saturation, saturationInv);
		double satstate2 = clip(state[2], saturation, saturationInv);

		dstate[0] = cutoffFrequency * (clip(input - resonance * state[3], saturation, saturationInv) - satstate0);
		dstate[1] = cutoffFrequency * (satstate0 - satstate1);
		dstate[2] = cutoffFrequency * (satstate1 - satstate2);
		dstate[3] = cutoffFrequency * (satstate2 - clip(state[3], saturation, saturationInv));
	}

	private void rungekutteSolver(double input, double[] state, double cutoffFrequency, double resonance) {
		int i;
		double[] deriv1 = new double[4];
		double[] deriv2 = new double[4];
		double[] deriv3 = new double[4];
		double[] deriv4 = new double[4];
		double[] tempState = new double[4];

		calculateDerivatives(input, deriv1, state, cutoffFrequency, resonance);

		for (i = 0; i < 4; i++) {
			tempState[i] = state[i] + 0.5 * stepSize * deriv1[i];
		}

		calculateDerivatives(input, deriv2, tempState, cutoffFrequency, resonance);

		for (i = 0; i < 4; i++) {
			tempState[i] = state[i] + 0.5 * stepSize * deriv2[i];
		}

		calculateDerivatives(input, deriv3, tempState, cutoffFrequency, resonance);

		for (i = 0; i < 4; i++) {
			tempState[i] = state[i] + stepSize * deriv3[i];
		}

		calculateDerivatives(input, deriv4, tempState, cutoffFrequency, resonance);

		for (i = 0; i < 4; i++) {
			state[i] += (1.0 / 6.0) * stepSize * (deriv1[i] + 2.0 * deriv2[i] + 2.0 * deriv3[i] + deriv4[i]);
		}
	}
}
