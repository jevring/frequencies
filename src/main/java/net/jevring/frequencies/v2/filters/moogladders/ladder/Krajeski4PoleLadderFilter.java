// This code is Unlicensed (i.e. public domain); in an email exchange on
// 6.14.2019 David Lowenfels stated "You're welcome to use the Moog~ code, 
// license it free as in beer or whatever :)"

// Source: https://github.com/ddiakopoulos/MoogLadders/blob/master/src/StilsonModel.h

package net.jevring.frequencies.v2.filters.moogladders.ladder;

import net.jevring.frequencies.v2.filters.ModulatedFilter;
import net.jevring.frequencies.v2.math.Clamp;
import net.jevring.frequencies.v2.math.Interpolation;

import java.util.Arrays;

public class Krajeski4PoleLadderFilter extends AbstractLadderFilter implements ModulatedFilter {

	/**
	 * The angular frequency of the cutoff.
	 */
	private double wc;
	/**
	 * A derived parameter for the cutoff frequency
	 */
	private double g;
	/**
	 * A similar derived parameter for resonance.
	 */
	private double gRes;
	/**
	 * Compensation factor.
	 * todo: expose via control?
	 */
	private double gComp = 1.0;
	/**
	 * A parameter that controls intensity of nonlinearities.
	 * todo: expose via control?
	 */
	private double drive = 1.0;
	private double[] delay = new double[5];
	private double[] state = new double[5];

	public Krajeski4PoleLadderFilter(double sampleRate) {
		super(sampleRate);
	}

	@Override
	public void reset() {
		Arrays.fill(state, 0);
		Arrays.fill(delay, 0);
	}

	@Override
	public void setResonance(double r) {
		resonance = r;
		gRes = resonance * (1.0029 + 0.0526 * wc - 0.926 * Math.pow(wc, 2) + 0.0218 * Math.pow(wc, 3));
	}

	@Override
	public double getMaxResonance() {
		// no idea what this should be. no docs
		return 20d;
	}

	@Override
	public void setCutoffFrequency(double c) {
		cutoffFrequency = c;
		wc = 2 * Math.PI * cutoffFrequency / sampleRate;
		g = 0.9892 * wc - 0.4342 * Math.pow(wc, 2) + 0.1381 * Math.pow(wc, 3) - 0.0202 * Math.pow(wc, 4);
	}

	@Override
	public double[] apply(double[] input, double[] frequencyModulation, double[] resonanceModulation) {
		double initialCutoffFrequency = this.cutoffFrequency;
		double initialResonance = this.resonance;
		double[] samples = new double[input.length];
		System.arraycopy(input, 0, samples, 0, input.length);
		for (int s = 0; s < input.length; ++s) {
			setCutoffFrequency(Clamp.clamp(initialCutoffFrequency + (frequencyModulation[s] * CUTOFF_FREQUENCY_MODULATION_RANGE), 20, 20_000));
			setResonance(initialResonance + Interpolation.linear(0, 1, resonanceModulation[s], getMinResonance(), getMaxResonance()));
			loop(samples, s);
		}
		setCutoffFrequency(initialCutoffFrequency);
		setResonance(initialResonance);
		return samples;
	}

	@Override
	public double[] apply(double[] input) {
		double[] samples = new double[input.length];
		System.arraycopy(input, 0, samples, 0, input.length);
		for (int s = 0; s < input.length; ++s) {
			loop(samples, s);
		}
		return samples;
	}

	private void loop(double[] samples, int s) {
		state[0] = Math.tanh(drive * (samples[s] - 4 * gRes * (state[4] - gComp * samples[s])));

		// Are these the 4 poles? i.e. i==pole
		for (int i = 0; i < 4; i++) {
			state[i + 1] = g * (0.3 / 1.3 * state[i] + 1 / 1.3 * delay[i] - state[i + 1]) + state[i + 1];
			delay[i] = state[i];
		}
		samples[s] = state[4];
	}
}
