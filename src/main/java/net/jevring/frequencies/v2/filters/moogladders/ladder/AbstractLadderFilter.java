package net.jevring.frequencies.v2.filters.moogladders.ladder;

import net.jevring.frequencies.v2.filters.AbstractFilter;

/**
 * @author markus@jevring.net
 * @created 2020-02-22 00:30
 */
public abstract class AbstractLadderFilter extends AbstractFilter {
	public AbstractLadderFilter(double sampleRate) {
		super(sampleRate, 1000d, 0.1d);
	}

	protected double snapToZero(double n) {
		if (!(n < -1.0e-8 || n > 1.0e-8)) {
			return 0;
		} else {
			return n;
		}
	}

	/**
	 * Clamp without branching
	 * If input - _limit < 0, then it really subtracts, and the 0.5 to make it half the 2 inputs.
	 * If > 0 then they just cancel, and keeps input normal.
	 * The easiest way to understand it is check what happens on both cases.
	 */
	protected double saturate(double input) {
		double x1 = Math.abs(input + 0.95d);
		double x2 = Math.abs(input - 0.95d);
		return 0.5d * (x1 - x2);
	}

	protected double min(double a, double b) {
		a = b - a;
		a += Math.abs(a);
		a *= 0.5f;
		a = b - a;
		return a;
	}

	/**
	 * Linear interpolation, used to crossfade a gain table
	 */
	protected double lerp(double amount, double a, double b) {
		return (1.0f - amount) * a + amount * b;
	}

	protected double fastTanh(double x) {
		double x2 = x * x;
		return x * (27.0 + x2) / (27.0 + 9.0 * x2);
	}

	/**
	 * Imitate the (tanh) clipping function of a transistor pair.
	 * to 4th order, tanh is x - x*x*x/3; this cubic's
	 * plateaus are at +/- 1 so clip to 1 and evaluate the cubic.
	 * This is pretty coarse - for instance if you clip a Sineoid this way you
	 * can sometimes hear the discontinuity in 4th derivative at the clip point
	 */
	protected double clip(double value, double saturation, double saturationInverse) {
		double v2;
		if (value * saturationInverse > 1) {
			v2 = 1;
		} else {
			if (value * saturationInverse < -1) {
				v2 = -1;
			} else {
				v2 = value * saturationInverse;
			}
		}
		return (saturation * (v2 - (1. / 3.) * v2 * v2 * v2));
	}
}
