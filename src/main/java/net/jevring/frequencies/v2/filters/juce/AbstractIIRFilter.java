/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.filters.juce;

import net.jevring.frequencies.v2.filters.CutoffFrequencyFilter;
import net.jevring.frequencies.v2.filters.ModulatedFilter;
import net.jevring.frequencies.v2.filters.QFilter;
import net.jevring.frequencies.v2.math.Clamp;

/**
 * This is the base class for all the JUCE-ISC filters.
 *
 * @author markus@jevring.net
 * @created 2020-02-22 18:06
 */
public abstract class AbstractIIRFilter implements QFilter, CutoffFrequencyFilter, ModulatedFilter {
	protected final double sampleRate;

	protected volatile Coefficients coefficients;
	protected volatile double v1;
	protected volatile double v2;

	private volatile double cutoffFrequency;
	private volatile double Q;

	protected AbstractIIRFilter(double sampleRate) {
		this.sampleRate = sampleRate;
		this.cutoffFrequency = sampleRate * 0.4;
		this.Q = 1d / Math.sqrt(2);
		// Just below what I believe is the nyquist frequency, which I believe is basically just half of the sample rate
		configure(cutoffFrequency, Q);
	}

	protected abstract void configure(double cutoffFrequency, double q);

	protected void setCoefficients(Coefficients coefficients) {
		this.coefficients = coefficients;
	}

	@Override
	public void setCutoffFrequency(double cutoffFrequency) {
		this.cutoffFrequency = cutoffFrequency;
		configure(cutoffFrequency, Q);
	}

	@Override
	public double getCutoffFrequency() {
		return this.cutoffFrequency;
	}

	@Override
	public void setResonance(double q) {
		this.Q = q;
		configure(cutoffFrequency, q);
	}

	@Override
	public double getResonance() {
		return this.Q;
	}

	@Override
	public double getMaxResonance() {
		// No idea what this should be. No docs
		return 10d;
	}

	@Override
	public double[] apply(double[] input, double[] frequencyModulation, double[] resonanceModulation) {
		double initialCutoffFrequency = this.cutoffFrequency;
		double initialQ = this.Q;
		// Fetch the state from the previous invocation
		double lv1 = Double.isNaN(v1) ? 0 : v1;
		double lv2 = Double.isNaN(v2) ? 0 : v2;

		double[] samples = new double[input.length];
		System.arraycopy(input, 0, samples, 0, input.length);
		for (int i = 0; i < samples.length; ++i) {
			configure(Clamp.clamp(initialCutoffFrequency + (frequencyModulation[i] * CUTOFF_FREQUENCY_MODULATION_RANGE), 20, 20_000), initialQ + resonanceModulation[i]);
			// Transform current sample
			double in = samples[i];
			double out = coefficients.c0 * in + lv1;
			samples[i] = out;

			// Change our local state so that we can have some extra history mixed in to the next state
			lv1 = coefficients.c1 * in - coefficients.c3 * out + lv2;
			lv2 = coefficients.c2 * in - coefficients.c4 * out;
		}

		// Save the local state for the next invocation
		v1 = lv1;
		v2 = lv2;

		configure(initialCutoffFrequency, initialQ);

		return samples;
	}

	@Override
	public double[] apply(double[] input) {
		// Fetch the state from the previous invocation
		double lv1 = Double.isNaN(v1) ? 0 : v1;
		double lv2 = Double.isNaN(v2) ? 0 : v2;

		Coefficients localCoefficients = coefficients;
		double c0 = localCoefficients.c0;
		double c1 = localCoefficients.c1;
		double c2 = localCoefficients.c2;
		double c3 = localCoefficients.c3;
		double c4 = localCoefficients.c4;

		double[] samples = new double[input.length];
		System.arraycopy(input, 0, samples, 0, input.length);
		for (int i = 0; i < samples.length; ++i) {
			// Transform current sample
			double in = samples[i];
			double out = c0 * in + lv1;
			samples[i] = out;

			// Change our local state so that we can have some extra history mixed in to the next state
			lv1 = c1 * in - c3 * out + lv2;
			lv2 = c2 * in - c4 * out;
		}

		// Save the local state for the next invocation
		v1 = lv1;
		v2 = lv2;

		return samples;
	}

	@Override
	public void reset() {
		this.v1 = 0;
		this.v2 = 0;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}

	protected static final class Coefficients {
		private final double c0;
		private final double c1;
		private final double c2;
		private final double c3;
		private final double c4;

		public Coefficients(double c0, double c1, double c2, double c3, double c4) {
			this(c0, c1, c2, c3, c4, 1d);
		}

		public Coefficients(double c0, double c1, double c2, double c3, double c4, double shelfDivisor) {
			double multiplier = 1d / shelfDivisor;
			this.c0 = (c0 * multiplier);
			this.c1 = (c1 * multiplier);
			this.c2 = (c2 * multiplier);
			this.c3 = (c3 * multiplier);
			this.c4 = (c4 * multiplier);
		}
	}
}
