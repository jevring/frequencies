package net.jevring.frequencies.v2.filters.moogladders.biquad;

/**
 * https://github.com/ddiakopoulos/MoogLadders/blob/master/src/Filters.h
 *
 * @author markus@jevring.net
 * @created 2020-02-22 01:24
 */
public class LowShelfRBJFilter extends AbstractRBJFilter {

	public LowShelfRBJFilter(double sampleRate) {
		super(sampleRate);
	}

	@Override
	protected void typeDependentUpdateCoefficient() {
		alpha = sinOmega / 2.0 * Math.sqrt((A + 1.0 / A) * (1.0 / Q - 1.0) + 2.0);
		b[0] = A * ((A + 1) - ((A - 1) * cosOmega) + (2 * Math.sqrt(A) * alpha));
		b[1] = 2 * A * ((A - 1) - ((A + 1) * cosOmega));
		b[2] = A * ((A + 1) - ((A - 1) * cosOmega) - (2 * Math.sqrt(A) * alpha));
		a[0] = ((A + 1) + ((A - 1) * cosOmega) + (2 * Math.sqrt(A) * alpha));
		a[1] = -2 * ((A - 1) + ((A + 1) * cosOmega));
		a[2] = ((A + 1) + ((A - 1) * cosOmega) - (2 * Math.sqrt(A) * alpha));
	}
}
