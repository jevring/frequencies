// todo: not for commercial use! Not that I'm ever likely to use this for commercial use, but if you do, you cannot use this filter!

package net.jevring.frequencies.v2.filters.moogladders.ladder;

import net.jevring.frequencies.v2.filters.ModulatedFilter;
import net.jevring.frequencies.v2.math.Clamp;
import net.jevring.frequencies.v2.math.Interpolation;

import java.util.Arrays;

/**
 * https://github.com/ddiakopoulos/MoogLadders/blob/master/src/StilsonModel.h
 * <p>
 * The simplified nonlinear Moog filter is based on the full Huovilainen model,
 * with five nonlinear (Math.tanh) functions (4 first-order sections and a feedback).
 * Like the original, this model needs an oversampling factor of at least two when
 * these nonlinear functions are used to reduce possible aliasing. This model
 * maintains the ability to self oscillate when the feedback gain is >= 1.0.
 * <p>References: DAFX - Zolzer (ed) (2nd ed)
 * Original implementation: Valimaki, Bilbao, Smith, Abel, Pakarinen, Berners (DAFX)
 * This is a transliteration into C++ of the original matlab source (moogvcf.m)
 * <p>Considerations for oversampling:
 * http://music.columbia.edu/pipermail/music-dsp/2005-February/062778.html
 * http://www.synthmaker.co.uk/dokuwiki/doku.php?id=tutorials:oversampling
 *
 * @author markus@jevring.net
 * @created 2020-02-21 22:45
 */
public class Simplified4PoleLadderFilter extends AbstractLadderFilter implements ModulatedFilter {
	private double output;
	private double lastStage;

	private double[] stage = new double[4];
	private double[] stageZ1 = new double[4];
	private double[] stageTanh = new double[3];

	private double h;
	private double h0;
	private double g;

	double gainCompensation;

	public Simplified4PoleLadderFilter(double sampleRate) {
		super(sampleRate);
		// To keep the overall level approximately constant, comp should be set
		// to 0.5 resulting in a 6 dB passband gain decrease at the maximum resonance
		// (compared to a 12 dB decrease in the original Moog model
		gainCompensation = 0.5;
	}

	@Override
	public void setCutoffFrequency(double c) {
		// todo: This craps out when the cut-off frequency is too high, but it's not entirely clear what "too high" means
		cutoffFrequency = Math.min(c, sampleRate / 10d);

		// Not being oversampled at the moment... * 2 when functional
		double fs2 = sampleRate;

		// Normalized cutoff [0, 1] in radians: ((2*pi) * cutoff / samplerate)
		g = (2 * Math.PI) * cutoffFrequency / fs2; // feedback coefficient at fs*2 because of doublesampling
		g *= Math.PI / 1.3; // correction factor that allows _cutoff to be supplied Hertz

		// FIR part with gain g
		h = g / 1.3;
		h0 = g * 0.3 / 1.3;
	}

	@Override
	public double getMaxResonance() {
		// No idea what this should be. No docs
		return 10d;
	}

	@Override
	public void reset() {
		Arrays.fill(stage, 0);
		Arrays.fill(stageZ1, 0);
		Arrays.fill(stageTanh, 0);
		output = 0;
	}

	@Override
	public double[] apply(double[] input, double[] frequencyModulation, double[] resonanceModulation) {
		double initialCutoffFrequency = this.cutoffFrequency;
		double initialResonance = this.resonance;
		double[] samples = new double[input.length];
		System.arraycopy(input, 0, samples, 0, input.length);
		// Processing still happens at sample rate...
		for (int s = 0; s < input.length; ++s) {
			setCutoffFrequency(Clamp.clamp(initialCutoffFrequency + (frequencyModulation[s] * CUTOFF_FREQUENCY_MODULATION_RANGE), 20, 20_000));
			setResonance(initialResonance + Interpolation.linear(0, 1, resonanceModulation[s], getMinResonance(), getMaxResonance()));
			loop(samples, s);
		}
		setCutoffFrequency(initialCutoffFrequency);
		setResonance(initialResonance);
		return samples;
	}

	/**
	 * This system is nonlinear so we are probably going to create a signal with components that exceed nyquist.
	 * To prevent aliasing distortion, we need to oversample this processing chunk. Where do these extra samples
	 * come from? Todo! We can use polynomial interpolation to generate the extra samples, but this is expensive.
	 * The cheap solution is to zero-stuff the incoming sample buffer.
	 * With resampling, numSamples should be 2x the frame size of the existing sample rate.
	 * The output of this filter needs to be run through a decimator to return to the original samplerate.
	 */
	@Override
	public double[] apply(double[] input) {
		double[] samples = new double[input.length];
		System.arraycopy(input, 0, samples, 0, input.length);
		// Processing still happens at sample rate...
		for (int s = 0; s < input.length; ++s) {
			loop(samples, s);
		}
		return samples;
	}

	private void loop(double[] samples, int s) {
		for (int stageIdx = 0; stageIdx < 4; ++stageIdx) {
			if (stageIdx != 0) {
				double in = stage[stageIdx - 1];
				stageTanh[stageIdx - 1] = Math.tanh(in);
				stage[stageIdx] = (h * stageZ1[stageIdx] + h0 * stageTanh[stageIdx - 1]) +
				                  (1.0 - g) * (stageIdx != 3 ? stageTanh[stageIdx] : Math.tanh(stageZ1[stageIdx]));
			} else {
				double in = samples[s] - ((4.0 * resonance) * (output - gainCompensation * samples[s]));
				stage[stageIdx] = (h * Math.tanh(in) + h0 * stageZ1[stageIdx]) + (1.0 - g) * stageTanh[stageIdx];
			}

			stageZ1[stageIdx] = stage[stageIdx];
		}

		output = stage[3];
		output = snapToZero(output);
		samples[s] = output;
	}
}
