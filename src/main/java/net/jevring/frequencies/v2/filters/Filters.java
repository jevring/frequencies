/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.filters;

import net.jevring.frequencies.v2.filters.juce.BandPassJuceIIRFilter;
import net.jevring.frequencies.v2.filters.juce.HighPassJuceIIRFilter;
import net.jevring.frequencies.v2.filters.juce.LowPassJuceIIRFilter;
import net.jevring.frequencies.v2.filters.moogladders.biquad.BandPassRBJFilter;
import net.jevring.frequencies.v2.filters.moogladders.ladder.RKSimulation4PoleLadderFilter;
import net.jevring.frequencies.v2.filters.moogladders.ladder.Simplified4PoleLadderFilter;
import net.jevring.frequencies.v2.filters.moogladders.ladder.Stilson4PoleLadderFilter;
import net.jevring.frequencies.v2.filters.open303.Open303TeeBeeFilter;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

/**
 * Keeps track of all the filters.
 *
 * @author markus@jevring.net
 * @created 2020-02-23 21:46
 */
public class Filters {
	private final Map<String, Supplier<Filter>> suppliers = new LinkedHashMap<>();

	public Filters(double sampleRate) {
//      This isn't great. It's just there for experimentation.
//		suppliers.put("ChatGPTResonantLowPassFilter", () -> new ChatGPTResonantLowPassFilter(sampleRate));

		// 4-pole ladder filters (moog ladders)
		suppliers.put("Stilson4PoleLadderFilter", () -> new Stilson4PoleLadderFilter(sampleRate));
		suppliers.put("Simplified4PoleLadderFilter", () -> new Simplified4PoleLadderFilter(sampleRate));
		suppliers.put("RKSimulation4PoleLadderFilter", () -> new RKSimulation4PoleLadderFilter(sampleRate));
//		suppliers.put("DAngeloValimaki4PoleLadderFilter", () -> new DAngeloValimaki4PoleLadderFilter(sampleRate));
//		suppliers.put("Oberheim4PoleLadderFilter", () -> new Oberheim4PoleLadderFilter(sampleRate));
//		suppliers.put("MusicDSP4PoleLadderFilter", () -> new MusicDSP4PoleLadderFilter(sampleRate));
//		suppliers.put("Krajeski4PoleLadderFilter", () -> new Krajeski4PoleLadderFilter(sampleRate));
//		suppliers.put("Microtracker4PoleLadderFilter", () -> new Microtracker4PoleLadderFilter(sampleRate));

		suppliers.put("DiodeLadderFilter", () -> new DiodeLadderFilter(sampleRate));

		// JUCE IIR-derived filters of all kinds
		suppliers.put("LowPassJuceIIRFilter", () -> new LowPassJuceIIRFilter(sampleRate));
		suppliers.put("HighPassJuceIIRFilter", () -> new HighPassJuceIIRFilter(sampleRate));
		//suppliers.put("AllPassJuceIIRFilter", () -> new AllPassJuceIIRFilter(sampleRate));
		suppliers.put("BandPassJuceIIRFilter", () -> new BandPassJuceIIRFilter(sampleRate));
		//suppliers.put("NotchJuceIIRFilter", () -> new NotchJuceIIRFilter(sampleRate));

		// Does nothing
		suppliers.put("NoopFilter", NoopFilter::new);

		// Open303
		suppliers.put("Open303", () -> new Open303TeeBeeFilter(sampleRate));

//		// RC Filters derived from the wikipedia pages on low-pass and high-pass filters. 
//		// 0 poles, no memory
//		suppliers.put("WikipediaRCLowPassFilter", () -> new WikipediaRCLowPassFilter(sampleRate));
//		suppliers.put("WikipediaRCHighPassFilter", () -> new WikipediaRCHighPassFilter(sampleRate));
//
//		// Even simpler 0-pole, no memory, normalized cut-off frequency filters. 
//		// Only accepts normalized values for cut-off. No semantic values, other than potentially 0..0.5 -> 0..nyquist
//		suppliers.put("AlphaLowPassFilter", AlphaLowPassFilter::new);
//		suppliers.put("AlphaHighPassFilter", AlphaHighPassFilter::new);
//
//		// Moog-ladders various Robert Bristow-Johnson (RBJ) filters.
//		// https://www.w3.org/2011/audio/audio-eq-cookbook.html
//		suppliers.put("AllPassRBJFilter", () -> new AllPassRBJFilter(sampleRate));
		// The RBJ band pass sounds better than the juce band-pass. 
		// If we we ever decide on just one, it'll likely be the RBJ one.
		suppliers.put("BandPassRBJFilter", () -> new BandPassRBJFilter(sampleRate));
//		suppliers.put("HighPassRBJFilter", () -> new HighPassRBJFilter(sampleRate));
//		suppliers.put("HighShelfRBJFilter", () -> new HighShelfRBJFilter(sampleRate));
//		suppliers.put("LowPassRBJFilter", () -> new LowPassRBJFilter(sampleRate));
//		suppliers.put("LowShelfRBJFilter", () -> new LowShelfRBJFilter(sampleRate));
//		suppliers.put("NotchRBJFilter", () -> new NotchRBJFilter(sampleRate));
//		suppliers.put("PeakRBJFilter", () -> new PeakRBJFilter(sampleRate));
	}

	public Filter create(String name) {
		return suppliers.get(name).get();
	}

	public List<String> all() {
		return suppliers.keySet().stream().toList();
	}
}
