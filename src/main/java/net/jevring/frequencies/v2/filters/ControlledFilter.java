/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.filters;

import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.control.ControlListener;
import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.control.DiscreteControlListener;
import net.jevring.frequencies.v2.filters.alpha.AlphaFilter;
import net.jevring.frequencies.v2.math.Interpolation;

/**
 * A filter that automatically configures its controls.
 *
 * @author markus@jevring.net
 * @created 2020-06-14 00:12
 */
public class ControlledFilter implements ModulatedFilter {
	private volatile Filter filter = new NoopFilter();

	public ControlledFilter(String filterControlPrefix, Controls controls, Filters filters) {
		Control resonanceControl = controls.getControl(controlName(filterControlPrefix, "q-resonance-emphasis"));
		resonanceControl.addListener(new ControlListener() {
			@Override
			public void valueChanged(double min, double newValue, double max, Object source) {
				if (filter instanceof QFilter qFilter) {
					qFilter.setResonance(Interpolation.linear(min, max, newValue, qFilter.getMinResonance(), qFilter.getMaxResonance()));
				}
			}
		});
		Control filterCutoffFrequencyControl = controls.getControl(controlName(filterControlPrefix, "filter-cutoff-frequency"));
		filterCutoffFrequencyControl.addListener(new ControlListener() {
			@Override
			public void valueChanged(double min, double newValue, double max, Object source) {
				if (filter instanceof CutoffFrequencyFilter cutoffFrequencyFilter) {
					cutoffFrequencyFilter.setCutoffFrequency(newValue);
				} else if (filter instanceof AlphaFilter alphaFilter) {
					double alpha = Interpolation.linear(min, max, newValue, 0, 0.5);
					alphaFilter.setAlpha(alpha);
				}
			}
		});
		// adding the listener will call it back, which will set the initial value of the filter
		controls.getDiscreteControl(controlName(filterControlPrefix, "filter")).mapping(filters::create).addListener(new DiscreteControlListener<>() {
			@Override
			public void valueChanged(Filter selectedFilter, Object source) {
				filter = selectedFilter;
				filterCutoffFrequencyControl.updateListeners(this);
				resonanceControl.updateListeners(this);
			}
		});
	}

	private String controlName(String filterControlPrefix, String controlSuffix) {
		if (filterControlPrefix == null) {
			return controlSuffix;
		} else {
			return filterControlPrefix + "-" + controlSuffix;
		}
	}

	@Override
	public double[] apply(double[] input, double[] frequencyModulation, double[] resonanceModulation) {
		if (filter instanceof ModulatedFilter modulatedFilter) {
			return modulatedFilter.apply(input, frequencyModulation, resonanceModulation);
		} else {
			return apply(input);
		}
	}

	@Override
	public double[] apply(double[] input) {
		return filter.apply(input);
	}

	@Override
	public void reset() {
		filter.reset();
	}
}
