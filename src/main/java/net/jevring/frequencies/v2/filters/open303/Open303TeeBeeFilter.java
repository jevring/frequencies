package net.jevring.frequencies.v2.filters.open303;

import net.jevring.frequencies.v2.filters.AbstractFilter;
import net.jevring.frequencies.v2.filters.ModulatedFilter;
import net.jevring.frequencies.v2.math.Clamp;
import net.jevring.frequencies.v2.math.Interpolation;

/**
 * This class is a filter that aims to emulate the filter in the Roland TB 303. It's a variation of
 * the Moog ladder filter which includes a highpass in the feedback path that reduces the resonance
 * on low cutoff frequencies. Moreover, it has a highpass and an allpass filter in the input path to
 * pre-shape the input signal (important for the sonic character of internal and subsequent
 * nonlinearities).
 * <p>
 * ...18 vs. 24 dB? blah?
 *
 * @author markus@jevring.net
 * @created 2020-03-25 21:02
 */
public class Open303TeeBeeFilter extends AbstractFilter implements ModulatedFilter {
	private static final double SQRT2 = 1.4142135623730950488016887242097;
	private static final double ONE_OVER_SQRT2 = 0.70710678118654752440084436210485;
	double b0, a1;              // coefficients for the first order sections
	double y1, y2, y3, y4;      // output signals of the 4 filter stages 
	double c0, c1, c2, c3, c4;  // coefficients for combining various ouput stages
	double k;                   // feedback factor in the loop
	double g;                   // output gain
	double driveFactor;         // filter drive as raw factor
	double drive;               // filter drive in decibels
	double resonanceRaw;        // resonance parameter (normalized to 0...1)
	double resonanceSkewed;     // mapped resonance parameter to make it behave more musical
	double twoPiOverSampleRate; // 2*PI/sampleRate
	Mode mode;                // the selected filter-mode

	OnePoleFilter feedbackHighpass = new OnePoleFilter();

	public Open303TeeBeeFilter(double sampleRate) {
		super(sampleRate);
		setCutoffFrequency(1000.0);
		drive = 0.0;
		driveFactor = 1.0;
		resonanceRaw = 0.0;
		resonanceSkewed = 0.0;
		g = 1.0;
		twoPiOverSampleRate = 2.0 * Math.PI / sampleRate;

		feedbackHighpass.setMode(OnePoleFilter.Mode.HIGHPASS);
		feedbackHighpass.setCutoff(150.0);

		//setMode(Mode.LP_18);
		setMode(Mode.TB_303);
		calculateCoefficientsExact();
		reset();
	}

	@Override
	public double[] apply(double[] input, double[] frequencyModulation, double[] resonanceModulation) {
		double initialCutoffFrequency = cutoffFrequency;
		double initialResonance = getResonance();
		double[] output = new double[input.length];
		for (int i = 0; i < input.length; i++) {
			setCutoffFrequency(initialCutoffFrequency + (frequencyModulation[i] * CUTOFF_FREQUENCY_MODULATION_RANGE));
			setResonance(initialResonance + Interpolation.linear(0, 1, resonanceModulation[i], getMinResonance(), getMaxResonance()));
			output[i] = getSample(input[i]);
		}
		setCutoffFrequency(initialCutoffFrequency);
		setResonance(initialResonance);
		return output;
	}

	@Override
	public double[] apply(double[] input) {
		double[] output = new double[input.length];
		for (int i = 0; i < input.length; i++) {
			output[i] = getSample(input[i]);
		}
		return output;
	}

	private static double dB2amp(double dB) {
		return Math.exp(dB * 0.11512925464970228420089957273422);
		//return pow(10.0, (0.05*dB)); // naive, inefficient version
	}

	void setDrive(double newDrive) {
		drive = newDrive;
		driveFactor = dB2amp(drive);
	}

	void setMode(Mode newMode) {
		mode = newMode;
		switch (mode) {
			//@formatter:off
			case FLAT:      c0 =  1.0; c1 =  0.0; c2 =  0.0; c3 =  0.0; c4 =  0.0;  break;
			case LP_6:      c0 =  0.0; c1 =  1.0; c2 =  0.0; c3 =  0.0; c4 =  0.0;  break;
			case LP_12:     c0 =  0.0; c1 =  0.0; c2 =  1.0; c3 =  0.0; c4 =  0.0;  break;
			case LP_18:     c0 =  0.0; c1 =  0.0; c2 =  0.0; c3 =  1.0; c4 =  0.0;  break;
			case LP_24:     c0 =  0.0; c1 =  0.0; c2 =  0.0; c3 =  0.0; c4 =  1.0;  break;
			case HP_6:      c0 =  1.0; c1 = -1.0; c2 =  0.0; c3 =  0.0; c4 =  0.0;  break;
			case HP_12:     c0 =  1.0; c1 = -2.0; c2 =  1.0; c3 =  0.0; c4 =  0.0;  break;
			case HP_18:     c0 =  1.0; c1 = -3.0; c2 =  3.0; c3 = -1.0; c4 =  0.0;  break;
			case HP_24:     c0 =  1.0; c1 = -4.0; c2 =  6.0; c3 = -4.0; c4 =  1.0;  break;
			case BP_12_12:  c0 =  0.0; c1 =  0.0; c2 =  1.0; c3 = -2.0; c4 =  1.0;  break;
			case BP_6_18:   c0 =  0.0; c1 =  0.0; c2 =  0.0; c3 =  1.0; c4 = -1.0;  break;
			case BP_18_6:   c0 =  0.0; c1 =  1.0; c2 = -3.0; c3 =  3.0; c4 = -1.0;  break;
			case BP_6_12:   c0 =  0.0; c1 =  0.0; c2 =  1.0; c3 = -1.0; c4 =  0.0;  break;
			case BP_12_6:   c0 =  0.0; c1 =  1.0; c2 = -2.0; c3 =  1.0; c4 =  0.0;  break;
			case BP_6_6:    c0 =  0.0; c1 =  1.0; c2 = -1.0; c3 =  0.0; c4 =  0.0;  break;
			default:        c0 =  1.0; c1 =  0.0; c2 =  0.0; c3 =  0.0; c4 =  0.0;  // flat
			//@formatter:on
		}
		calculateCoefficientsApprox4();
	}

	public void reset() {
		feedbackHighpass.reset();
		y1 = 0.0;
		y2 = 0.0;
		y3 = 0.0;
		y4 = 0.0;
	}


	@Override
	public void setCutoffFrequency(double cutoffFrequency) {
		super.setCutoffFrequency(Clamp.clamp(cutoffFrequency, 200, 20_000));
		calculateCoefficientsApprox4();
	}

	/**
	 * Returns the resonance parameter of this filter.
	 */
	public double getResonance() {
		return 100.0 * resonanceRaw;
	}

	/**
	 * Sets the resonance in percent where 100% is self oscillation.
	 */
	public void setResonance(double newResonance) {
		resonanceRaw = 0.01 * newResonance;
		resonanceSkewed = (1.0 - Math.exp(-3.0 * resonanceRaw)) / (1.0 - Math.exp(-3.0));
		calculateCoefficientsApprox4();
	}

	@Override
	public double getMaxResonance() {
		return 100d;
	}

	void calculateCoefficientsExact() {
		// calculate intermediate variables:
		double wc = twoPiOverSampleRate * cutoffFrequency;
		double s, c;
		s = Math.sin(wc);
		c = Math.cos(wc);
		//sinCos(wc, &s, &c);             // c = cos(wc); s = sin(wc);
		double t = Math.tan(0.25 * (wc - Math.PI));
		double r = resonanceSkewed;

		// calculate filter a1-coefficient tuned such the resonance frequency is just right:
		double a1_fullRes = t / (s - c * t);

		// calculate filter a1-coefficient as if there were no resonance:
		double x = Math.exp(-wc);
		double a1_noRes = -x;

		// use a weighted sum between the resonance-tuned and no-resonance coefficient:
		a1 = r * a1_fullRes + (1.0 - r) * a1_noRes;

		// calculate the b0-coefficient from the condition that each stage should be a leaky
		// integrator:
		b0 = 1.0 + a1;

		// calculate feedback factor by dividing the resonance parameter by the magnitude at the
		// resonant frequency:
		double gsq = b0 * b0 / (1.0 + a1 * a1 + 2.0 * a1 * c);
		k = r / (gsq * gsq);

		if (mode == Mode.TB_303) {
			k *= (17.0 / 4.0);
		}
	}

	void calculateCoefficientsApprox4() {
		// calculate intermediate variables:
		double wc = twoPiOverSampleRate * cutoffFrequency;
		double wc2 = wc * wc;
		double r = resonanceSkewed;
		double tmp;

		// compute the filter coefficient via a 12th order polynomial approximation (polynomial 
		// evaluation is done with a Horner-rule alike scheme with nested quadratic factors in the hope
		// for potentially better parallelization compared to Horner's rule as is):
		double pa12 = -1.341281325101042e-02;
		double pa11 = 8.168739417977708e-02;
		double pa10 = -2.365036766021623e-01;
		double pa09 = 4.439739664918068e-01;
		double pa08 = -6.297350825423579e-01;
		double pa07 = 7.529691648678890e-01;
		double pa06 = -8.249882473764324e-01;
		double pa05 = 8.736418933533319e-01;
		double pa04 = -9.164580250284832e-01;
		double pa03 = 9.583192455599817e-01;
		double pa02 = -9.999994950291231e-01;
		double pa01 = 9.999999927726119e-01;
		double pa00 = -9.999999999857464e-01;
		tmp = wc2 * pa12 + pa11 * wc + pa10;
		tmp = wc2 * tmp + pa09 * wc + pa08;
		tmp = wc2 * tmp + pa07 * wc + pa06;
		tmp = wc2 * tmp + pa05 * wc + pa04;
		tmp = wc2 * tmp + pa03 * wc + pa02;
		a1 = wc2 * tmp + pa01 * wc + pa00;
		b0 = 1.0 + a1;

		// compute the scale factor for the resonance parameter (the factor to obtain k from r) via an
		// 8th order polynomial approximation:
		double pr8 = -4.554677015609929e-05;
		double pr7 = -2.022131730719448e-05;
		double pr6 = 2.784706718370008e-03;
		double pr5 = 2.079921151733780e-03;
		double pr4 = -8.333236384240325e-02;
		double pr3 = -1.666668203490468e-01;
		double pr2 = 1.000000012124230e+00;
		double pr1 = 3.999999999650040e+00;
		double pr0 = 4.000000000000113e+00;
		tmp = wc2 * pr8 + pr7 * wc + pr6;
		tmp = wc2 * tmp + pr5 * wc + pr4;
		tmp = wc2 * tmp + pr3 * wc + pr2;
		tmp = wc2 * tmp + pr1 * wc + pr0; // this is now the scale factor
		k = r * tmp;
		g = 1.0;

		if (mode == Mode.TB_303) {
			double fx = wc * ONE_OVER_SQRT2 / (2 * Math.PI);
			b0 = (0.00045522346 + 6.1922189 * fx) / (1.0 + 12.358354 * fx + 4.4156345 * (fx * fx));
			k = fx * (fx * (fx * (fx * (fx * (fx + 7198.6997) - 5837.7917) - 476.47308) + 614.95611) + 213.87126) + 16.998792;
			g = k * 0.058823529411764705882352941176471; // 17 reciprocal 
			g = (g - 1.0) * r + 1.0;                     // r is 0 to 1.0
			g = (g * (1.0 + r));
			k = k * r;                                   // k is ready now 
		}
	}

	double shape(double x) {
		// return tanhApprox(x); // \todo: find some more suitable nonlinearity here
		//return x; // test

		double r6 = 1.0 / 6.0;
		x = Clamp.clamp(x, -SQRT2, SQRT2);
		return x - r6 * x * x * x;

		//return Clamp.clamp(x, -1.0, 1.0);
	}

	double getSample(double in) {
		double y0;

		if (mode == Mode.TB_303) {
			//y0  = in - feedbackHighpass.getSample(k * shape(y4));  
			y0 = in - feedbackHighpass.getSample(k * y4);
			//y0  = in - k*shape(y4);  
			//y0  = in-k*y4;  
			y1 += 2 * b0 * (y0 - y1 + y2);
			y2 += b0 * (y1 - 2 * y2 + y3);
			y3 += b0 * (y2 - 2 * y3 + y4);
			y4 += b0 * (y3 - 2 * y4);
			return 2 * g * y4;
			//return 3*y4;
		}

		// apply drive and feedback to obtain the filter's input signal:
		//double y0 = inputFilter.getSample(0.125*driveFactor*in) - feedbackHighpass.getSample(k*y4);
		y0 = 0.125 * driveFactor * in - feedbackHighpass.getSample(k * y4);  

    /*
    // cascade of four 1st order sections with nonlinearities:
    y1 = shape(b0*y0 - a1*y1);
    y2 = shape(b0*y1 - a1*y2);
    y3 = shape(b0*y2 - a1*y3);
    y4 = shape(b0*y3 - a1*y4);
    */

		// cascade of four 1st order sections with only 1 nonlinearity:
    /*
    y1 =       b0*y0 - a1*y1;
    y2 =       b0*y1 - a1*y2;
    y3 =       b0*y2 - a1*y3;
    y4 = shape(b0*y3 - a1*y4);
    */
		y1 = y0 + a1 * (y0 - y1);
		y2 = y1 + a1 * (y1 - y2);
		y3 = y2 + a1 * (y2 - y3);
		y4 = y3 + a1 * (y3 - y4); // \todo: performance test both versions of the ladder
		//y4 = shape(y3 + a1*(y3-y4)); // \todo: performance test both versions of the ladder

		return 8.0 * (c0 * y0 + c1 * y1 + c2 * y2 + c3 * y3 + c4 * y4);
	}

	/**
	 * Enumeration of the available filter modes.
	 */
	enum Mode {
		FLAT,
		LP_6,
		LP_12,
		LP_18,
		LP_24,
		HP_6,
		HP_12,
		HP_18,
		HP_24,
		BP_12_12,
		BP_6_18,
		BP_18_6,
		BP_6_12,
		BP_12_6,
		BP_6_6,
		TB_303,      // ala mystran & kunn (page 40 in the kvr-thread)
	}
}
