/**
 * The filters in here are ported from the ISC-licensed filters from JUCE.
 *
 * @author markus@jevring.net
 * @created 2020-02-22 18:05
 */
package net.jevring.frequencies.v2.filters.juce;