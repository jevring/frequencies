/**
 * This whole package is a port of the filters found in the <a href="https://github.com/ddiakopoulos/MoogLadders">MoogLadders github repo</a>.
 * We've only ported the ones that have a sufficiently permissive license, but that's almost all of them.
 * <p>
 * From the original github repository:
 * <blockquote>
 * This project contains different digital implementations of the classic 4-pole, 24 dB/octave analog filter introduced in 1965.
 * The filter is well-regarded to add a nice character to any sound source, either synthesized or acoustic.
 * <p>
 * The ladder structure consists of four one-pole filters and a global negative feedback loop.
 * </blockquote>
 *
 * @author markus@jevring.net
 * @created 2020-02-21 22:39
 */
package net.jevring.frequencies.v2.filters.moogladders;