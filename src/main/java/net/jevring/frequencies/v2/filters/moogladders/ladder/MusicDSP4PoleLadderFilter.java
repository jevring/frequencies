package net.jevring.frequencies.v2.filters.moogladders.ladder;

import java.util.Arrays;

/**
 * https://github.com/ddiakopoulos/MoogLadders/blob/master/src/MusicDSPModel.h
 *
 * @author markus@jevring.net
 * @created 2020-02-21 22:45
 */
public class MusicDSP4PoleLadderFilter extends AbstractLadderFilter {
	// todo: this produces absolutely no sound what so ever. I wonder what's wrong?

	private double p;
	private double k;
	private double t1;
	private double t2;
	private double[] delay = new double[4];
	private double[] stage = new double[4];

	public MusicDSP4PoleLadderFilter(double sampleRate) {
		super(sampleRate);
		setCutoffFrequency(1000);
		setResonance(0.1);
	}

	@Override
	public void reset() {
		Arrays.fill(delay, 0);
		Arrays.fill(stage, 0);
	}

	@Override
	public void setResonance(double r) {
		resonance = r * (t2 + 6.0 * t1) / (t2 - 6.0 * t1);
	}

	@Override
	public double getMaxResonance() {
		// no idea what this should be. no docs
		return 20d;
	}

	@Override
	public void setCutoffFrequency(double c) {
		cutoffFrequency = 2.0 * c / sampleRate;

		p = cutoffFrequency * (1.8 - 0.8 * cutoffFrequency);
		k = 2.0 * Math.sin(cutoffFrequency * Math.PI * 0.5) - 1.0;
		t1 = (1.0 - p) * 1.386249;
		t2 = 12.0 + t1 * t1;

		setResonance(resonance);
	}

	@Override
	public double[] apply(double[] input) {
		double[] samples = new double[input.length];
		System.arraycopy(input, 0, samples, 0, input.length);
		for (int s = 0; s < input.length; ++s) {
			double x = samples[s] - resonance * stage[3];

			// Four cascaded one-pole filters (bilinear transform)
			stage[0] = x * p + delay[0] * p - k * stage[0];
			stage[1] = stage[0] * p + delay[1] * p - k * stage[1];
			stage[2] = stage[1] * p + delay[2] * p - k * stage[2];
			stage[3] = stage[2] * p + delay[3] * p - k * stage[3];

			// Clipping band-limited sigmoid
			stage[3] -= (stage[3] * stage[3] * stage[3]) / 6.0;

			delay[0] = x;
			delay[1] = stage[0];
			delay[2] = stage[1];
			delay[3] = stage[2];

			samples[s] = stage[3];
		}
		return samples;
	}
}
