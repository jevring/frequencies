package net.jevring.frequencies.v2.filters.moogladders.biquad;

import net.jevring.frequencies.v2.filters.AbstractFilter;
import net.jevring.frequencies.v2.filters.ModulatedFilter;
import net.jevring.frequencies.v2.math.Clamp;
import net.jevring.frequencies.v2.math.Interpolation;

import java.util.Arrays;

/**
 * @author markus@jevring.net
 * @created 2020-02-22 00:34
 */
public abstract class AbstractBiQuadFilter extends AbstractFilter implements ModulatedFilter {
	/**
	 * b0, b1, b2
	 */
	protected double[] bCoef = new double[3];
	/**
	 * a1, a2
	 */
	protected double[] aCoef = new double[2];
	/**
	 * delays
	 */
	protected double[] w = new double[2];

	public AbstractBiQuadFilter(double sampleRate) {
		super(sampleRate);
	}

	@Override
	public void reset() {
		Arrays.fill(w, 0);
	}

	@Override
	public double[] apply(double[] input, double[] frequencyModulation, double[] resonanceModulation) {
		double initialCutoffFrequency = this.cutoffFrequency;
		double initialResonance = this.resonance;
		double[] samples = new double[input.length];
		System.arraycopy(input, 0, samples, 0, input.length);
		for (int s = 0; s < input.length; ++s) {
			setCutoffFrequency(Clamp.clamp(initialCutoffFrequency + (frequencyModulation[s] * CUTOFF_FREQUENCY_MODULATION_RANGE), 20, 20_000));
			setResonance(initialResonance + Interpolation.linear(0, 1, resonanceModulation[s], getMinResonance(), getMaxResonance()));
			loop(samples, s);
		}
		setCutoffFrequency(initialCutoffFrequency);
		setResonance(initialResonance);
		return samples;
	}

	private void loop(double[] samples, int s) {
		double out;
		out = bCoef[0] * samples[s] + w[0];
		w[0] = bCoef[1] * samples[s] - aCoef[0] * out + w[1];
		w[1] = bCoef[2] * samples[s] - aCoef[1] * out;
		samples[s] = out;
	}

	// DF-II impl
	@Override
	public double[] apply(double[] input) {
		double[] samples = new double[input.length];
		System.arraycopy(input, 0, samples, 0, input.length);
		for (int s = 0; s < input.length; ++s) {
			loop(samples, s);
		}
		return samples;
	}

	double tick(double s) {
		double out = bCoef[0] * s + w[0];
		w[0] = bCoef[1] * s - aCoef[0] * out + w[1];
		w[1] = bCoef[2] * s - aCoef[1] * out;
		return out;
	}

	void setBiquadCoefs(double[] b, double[] a) {
		bCoef = b;
		aCoef = a;
	}
}
