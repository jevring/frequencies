package net.jevring.frequencies.v2.filters.moogladders.biquad;

/**
 * https://github.com/ddiakopoulos/MoogLadders/blob/master/src/Filters.h
 *
 * @author markus@jevring.net
 * @created 2020-02-22 01:24
 */
public class NotchRBJFilter extends AbstractRBJFilter {

	public NotchRBJFilter(double sampleRate) {
		super(sampleRate);
	}

	@Override
	protected void typeDependentUpdateCoefficient() {
		alpha = sinOmega * Math.sinh(Math.log(2.0) / 2.0 * Q * omega / sinOmega);
		b[0] = 1;
		b[1] = -2 * cosOmega;
		b[2] = 1;
		a[0] = 1 + alpha;
		a[1] = b[1];
		a[2] = 1 - alpha;
	}
}
