package net.jevring.frequencies.v2.filters.moogladders.biquad;

/**
 * https://github.com/ddiakopoulos/MoogLadders/blob/master/src/Filters.h
 *
 * @author markus@jevring.net
 * @created 2020-02-22 01:24
 */
public class BandPassRBJFilter extends AbstractRBJFilter {

	public BandPassRBJFilter(double sampleRate) {
		super(sampleRate);
	}

	@Override
	public double getMaxResonance() {
		return 2;
	}

	@Override
	protected void typeDependentUpdateCoefficient() {
		alpha = sinOmega * Math.sinh(Math.log(2.0) / 2.0 * Q * omega / sinOmega);
		b[0] = sinOmega / 2;
		b[1] = 0;
		b[2] = -b[0];
		a[0] = 1 + alpha;
		a[1] = -2 * cosOmega;
		a[2] = 1 - alpha;
	}
}
