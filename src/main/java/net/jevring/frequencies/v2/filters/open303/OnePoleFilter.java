package net.jevring.frequencies.v2.filters.open303;

/**
 * From https://sourceforge.net/projects/open303/
 *
 * @author markus@jevring.net
 * @created 2020-03-25 20:54
 */
public class OnePoleFilter {


	// buffering:
	double x1, y1;

	// filter coefficients:
	double b0; // feedforward coeffs
	double b1;
	double a1; // feedback coeff

	// filter parameters:
	double cutoff;
	double shelvingGain;
	Mode mode = Mode.BYPASS;

	double sampleRate;
	double sampleRateRec;  // reciprocal of the sampleRate

	public OnePoleFilter() {
		setShelvingGain(1.0);
		setSampleRate(44100.0);  // sampleRate = 44100 Hz by default
		setMode(Mode.BYPASS);        // bypass by default
		setCutoff(20000.0);  // cutoff = 20000 Hz by default
		reset();
	}

	void setSampleRate(double newSampleRate) {
		if (newSampleRate > 0.0) {
			sampleRate = newSampleRate;
		}
		sampleRateRec = 1.0 / sampleRate;

		calcCoeffs();
	}

	void setMode(Mode newMode) {
		mode = newMode; // 0:bypass, 1:Low Pass, 2:High Pass
		calcCoeffs();
	}

	void setCutoff(double newCutoff) {
		if ((newCutoff > 0.0) && (newCutoff <= 20000.0)) {
			cutoff = newCutoff;
		} else {
			cutoff = 20000.0;
		}

		calcCoeffs();
	}

	void setShelvingGain(double newGain) {
		if (newGain > 0.0) {
			shelvingGain = newGain;
			calcCoeffs();
		}
		//else
		//DEBUG_BREAK; // this is a linear gain factor and must be >= 0.0
	}

	void setShelvingGainInDecibels(double newGain) {
		setShelvingGain(dB2amp(newGain));
	}

	void setCoefficients(double newB0, double newB1, double newA1) {
		b0 = newB0;
		b1 = newB1;
		a1 = newA1;
	}

	void setInternalState(double newX1, double newY1) {
		x1 = newX1;
		y1 = newY1;
	}

	void calcCoeffs() {
		switch (mode) {
			case LOWPASS: {
				// formula from dspguide:
				double x = Math.exp(-2.0 * Math.PI * cutoff * sampleRateRec);
				b0 = 1 - x;
				b1 = 0.0;
				a1 = x;
			}
			break;
			case HIGHPASS: {
				// formula from dspguide:
				double x = Math.exp(-2.0 * Math.PI * cutoff * sampleRateRec);
				b0 = 0.5 * (1 + x);
				b1 = -0.5 * (1 + x);
				a1 = x;
			}
			break;
			case LOWSHELV: {
				// formula from DAFX:
				double c = 0.5 * (shelvingGain - 1.0);
				double t = Math.tan(Math.PI * cutoff * sampleRateRec);
				double a;
				if (shelvingGain >= 1.0) {
					a = (t - 1.0) / (t + 1.0);
				} else {
					a = (t - shelvingGain) / (t + shelvingGain);
				}

				b0 = 1.0 + c + c * a;
				b1 = c + c * a + a;
				a1 = -a;
			}
			break;
			case HIGHSHELV: {
				// formula from DAFX:
				double c = 0.5 * (shelvingGain - 1.0);
				double t = Math.tan(Math.PI * cutoff * sampleRateRec);
				double a;
				if (shelvingGain >= 1.0) {
					a = (t - 1.0) / (t + 1.0);
				} else {
					a = (shelvingGain * t - 1.0) / (shelvingGain * t + 1.0);
				}

				b0 = 1.0 + c - c * a;
				b1 = a + c * a - c;
				a1 = -a;
			}
			break;

			case ALLPASS: {
				// formula from DAFX:
				double t = Math.tan(Math.PI * cutoff * sampleRateRec);
				double x = (t - 1.0) / (t + 1.0);

				b0 = x;
				b1 = 1.0;
				a1 = -x;
			}
			break;

			default: // bypass
			{
				b0 = 1.0;
				b1 = 0.0;
				a1 = 0.0;
			}
			break;
		}
	}

	void reset() {
		x1 = 0.0;
		y1 = 0.0;
	}

	private static double dB2amp(double dB) {
		return Math.exp(dB * 0.11512925464970228420089957273422);
		//return pow(10.0, (0.05*dB)); // naive, inefficient version
	}

	double getSample(double in) {
		// calculate the output sample:
		y1 = b0 * in + b1 * x1 + a1 * y1 + Double.MIN_VALUE;

		// update the buffer variables:
		x1 = in;

		return y1;
	}

	enum Mode {
		BYPASS,
		LOWPASS,
		HIGHPASS,
		LOWSHELV,
		HIGHSHELV,
		ALLPASS
	}
}
