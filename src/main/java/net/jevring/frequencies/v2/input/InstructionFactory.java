/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.input;

import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.control.MappingDiscreteControl;
import net.jevring.frequencies.v2.envelopes.Envelope;
import net.jevring.frequencies.v2.envelopes.Phase;
import net.jevring.frequencies.v2.input.midi.MidiFrequencyMapper;
import net.jevring.frequencies.v2.input.scale.Scale;

import java.util.Arrays;

/**
 * Created instructions from key timings.
 *
 * @author markus@jevring.net
 * @created 2021-05-24 12:41
 */
public class InstructionFactory {
	private final MidiFrequencyMapper midiFrequencyMapper;
	private final MappingDiscreteControl<Scale> scale;
	private final Control scaleRootNote;
	private final Envelope volumeEnvelope;

	public InstructionFactory(MidiFrequencyMapper midiFrequencyMapper, MappingDiscreteControl<Scale> scale, Control scaleRootNote, Envelope volumeEnvelope) {
		this.midiFrequencyMapper = midiFrequencyMapper;
		this.scale = scale;
		this.scaleRootNote = scaleRootNote;
		this.volumeEnvelope = volumeEnvelope;
	}

	public Instruction keyTimingToInstruction(KeyTiming keyTiming) {
		int key = keyTiming.key();
		int closestKeyInScale = getClosestKeyInScale(key);
		double frequency = midiFrequencyMapper.frequencyOf(closestKeyInScale);
		long age = keyTiming.nanosecondsActivated() + keyTiming.nanosecondsDeactivated();
		Phase volumeEnvelopePhase = volumeEnvelope.phase(keyTiming.nanosecondsActivated(), keyTiming.nanosecondsDeactivated());
		if (volumeEnvelopePhase != Phase.IDLE) {
			//System.out.printf("%d volume: %s filter: %s%n", key, volumeEnvelopePhase, filterEnvelopePhase);
			return new Instruction(key,
			                       age,
			                       frequency,
			                       false,
			                       volumeEnvelopePhase,
			                       keyTiming.nanosecondsActivated(),
			                       keyTiming.nanosecondsDeactivated(),
			                       keyTiming.velocity(),
			                       keyTiming.lastTimeDown());
		} else {
			return null;
		}
	}

	private int getClosestKeyInScale(int key) {
		int octave = key / 12;
		int note = key - (octave * 12);
		// To wrap around we will add with modulo and then sort, because otherwise we'll end up with an array like 10,12,2,4,6.
		// Sorting it is a lot easier than finding out where the change happens, and then rotating it.
		int[] includedNotes = Arrays.stream(scale.get().getIncludedNotes())
		                            .map(includedNote -> ((int) scaleRootNote.getCurrentValue() + includedNote) % 12)
		                            .sorted()
		                            .toArray();
		int invertedInsertionPoint = Arrays.binarySearch(includedNotes, note);
		if (invertedInsertionPoint >= 0) {
			// The notes include this one, so return the original key
			return key;
		} else {
			// The notes did NOT include this one, so we have to find the closest note in the scale, 
			// and calculate which key that would have been.

			int actualInsertionPoint = -invertedInsertionPoint - 1;
			int lower;
			int higher;
			if (actualInsertionPoint == includedNotes.length) {
				// This means it would have been appended, which means that we're looking at the first note, but in the higher octave
				lower = includedNotes[includedNotes.length - 1];
				higher = 12 + includedNotes[0];
			} else if (actualInsertionPoint == 0) {
				// This means that it would have been prepended, which means that we're looking at the last note in the lower octave
				lower = includedNotes[includedNotes.length - 1] - 12;
				higher = includedNotes[0];
			} else {
				lower = includedNotes[actualInsertionPoint - 1];
				higher = includedNotes[actualInsertionPoint];
			}
			int distanceToLower = note - lower;
			int distanceToHigher = higher - note;
			// todo: these distances will almost always be the same. How should we break the tie?
			if (distanceToLower < distanceToHigher) {
				return (octave * 12) + lower;
			} else {
				return (octave * 12) + higher;
			}
		}
	}
}
