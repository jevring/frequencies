/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.input.tempo;

import net.jevring.frequencies.v2.input.sequencer.Sequencer;

import java.util.HashSet;
import java.util.Set;

/**
 * Base class for tempo sources that handles listeners and rates etc.
 *
 * @author markus@jevring.net
 * @created 2021-02-28 11:50
 */
public abstract class AbstractTempoSource implements TempoSource {
	/**
	 * Order doesn't really matter, but what does matter is never having the same sequencer in here twice,
	 * as that would cause it to step through twice as fast
	 */
	protected final Set<Sequencer> sequencers = new HashSet<>();

	public void addSequencer(Sequencer sequencer) {
		if (!this.sequencers.add(sequencer)) {
			System.err.println(getClass().getSimpleName() + " already contained sequencer " + sequencer);
		}
	}

	public void removeSequencer(Sequencer sequencer) {
		if (!this.sequencers.remove(sequencer)) {
			System.err.println(getClass().getSimpleName() + " did not contain sequencer " + sequencer);
		}
	}
}
