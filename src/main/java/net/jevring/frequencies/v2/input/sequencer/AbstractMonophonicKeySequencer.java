/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.input.sequencer;

import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.hooks.SequencerVisualizer;
import net.jevring.frequencies.v2.input.KeyTimings;

import java.util.Arrays;

/**
 * Base class for all monophonic sequencers
 *
 * @author markus@jevring.net
 * @created 2021-03-20 15:25
 */
public abstract class AbstractMonophonicKeySequencer<T extends MonophonicSequencerStep, V extends SequencerVisualizer> extends AbstractPolyphonicKeySequencer<WrappingPolyphonicSequencerStep<T>, V> {
	protected AbstractMonophonicKeySequencer(T[] sequence, KeyTimings keyTimings, Controls controls, V sequenceVisualizer) {
		super(wrap(sequence), keyTimings, controls, 1, sequenceVisualizer);
	}

	@SuppressWarnings("unchecked")
	private static <M extends MonophonicSequencerStep> WrappingPolyphonicSequencerStep<M>[] wrap(M[] sequence) {
		// This is type unsafe, and that's unfortunate, but there's very little we can do short of turning everything into lists.
		return Arrays.stream(sequence)
		             .map(monophonicSequencerStep -> new WrappingPolyphonicSequencerStep<>(new MonophonicSequencerStep[]{monophonicSequencerStep}))
		             .toArray(WrappingPolyphonicSequencerStep[]::new);
	}
}
