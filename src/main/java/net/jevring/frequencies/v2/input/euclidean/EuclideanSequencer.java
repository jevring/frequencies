/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.input.euclidean;

import net.jevring.frequencies.v2.control.ControlListener;
import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.control.DiscreteControlListener;
import net.jevring.frequencies.v2.hooks.EuclideanSequencerVisualizer;
import net.jevring.frequencies.v2.input.KeyTimings;
import net.jevring.frequencies.v2.input.sequencer.AbstractMonophonicKeySequencer;
import net.jevring.frequencies.v2.input.sequencer.MutableMonophonicSequencerStep;

import java.util.ArrayList;
import java.util.List;

/**
 * A sequencer using euclidean rhythms. It evenly distributes a number of "inner" beats in the space of a number of "outer" steps.
 * https://splice.com/blog/euclidean-rhythms/
 * http://cgm.cs.mcgill.ca/~godfried/publications/banff.pdf
 *
 * @author markus@jevring.net
 * @created 2020-06-07 21:04
 */
public class EuclideanSequencer extends AbstractMonophonicKeySequencer<MutableMonophonicSequencerStep, EuclideanSequencerVisualizer> {
	public static final int MAX_STEPS = 32;
	private volatile int innerSteps = 16;
	private volatile int outerSteps = 16;
	private volatile double gate = 0.5;
	private volatile int note = 60;
	private volatile int noteVariance = 0;
	private volatile int rotation = 0;

	public EuclideanSequencer(Controls controls, KeyTimings keyTimings, EuclideanSequencerVisualizer sequenceVisualizer) {
		super(sequence(), keyTimings, controls, sequenceVisualizer);
		controls.getControl("euclidean-sequencer-gate").addListener(new ControlListener() {
			@Override
			public void valueChanged(double min, double gate, double max, Object source) {
				EuclideanSequencer.this.gate = gate;
				recalculate();
			}
		});
		controls.getControl("euclidean-sequencer-note").addListener(new ControlListener() {
			@Override
			public void valueChanged(double min, double note, double max, Object source) {
				EuclideanSequencer.this.note = (int) note;
				recalculate();
			}
		});
		controls.getControl("euclidean-sequencer-note-variance").addListener(new ControlListener() {
			@Override
			public void valueChanged(double min, double note, double max, Object source) {
				EuclideanSequencer.this.noteVariance = (int) note;
				randomizeNotes();
			}
		});
		controls.getControl("euclidean-sequencer-rotation").addListener(new ControlListener() {
			@Override
			public void valueChanged(double min, double rotation, double max, Object source) {
				EuclideanSequencer.this.rotation = (int) rotation;
				recalculate();
			}
		});
		controls.getControl("euclidean-sequencer-inner-steps").addListener(new ControlListener() {
			@Override
			public void valueChanged(double min, double innerSteps, double max, Object source) {
				EuclideanSequencer.this.innerSteps = (int) innerSteps;
				recalculate();
			}
		});
		controls.getControl("euclidean-sequencer-outer-steps").addListener(new ControlListener() {
			@Override
			public void valueChanged(double min, double outerSteps, double max, Object source) {
				EuclideanSequencer.this.outerSteps = (int) outerSteps;
				recalculate();
			}
		});
		controls.getBooleanControl("euclidean-sequencer-active").addListener(new DiscreteControlListener<>() {
			@Override
			public void valueChanged(Boolean active, Object source) {
				setActive(active);
			}
		});
	}

	private void recalculate() {
		boolean wasActive = isActive();
		setActive(false);
		int activeInnerSteps;
		if (innerSteps > outerSteps) {
			// We can't calculate this. The sequencer goes silent.
			activeInnerSteps = outerSteps;
		} else {
			activeInnerSteps = innerSteps;
		}
		setSteps(outerSteps);

		List<Integer> sequence = cmdEuclideanRhythm(activeInnerSteps, outerSteps);
		sequence = rotate(sequence, rotation);
		for (int i = 0; i < sequence.size(); i++) {
			Integer step = sequence.get(i);
			MutableMonophonicSequencerStep mutableMonophonicSequencerStep = sequencerStep(i).getForVoice(0);
			mutableMonophonicSequencerStep.setGate(step == 0 ? 0 : gate);
			mutableMonophonicSequencerStep.setNote(getNoteIncludingVariance());
		}
		// todo: the first time this happens, the visualizer is just a NOOP, so nothing happens, and we end up with an empty display.
		//  Reconsider how we set up Thief at the start, and what does and does not go into the constructor
		visualizeStep(currentStep);
		if (wasActive) {
			setActive(true);
		}
	}

	private int getNoteIncludingVariance() {
		int randomNoteVariance = (int) ((((Math.random() * 2d) - 1d)) * noteVariance);
		return this.note + randomNoteVariance;
	}

	private void randomizeNotes() {
		for (int i = 0; i < MAX_STEPS; i++) {
			sequencerStep(i).getForVoice(0).setNote(getNoteIncludingVariance());
		}
	}

	private static List<Integer> rotate(List<Integer> sequence, int rotation) {
		for (int i = 0; i < sequence.size(); i++) {
			if (sequence.get(i) == 1) {
				List<Integer> outputSequence = new ArrayList<>();
				outputSequence.addAll(sequence.subList((i + rotation) % sequence.size(), sequence.size()));
				outputSequence.addAll(sequence.subList(0, (i + rotation) % sequence.size()));
				return outputSequence;
			}
		}
		System.err.println("Couldn't find a single pulse in " + sequence);
		return sequence;
	}

	/**
	 * <a href="https://www.computermusicdesign.com/simplest-euclidean-rhythm-algorithm-explained/">This</a> was <b>by far</b> the best algorithm I
	 * found to do this. It's not immediately obvious that the result is the same, but it is, and it means that we don't have to deal with lists of
	 * list and shit like that. While it distributes the pulses according to the same pattern as the original paper, the offset is completely different.
	 * To remedy this, we're going to do a {@link #rotate(List, int) default rotation} so that we start on the first pulse we find.
	 * <a href="http://cgm.cs.mcgill.ca/~godfried/publications/banff.pdf">The original paper</a> does a terrible job at both connecting it to the
	 * euclidean algorithm and explaining how it actually works.
	 * <a href="https://medium.com/code-music-noise/euclidean-rhythms-391d879494df">This is also a reasonably clear algorithm.</a> Not as clear as
	 * the one we end up using, but significantly better then the one from the original paper.
	 *
	 * <p>This algorithm was found without a license or attribution on 2020-06-07, and so is assumed to be in the public domain.</p>
	 *
	 * @return A list of 1 and 0, where the 1 indicates the steps where the sequencer should play a note.
	 */
	private static List<Integer> cmdEuclideanRhythm(int innerSteps, int outerSteps) {
		List<Integer> sequence = new ArrayList<>();

		int bucket = 0;
		for (int i = 0; i < outerSteps; i++) {
			bucket += innerSteps;
			if (bucket >= outerSteps) {
				bucket -= outerSteps; // we don't want to reset it, we just want to deal with the overflow
				sequence.add(1);
			} else {
				sequence.add(0);
			}
		}
		return sequence;
	}


	private static MutableMonophonicSequencerStep[] sequence() {
		// 32 is the max. We'll likely never use that many
		MutableMonophonicSequencerStep[] sequence = new MutableMonophonicSequencerStep[MAX_STEPS];
		for (int i = 0; i < MAX_STEPS; i++) {
			sequence[i] = new MutableMonophonicSequencerStep();
		}
		return sequence;
	}

	@Override
	protected void nextCycle() {
		// we need to re-randomize the notes for the next cycle
		randomizeNotes();
	}

	public void initializeSequencerVisualization() {
		visualizeStep(-1);
	}

	@Override
	protected void visualizeStep(int currentStep) {
		boolean[] steps = new boolean[outerSteps];
		for (int i = 0; i < outerSteps; i++) {
			steps[i] = sequencerStep(i).getForVoice(0).getGate() > 0;
		}
		sequencerVisualizer.visualizedSequencerStep(currentStep, true, steps);
	}

	@Override
	protected void visualizeBeat() {
		sequencerVisualizer.visualizedSequencerBeat(this);
	}

	public static void main(String[] args) {
		List<Integer> sequence = EuclideanSequencer.cmdEuclideanRhythm(6, 16);
		System.out.println(sequence);
		System.out.println(EuclideanSequencer.rotate(sequence, 0));
	}
}
