/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.input.midi;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Transmitter;
import java.util.ArrayList;
import java.util.List;

/**
 * Dispatches MIDI input signals to the rest of the application.
 *
 * @author markus@jevring.net
 * @created 2020-02-10 22:14
 */
public class MidiDevices {
	private final MidiReceiver receiver;

	public MidiDevices(MidiReceiver receiver) {
		this.receiver = receiver;
	}

	/*
	It seems that mid-C is note 60 in the midi keyboard.
	
	https://en.wikipedia.org/wiki/Piano_key_frequencies
	https://newt.phys.unsw.edu.au/jw/notes.html	
	 */

	public List<String> compatibleDevices() {
		List<String> compatibleDevices = new ArrayList<>();
		try {
			for (MidiDevice.Info info : MidiSystem.getMidiDeviceInfo()) {
				MidiDevice midiDevice = MidiSystem.getMidiDevice(info);
				if (midiDevice.getMaxReceivers() != -1) {
					compatibleDevices.add(info.getName());
				}
			}
		} catch (MidiUnavailableException e) {
			e.printStackTrace();
		}
		return compatibleDevices;
	}

	public MidiDevice open(String device) {
		for (MidiDevice.Info info : MidiSystem.getMidiDeviceInfo()) {
			try {
				MidiDevice midiDevice = MidiSystem.getMidiDevice(info);
				if (device.equals(info.getName()) && midiDevice.getMaxReceivers() != -1) {
					Transmitter transmitter = midiDevice.getTransmitter();
					transmitter.setReceiver(receiver);
					midiDevice.open();
					// after we call open, the midi device is held open
					return midiDevice;
				}
			} catch (MidiUnavailableException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}
