/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.input.polyphonic;

import net.jevring.frequencies.v2.input.Instruction;
import net.jevring.frequencies.v2.input.InstructionFactory;
import net.jevring.frequencies.v2.input.KeyTimings;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Determines which notes are being played.
 *
 * @author markus@jevring.net
 * @created 2020-05-24 00:37
 */
public class PolyphonicInput {
	private final Map<Integer, Integer> keyToSlot = new HashMap<>();
	private final InstructionFactory instructionFactory;
	private final KeyTimings keyTimings;
	private final int numberOfVoices;


	public PolyphonicInput(InstructionFactory instructionFactory, KeyTimings keyTimings, int numberOfVoices) {
		this.instructionFactory = instructionFactory;
		this.keyTimings = keyTimings;
		this.numberOfVoices = numberOfVoices;
	}

	/**
	 * Will return a list that is {@link #numberOfVoices} long where each entry is mapped to the corresponding voice.
	 * This means that when there are fewer notes playing that there are voices, there will be nulls in the
	 * list. In this case, the corresponding voice should be silent.
	 */
	public List<Instruction> currentInput() {
		// @formatter:off
		List<Instruction> instructions = keyTimings.keys()
		                                           .stream()
		                                           .map(instructionFactory::keyTimingToInstruction)
		                                           .filter(Objects::nonNull) // skip the ones that are idle, which will be a lot
		                                           .sorted(Comparator.comparing(Instruction::getAge))
		                                           .limit(numberOfVoices) // limit to the number of voices we have
		                                           .collect(Collectors.toList());
		// @formatter:on

		List<Instruction> slottedInstructions = new ArrayList<>(numberOfVoices);
		// pre-populate the slotted instructions so that we can rely on there being nulls in all non-filled places
		for (int slot = 0; slot < numberOfVoices; slot++) {
			slottedInstructions.add(null);
		}
		// Assign instructions to existing slots
		Iterator<Instruction> iterator = instructions.iterator();
		while (iterator.hasNext()) {
			Instruction instruction = iterator.next();
			Integer slot = keyToSlot.get(instruction.getKey());
			if (slot != null) {
				slottedInstructions.set(slot, instruction);
				iterator.remove();
			}
		}
		// assign remaining instructions to new slots
		for (Instruction instruction : instructions) {
			// anything assigned here will be a new instruction, and should be tagged as such
			for (int slot = 0; slot < numberOfVoices; slot++) {
				Instruction slottedInstruction = slottedInstructions.get(slot);
				if (slottedInstruction == null) {
					slottedInstructions.set(slot, toNew(instruction));
				}
			}
		}

		// re-assign slots
		keyToSlot.clear();
		for (int slot = 0; slot < numberOfVoices; slot++) {
			Instruction instruction = slottedInstructions.get(slot);
			if (instruction != null) {
				keyToSlot.put(instruction.getKey(), slot);
			}
		}
		return slottedInstructions;
	}


	private Instruction toNew(Instruction instruction) {
		return new Instruction(instruction.getKey(),
		                       instruction.getAge(),
		                       instruction.getFrequency(),
		                       true,
		                       instruction.getVolumeEnvelopePhase(),
		                       instruction.getNanosecondsActivated(),
		                       instruction.getNanosecondsDeactivated(),
		                       instruction.getVelocity(),
		                       instruction.getTimeDown());
	}
}
