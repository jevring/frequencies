/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.input.midi;

import net.jevring.frequencies.v2.control.BooleanControl;
import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.control.DiscreteControl;
import net.jevring.frequencies.v2.input.KeyTimings;
import net.jevring.frequencies.v2.input.tempo.MidiClockTempoSource;
import net.jevring.frequencies.v2.math.Interpolation;

import javax.sound.midi.*;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;

/**
 * Receives midi messages and translates them to instructions for the synthesizer.
 *
 * @author markus@jevring.net
 * @created 2020-05-08 12:09
 */
public class MidiReceiver implements Receiver {
	private static final boolean DEBUG = false;
	private final Set<Integer> noteChannels = new HashSet<>();
	private final Map<Integer, String> commands = new HashMap<>();
	private final MidiClockTempoSource midiClockTempoSource;
	private final MidiMappings midiMappings;
	private final KeyTimings keyTimings;

	private volatile MidiNoteListener midiNoteListener;

	public MidiReceiver(MidiMappings midiMappings, KeyTimings keyTimings, MidiClockTempoSource midiClockTempoSource) {
		this.midiMappings = midiMappings;
		this.keyTimings = keyTimings;
		this.midiNoteListener = keyTimings;
		this.midiClockTempoSource = midiClockTempoSource;

		try {
			for (Field field : ShortMessage.class.getFields()) {
				if (Modifier.isPublic(field.getModifiers()) &&
				    Modifier.isStatic(field.getModifiers()) &&
				    Modifier.isFinal(field.getModifiers()) &&
				    field.getType().equals(int.class)) {
					int value = (int) field.get(ShortMessage.class);
					commands.put(value, field.getName());
				}
			}
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	public void listenOnNoChannels() {
		noteChannels.clear();
	}

	public boolean listensOnNoChannels() {
		return noteChannels.isEmpty();
	}

	public boolean listensOnAllChannels() {
		return noteChannels.size() == 16;
	}

	public void listenOnAllChannels() {
		for (int channel = 1; channel <= 16; channel++) {
			noteChannels.add(channel);
		}
	}

	public boolean listensToNotesOnChannel(int channel) {
		return noteChannels.contains(channel);
	}

	public void listenToNotesOnChannel(int channel, boolean listen) {
		if (listen) {
			noteChannels.add(channel);
		} else {
			noteChannels.remove(channel);
		}
	}

	/**
	 * This is just for loading and storing.
	 */
	Set<Integer> getNoteChannels() {
		return noteChannels;
	}

	/**
	 * Replace the current listener with the provided listener.
	 */
	public void listen(MidiNoteListener midiNoteListener) {
		this.midiNoteListener = midiNoteListener;
	}

	/**
	 * Resets the default listener.
	 */
	public void stopListening() {
		this.midiNoteListener = keyTimings;
	}

	@Override
	public void send(MidiMessage message, long timeStamp) {
		if (message instanceof ShortMessage shortMessage) {
			int command = shortMessage.getCommand();
			// In the Akai MPK mini configurator, channel 1 actually returns a 0 here, and channel 2 a 1, etc
			int channel = shortMessage.getChannel() + 1;
			int control = shortMessage.getData1();
			int value = shortMessage.getData2();
			int status = shortMessage.getStatus();
			try {
				if (DEBUG) {
					System.out.printf("channel: %d command: %d (%s) data1: %d data2: %d status: %d time: %d length: %d status name: %s%n",
					                  channel,
					                  command,
					                  commands.get(command),
					                  control,
					                  value,
					                  status,
					                  timeStamp,
					                  message.getLength(),
					                  commands.get(status));

				}
				if (status == ShortMessage.TIMING_CLOCK) {
					// Looking at the status instead of the command made it clear that what both the Novation Circuit and Ableton live was 
					// sending was, in fact, MIDI clock, and not some Sysex message.
					midiClockTempoSource.nextTick();
					// todo. Handle SONG_POSITION_POINTER, START, STOP, and CONTINUE. We should support them for the sequencer and arpeggiator.
					//  START is just a "make ready for the next TIMING_CLOCK message" message. It's a primer. 
					// http://midi.teragonaudio.com/tech/midispec/clock.htm
					// http://midi.teragonaudio.com/tech/midispec/seq.htm
				} else if (command == ShortMessage.NOTE_ON && noteChannels.contains(channel)) {
					midiNoteListener.down(control, value);
				} else if (command == ShortMessage.NOTE_OFF && noteChannels.contains(channel)) {
					midiNoteListener.up(control);
				} else if (command == ShortMessage.CONTROL_CHANGE) {
					MidiKey midiKey = new MidiKey(channel, control);
					Control c = midiMappings.getControlFor(midiKey);
					if (c != null) {
						c.set(0, value, 127, this);
					} else {
						BooleanControl bc = midiMappings.getBooleanControlFor(midiKey);
						if (bc != null) {
							bc.set(value > 0, this);
						} else {
							DiscreteControl dc = midiMappings.getDiscreteControlFor(midiKey);
							if (dc != null) {
								List<String> allowedValues = dc.getAllowedValues();
								String interpolatedValue = allowedValues.get((int) Interpolation.linear(0, 127, value, 0, allowedValues.size() - 1));
								dc.set(interpolatedValue, this);
							}
						}
					}
				} else if (command == ShortMessage.PITCH_BEND) {
					int pitchBend = ((shortMessage.getData2() * 128) + shortMessage.getData1()) - 8192;
					midiMappings.getPitchBendControl().set(-8192, pitchBend, 8191, this);
				} else if (command == ShortMessage.POLY_PRESSURE) {
					// todo: implement this if I ever get my hands on a midi controller that provides polyphonic aftertouch.
					System.out.println("polyphonic aftertouch: " + Arrays.toString(message.getMessage()));
				} else if (command == ShortMessage.MIDI_TIME_CODE) {
					System.out.println("midi time code: " + Arrays.toString(message.getMessage()));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (message instanceof MetaMessage metaMessage) {
			System.out.println("Got meta MIDI message " + Arrays.toString(metaMessage.getMessage()));
		} else if (message instanceof SysexMessage sysexMessage) {
			System.out.println("Got sysex MIDI message " + Arrays.toString(sysexMessage.getMessage()));
		} else {
			System.out.println("Got unknown MIDI message " + Arrays.toString(message.getMessage()));
		}
	}

	@Override
	public void close() {
		// we do nothing here, and rely on MidiDevice.close() to be called instead
	}
}
