/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.input;

import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.control.MappingDiscreteControl;
import net.jevring.frequencies.v2.input.monophonic.MonophonicInput;
import net.jevring.frequencies.v2.input.polyphonic.PolyphonicInput;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Determine the input instructions from the (midi) keyboard.
 * 
 * todo: better name for this class
 *
 * @author markus@jevring.net
 * @created 2020-06-13 20:33
 */
public class SwitchableMonophonicAndPolyphonicInstructionInput implements InstructionInput {
	private final MappingDiscreteControl<InputMode> inputMode;
	private final PolyphonicInput polyphonicInput;
	private final MonophonicInput monophonicInput;
	private final int numberOfVoices;

	public SwitchableMonophonicAndPolyphonicInstructionInput(PolyphonicInput polyphonicInput, MonophonicInput monophonicInput, Controls controls, int numberOfVoices) {
		this.inputMode = controls.getDiscreteControl("input-mode").mapping(InputMode::valueOf);
		this.polyphonicInput = polyphonicInput;
		this.monophonicInput = monophonicInput;
		this.numberOfVoices = numberOfVoices;
	}

	@Override
	public List<Instruction> inputInstructions() {
		if (inputMode.get() == InputMode.POLYPHONIC) {
			return polyphonicInput.currentInput();
		} else {
			List<Instruction> instructions = new ArrayList<>(numberOfVoices);
			for (int i = 0; i < numberOfVoices; i++) {
				// The returned list must *always* have the correct number of voices, even if they're not all used
				instructions.add(null);
			}
			Optional<Instruction> potentialInstruction = monophonicInput.currentInput();
			if (potentialInstruction.isPresent()) {
				Instruction instruction = potentialInstruction.get();
				// Monophonic single instruction
				instructions.set(0, instruction);
			}
			return instructions;
		}
	}
}
