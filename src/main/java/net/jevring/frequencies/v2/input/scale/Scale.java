/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.input.scale;

/**
 * The various scales. I just cribbed what was in the Novation circuit manual for this. There are more things, no doubt, but if it's
 * good enough for Novation, it's good enough for me.
 *
 * @author markus@jevring.net
 * @created 2021-05-24 11:19
 */
public enum Scale {
	/**
	 * Chromatic should always be the default, since it doesn't impost any changes on the input.
	 */
	CHROMATIC("Chromatic", 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11),
	NATURAL_MINOR("Natural minor", 0, 2, 3, 5, 7, 8, 10),
	MAJOR("Major", 0, 2, 4, 5, 7, 9, 11),
	DORIAN("Dorian", 0, 2, 3, 5, 7, 9, 10),
	PHRYGIAN("Phrygian", 0, 1, 3, 5, 7, 8, 10),
	MIXOLYDIAN("Mixolydian", 0, 2, 4, 5, 7, 9, 10),
	MELODIC_MINOR("Melodic minor", 0, 2, 4, 5, 7, 9, 10),
	HARMONIC_MINOR("Harmonic minor", 0, 2, 3, 5, 7, 8, 11),
	BEBOP_DORIAN("Bebop dorian", 0, 3, 4, 5, 7, 9, 10),
	BLUES("Blues", 0, 3, 5, 6, 7, 10),
	MINOR_PENTATONIC("Minor pentatonic", 0, 3, 5, 7, 10),
	HUNGARIAN_MINOR("Hungarian minor", 0, 2, 3, 6, 7, 8, 11),
	UKRANIAN_DORIAN("Ukranian dorian", 0, 2, 3, 6, 7, 9, 10),
	MARVA("Marva", 0, 1, 4, 6, 7, 9, 11),
	TODI("Todi", 0, 1, 3, 6, 7, 8, 11),
	WHOLE_TONE("Whole tone", 0, 2, 4, 6, 8, 10);

	private final String displayName;
	private final int[] includedNotes;

	private Scale(String displayName, int... includedNotes) {
		this.displayName = displayName;
		this.includedNotes = includedNotes;
	}

	public String getDisplayName() {
		return displayName;
	}

	public int[] getIncludedNotes() {
		return includedNotes;
	}

	public static Scale fromDisplayName(String displayName) {
		for (Scale value : values()) {
			if (value.getDisplayName().equals(displayName)) {
				return value;
			}
		}
		System.err.println("Could not match display name " + displayName + ", returning " + CHROMATIC);
		return CHROMATIC;
	}
}
