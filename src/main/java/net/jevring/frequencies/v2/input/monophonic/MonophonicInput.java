/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.input.monophonic;

import net.jevring.frequencies.v2.control.BooleanControl;
import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.input.Instruction;
import net.jevring.frequencies.v2.input.InstructionFactory;
import net.jevring.frequencies.v2.input.KeyTimings;
import net.jevring.frequencies.v2.math.Interpolation;

import java.util.Comparator;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * Calculates the input for a monophonic synthesizer.
 *
 * @author markus@jevring.net
 * @created 2020-03-01 21:43
 */
public class MonophonicInput {
	private static final long MAX_GLIDE_LENGTH_NANOS = TimeUnit.SECONDS.toNanos(1);
	private final InstructionFactory instructionFactory;
	private final Control glideLengthControl;
	private final BooleanControl legato;
	private final KeyTimings keyTimings;
	/**
	 * Used for glide.
	 */
	private volatile double previousToneFrequency = 0;
	private volatile Instruction previousInstruction = null;


	public MonophonicInput(InstructionFactory instructionFactory, KeyTimings keyTimings, Controls controls) {
		this.instructionFactory = instructionFactory;
		this.glideLengthControl = controls.getControl("glide-length");
		this.legato = controls.getBooleanControl("legato");
		this.keyTimings = keyTimings;
	}

	public Optional<Instruction> currentInput() {
		// Find the youngest tone that has not yet reached release.
		// If there is no sustaining tone, just the youngest tone
		Optional<Instruction> instruction = keyTimings.keys()
		                                              .stream()
		                                              .map(instructionFactory::keyTimingToInstruction)
		                                              .filter(Objects::nonNull)
		                                              .min(Comparator.comparing(Instruction::isInVolumeRelease).thenComparing(Instruction::getAge))
		                                              .map(this::markEnvelopeResetIfDifferentFromPrevious)
		                                              .map(this::applyGlide);
		if (instruction.isEmpty()) {
			previousToneFrequency = 0;
			previousInstruction = null;
		}
		return instruction;
	}

	private Instruction markEnvelopeResetIfDifferentFromPrevious(Instruction instruction) {
		if (previousInstruction == null ||
		    previousInstruction.getKey() != instruction.getKey() ||
		    previousInstruction.getTimeDown() != instruction.getTimeDown()) {
			// Legato doesn't retrigger the envelopes! That's interesting! https://youtu.be/TN6NLwyL0rk?t=300
			// This is orthogonal to glide/portamento, apparently
			boolean newInstruction = previousInstruction == null || !legato.get();
			previousInstruction = instruction;
			return new Instruction(instruction.getKey(),
			                       instruction.getAge(),
			                       instruction.getFrequency(),
			                       newInstruction,
			                       instruction.getVolumeEnvelopePhase(),
			                       instruction.getNanosecondsActivated(),
			                       instruction.getNanosecondsDeactivated(),
			                       instruction.getVelocity(),
			                       instruction.getTimeDown());

		}
		return instruction;
	}

	/**
	 * This method will be called for a single application instruction. As it accesses member variables, it must not be called
	 * for more than the currently active note.
	 */
	private Instruction applyGlide(Instruction instruction) {
		double glideValue = glideLengthControl.getCurrentValue();
		if (glideValue > 0) {
			double frequency = instruction.getFrequency();
			long age = instruction.getAge();
			long maxGlideLength = (long) (MAX_GLIDE_LENGTH_NANOS * glideValue);
			if (previousToneFrequency > 0 && previousToneFrequency != frequency && age < maxGlideLength) {
				frequency = Interpolation.linear(0, maxGlideLength, age, previousToneFrequency, frequency);
			} else {
				// we enter here on the first cycle, but that's fine, because we'll then set the same frequency
				// we're on, which means we can glide between the same values without any difference
				previousToneFrequency = frequency;
			}
			return new Instruction(instruction.getKey(),
			                       instruction.getAge(),
			                       frequency,
			                       instruction.isNewInstruction(),
			                       instruction.getVolumeEnvelopePhase(),
			                       instruction.getNanosecondsActivated(),
			                       instruction.getNanosecondsDeactivated(),
			                       instruction.getVelocity(),
			                       instruction.getTimeDown());
		}
		return instruction;
	}
}
