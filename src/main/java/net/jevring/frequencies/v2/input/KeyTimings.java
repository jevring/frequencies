/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.input;

import net.jevring.frequencies.v2.input.midi.MidiNoteListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Tracks key timings for all keys.
 *
 * @author markus@jevring.net
 * @created 2020-02-10 21:08
 */
public class KeyTimings implements MidiNoteListener {
	private static final int MAX_KEYS = 128;
	// Contains timestamps, in nanoseconds, when a value was down or up.
	// keyed by the input note, which depends on the system it comes from.
	// This keying is determined by the caller, but must not stray outside the range of 0-127 
	private final Note[] down = new Note[MAX_KEYS];
	private final long[] up = new long[MAX_KEYS];

	public KeyTimings() {
		// This works because Note is immutable, and we replace it. If it was mutable, we'd change the same note everywhere, which would break.
		Arrays.fill(down, new Note(0, 0));
	}

	/**
	 * @param velocity a value between 0 and 127, as per the midi standard.
	 */
	@Override
	public void down(int key, int velocity) {
		if (key < 0 || key > MAX_KEYS - 1) {
			System.err.println("Got invalid key down: " + key);
			return;
		}
		double scaledVelocity;
		if (velocity < 0 || velocity > 127) {
			System.err.println("Got invalid velocity " + velocity + ", faking full hit");
			scaledVelocity = 1;
		} else {
			scaledVelocity = ((double) velocity / 127d);
		}

		long nowNanos = System.nanoTime();
		down[key] = new Note(nowNanos, scaledVelocity);
		// If it's currently down, it's certainly not up
		up[key] = 0L;
	}

	@Override
	public void up(int key) {
		if (key < 0 || key > MAX_KEYS - 1) {
			System.err.println("Got invalid key up: " + key);
			return;
		}

		long nowNanos = System.nanoTime();
		up[key] = nowNanos;
		//System.out.println("Updating key up for key " + key);
	}

	public List<KeyTiming> keys() {
		long nowNanos = System.nanoTime();
		List<KeyTiming> keyTimings = new ArrayList<>();

		for (int key = 0; key < MAX_KEYS; key++) {
			Note lastDown = down[key];
			long lastTimeDown = lastDown.time;
			long lastTimeUp = up[key];

			if (lastTimeDown > 0L && (lastTimeUp == 0L || lastTimeDown > lastTimeUp)) {
				// key has been pushed down, and has either never ever been released, or it was pushed down after it was last released.
				// Key is currently down.
				keyTimings.add(new KeyTiming(key, lastDown.velocity, nowNanos - lastTimeDown, 0, lastTimeDown));
			} else if (lastTimeUp > 0L) {
				long nanosecondsDeactivated = nowNanos - lastTimeUp;

				// We assume that the key was held down until it was released, and that release was effectively instantaneous.
				// Or at the very least that the key registered as up at the same time it stopped being registered as down
				long nanosecondsActivated = lastTimeUp - lastTimeDown;
				keyTimings.add(new KeyTiming(key, lastDown.velocity, nanosecondsActivated, nanosecondsDeactivated, lastTimeDown));
			}
		}
		return keyTimings;
	}

	private record Note(long time, double velocity) {
		public Note(long time, double velocity) {
			this.time = time;
			if (velocity < 0 || velocity > 1) {
				System.err.println("Wrong velocity " + velocity + ", faking full hit");
				this.velocity = 1;
			} else {
				this.velocity = velocity;
			}
		}
	}
}
