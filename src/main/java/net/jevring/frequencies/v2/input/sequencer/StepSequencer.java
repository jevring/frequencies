/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.input.sequencer;

import net.jevring.frequencies.v2.control.*;
import net.jevring.frequencies.v2.hooks.SequencerVisualizer;
import net.jevring.frequencies.v2.input.KeyTimings;
import net.jevring.frequencies.v2.input.midi.MidiNoteListener;
import net.jevring.frequencies.v2.input.midi.MidiReceiver;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A step sequencer.
 *
 * @author markus@jevring.net
 * @created 2020-05-07 20:04
 */
public class StepSequencer extends AbstractMonophonicKeySequencer<ControlledSequencerStep, SequencerVisualizer> {
	// todo: for a polyphonic sequencer, or perhaps just in general, maybe we can take inspiration from this: https://www.youtube.com/watch?v=1z41PCH06xc

	/**
	 * Similarly to {@link AbstractPolyphonicKeySequencer.Voice#currentlyPlayingNotes} this should ideally only contain a single thing, but because we can change the tempo and
	 * the sequence length, we occasionally mess it up.
	 */
	private final List<Integer> currentlyHighlightedSteps = new ArrayList<>();
	/**
	 * Used to record steps for the sequencer.
	 */
	private final MidiReceiver midiReceiver;
	private final BooleanControl sequencerListen;
	private final Controls controls;

	public StepSequencer(MidiReceiver midiReceiver, Controls controls, KeyTimings keyTimings, SequencerVisualizer sequenceVisualizer) {
		super(sequence(controls), keyTimings, controls, sequenceVisualizer);
		this.controls = controls;
		this.midiReceiver = midiReceiver;
		controls.getBooleanControl("sequencer-active").addListener(new DiscreteControlListener<>() {
			@Override
			public void valueChanged(Boolean active, Object source) {
				setActive(active);
			}
		});
		controls.getControl("sequencer-steps").addListener(new ControlListener() {
			@Override
			public void valueChanged(double min, double newValue, double max, Object source) {
				// NOTE: This assumes that the control is pure, in that it returns exactly the value we want it to return, 
				// with no scaling required
				setSteps((int) newValue);
			}
		});
		sequencerListen = controls.getBooleanControl("sequencer-listen");
		sequencerListen.addListener(new DiscreteControlListener<>() {
			@Override
			public void valueChanged(Boolean listening, Object source) {
				if (listening) {
					listenForNotes();
				} else {
					midiReceiver.stopListening();
				}
			}
		});
	}

	private static ControlledSequencerStep[] sequence(Controls controls) {
		ControlledSequencerStep[] sequence = new ControlledSequencerStep[16];
		for (int step = 0; step < 16; step++) {
			sequence[step] = new ControlledSequencerStep(step, controls);
		}
		return sequence;
	}

	private void listenForNotes() {
		for (int i = 0; i < getSteps(); i++) {
			sequencerVisualizer.visualizedSequencerStep(i, true);
		}
		midiReceiver.listen(new MidiNoteListener() {
			private int stepsRecorded = 0;

			@Override
			public void down(int key, int velocity) {
				// proxy it to the KeyTimings so that we can hear the note we just played 
				keyTimings.down(key, velocity);


				int currentStep = stepsRecorded++;
				Control stepControl = controls.getControl("sequencer-note-" + currentStep);
				stepControl.set(stepControl.getMin(), key, stepControl.getMax(), this);
				sequencerVisualizer.visualizedSequencerStep(currentStep, false);
				if (stepsRecorded == getSteps()) {
					midiReceiver.stopListening();
					// We need to reset the control so that we can start recording again later
					sequencerListen.set(false, this);
				}
			}

			@Override
			public void up(int key) {
				// proxy it to the KeyTimings so that we can hear the note we just played 
				keyTimings.up(key);
			}
		});
	}

	protected void visualizeStep(int step) {
		if (currentlyHighlightedSteps.size() > 1) {
			System.out.println("There were currently " + currentlyHighlightedSteps.size() + " steps highlighted: " + currentlyHighlightedSteps);
		}
		Iterator<Integer> iterator = currentlyHighlightedSteps.iterator();
		while (iterator.hasNext()) {
			Integer currentlyHighlightedStep = iterator.next();
			iterator.remove();
			sequencerVisualizer.visualizedSequencerStep(currentlyHighlightedStep, false);
		}
		currentlyHighlightedSteps.add(step);
		sequencerVisualizer.visualizedSequencerStep(step, true);
	}

	@Override
	protected void nextCycle() {
		// do nothing. This does not concern us
	}

	@Override
	protected void visualizeBeat() {
		sequencerVisualizer.visualizedSequencerBeat(this);
	}

	@Override
	protected void setActive(boolean active) {
		super.setActive(active);
		if (sequencerVisualizer != null) {
			// Normally we don't do a null check for the sequencer visualizer, because once we're running, it's never supposed to be null.
			// But when we configure the controls, we haven't yet had time to set it, so we have to check.
			for (int step = 0; step < 16; step++) {
				// Doing this for all allows us to potentially make cool shutdown effects.
				// It also ensures that everything is properly turned off
				sequencerVisualizer.visualizedSequencerStep(step, false);
			}
		}
	}
}
