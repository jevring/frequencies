/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.input.midi;

import net.jevring.frequencies.v2.control.BooleanControl;
import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.control.DiscreteControl;

import java.util.HashMap;
import java.util.Map;

/**
 * A mapping from midi input to controls.
 *
 * @author markus@jevring.net
 * @created 2020-02-23 15:07
 */
public class MidiMappings {
	private final Map<MidiKey, String> mappings = new HashMap<>();
	private final Controls controls;
	/**
	 * Whenever this is not null, any midi mapping that comes in is mapped to this control.
	 */
	private String mappableControl;

	public MidiMappings(Controls controls) {
		this.controls = controls;
	}

	public void map(MidiKey midiKey, String control) {
		mappings.put(midiKey, control);
	}

	private String listenOrGetMapping(MidiKey midiKey) {
		if (mappableControl != null) {
			// This will map the midi to a new control. It will *not* unmap the old one.
			// The only way to unmap an old one is to remap it to something new.
			// The reason for this is that we want to support mapping multiple CCs, 
			// possibly from multiple midi devices, to the same control
			mappings.put(midiKey, mappableControl);
			return mappableControl;
		} else {
			return mappings.get(midiKey);
		}
	}

	public Control getControlFor(MidiKey midiKey) {
		String controlKey = listenOrGetMapping(midiKey);
		if (controlKey == null) {
			return null;
		}
		return controls.getControl(controlKey);
	}

	public DiscreteControl getDiscreteControlFor(MidiKey midiKey) {
		String controlKey = listenOrGetMapping(midiKey);
		if (controlKey == null) {
			return null;
		}
		return controls.getDiscreteControl(controlKey);
	}

	public BooleanControl getBooleanControlFor(MidiKey midiKey) {
		String controlKey = listenOrGetMapping(midiKey);
		if (controlKey == null) {
			return null;
		}
		return controls.getBooleanControl(controlKey);
	}

	/**
	 * Since pitch bend doesn't have a channel etc, it gets its own custom control
	 */
	public Control getPitchBendControl() {
		return controls.getControl("pitch-bend");
	}

	/**
	 * This is just for loading and storing.
	 */
	Map<MidiKey, String> getMappings() {
		return mappings;
	}

	public void listenForNewMidiMapping(String control) {
		this.mappableControl = control;
	}

	public void stopListeningForNewMidiMapping() {
		this.mappableControl = null;
	}
}
