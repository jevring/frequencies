/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.input.midi;

import net.jevring.frequencies.v2.configuration.ConfigurationReader;
import net.jevring.frequencies.v2.configuration.ConfigurationWriter;
import net.jevring.frequencies.v2.configuration.StoredConfigurationCallback;
import net.jevring.frequencies.v2.control.BooleanControl;
import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.input.KeyTimings;
import net.jevring.frequencies.v2.input.tempo.MidiClockTempoSource;

import javax.sound.midi.MidiDevice;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Manages all things MIDI.
 *
 * @author markus@jevring.net
 * @created 2020-04-12 18:56
 */
public class MidiManager {
	private final File midiPropertiesFile;
	private final MidiDevices midiDevices;
	private final MidiMappings midiMappings;
	private final MidiReceiver receiver;

	private final Map<String, MidiDevice> currentMidiDevices = new HashMap<>();

	private final BooleanControl receiveMidiClock;

	public MidiManager(Controls controls, KeyTimings keyTimings, MidiClockTempoSource midiClockTempoSource, File dotConfigDirectory) {
		this.midiMappings = new MidiMappings(controls);
		this.receiver = new MidiReceiver(midiMappings, keyTimings, midiClockTempoSource);
		this.midiDevices = new MidiDevices(receiver);
		this.receiveMidiClock = controls.getBooleanControl("receive-midi-clock-active");
		this.midiPropertiesFile = midiPropertiesFile(dotConfigDirectory);
	}

	private static File midiPropertiesFile(File dotConfigDirectory) {
		String midiPropertiesOverrideFilePath = System.getProperty("midi.properties.file");
		if (midiPropertiesOverrideFilePath != null) {
			System.out.println("Using midi properties override from " + midiPropertiesOverrideFilePath);
			return new File(midiPropertiesOverrideFilePath);
		} else {
			return new File(dotConfigDirectory, "midi.conf");
		}
	}

	public void initializeMidi() {
		if (!midiPropertiesFile.exists()) {
			// write defaults so we have something to start with.
			try {
				midiPropertiesFile.getParentFile().mkdirs();
				Files.copy(MidiManager.class.getResourceAsStream("/midi.default.conf"), midiPropertiesFile.toPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (midiPropertiesFile.exists()) {
			load(midiPropertiesFile);
		}

		if (currentMidiDevices.isEmpty()) {
			List<String> compatibleDevices = midiDevices.compatibleDevices();
			if (!compatibleDevices.isEmpty()) {
				String firstCompatibleMidiDevice = compatibleDevices.get(0);
				System.out.println("No midi device configured. Choosing first compatible device " + firstCompatibleMidiDevice);
				MidiDevice midiDevice = midiDevices.open(firstCompatibleMidiDevice);
				if (midiDevice == null) {
					System.err.println("Could not open midi device " + firstCompatibleMidiDevice);
				} else {
					currentMidiDevices.put(firstCompatibleMidiDevice, midiDevice);
				}
				// Default to this so that we will get something when we hit the keys
				receiver.listenOnAllChannels();
			}
		}
	}

	public void writeToDefaultFile() {
		writeToFile(midiPropertiesFile);
	}

	public void writeToFile(File output) {
		if (!output.getParentFile().exists()) {
			if (!output.getParentFile().mkdirs()) {
				System.err.println("Could not create directories " + output.getParentFile());
				return;
			}
		}
		try (ConfigurationWriter writer = new ConfigurationWriter("2", output)) {
			for (String device : currentMidiDevices.keySet()) {
				writer.add("devices", device);
			}
			for (Map.Entry<MidiKey, String> entry : midiMappings.getMappings().entrySet()) {
				String controlName = entry.getValue();
				MidiKey midiKey = entry.getKey();
				int midiChannel = midiKey.channel();
				int midiControl = midiKey.control();
				writer.add("mappings", controlName + "," + midiChannel + "," + midiControl);
			}

			writer.add("note-channels", receiver.getNoteChannels().stream().sorted().map(String::valueOf).collect(Collectors.joining(",")));
			writer.add("midi-clock", String.format(Locale.US, "%s,%b", "receive", true));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void load(File input) {
		midiMappings.getMappings().clear();
		try {
			ConfigurationReader.read(input, new StoredConfigurationCallback() {
				@Override
				public void handle(String version, String section, String setting) {
					if ("devices".equals(section)) {
						System.out.println("Loading pre-configured midi device " + setting);
						enableMidiDevice(setting, false);
					} else if ("mappings".equals(section)) {
						String[] parts = setting.split(",", 3);
						String mappingName = parts[0];
						int channel = Integer.parseInt(parts[1]);
						int control = Integer.parseInt(parts[2]);
						midiMappings.map(new MidiKey(channel, control), mappingName);
					} else if ("note-channels".equals(section)) {
						receiver.getNoteChannels().clear();
						Arrays.stream(setting.split(",")).map(Integer::parseInt).forEach(channel -> receiver.listenToNotesOnChannel(channel, true));
					} else if ("midi-clock".equals(section)) {
						String[] parts = setting.split(",", 3);
						if ("receive".equals(parts[0])) {
							// All controls that end if "active" are dropped from load and save. 
							// This means that it'll always default to whatever was set during control creation.
							// This load of the midi settings here will set it right.
							receiveMidiClock.set(Boolean.parseBoolean(parts[1]), this);
						}// eventually we might support sending it as well
					} else {
						System.err.println("Unknown MIDI setting: " + section + " -> " + setting);
					}
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public MidiDevices getMidiDevices() {
		return midiDevices;
	}

	public MidiMappings getMidiMappings() {
		return midiMappings;
	}

	public MidiReceiver getReceiver() {
		return receiver;
	}

	public void enableMidiDevice(String device) {
		enableMidiDevice(device, true);
	}

	public void disableMidiDevice(String device) {
		MidiDevice midiDevice = currentMidiDevices.remove(device);
		if (midiDevice == null) {
			System.err.println("Device " + device + " isn't open, so it can't be closed");
			return;
		}
		writeToFile(midiPropertiesFile);
		midiDevice.close();
	}

	private void enableMidiDevice(String device, boolean store) {
		if (currentMidiDevices.containsKey(device)) {
			System.err.println("Midi device " + device + " was already open");
			return;
		}
		MidiDevice midiDevice = midiDevices.open(device);
		if (midiDevice == null) {
			System.err.println("Could not open midi device " + device);
		} else {
			currentMidiDevices.put(device, midiDevice);
		}
		if (store) {
			writeToFile(midiPropertiesFile);
		}
	}

	public boolean isCurrentDevice(String compatibleDevice) {
		return currentMidiDevices.containsKey(compatibleDevice);
	}
}
