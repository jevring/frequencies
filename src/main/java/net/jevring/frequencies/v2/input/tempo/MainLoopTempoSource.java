/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.input.tempo;

import net.jevring.frequencies.v2.control.ControlListener;
import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.input.sequencer.Sequencer;

/**
 * A {@link TempoSource} driven by a main loop in a thread.
 *
 * @author markus@jevring.net
 * @created 2021-02-24 22:14
 */
public class MainLoopTempoSource extends AbstractTempoSource {
	private static final double NANOS_PER_SECOND = 1e9;

	private volatile boolean running;
	private volatile Thread thread;
	private volatile double tempo = 120;

	public MainLoopTempoSource(Controls controls) {
		super();
		controls.getControl("tempo").addListener(new ControlListener() {
			@Override
			public void valueChanged(double min, double newValue, double max, Object source) {
				// NOTE: This assumes that the control is pure, in that it returns exactly the value we want it to return, 
				// with no scaling required
				MainLoopTempoSource.this.tempo = newValue;
			}
		});
	}

	private void loop() {
		long previousNanos = System.nanoTime();
		while (running) {
			//Thread.onSpinWait();
			try {
				//noinspection BusyWait
				Thread.sleep(1);
			} catch (InterruptedException ignored) {
			}
			long nowNanos = System.nanoTime();
			long elapsedNanos = nowNanos - previousNanos;
			try {
				double secondsPerBeat = 60d / tempo;
				long nanosPerBeat = (long) (secondsPerBeat * NANOS_PER_SECOND);
				long nanosPerTick = nanosPerBeat / TICKS_PER_BEAT;

				if (elapsedNanos >= nanosPerTick) {
					for (Sequencer sequencer : sequencers) {
						sequencer.nextTick();
					}
					previousNanos = nowNanos;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public synchronized void start() {
		if (thread != null) {
			throw new IllegalStateException("Already running");
		}

		running = true;
		thread = new Thread(this::loop, "main-loop-tempo-source");
		thread.start();
	}

	public synchronized void stop() {
		if (thread == null) {
			throw new IllegalStateException("Not running");
		}
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		thread = null;
	}
}
