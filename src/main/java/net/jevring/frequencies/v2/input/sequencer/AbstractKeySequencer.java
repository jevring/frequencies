/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.input.sequencer;

import net.jevring.frequencies.v2.control.ControlListener;
import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.hooks.SequencerVisualizer;
import net.jevring.frequencies.v2.input.KeyTimings;
import net.jevring.frequencies.v2.util.TempoTools;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * A basic sequencer.
 *
 * @author markus@jevring.net
 * @created 2020-05-22 16:00
 */
public abstract class AbstractKeySequencer<T extends SequencerStep, V extends SequencerVisualizer> implements Sequencer {
	protected final AtomicBoolean immediateStart = new AtomicBoolean();
	protected final T[] sequence;
	protected final AtomicBoolean requestStopPlayingEverything = new AtomicBoolean();
	protected final KeyTimings keyTimings;

	/**
	 * The sequencer only produces midi messages if it's active.
	 */
	protected volatile boolean active;
	protected volatile int steps = 16;
	protected int ppqnRate = 24;
	protected int tick = 0;
	protected int currentStep = 0;

	protected volatile V sequencerVisualizer;

	protected AbstractKeySequencer(T[] sequence, KeyTimings keyTimings, Controls controls, V sequenceVisualizer) {
		this.sequence = sequence;
		this.keyTimings = keyTimings;
		this.sequencerVisualizer = sequenceVisualizer;
		controls.getControl("rate").addListener(new ControlListener() {
			@Override
			public void valueChanged(double min, double newValue, double max, Object source) {
				// NOTE: This assumes that the control is pure, in that it returns exactly the value we want it to return, 
				// with no scaling required
				AbstractKeySequencer.this.ppqnRate = TempoTools.normalize((int) newValue);
			}
		});
	}

	protected void setSteps(int steps) {
		int previousSteps = this.steps;
		this.steps = steps;
		if (previousSteps == 0) {
			// don't wait for the next cycle, just hit it!
			this.immediateStart.set(true);
		}
	}

	protected int getSteps() {
		return steps;
	}

	protected T sequencerStep(int step) {
		return sequence[step];
	}

	/**
	 * This turns the sequencer on and off. It's always running, but unless it's active, it doesn't actually send any instructions.
	 *
	 * @param active whether the sequencer should send instructions or not.
	 */
	protected void setActive(boolean active) {
		this.active = active;
		reset();
	}

	public void setSequencerVisualizer(V sequencerVisualizer) {
		this.sequencerVisualizer = sequencerVisualizer;
	}

	public boolean isActive() {
		return active;
	}

	protected void reset() {
		this.currentStep = 0;
		this.requestStopPlayingEverything.set(true);
		this.immediateStart.set(true);
	}

	/**
	 * Called when a new cycle begins, which happens after {@link #steps} have been stepped through.
	 */
	protected abstract void nextCycle();

	protected abstract void visualizeStep(int currentStep);

	/**
	 * Even if there are no steps, the beat can be visualized. This helps us know that something is on, without running.
	 */
	protected abstract void visualizeBeat();

	protected void startPlaying(int note) {
		keyTimings.down(note, 127);
	}

	protected void stopPlaying(int note) {
		keyTimings.up(note);
	}
}
