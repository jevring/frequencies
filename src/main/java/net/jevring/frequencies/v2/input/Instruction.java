/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.input;

import net.jevring.frequencies.v2.envelopes.Phase;

/**
 * This is a combination of input keys, frequency mapping, and volume envelope.
 * This represents what should be played in a given iteration of the main loop.
 * One for each key.
 *
 * @author markus@jevring.net
 * @created 2020-02-15 23:13
 */
public class Instruction {
	private final int key;
	private final long age;
	private final double frequency;
	/**
	 * If true, this is not the same instruction as the previous one, regardless of the phase etc.
	 */
	private final boolean newInstruction;
	private final Phase volumeEnvelopePhase;
	private final long nanosecondsActivated;
	private final long nanosecondsDeactivated;
	/**
	 * In the range 0 to 1.
	 */
	private final double velocity;
	private final long timeDown;

	public Instruction(int key,
	                   long age,
	                   double frequency,
	                   boolean newInstruction,
	                   Phase volumeEnvelopePhase,
	                   long nanosecondsActivated,
	                   long nanosecondsDeactivated,
	                   double velocity,
	                   long timeDown) {
		this.key = key;
		this.age = age;
		this.frequency = frequency;
		this.newInstruction = newInstruction;
		this.volumeEnvelopePhase = volumeEnvelopePhase;
		this.nanosecondsActivated = nanosecondsActivated;
		this.nanosecondsDeactivated = nanosecondsDeactivated;
		this.velocity = velocity;
		this.timeDown = timeDown;
	}

	public int getKey() {
		return key;
	}

	public long getAge() {
		return age;
	}

	public double getFrequency() {
		return frequency;
	}

	/**
	 * If it's a new instruction, the envelope and filter etc should be reset.
	 */
	public boolean isNewInstruction() {
		return newInstruction;
	}

	public Phase getVolumeEnvelopePhase() {
		return volumeEnvelopePhase;
	}

	public long getNanosecondsActivated() {
		return nanosecondsActivated;
	}

	public long getNanosecondsDeactivated() {
		return nanosecondsDeactivated;
	}

	public double getVelocity() {
		return velocity;
	}

	public long getTimeDown() {
		return timeDown;
	}

	public boolean isInVolumeRelease() {
		return volumeEnvelopePhase == Phase.RELEASE;
	}
}
