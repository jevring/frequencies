/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.input.sequencer;

import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.hooks.SequencerVisualizer;
import net.jevring.frequencies.v2.input.KeyTimings;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Base class for all polyphonic sequencers.
 *
 * @author markus@jevring.net
 * @created 2021-03-20 15:42
 */
public abstract class AbstractPolyphonicKeySequencer<T extends PolyphonicSequencerStep<?>, V extends SequencerVisualizer> extends AbstractKeySequencer<T, V> {
	private final Voice[] voices;

	protected AbstractPolyphonicKeySequencer(T[] sequence, KeyTimings keyTimings, Controls controls, int voices, V sequenceVisualizer) {
		super(sequence, keyTimings, controls, sequenceVisualizer);
		this.voices = new Voice[voices];
		for (int i = 0; i < voices; i++) {
			this.voices[i] = new Voice();
		}
	}

	@Override
	public void nextTick() {
		if (requestStopPlayingEverything.compareAndSet(true, false)) {
			for (Voice voice : voices) {
				stopPlayingEverything(voice);
			}
		}
		// The immediate start let's us start as soon as the user hits the button, rather than waiting for the next beat.
		// If we don't do this, if feels super laggy 
		boolean immediateStart = this.immediateStart.compareAndSet(true, false);
		if (immediateStart) {
			tick = 0;
			// starting immediately means that we have to stop whatever else we where doing.
			for (Voice voice : voices) {
				if (!voice.currentlyPlayingNotes.isEmpty()) {
					stopPlayingEverything(voice);
					voice.noteEndsOnTick = -1; // disable until next note-on
				}
			}
		} else {
			tick = (tick + 1) % ppqnRate;
		}

		if (active && tick == 0) {
			visualizeBeat();
			if (steps > 0) {
				visualizeStep(currentStep);
				T sequencerStep = sequence[currentStep];
				for (int i = 0; i < voices.length; i++) {
					Voice voice = voices[i];
					play(sequencerStep.getForVoice(i), voice);
				}

				// Next step
				currentStep = (currentStep + 1) % steps;
				if (currentStep == 0) {
					nextCycle();
				}
			}
		} else {
			for (Voice voice : voices) {
				stop(voice, tick);
			}
		}
	}

	/**
	 * Checks the current step and plays all the notes (potentially none) that should be played.
	 */
	private void play(MonophonicSequencerStep monophonicSequencerStep, Voice voice) {
		if (monophonicSequencerStep.getGate() == 0) {
			// mute
			voice.noteEndsOnTick = -1;
		} else if (monophonicSequencerStep.getGate() == 1) {
			// tie
			voice.noteEndsOnTick = -1;
			int note = monophonicSequencerStep.getNote();
			if (!voice.currentlyPlayingNotes.contains(note)) {
				startPlaying(note);
				voice.currentlyPlayingNotes.add(note);
			}
		} else {
			// normal
			voice.noteEndsOnTick = (int) Math.round(monophonicSequencerStep.getGate() * ppqnRate);
			int note = monophonicSequencerStep.getNote();
			startPlaying(note);
			voice.currentlyPlayingNotes.add(note);
		}
	}

	/**
	 * Checks what's playing for the current voice, and stops playing it if necessary.
	 */
	private void stop(Voice voice, int tick) {
		if (tick == voice.noteEndsOnTick) {
			// if we're exact like this we get fewer callbacks, but things like instant start and changing the rate can mess it up
			if (!voice.currentlyPlayingNotes.isEmpty()) {
				stopPlayingEverything(voice);
				voice.noteEndsOnTick = -1; // disable until next note-on
			}
		}
	}

	private void stopPlayingEverything(Voice voice) {
		if (voice.currentlyPlayingNotes.size() > 1) {
			System.out.println("There were currently " + voice.currentlyPlayingNotes.size() + " playing notes: " + voice.currentlyPlayingNotes);
		}
		Iterator<Integer> iterator = voice.currentlyPlayingNotes.iterator();
		while (iterator.hasNext()) {
			Integer currentlyPlayingNote = iterator.next();
			iterator.remove();
			stopPlaying(currentlyPlayingNote);
		}
	}

	private static final class Voice {
		/**
		 * This should <b>ideally</b> only ever contain one thing, but since we support changing the number of steps and resetting the sequencer
		 * using the start button and so on, it's possible that there are times when there might be more than one. As it is more important to
		 * make sure that things things stop playing, we're going to clean this out every so often.
		 * <p>
		 * NOTE: This list must ONLY be mutated from the main loop. Do not add to it or call {@link #stopPlayingEverything(Voice)} from anywhere
		 * other than the main loop
		 */
		private final List<Integer> currentlyPlayingNotes = new ArrayList<>();
		private int noteEndsOnTick = -1;
	}
}
