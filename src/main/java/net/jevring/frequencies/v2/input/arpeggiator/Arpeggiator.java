/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.input.arpeggiator;

import net.jevring.frequencies.v2.control.ControlListener;
import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.control.DiscreteControlListener;
import net.jevring.frequencies.v2.hooks.SequencerVisualizer;
import net.jevring.frequencies.v2.input.KeyTimings;
import net.jevring.frequencies.v2.input.midi.MidiNoteListener;
import net.jevring.frequencies.v2.input.midi.MidiReceiver;
import net.jevring.frequencies.v2.input.sequencer.AbstractMonophonicKeySequencer;
import net.jevring.frequencies.v2.input.sequencer.MutableMonophonicSequencerStep;

import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * An arpeggiator.
 *
 * @author markus@jevring.net
 * @created 2020-05-21 23:50
 */
public class Arpeggiator extends AbstractMonophonicKeySequencer<MutableMonophonicSequencerStep, SequencerVisualizer> {
	// todo: the arpeggiator doesn't work when using the computer keyboard. Works fine with MIDI, though.

	// todo: We can make a "hold" mode, because assuming that the mpk mini is representative of hold modes, letting a key go during hold mode doesn't disable it.
	// This means that any delay or anything else like that to determine what is *currently* held is irrelevant.
	// The hold mode would just *add* notes to what is held, and only ever stop when *all* keys are released.

	/**
	 * Which midi keys are down at any given time.
	 */
	private final NavigableSet<Integer> keys = new ConcurrentSkipListSet<>();
	private final MidiReceiver midiReceiver;
	private Mode mode = Mode.UP_AND_DOWN;
	private int octaves = 1;
	private double gate = 0.5;

	public Arpeggiator(MidiReceiver midiReceiver, Controls controls, KeyTimings keyTimings, SequencerVisualizer sequenceVisualizer) {
		super(sequence(), keyTimings, controls, sequenceVisualizer);
		this.midiReceiver = midiReceiver;
		// If we activate the arpeggiator while notes are on, they'll be locked in on mode and won't be released again
		// This actually also happens on the mpk mini, so maybe it's not that big of a deal
		controls.getBooleanControl("arpeggiator-active").addListener(new DiscreteControlListener<>() {
			@Override
			public void valueChanged(Boolean active, Object source) {
				setActive(active);
			}
		});
		controls.getDiscreteControl("arpeggiator-mode").mapping(Mode::valueOf).addListener(new DiscreteControlListener<>() {
			@Override
			public void valueChanged(Mode value, Object source) {
				Arpeggiator.this.mode = value;
				configureSequence();
			}
		});
		controls.getControl("arpeggiator-octaves").addListener(new ControlListener() {
			@Override
			public void valueChanged(double min, double octaves, double max, Object source) {
				Arpeggiator.this.octaves = (int) octaves;
				configureSequence();
			}
		});
		controls.getControl("arpeggiator-gate").addListener(new ControlListener() {
			@Override
			public void valueChanged(double min, double gate, double max, Object source) {
				Arpeggiator.this.gate = gate;
				configureSequence();
			}
		});
	}

	private static MutableMonophonicSequencerStep[] sequence() {
		MutableMonophonicSequencerStep[] sequence = new MutableMonophonicSequencerStep[256];
		// 256 gives us room to play the whole midi range up and down. We should never need to play any more than that.
		for (int i = 0; i < 256; i++) {
			sequence[i] = new MutableMonophonicSequencerStep();
		}
		return sequence;
	}

	/**
	 * Configures the sequence to play based on the keys that are down and the mode and all the other settings
	 */
	private void configureSequence() {
		int steps = 0;
		if (mode == Mode.UP) {
			for (int octave = 0; octave < octaves; octave++) {
				for (Integer key : keys) {
					step(steps++, octave, key);
				}
			}
		} else if (mode == Mode.DOWN) {
			for (int octave = octaves; octave > 0; octave--) {
				Iterator<Integer> it = keys.descendingIterator();
				while (it.hasNext()) {
					Integer key = it.next();
					step(steps++, octave, key);
				}
			}
		} else if (mode == Mode.UP_AND_DOWN) {
			for (int octave = 0; octave < octaves; octave++) {
				for (Integer key : keys) {
					step(steps++, octave, key);
				}
			}
			for (int octave = octaves; octave > 0; octave--) {
				Iterator<Integer> it = keys.descendingIterator();
				while (it.hasNext()) {
					Integer key = it.next();
					step(steps++, octave, key);
				}
			}
		} else if (mode == Mode.RANDOM) {
			steps = randomizeSteps();
		} else {
			System.err.println("Unknown arpeggiator mode " + mode);
			steps = 0;
		}
		setSteps(steps);
	}

	@Override
	protected void visualizeStep(int currentStep) {
		// do nothing. there's nothing to visualize for the arpeggiator
	}

	@Override
	protected void visualizeBeat() {
		sequencerVisualizer.visualizedSequencerBeat(this);
	}

	private int randomizeSteps() {
		int steps = 0;
		List<Integer> randomKeys = new ArrayList<>(keys);
		Collections.shuffle(randomKeys);
		for (int octave = 0; octave < octaves; octave++) {
			// This keeps the shuffle order within each octave
			for (Integer key : randomKeys) {
				step(steps++, octave, key);
			}
		}
		return steps;
	}

	private void step(int steps, int octave, Integer key) {
		MutableMonophonicSequencerStep arpeggiatorStep = sequencerStep(steps).getForVoice(0);
		arpeggiatorStep.setGate(gate);
		arpeggiatorStep.setNote(key + (octave * 12));
	}

	@Override
	protected void nextCycle() {
		if (mode == Mode.RANDOM) {
			// randomize each cycle
			randomizeSteps();
		}
	}

	@Override
	protected void setActive(boolean active) {
		keys.clear();
		super.setActive(active);
		if (active) {
			midiReceiver.listen(new MidiNoteListener() {
				@Override
				public void down(int key, int velocity) {
					// store the key so that we can play it
					boolean immediateStart = keys.isEmpty();
					keys.add(key);
					configureSequence();
					if (immediateStart) {
						reset();
					}
				}

				@Override
				public void up(int key) {
					// remove the key so we no longer play it
					keys.remove(key);
					configureSequence();
				}
			});
		} else {
			midiReceiver.stopListening();
		}
	}
}
