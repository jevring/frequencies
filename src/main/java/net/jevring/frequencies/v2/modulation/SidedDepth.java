/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.modulation;

/**
 * How much modulation depth we have in either direction.
 * For a bipolar modulator, we get an equal negative and positive depth.
 * A unipolar modulator will add its modulation on either side.
 * We might have a bipolar modulator with depth 0.5, and a unipolar
 * modulator with depth -0.5. In this case we'd end up with -1, 0.5,
 * as the -0.5 from the bipolar and the -0.5 for the unipolar would add
 * up on the negative side. The 0.5 on the positive side comes from just
 * the bipolar modulator.
 *
 * @author markus@jevring.net
 * @created 2021-02-10 00:20
 */
public record SidedDepth(double positive, double negative) {

	public static SidedDepth unipolar(double depth) {
		if (depth < 0) {
			return new SidedDepth(0, depth);
		} else {
			return new SidedDepth(depth, 0);
		}
	}
}
