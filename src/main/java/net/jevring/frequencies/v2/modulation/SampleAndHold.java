/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.modulation;

import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.control.curves.Linear;

import java.util.Locale;

/**
 * Samples a random signal with a given frequency.
 *
 * @author markus@jevring.net
 * @created 2020-05-20 23:40
 */
public class SampleAndHold {
	private final Control frequency;
	private final Control slew;
	private final float sampleRate;
	private double[] samples;
	private int samplesRemainingInLastInterval;
	private int slewSamplesRemainingInLastInterval;
	private double randomValueInLastInterval;
	private double previousSlewTick;
	private double lastGeneratedRandomValue;

	public SampleAndHold(Control frequency, Control slew, float sampleRate) {
		this.frequency = frequency;
		this.slew = slew;
		this.sampleRate = sampleRate;
	}

	public double[] getSamples(int samplesToGenerate) {
		if (this.samples == null) {
			samples = new double[samplesToGenerate];
			double freq = frequency.getCurrentValue();
			if (freq > 0d) {
				int samplesPerValue = (int) Math.round(sampleRate / freq);

				double slew = this.slew.getCurrentValue();
				int slewSamplesPerValue = (int) Math.round(samplesPerValue * slew);

				double slewTick = previousSlewTick;
				double randomValue = randomValueInLastInterval;
				int samplesRemainingInInterval = Math.min(samplesRemainingInLastInterval, samplesPerValue);
				int slewSamplesRemainingInInterval = Math.min(slewSamplesRemainingInLastInterval, slewSamplesPerValue);
				for (int i = 0; i < samplesToGenerate; i++) {
					if (samplesRemainingInInterval > 0) {
						samplesRemainingInInterval--;
						if (slewSamplesRemainingInInterval > 0) {
							slewSamplesRemainingInInterval--;
							randomValue += slewTick;
						} // else we've just reached the level we were slewing to, and there's nothing to do
					} else {
						samplesRemainingInInterval = samplesPerValue;
						slewSamplesRemainingInInterval = slewSamplesPerValue;
						// This is the value we want to reach with our slewed steps
						double nextValue = randomValue();
						if (slewSamplesRemainingInInterval > 0) {
							// How far are we from the next value
							double diff = nextValue - lastGeneratedRandomValue;
							slewTick = diff / slewSamplesPerValue;
						}
						randomValue = lastGeneratedRandomValue;
						lastGeneratedRandomValue = nextValue;
					}
					samples[i] = randomValue;
				}
				samplesRemainingInLastInterval = samplesRemainingInInterval;
				slewSamplesRemainingInLastInterval = slewSamplesRemainingInInterval;
				randomValueInLastInterval = randomValue;
				previousSlewTick = slewTick;
			}
			return samples;
		} else if (this.samples.length != samplesToGenerate) {
			throw new IllegalArgumentException("Can't call getSamples with a different number of samples to generate between calls to next()");
		} else {
			return this.samples;
		}
	}

	/**
	 * Produces a random value in the range -1 .. +1
	 */
	private double randomValue() {
		return (Math.random() * 2d) - 1d;
	}

	public void next() {
		this.samples = null;
	}

	public static void main(String[] args) {
		SampleAndHold sampleAndHold =
				new SampleAndHold(new Control("dummy", 0, 25, 50, new Linear(), false), new Control("dummy", 0, 0.5, 1, new Linear(), false), 44100);

		int chunkSize = 441;
		for (int i = 0; i <= 44100; i += chunkSize) {
			double[] values = sampleAndHold.getSamples(chunkSize);

			for (double d : values) {
				System.out.printf(Locale.US, "%f%n", d);
			}
			sampleAndHold.next();
		}
	}
}
