/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.modulation.matrix;

import net.jevring.frequencies.v2.input.Instruction;
import net.jevring.frequencies.v2.modular.AdditiveMultiSource;
import net.jevring.frequencies.v2.modular.AttenuverterModule;
import net.jevring.frequencies.v2.modulation.SidedDepth;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Tracks which modulation sources are connected to which targets.
 * This is so that {@link AdditiveMultiSource} can be used without the modulation matrix.
 *
 * @author markus@jevring.net
 * @created 2021-02-07 23:38
 */
public class Connection {
	private final Set<ModulationSource> hardwiredSources = EnumSet.noneOf(ModulationSource.class);
	private final Map<ModulationSource, AttenuverterModule> connections = new LinkedHashMap<>();
	private final AdditiveMultiSource additiveMultiSource;

	public Connection(AdditiveMultiSource additiveMultiSource) {
		this.additiveMultiSource = additiveMultiSource;
	}

	public void connect(ModulationSource modulationSource, AttenuverterModule input, boolean hardwired) {
		if (connections.putIfAbsent(modulationSource, input) != null) {
			System.err.println("Connection to " + modulationSource + " already present. Skipping.");
			return;
		}
		additiveMultiSource.connect(input);
		if (hardwired) {
			hardwiredSources.add(modulationSource);
		}
	}

	public void disconnect(ModulationSource modulationSource) {
		if (hardwiredSources.contains(modulationSource)) {
			System.err.println("Modulation source is hardwired: " + modulationSource);
			return;
		}
		additiveMultiSource.disconnect(connections.remove(modulationSource));
	}

	public SidedDepth getSidedDepth() {
		// For visualization only
		double positive = 0;
		double negative = 0;
		for (Map.Entry<ModulationSource, AttenuverterModule> entry : connections.entrySet()) {
			ModulationSource modulationSource = entry.getKey();
			AttenuverterModule attenuverterModule = entry.getValue();
			double depth = attenuverterModule.getDepth().getCurrentValue();
			if (modulationSource.isBipolar()) {
				// No matter the sign of the depth, it can go both ways in equal amount
				positive += Math.abs(depth);
				negative -= Math.abs(depth);
			} else if (depth < 0) {
				// add the negative to make it more negative
				negative += depth;
			} else {
				// add the positive to make it more positive
				positive += depth;
			}
		}
		return new SidedDepth(positive, negative);
	}

	public double[] generateSamples(int samplesToGenerate, Instruction instruction) {
		return additiveMultiSource.generateSamples(samplesToGenerate, instruction);
	}

	public void clear() {
		Iterator<Map.Entry<ModulationSource, AttenuverterModule>> ic = connections.entrySet().iterator();
		while (ic.hasNext()) {
			Map.Entry<ModulationSource, AttenuverterModule> next = ic.next();
			if (!hardwiredSources.contains(next.getKey())) {
				ic.remove();
				additiveMultiSource.disconnect(next.getValue());
			}
		}
	}

	/**
	 * This should only be used for loading and saving.
	 * @param includeHardwired
	 */
	public Map<ModulationSource, AttenuverterModule> getConnections(boolean includeHardwired) {
		if (includeHardwired) {
			return Collections.unmodifiableMap(connections);
		} else {
			return connections.entrySet()
			                  .stream()
			                  .filter(entry -> !hardwiredSources.contains(entry.getKey()))
			                  .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		}
	}
}
