/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.modulation.matrix;

import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.input.Instruction;
import net.jevring.frequencies.v2.modular.AdditiveMultiSource;
import net.jevring.frequencies.v2.modular.AttenuverterModule;
import net.jevring.frequencies.v2.modular.Source;
import net.jevring.frequencies.v2.modulation.SidedDepth;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * The instance of a modulation matrix for a specific voice.
 *
 * @author markus@jevring.net
 * @created 2020-05-23 21:41
 */
public class ModulationMatrixVoice {
	private final Map<ModulationSource, Source> sources = new LinkedHashMap<>();
	/**
	 * Had we had an actual modular thing, with individual cables, we wouldn't have needed this, as the
	 * cables would have identity. However, since we don't, we need to keep track of these virtual cables
	 * somewhere, and that somewhere is here.
	 */
	private final Map<ModulationTarget, Connection> connections = new LinkedHashMap<>();

	public void registerSource(ModulationSource modulationSource, Source source) {
		sources.put(modulationSource, source);
	}

	public void registerTarget(ModulationTarget modulationTarget, AdditiveMultiSource target) {
		connections.put(modulationTarget, new Connection(target));
	}

	public SidedDepth getModulationMaxForTarget(ModulationTarget modulationTarget) {
		// For visualization only
		Connection connection = connections.get(modulationTarget);
		if (connection == null) {
			return null;
		} else {
			return connection.getSidedDepth();
		}
	}

	public double[] getModulationForTarget(ModulationTarget modulationTarget, int samplesToGenerate, Instruction instruction) {
		// For visualization only
		Connection connection = connections.get(modulationTarget);
		if (connection == null) {
			return new double[samplesToGenerate];
		} else {
			return connection.generateSamples(samplesToGenerate, instruction);
		}
	}

	/**
	 * This adds a <b>permanent</b> mapping to the modulation matrix. Calling {@link #clear()} does not affect things that are hard wired.
	 */
	public void hardwire(ModulationSource modulationSource, ModulationTarget modulationTarget, Control depth) {
		add(modulationSource, modulationTarget, depth, true);
	}

	void add(ModulationSource modulationSource, ModulationTarget modulationTarget, Control modulationDepthControl, boolean hardwired) {
		// Multiple things can already pull from a source, so one source for multiple targets is already solved by default.
		Source source = sources.get(modulationSource);

		// For multiple sources to go in to a single target, however, we need to collect all those inputs, 
		// then add them together, and send the final addition to the target.
		Connection connection = connections.get(modulationTarget);
		if (connection == null) {
			// This shouldn't happen
			System.err.println("No target registered for " + modulationTarget);
			return;
		}
		connection.connect(modulationSource, new AttenuverterModule(modulationDepthControl, source), hardwired);
	}

	void remove(ModulationSource modulationSource, ModulationTarget modulationTarget) {
		if (modulationSource == null && modulationTarget == null) {
			return;
		}
		Connection connection = connections.get(modulationTarget);
		if (connection != null) {
			connection.disconnect(modulationSource);
		} else {
			System.err.println("Expected target proxy for " + modulationSource + " and " + modulationTarget + " to be present, but it wasn't.");
		}
	}

	void clear() {
		for (Connection connection : connections.values()) {
			connection.clear();
		}
	}

	/**
	 * This should only be used for loading and saving.
	 */
	List<ModulationMatrixRow> getModulationConfig(boolean includeHardwired) {
		return connections.entrySet().stream().flatMap(entry -> {
			ModulationTarget target = entry.getKey();
			Connection connectionProxy = entry.getValue();

			return connectionProxy.getConnections(includeHardwired)
			                      .entrySet()
			                      .stream()
			                      .map(tpe -> new ModulationMatrixRow(tpe.getKey(), target, tpe.getValue().getDepth()));
		}).collect(Collectors.toUnmodifiableList());
	}
}
