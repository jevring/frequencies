/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.modulation.matrix;

/**
 * Sources for modulation.
 *
 * @author markus@jevring.net
 * @created 2020-04-10 20:55
 */
public enum ModulationSource {
	LFO1(true),
	LFO1_POSITIVE(false),
	LFO2(true),
	LFO2_POSITIVE(false),
	LFO3(true),
	LFO3_POSITIVE(false),
	SAMPLE_AND_HOLD(true),
	SAMPLE_AND_HOLD_POSITIVE(false),

	VELOCITY(false),
	PITCH_BEND(true),
	RANDOM_NOISE(true),
	FILTER_ENVELOPE(false),
	KEYBOARD_TRACKING(false),
	AMPLIFIER_ENVELOPE(false),
	MODULATION_ENVELOPE(false);

	private final boolean bipolar;

	ModulationSource(boolean bipolar) {
		this.bipolar = bipolar;
	}

	public boolean isBipolar() {
		return bipolar;
	}


	@Override
	public String toString() {
		if (this == LFO1_POSITIVE) {
			return "LFO1+";
		} else if (this == LFO2_POSITIVE) {
			return "LFO2+";
		} else if (this == LFO3_POSITIVE) {
			return "LFO3+";
		} else if (this == SAMPLE_AND_HOLD_POSITIVE) {
			return "SAMPLE_AND_HOLD+";
		} else {
			return super.toString();
		}
	}
}
