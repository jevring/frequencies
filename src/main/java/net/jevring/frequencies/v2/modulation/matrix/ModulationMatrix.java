/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.modulation.matrix;

import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.control.curves.Linear;

import java.util.List;

/**
 * Contains all the possible sources and targets for the modulation matrix, as well as the matrix itself.
 *
 * @author markus@jevring.net
 * @created 2020-04-02 18:58
 */
public class ModulationMatrix {
	private final ModulationMatrixVoice[] voices;
	private final Controls controls;

	public ModulationMatrix(Controls controls, int numberOfVoices) {
		this.controls = controls;
		voices = new ModulationMatrixVoice[numberOfVoices];
		for (int i = 0; i < numberOfVoices; i++) {
			voices[i] = new ModulationMatrixVoice();
		}
	}

	public ModulationMatrixVoice voice(int i) {
		return voices[i];
	}

	public Control add(ModulationSource modulationSource, ModulationTarget modulationTarget) {
		String depthControlName = controlName(modulationSource, modulationTarget);
		Control modulationDepthControl = controls.getControl(depthControlName);
		if (modulationDepthControl == null) {
			// This will happen when we add new things in the matrix. 
			// When we're loading from config file, this should already exist
			modulationDepthControl = controls.createControl(depthControlName, -1, 1, 0, new Linear());
		}
		for (ModulationMatrixVoice voice : voices) {
			voice.add(modulationSource, modulationTarget, modulationDepthControl, false);
		}
		return modulationDepthControl;
	}

	public void remove(ModulationSource modulationSource, ModulationTarget modulationTarget) {
		for (ModulationMatrixVoice voice : voices) {
			voice.remove(modulationSource, modulationTarget);
		}
	}

	private String controlName(ModulationSource modulationSource, ModulationTarget modulationTarget) {
		return lowerCaseSnakeCase(modulationSource.name()) + "-to-" + lowerCaseSnakeCase(modulationTarget.name()) + "-depth";
	}

	private String lowerCaseSnakeCase(String s) {
		return s.toLowerCase().replaceAll("_", "-");
	}

	public void clear() {
		for (ModulationMatrixVoice voice : voices) {
			voice.clear();
		}
	}

	/**
	 * This should <b>only</b> be used when loading and saving. For other uses, please see {@link #add(ModulationSource, ModulationTarget)}
	 */
	public List<ModulationMatrixRow> getModulationConfig(boolean includeHardwired) {
		// as the modulation, in the sense of what is and isn't configured, should be identical for all voices, we only need to look at the first voice
		return voices[0].getModulationConfig(includeHardwired);
	}
}
