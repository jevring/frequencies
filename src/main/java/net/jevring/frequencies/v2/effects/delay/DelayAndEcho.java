/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.effects.delay;

import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.effects.CircularBuffer;
import net.jevring.frequencies.v2.effects.DryWetMixer;
import net.jevring.frequencies.v2.effects.Effect;

/**
 * A delay/echo effect that blends current samples with older samples according to some mix.
 * <p>
 * A nice explanation of settings (though we don't have all of these), and the difference
 * between delay and echo: <a href="https://helpx.adobe.com/audition/using/delay-echo-effects.html">Adobe Delay and echo effects.</a>
 *
 * @author markus@jevring.net
 * @created 2020-05-01 13:17
 */
public class DelayAndEcho implements Effect {
	/**
	 * When this is set to 1, it's 100% dry. When this is set to 0, it's 100% wet.
	 */
	private final Control dryWetMix;
	/**
	 * No matter the setting, this can be no longer than <code>(sampleRate / 1000d) * maxDelaySamples</code>
	 */
	private final Control delayInMilliseconds;
	/**
	 * How much we should reduce the wet part. A higher decay means the effect dies out faster. From 0 to 1.
	 */
	private final Control decay;
	private final CircularBuffer buffer;
	private final double samplesPerMillisecond;

	public DelayAndEcho(float sampleRate, Control dryWetMix, Control delayInMilliseconds, Control decay) {
		this.decay = decay;
		this.dryWetMix = dryWetMix;
		this.delayInMilliseconds = delayInMilliseconds;
		this.samplesPerMillisecond = sampleRate / 1000d;
		this.buffer = new CircularBuffer(sampleRate, 3);
	}

	private int delayInSamples() {
		return (int) (samplesPerMillisecond * Math.round(delayInMilliseconds.getCurrentValue()));
	}

	@Override
	public double[] apply(double[] input) {
		int delayInSamples = delayInSamples();

		buffer.add(input);

		double decay = (1d - this.decay.getCurrentValue());

		double[] wet = new double[input.length];
		for (int i = 0; i < input.length; i++) {
			double delayValue = buffer.get(i - delayInSamples);
			// The decay applies to the *next* time we encounter this sample.
			// That's why it's applied here, and not in the 'wet' variable.
			// Otherwise the decay starts too early
			buffer.increase(i, delayValue * decay);
			wet[i] = delayValue;
		}

		return DryWetMixer.mix(input, wet, dryWetMix.getCurrentValue());
	}

	public void reset() {
		buffer.reset();
	}
}
