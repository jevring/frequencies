/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.effects;

import java.util.Arrays;

/**
 * A circular buffer.
 *
 * @author markus@jevring.net
 * @created 2021-02-19 21:21
 */
public class CircularBuffer {
	private final double[] buffer;

	private int endOfBufferPointer = 0;
	private int startOfBufferPointer = 0;

	public CircularBuffer(double sampleRate, double bufferLengthInSeconds) {
		this((int) Math.round(sampleRate * bufferLengthInSeconds));
	}

	public CircularBuffer(int lengthInSamples) {
		this.buffer = new double[lengthInSamples];
	}

	private void copyIntoBuffer(double[] input) {
		int remainingInBuffer = buffer.length - endOfBufferPointer;
		if (input.length < remainingInBuffer) {
			System.arraycopy(input, 0, buffer, endOfBufferPointer, input.length);
			endOfBufferPointer += input.length;
		} else {
			// split it between the end and the start
			System.arraycopy(input, 0, buffer, endOfBufferPointer, remainingInBuffer);
			System.arraycopy(input, remainingInBuffer, buffer, 0, input.length - remainingInBuffer);
			endOfBufferPointer = input.length - remainingInBuffer;
		}
	}

	public void add(double[] input) {
		startOfBufferPointer = endOfBufferPointer;
		copyIntoBuffer(input); // changes the buffer pointer
	}

	public double get(int relativeBufferPointer) {
		return buffer[getBufferPointer(relativeBufferPointer)];
	}

	public void set(int relativeBufferPointer, double value) {
		buffer[getBufferPointer(relativeBufferPointer)] = value;
	}

	public void increase(int relativeBufferPointer, double by) {
		buffer[getBufferPointer(relativeBufferPointer)] += by;
	}

	private int getBufferPointer(int relativeBufferPointer) {
		int bufferPointer = startOfBufferPointer + relativeBufferPointer;
		if (bufferPointer < 0) {
			// Shift this back into the end of the buffer
			bufferPointer = bufferPointer + buffer.length;
		}
		return bufferPointer % buffer.length;
	}

	public void reset() {
		Arrays.fill(buffer, 0d);
	}

	public double[] getLast(int length) {
		if (endOfBufferPointer > length) {
			// there's one contiguous chunk of data we can copy
			return Arrays.copyOfRange(buffer, endOfBufferPointer - length, endOfBufferPointer);
		} else {
			// we have to copy two chunks because we're wrapping around
			double[] output = new double[length];
			int fromStart = length - endOfBufferPointer;
			int fromEnd = length - fromStart;
			// start to middle
			System.arraycopy(buffer, buffer.length - fromStart, output, 0, fromStart);
			// middle to end 
			System.arraycopy(buffer, 0, output, fromStart, fromEnd);
			return output;
		}
	}


	public static void main(String[] args) {
		CircularBuffer b = new CircularBuffer(10, 1);
		b.add(new double[]{1, 2, 3, 4});
		b.add(new double[]{5, 6, 7, 8, 9, 10});
		b.add(new double[]{11, 12, 13, 14, 15});
		System.out.println("last 8 = " + Arrays.toString(b.getLast(8)));
		System.out.println("last 10 = " + Arrays.toString(b.getLast(10)));
		System.out.println("last 3 = " + Arrays.toString(b.getLast(3)));
	}
}
