/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.effects.reverb;

import net.jevring.frequencies.v2.effects.CircularBuffer;

/**
 * An allpass filter. Resources:
 * <ul>
 *     <li><a href="https://ccrma.stanford.edu/~jos/pasp/Allpass_Two_Combs.html">https://ccrma.stanford.edu/~jos/pasp/Allpass_Two_Combs.html</a></li>
 *     <li><a href="https://www.kvraudio.com/forum/viewtopic.php?t=416031">https://www.kvraudio.com/forum/viewtopic.php?t=416031</a></li>
 *     <li><a href="https://dsp.stackexchange.com/questions/19998/allpass-filter-feedforward-feedback-design-and-code">https://dsp.stackexchange.com/questions/19998/allpass-filter-feedforward-feedback-design-and-code</a></li>
 * </ul>
 *
 * @author markus@jevring.net
 * @created 2021-02-18 23:32
 */
public class AllpassFilter {
	private final CircularBuffer outputBuffer;
	private final CircularBuffer inputBuffer;
	private final double gain;

	private int delayInSamples;

	public AllpassFilter(double sampleRate, int delayInSamples, double gain) {
		this.outputBuffer = new CircularBuffer(sampleRate, 3d);
		this.inputBuffer = new CircularBuffer(sampleRate, 3d);
		this.delayInSamples = delayInSamples;
		this.gain = gain;
	}

	public void setDelayInSamples(int delayInSamples) {
		this.delayInSamples = delayInSamples;
	}

	public double[] apply(double[] input) {
		inputBuffer.add(input);
		outputBuffer.add(new double[input.length]);

		for (int i = 0; i < input.length; i++) {
			// y(n) = bx(n) + x(n - M) - ay(n - M)
			// y is output
			// x is input
			// b = a = gain
			double value = (gain * inputBuffer.get(i)) + (inputBuffer.get(i - delayInSamples)) - (gain * outputBuffer.get(i - delayInSamples));
			outputBuffer.set(i, value);
			// NOTE TO SELF: Even though we do this in the comb filters, DO NOT DO IT HERE!
			// It completely changes the characteristics of the filter, and is not desired.
			//inputBuffer.increase(i, value);
		}

		return outputBuffer.getLast(input.length);
	}
}
