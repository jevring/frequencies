/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.effects;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Locale;

/**
 * Tools for finding coprimes for delays. Might be a bit crude, but it's fast enough.
 *
 * @author markus@jevring.net
 * @created 2021-02-20 20:02
 */
public class Coprimes {
	public static int[] findCoprimesAtScale(int[] input, double scale) {
		int mutatorStep = 0;
		int[] mutable = scale(input, scale);
		for (int attempt = 0; attempt <= 500; attempt++) {
			if (coprime(mutable)) {
				System.out.printf(Locale.US, "Found coprimes for scale %.3f after %3d attempts -> %s%n", scale, attempt, Arrays.toString(mutable));
				return mutable;
			} else {
				mutable[mutatorStep++ % mutable.length]++;
			}
		}
		System.err.println("Could not find coprimes after 500 attempts at scale " + scale);
		return input;
	}

	private static int[] scale(int[] is, double scale) {
		int[] output = Arrays.copyOf(is, is.length);
		for (int i = 0; i < output.length; i++) {
			output[i] = (int) Math.round(output[i] * scale);
		}
		return output;
	}

	private static boolean coprime(int[] input) {
		for (int i = 0; i < input.length; i++) {
			for (int j = 0; j < input.length; j++) {
				if (i == j) {
					// don't check against itself
					continue;
				}
				if (!coprime(input[i], input[j])) {
					return false;
				}
			}
		}
		return true;
	}

	private static boolean coprime(int a, int b) {
		return BigInteger.valueOf(a).gcd(BigInteger.valueOf(b)).equals(BigInteger.ONE);
	}

	public static void main(String[] args) {
		int[] baseDelays = new int[]{1687, 1601, 2053, 2251};
		double scale = 1.0;

		// walk up 500 steps and see if we can (quickly) generate values that are mutually prime
		for (double s = scale; s < 5d; s += 0.01) {
			findCoprimesAtScale(baseDelays, s);
		}
	}
}
