/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.effects.reverb;

import net.jevring.frequencies.v2.effects.CircularBuffer;

/**
 * Implementation of a comb filter. Resources:
 * <ul>
 *     <li><a href="https://ccrma.stanford.edu/~jos/filters/Analysis_Digital_Comb_Filter.html">https://ccrma.stanford.edu/~jos/filters/Analysis_Digital_Comb_Filter.html</a></li>
 *     <li><a href="https://ccrma.stanford.edu/~jos/pasp/Feedback_Comb_Filters.html">https://ccrma.stanford.edu/~jos/pasp/Feedback_Comb_Filters.html</a></li>
 * </ul>
 *
 * @author markus@jevring.net
 * @created 2021-02-18 23:28
 */
public class FeedbackCombFilter {
	private final CircularBuffer buffer;
	private final double gain;
	private int delayInSamples;

	public FeedbackCombFilter(float sampleRate, int delayInSamples, double gain) {
		this.buffer = new CircularBuffer(sampleRate, 3d);
		this.delayInSamples = delayInSamples;
		this.gain = gain;
	}

	public void setDelayInSamples(int delayInSamples) {
		this.delayInSamples = delayInSamples;
	}

	public double[] apply(double[] input) {
		buffer.add(input);
		for (int i = 0; i < input.length; i++) {
			// y(n) = x(n) + gy(n-M)
			// We're not picking up x(n) here because we're using buffer.increase() instead of buffer.set(), 
			// which is a buffer[i] += operation, which means that the x(n) is picked up implicitly
			// we're just adding the delayed signal into the original signal
			double delayValue = gain * buffer.get(i - delayInSamples);
			buffer.increase(i, delayValue);
		}
		return buffer.getLast(input.length);
	}

	public void reset() {
		buffer.reset();
	}
}
