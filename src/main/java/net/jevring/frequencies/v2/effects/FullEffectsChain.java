/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.effects;

import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.effects.bitcrusher.BitCrusher;
import net.jevring.frequencies.v2.effects.chorus.Chorus;
import net.jevring.frequencies.v2.effects.delay.DelayAndEcho;
import net.jevring.frequencies.v2.effects.reverb.SchroederReverb;

import java.util.ArrayList;
import java.util.List;

/**
 * A single chain of effects that's invoked from one single place.
 * 
 * todo: come up with a better name for this.
 *
 * @author markus@jevring.net
 * @created 2021-02-24 19:25
 */
public class FullEffectsChain implements EffectsChain {
	private final List<Effect> chain = new ArrayList<>();
	private final DelayAndEcho delayAndEcho;
	private final SchroederReverb reverb;
	private final BitCrusher bitCrusher;
	private final Chorus chorus;

	// todo: maybe a stereo width effect?

	public FullEffectsChain(Controls controls, float sampleRate) {
		this.bitCrusher = new BitCrusher(controls.getControl("bit-crusher-downsampling"), controls.getControl("bit-crusher-resolution"));
		this.delayAndEcho = new DelayAndEcho(sampleRate,
		                                     controls.getControl("delay-dry-wet-mix"),
		                                     controls.getControl("delay-in-milliseconds"),
		                                     controls.getControl("delay-decay"));
		this.chorus = new Chorus(sampleRate,
		                         controls.getControl("chorus-dry-wet-mix"),
		                         controls.getControl("chorus-delay-in-milliseconds-1"),
		                         controls.getControl("chorus-delay-in-milliseconds-2"),
		                         controls.getControl("chorus-rate-1"),
		                         controls.getControl("chorus-rate-2"),
		                         controls.getControl("chorus-depth-1"),
		                         controls.getControl("chorus-depth-2"));
		this.reverb = new SchroederReverb(sampleRate, controls.getControl("reverb-dry-wet-mix"), controls.getControl("reverb-length"));

		chain.add(delayAndEcho);
		chain.add(reverb);
	}

	@Override
	public double[] apply(double[] samples) {
		for (Effect effect : chain) {
			samples = effect.apply(samples);
		}
		return samples;
	}
}
