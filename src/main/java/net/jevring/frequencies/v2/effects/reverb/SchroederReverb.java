/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.effects.reverb;

import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.control.ControlListener;
import net.jevring.frequencies.v2.effects.Coprimes;
import net.jevring.frequencies.v2.effects.DryWetMixer;
import net.jevring.frequencies.v2.effects.Effect;
import net.jevring.frequencies.v2.util.SignalEnergyAdder;

/**
 * A schroeder reverb. Resources:
 * <ul>
 *     <li><a href="https://www.music.mcgill.ca/~gary/618/week3/node10.html">https://www.music.mcgill.ca/~gary/618/week3/node10.html</a></li>
 *     <li><a href="https://ccrma.stanford.edu/~jos/pasp/Schroeder_Reverberator_called_JCRev.html>https://ccrma.stanford.edu/~jos/pasp/Schroeder_Reverberator_called_JCRev.html</a></li><
 *     <li><a href="https://ccrma.stanford.edu/~jos/Reverb/A_Schroeder_Reverberator_called.html">https://ccrma.stanford.edu/~jos/Reverb/A_Schroeder_Reverberator_called.html</a></li>
 *     <li><a href="https://www.youtube.com/watch?v=ZgxjjBsB7_M">https://www.youtube.com/watch?v=ZgxjjBsB7_M</a></li>
 * </ul>
 *
 * @author markus@jevring.net
 * @created 2021-02-18 23:28
 */
public class SchroederReverb implements Effect {
	private static final double CCRMA_SAMPLE_RATE = 25_000d;
	private final AllpassFilter[] allpassFilters = new AllpassFilter[3];
	private final FeedbackCombFilter[] feedbackCombFilters = new FeedbackCombFilter[4];
	private final Control dryWetMix;

	public SchroederReverb(float sampleRate, Control dryWetMix, Control length) {
		this.dryWetMix = dryWetMix;
		double sampleRateMultiplier = sampleRate / CCRMA_SAMPLE_RATE;

		// Initial delays and gains from https://ccrma.stanford.edu/~jos/pasp/Schroeder_Reverberator_called_JCRev.html
		int[] combFilterDelays = Coprimes.findCoprimesAtScale(new int[]{1687, 1601, 2053, 2251}, sampleRateMultiplier);
		int[] allpassDelays = Coprimes.findCoprimesAtScale(new int[]{347, 113, 37}, sampleRateMultiplier);

		feedbackCombFilters[0] = new FeedbackCombFilter(sampleRate, combFilterDelays[0], 0.773);
		feedbackCombFilters[1] = new FeedbackCombFilter(sampleRate, combFilterDelays[1], 0.802);
		feedbackCombFilters[2] = new FeedbackCombFilter(sampleRate, combFilterDelays[2], 0.753);
		feedbackCombFilters[3] = new FeedbackCombFilter(sampleRate, combFilterDelays[3], 0.733);

		allpassFilters[0] = new AllpassFilter(sampleRate, allpassDelays[0], 0.7);
		allpassFilters[1] = new AllpassFilter(sampleRate, allpassDelays[1], 0.7);
		allpassFilters[2] = new AllpassFilter(sampleRate, allpassDelays[2], 0.7);

		length.addListener(new ControlListener() {
			@Override
			public void valueChanged(double min, double newValue, double max, Object source) {
				int[] modifiedCombFilterDelays = Coprimes.findCoprimesAtScale(combFilterDelays, newValue);
				for (int i = 0; i < feedbackCombFilters.length; i++) {
					FeedbackCombFilter feedbackCombFilter = feedbackCombFilters[i];
					feedbackCombFilter.setDelayInSamples(modifiedCombFilterDelays[i]);
				}

				// If we increase the allpass filters' delay, then it sounds super metallic. In fact very short delays are needed here
				// According to https://ccrma.stanford.edu/~jos/pasp05/Schroeder_Reverberator_called_JCRev.html
				// "The delay lengths in these comb filters may be used to adjust the illusion of ``room size''."
				// This means that we don't touch the allpass filters, which are normally for early echoes only anyway
				
				/*
				int[] modifiedAllpassFilterDelays = Coprimes.findCoprimesAtScale(allpassDelays, newValue);
				for (int i = 0; i < allpassFilters.length; i++) {
					AllpassFilter allpassFilter = allpassFilters[i];
					allpassFilter.setDelayInSamples(modifiedAllpassFilterDelays[i]);
				}
				*/
			}
		});
	}

	@Override
	public double[] apply(double[] input) {


		double[] wet = input;
		wet = allpassFiltersInSeries(wet);
		wet = combFiltersInParallel(wet);

		return DryWetMixer.mix(input, wet, dryWetMix.getCurrentValue());
	}

	private double[] allpassFiltersInSeries(double[] wet) {
		for (AllpassFilter allpassFilter : allpassFilters) {
			wet = allpassFilter.apply(wet);
		}
		return wet;
	}

	private double[] combFiltersInParallel(double[] input) {
		SignalEnergyAdder combFilterAdder = new SignalEnergyAdder(feedbackCombFilters.length, input.length);
		for (FeedbackCombFilter feedbackCombFilter : feedbackCombFilters) {
			combFilterAdder.add(feedbackCombFilter.apply(input));
		}
		return combFilterAdder.getOutput();
	}

}
