/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.effects.chorus;

import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.effects.CircularBuffer;
import net.jevring.frequencies.v2.effects.Effect;
import net.jevring.frequencies.v2.oscillators.Oscillator;
import net.jevring.frequencies.v2.waveforms.TriangleWaveform;

/**
 * A chorus effect copies the sound and slightly delays it with variable delay. This delay is shifted via an LFO.
 *
 * @author markus@jevring.net
 * @created 2021-02-12 21:37
 */
public class Chorus implements Effect {

	// https://www.reddit.com/r/edmproduction/comments/1o6jpr/what_is_the_difference_between_chorus_and_unison/ccp8j3j/?utm_source=share&utm_medium=web2x&context=3
	// https://www.izotope.com/en/learn/understanding-chorus-flangers-and-phasers-in-audio-production.html
	// https://transverseaudio.com/posts/how-to-use-a-chorus-effect
	/**
	 * When this is set to 1, it's 100% dry. When this is set to 0, it's 100% wet.
	 */
	private final Control dryWetMix;
	private final TinyModulatedDelay delay1;
	private final TinyModulatedDelay delay2;

	// todo: what settings do we need? 
	//  The only thing that seems to matter is the rate and the depth. They don't even seem to need to be independent.
	//  We can fix the delay at almost anything. Say 5 and 10ms, and then let the depth vary between 0 and 3ms and the rate from 0 to 5hz.
	//  We can make the second one slightly different from the first, but we should still derive everything from just these two controls.

	public Chorus(float sampleRate,
	              Control dryWetMix,
	              Control delay1InMilliseconds,
	              Control delay2InMilliseconds,
	              Control rate1,
	              Control rate2,
	              Control depth1,
	              Control depth2) {
		this.dryWetMix = dryWetMix;

		this.delay1 = new TinyModulatedDelay(sampleRate, delay1InMilliseconds, depth1, rate1);
		this.delay2 = new TinyModulatedDelay(sampleRate, delay2InMilliseconds, depth2, rate2);
	}

	@Override
	public double[] apply(double[] input) {
		double[] d1 = delay1.apply(input);
		double[] d2 = delay2.apply(input);
		double[] output = new double[input.length];

		double dryProportion = dryWetMix.getCurrentValue();
		double wetProportion = (1d - dryProportion);
		for (int i = 0; i < input.length; i++) {
			double dry = input[i] * dryProportion;
			double wet = (d1[i] + d2[i]) * wetProportion;
			double mix = dry + wet;

			output[i] = mix;
		}
		return output;
	}

	private static final class TinyModulatedDelay {
		private final TinyDelay tinyDelay;
		private final Control lfoDepth;
		private final Control lfoRate;
		private final Oscillator lfo;

		private TinyModulatedDelay(float sampleRate, Control delayInMilliseconds, Control lfoDepth, Control lfoRate) {
			this.tinyDelay = new TinyDelay(sampleRate, delayInMilliseconds);
			this.lfoDepth = lfoDepth;
			this.lfoRate = lfoRate;
			this.lfo = new Oscillator(sampleRate);
			this.lfo.setWaveform(new TriangleWaveform());
		}

		public double[] apply(double[] input) {
			double[] zeroes = new double[input.length];
			double[] delayModulation = lfo.generateSamples(input.length, lfoRate.getCurrentValue(), zeroes, zeroes, zeroes, zeroes, zeroes, zeroes, zeroes, zeroes);
			for (int i = 0; i < input.length; i++) {
				delayModulation[i] *= lfoDepth.getCurrentValue();
			}
			return tinyDelay.apply(input, delayModulation);
		}
	}

	/**
	 * This is a single delay line that's 100% wet. To use this mix it in with others.
	 */
	private static final class TinyDelay {
		private final double samplesPerMillisecond;
		private final Control delayInMilliseconds;
		private final CircularBuffer buffer;

		public TinyDelay(double sampleRate, Control delayInMilliseconds) {
			this.delayInMilliseconds = delayInMilliseconds;
			this.samplesPerMillisecond = sampleRate / 1000d;
			// A one second buffer
			this.buffer = new CircularBuffer(sampleRate, 1d);
		}

		public double[] apply(double[] input, double[] modulation) {
			double currentDelay = delayInMilliseconds.getCurrentValue();


			buffer.add(input);

			double[] output = new double[input.length];
			for (int i = 0; i < input.length; i++) {
				int delayInSamples = (int) Math.round(samplesPerMillisecond * (currentDelay + modulation[i]));
				output[i] = buffer.get(i - delayInSamples);
			}
			return output;
		}

		public void reset() {
			buffer.reset();
		}
	}
}
