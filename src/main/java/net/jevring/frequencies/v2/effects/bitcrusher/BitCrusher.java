/*
 * Copyright 2021 Markus Jevring                                              
 *                                                                            
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *                                                                            
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 *                                                                            
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 *                                                                            
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.effects.bitcrusher;

import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.control.curves.Linear;
import net.jevring.frequencies.v2.effects.Effect;
import net.jevring.frequencies.v2.math.Clamp;
import net.jevring.frequencies.v2.oscillators.Oscillator;
import net.jevring.frequencies.v2.waveforms.SawtoothWaveform;

import java.util.Arrays;
import java.util.Locale;

/**
 * Crushes bits.
 * https://en.wikipedia.org/wiki/Bitcrusher
 * Inspired by this: https://www.youtube.com/watch?v=sLvN4VpCjE8
 *
 * @author markus@jevring.net
 * @created 2020-06-14 22:35
 */
public class BitCrusher implements Effect {
	private final double[] buffer = new double[1024];
	private final Control downsampling;
	private final Control resolution;

	private int endOfBufferPointer = 0;
	private double leftoverBucket;
	private int leftoverSamplesRemainingInChunk;

	public BitCrusher(Control downsampling, Control resolution) {
		this.downsampling = downsampling;
		this.resolution = resolution;
	}

	private void copyIntoBuffer(double[] input) {
		int p = endOfBufferPointer;
		for (int i = 0; i < input.length; i++) {
			buffer[p++ % buffer.length] = input[i];
		}
		this.endOfBufferPointer = p % buffer.length;
	}

	// todo: have a dry/wet mix as well
	@Override
	public double[] apply(double[] input) {
		int samplesPerChunk = (int) downsampling.getCurrentValue();
		int resolutionInBits = (int) resolution.getCurrentValue();
		if (samplesPerChunk == 1 && resolutionInBits == 16) {
			// This effectively means that it's disabled
			return input;
		}
		double[] output = new double[input.length];

		int bufferStartPointer = endOfBufferPointer;
		copyIntoBuffer(input); // changes the buffer pointer

		// Expect this to be a value from 1 to like 16 or something
		int samplesRemainingInChunk = samplesPerChunk;
		if (leftoverSamplesRemainingInChunk != 0) {
			samplesRemainingInChunk = leftoverSamplesRemainingInChunk;
		}
		double bucket = 0;
		if (leftoverBucket != 0) {
			bucket = leftoverBucket;
		}
		int outputPointer = 0;
		for (int i = 0; i < input.length; i++) {
			int bufferPointer = bufferStartPointer + i;
			if (bufferPointer < 0) {
				// Shift this back into the end of the buffer
				bufferPointer = bufferPointer + buffer.length;
			}
			double sample = buffer[bufferPointer % buffer.length];
			bucket += sample;
			samplesRemainingInChunk--;
			if (samplesRemainingInChunk == 0) {
				double chunkAverage = Clamp.clamp(bucket / samplesPerChunk, -1, 1);
				double atReducedResolution = reduceResolutionMultiplyDivide(chunkAverage, resolutionInBits);

				int toIndex = Math.min(outputPointer + samplesPerChunk, output.length);
				Arrays.fill(output, outputPointer, toIndex, atReducedResolution);
				bucket = 0;
				samplesRemainingInChunk = samplesPerChunk;
				outputPointer = toIndex;
			}
		}

		// If it's not been reset to exactly the number the next iteration should have had, then we have something remaining
		if (samplesRemainingInChunk != samplesPerChunk) {
			// Dump anything that remains unprocessed into the output
			double chunkAverage = Clamp.clamp(bucket / (samplesPerChunk - samplesRemainingInChunk), -1, 1);
			double atReducedResolution = reduceResolutionMultiplyDivide(chunkAverage, resolutionInBits);
			Arrays.fill(output, outputPointer, output.length, atReducedResolution);
		}
		this.leftoverBucket = bucket;
		this.leftoverSamplesRemainingInChunk = samplesRemainingInChunk;
		return output;
	}

	/**
	 * I came across <a href="https://forum.cockos.com/archive/index.php/t-181951.html">this algorithm</a>, which also works,
	 * but it sounds just as bad as my original one. There's virtually no difference until you get down to like 1 bit, at
	 * which point it's just garbage
	 */
	private static double reduceResolutionMultiplyDivide(double v, int resolutionInBits) {
		double scaling = Math.pow(2, resolutionInBits);
		int scaledUp = (int) (v * scaling);
		double scaledBackDown = scaledUp / scaling;
		return scaledBackDown;

	}

	private static double reduceResolution(double v, int resolutionInBits) {
		int shiftAmount = 16 - resolutionInBits;
		short asShort = (short) (v * Short.MAX_VALUE);
		// reduce bit rate by shifting the value out so that we drop data
		asShort >>= shiftAmount;
		// shift the value back in so that we regain size
		asShort <<= shiftAmount;

		// turn it back into a double
		return asShort / (double) Short.MAX_VALUE;
	}

	public static void main(String[] args) {
		Control downsampling = new Control("", 1, 1, 128, new Linear(), true);
		Control resolution = new Control("", 1, 4, 16, new Linear(), true);
		BitCrusher bitCrusher = new BitCrusher(downsampling, resolution);

		int chunkSize = 44;
		double sampleRate = 44100d;
		Oscillator oscillator = new Oscillator(sampleRate);
		oscillator.setWaveform(new SawtoothWaveform(true));
		double[] zeroes = new double[4096];
		for (int i = 0; i <= 1000; i += chunkSize) {
			double[] values = bitCrusher.apply(oscillator.generateSamples(chunkSize, 440, zeroes, zeroes, zeroes, zeroes, zeroes, zeroes, zeroes, zeroes));

			for (double d : values) {
				System.out.printf(Locale.US, "%f%n", d);
			}
		}
	}
}
