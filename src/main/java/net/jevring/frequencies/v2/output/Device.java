/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.output;

import javax.sound.sampled.*;
import java.util.ArrayList;
import java.util.List;

/**
 * A sound device capable of producing output.
 *
 * @author markus@jevring.net
 * @created 2020-04-14 22:06
 */
public class Device {
	private final AudioFormat audioFormat;
	private final String description;
	private final Mixer mixer;

	private SourceDataLine sourceDataLine;

	private Device(AudioFormat audioFormat, String description, Mixer mixer) {
		this.audioFormat = audioFormat;
		this.description = description;
		this.mixer = mixer;
	}

	public String describe() {
		return description;
	}

	public SourceDataLine open() throws LineUnavailableException {
		if (mixer == null) {
			sourceDataLine = AudioSystem.getSourceDataLine(audioFormat);
		} else {
			sourceDataLine = (SourceDataLine) mixer.getLine(new DataLine.Info(SourceDataLine.class, audioFormat));
		}
		if (sourceDataLine == null) {
			throw new IllegalStateException("Could not get line for format " + audioFormat);
		}
		// 2 seconds
		sourceDataLine.open(audioFormat, (int) (audioFormat.getFrameSize() * audioFormat.getChannels() * audioFormat.getSampleRate() * 2));
		sourceDataLine.start();
		return sourceDataLine;
	}

	public void close() {
		if (sourceDataLine == null || !sourceDataLine.isOpen()) {
			throw new IllegalStateException("You can't close a line that isn't open");
		}
		sourceDataLine.close();
	}

	public static List<Device> supportedSoundDevices(AudioFormat format) {
		DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
		List<Device> supportedDevices = new ArrayList<>();
		for (Mixer.Info mixerInfo : AudioSystem.getMixerInfo()) {
			Mixer mixer = AudioSystem.getMixer(mixerInfo);
			if (mixer.isLineSupported(info)) {
				supportedDevices.add(new Device(format, mixerInfo.toString(), mixer));
			}
		}
		supportedDevices.add(defaultDevice(format));
		return supportedDevices;
	}

	public static Device defaultDevice(AudioFormat format) {
		return new Device(format, "Default", null);
	}
}
