/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.output;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

/**
 * Writes wave files from byte arrays.
 *
 * @author markus@jevring.net
 * @created 2020-05-12 18:35
 */
public class WaveFileWriter {
	private final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	private final AudioFormat audioFormat;
	private final boolean record;
	private int filesWritten = 0;

	public WaveFileWriter(AudioFormat audioFormat, boolean record) {
		this.audioFormat = audioFormat;
		this.record = record;
	}

	public void write(byte[] bytes) {
		if (!record) {
			return;
		}
		byteArrayOutputStream.writeBytes(bytes);
		if (byteArrayOutputStream.size() > 44100 * 10 * 4) {
			dumpSoundDataAndReset();
		}
	}

	public void dumpSoundDataAndReset() {
		if (!record) {
			return;
		}
		try {
			byte[] sound = byteArrayOutputStream.toByteArray();
			File out = new File("output-" + filesWritten++ + ".wav");
			System.out.println("Dumping WAVE data to " + out);
			AudioSystem.write(new AudioInputStream(new ByteArrayInputStream(sound), audioFormat, sound.length), AudioFileFormat.Type.WAVE, out);
		} catch (IOException e) {
			e.printStackTrace();
		}
		byteArrayOutputStream.reset();
	}
}
