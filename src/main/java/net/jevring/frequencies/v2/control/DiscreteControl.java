/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.control;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * Control for a discrete single value of something.
 *
 * @author markus@jevring.net
 * @created 2020-02-23
 */
public class DiscreteControl {
	private final List<DiscreteControlListener<String>> listeners = new ArrayList<>();
	private final List<String> allowedValues;
	private final String defaultValue;
	private volatile String value;
	private final String key;

	DiscreteControl(String key, List<String> allowedValues, String defaultValue) {
		this.allowedValues = List.copyOf(allowedValues);
		if (!allowedValues.contains(defaultValue)) {
			throw new IllegalArgumentException(String.format(
					"Can't specify a default value (%s) that doesn't exist in the list of allowed values %s for control %s",
					defaultValue,
					allowedValues,
					key));
		}
		this.defaultValue = defaultValue;
		this.value = defaultValue;
		this.key = key;
	}

	public void set(String myValue, Object source) {
		this.value = myValue;
		if (!allowedValues.contains(myValue)) {
			throw new IllegalArgumentException(String.format("Disallowed value %s for %s, allowed values: %s", myValue, key, allowedValues));
		}
		updateListeners(source);
	}

	private void updateListeners(Object source) {
		for (DiscreteControlListener<String> listener : listeners) {
			listener.valueChanged(this.value, source);
		}
	}

	public void reset(Object source) {
		this.value = defaultValue;
		updateListeners(source);
	}

	public List<String> getAllowedValues() {
		return allowedValues;
	}

	public String getKey() {
		return key;
	}

	public String get() {
		return this.value;
	}

	public <T> MappingDiscreteControl<T> mapping(Function<String, T> mapper) {
		return new MappingDiscreteControl<>(this, mapper);
	}

	/**
	 * Adds a listener that is notified whenever this control value change. <b>Do not take long in processing this callback!</b>.
	 * Ideally have it in some thread or something, as you are otherwise blocking the whole system.
	 * <p>Upon registration, the caller will get a callback with the current value and range.
	 *
	 * @param listener the listener that should receive the callback
	 * @return a registration that can be used to remove the registered listener
	 */
	public ListenerRegistration addListener(DiscreteControlListener<String> listener) {
		this.listeners.add(listener);
		// Update the listener with the latest value.
		listener.valueChanged(value, this);
		return new ListenerRegistration() {
			@Override
			public void unregister() {
				listeners.remove(listener);
			}
		};
	}
}
