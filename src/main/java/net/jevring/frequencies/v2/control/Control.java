/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.control;

import net.jevring.frequencies.v2.control.curves.Curve;
import net.jevring.frequencies.v2.math.Clamp;

import java.util.ArrayList;
import java.util.List;

/**
 * Control for a single value of something.
 *
 * @author markus@jevring.net
 * @created 2020-02-12 18:49
 */
public class Control {
	private final List<ControlListener> listeners = new ArrayList<>();
	private final List<RawDoubleControlListener> rawDoubleListeners = new ArrayList<>();
	private final List<RawLongControlListener> rawLongListeners = new ArrayList<>();
	private final double defaultValue;
	private final boolean round;
	private final String key;
	private volatile double min;
	private volatile double value;
	private volatile double max;
	private volatile Curve curve;

	public Control(String key, double min, double value, double max, Curve curve, boolean round) {
		this.key = key;
		this.curve = curve;
		this.round = round;
		if (round) {
			this.defaultValue = Math.round(value);
			this.min = Math.round(min);
			this.value = Math.round(value);
			this.max = Math.round(max);
		} else {
			this.defaultValue = value;
			this.min = min;
			this.value = value;
			this.max = max;
		}
	}

	public void reset(Object source) {
		this.value = defaultValue;
		updateListeners(source);
	}

	public String getKey() {
		return key;
	}

	public double getMin() {
		return min;
	}

	public double getMax() {
		return max;
	}

	public Curve getCurve() {
		return curve;
	}

	public boolean isStepped() {
		return round;
	}

	/**
	 * Call this to set the value with <b>your</b> known value. The value will be scaled automatically
	 * according to the curve and the min and max of this control.
	 *
	 * @param myMin   your min value for the input range of the control, for example 0
	 * @param myValue your actual value for the control right now, for example 75
	 * @param myMax   your max value for the input range of the control, for example 100
	 * @param source  the object responsible for setting the value. This can be used to avoid loops.
	 * @see #setRaw(double)
	 */
	public void set(double myMin, double myValue, double myMax, Object source) {
		double scaledValue = curve.scale(myMin, myMax, myValue, min, max);
		if (round) {
			this.value = Math.round(scaledValue);
		} else {
			this.value = scaledValue;
		}
		updateListeners(source);
	}

	public void updateListeners(Object source) {
		for (ControlListener listener : listeners) {
			listener.valueChanged(min, value, max, source);
		}
		for (RawDoubleControlListener rawListener : rawDoubleListeners) {
			rawListener.rawValueChanged(value);
		}
		for (RawLongControlListener rawListener : rawLongListeners) {
			rawListener.rawValueChanged((long) value);
		}
	}

	/**
	 * Sets the value of the control bypassing the curve. This method is almost certainly not what you want. You likely want {@link #set(double, double, double, Object)} instead.
	 *
	 * @see #set(double, double, double, Object)
	 */
	public void setRaw(double value) {
		this.value = Clamp.clamp(value, this.min, this.max);
		updateListeners(this);
	}

	public double getCurrentValue() {
		if (round) {
			return Math.round(this.value);
		} else {
			return this.value;
		}
	}

	/**
	 * Adds a listener that is notified whenever this control value change. <b>Do not take long in processing this callback!</b>.
	 * Ideally have it in some thread or something, as you are otherwise blocking the whole system.
	 * <p>Upon registration, the caller will get a callback with the current value and range.
	 *
	 * @param listener the listener that should receive the callback
	 * @return a registration that can be used to remove the registered listener
	 */
	public ListenerRegistration addListener(ControlListener listener) {
		return addListener(listener, true);
	}

	/**
	 * Adds a listener that is notified whenever this control value change. <b>Do not take long in processing this callback!</b>.
	 * Ideally have it in some thread or something, as you are otherwise blocking the whole system.
	 * <p>Upon registration, the caller will get a callback with the current value and range.
	 *
	 * @param listener the listener that should receive the callback
	 * @return a registration that can be used to remove the registered listener
	 */
	public ListenerRegistration addListener(ControlListener listener, boolean immediatelyCallBackListenerWithCurrentValue) {
		this.listeners.add(listener);
		if (immediatelyCallBackListenerWithCurrentValue) {
			// Update the listener with the latest value.
			listener.valueChanged(min, value, max, this);
		}
		return new ListenerRegistration() {
			@Override
			public void unregister() {
				listeners.remove(listener);
			}
		};
	}

	/**
	 * Adds a listener that is notified whenever this control value change. <b>Do not take long in processing this callback!</b>.
	 * Ideally have it in some thread or something, as you are otherwise blocking the whole system.
	 * <p>Upon registration, the caller will get a callback with the current value and range.
	 *
	 * <p>This is for listeners that know the value range and want the raw value. For example if they don't need to scale it.</p>
	 *
	 * @param rawListener the listener that should receive the callback
	 */
	public void addRawDoubleListener(RawDoubleControlListener rawListener) {
		this.rawDoubleListeners.add(rawListener);
		rawListener.rawValueChanged(value);
	}

	/**
	 * Adds a listener that is notified whenever this control value change. <b>Do not take long in processing this callback!</b>.
	 * Ideally have it in some thread or something, as you are otherwise blocking the whole system.
	 * <p>Upon registration, the caller will get a callback with the current value and range.
	 *
	 * <p>This is for listeners that know the value range and want the raw value. For example if they don't need to scale it.</p>
	 *
	 * @param rawListener the listener that should receive the callback
	 */
	public void addRawLongListener(RawLongControlListener rawListener) {
		this.rawLongListeners.add(rawListener);
		rawListener.rawValueChanged((long) value);
	}

	@Override
	public String toString() {
		return "Control{" + "listeners=" + listeners + ", min=" + min + ", value=" + value + ", max=" + max + ", curve=" + curve + '}';
	}
}
