/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.control;

import net.jevring.frequencies.v2.control.curves.Curve;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The thing into which all inputs go, and from which all signals are taken.
 * These controls <b>must</b> be {@link #createControl(String, double, double, double, Curve) created} up front. Otherwise they will map to nothing.
 *
 * @author markus@jevring.net
 * @created 2020-02-12 18:48
 */
public class Controls {
	private final Map<String, DiscreteControl> discreteControls = new HashMap<>();
	private final Map<String, BooleanControl> booleanControls = new HashMap<>();
	private final Map<String, Control> controls = new HashMap<>();

	private void assertNew(String key) {
		if (controls.containsKey(key)) {
			throw new AssertionError("Key " + key + " already exists as a control");
		}
		if (discreteControls.containsKey(key)) {
			throw new AssertionError("Key " + key + " already exists as a discrete control");
		}
		if (booleanControls.containsKey(key)) {
			throw new AssertionError("Key " + key + " already exists as a boolean control");
		}
	}

	public BooleanControl createBooleanControl(String key, boolean defaultValue) {
		assertNew(key);
		BooleanControl control = new BooleanControl(key, defaultValue);
		if (booleanControls.putIfAbsent(key, control) != null) {
			throw new IllegalArgumentException("Key " + key + " already mapped a control");
		}
		return control;
	}

	public DiscreteControl createDiscreteControl(String key, List<String> allowedValues, String defaultValue) {
		assertNew(key);
		DiscreteControl control = new DiscreteControl(key, allowedValues, defaultValue);
		if (discreteControls.putIfAbsent(key, control) != null) {
			throw new IllegalArgumentException("Key " + key + " already mapped a control");
		}
		return control;
	}

	public Control createControl(String key, double min, double max, double start, Curve curve) {
		return createControl(key, min, max, start, curve, false);
	}

	public Control createControl(String key, double min, double max, double start, Curve curve, boolean round) {
		Control control = new Control(key, min, start, max, curve, round);
		assertNew(key);
		if (controls.putIfAbsent(key, control) != null) {
			throw new IllegalArgumentException("Key " + key + " already mapped a control");
		}
		return control;
	}

	public Control getControl(String key) {
		return controls.get(key);
	}

	public DiscreteControl getDiscreteControl(String key) {
		return discreteControls.get(key);
	}

	public BooleanControl getBooleanControl(String key) {
		return booleanControls.get(key);
	}

	public void resetSynthesizer(Object source) {
		for (Control control : controls.values()) {
			if (!control.getKey().contains("sequencer-")) {
				control.reset(source);
			}
		}
		for (DiscreteControl discreteControl : discreteControls.values()) {
			if (!discreteControl.getKey().contains("sequencer-")) {
				discreteControl.reset(source);
			}
		}
		for (BooleanControl booleanControl : booleanControls.values()) {
			if (!booleanControl.getKey().contains("sequencer-")) {
				booleanControl.reset(source);
			}
		}
	}

	public void resetSequencer(Object source) {
		for (Control control : controls.values()) {
			if (control.getKey().contains("sequencer-")) {
				control.reset(source);
			}
		}
		for (DiscreteControl discreteControl : discreteControls.values()) {
			if (discreteControl.getKey().contains("sequencer-")) {
				discreteControl.reset(source);
			}
		}
		for (BooleanControl booleanControl : booleanControls.values()) {
			if (booleanControl.getKey().contains("sequencer-")) {
				booleanControl.reset(source);
			}
		}
	}

	/**
	 * This is <b>only</b> for loading and saving. For normal use please see {@link #createControl(String, double, double, double, Curve)} and {@link #getControl(String)}
	 */
	public Map<String, Control> getControls() {
		return controls;
	}

	/**
	 * This is <b>only</b> for loading and saving. For normal use please see {@link #createDiscreteControl(String, List, String)} and {@link #getDiscreteControl(String)}
	 */
	public Map<String, DiscreteControl> getDiscreteControls() {
		return discreteControls;
	}

	/**
	 * This is <b>only</b> for loading and saving. For normal use please see {@link #createBooleanControl(String, boolean)} and {@link #getBooleanControl(String)}
	 */
	public Map<String, BooleanControl> getBooleanControls() {
		return booleanControls;
	}

}
