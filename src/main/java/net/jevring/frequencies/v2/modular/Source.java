/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.modular;

import net.jevring.frequencies.v2.input.Instruction;

/**
 * An input connects sources and target. Think of it as a thing connected to the input jack on a module in a (semi-)modular synth.
 * It's not just the jack, though. It's also the thing that's connected to the jack.
 *
 * <p>A source is anything from which you can get samples. It's the producer side of something. Things can be both sources and targets.
 *
 * @author markus@jevring.net
 * @created 2020-06-13 21:17
 */
public interface Source {
	public static final Source DISCONNECTED = new Source() {
		@Override
		public double[] generateSamples(int samplesToGenerate, Instruction instruction) {
			return new double[samplesToGenerate];
		}

		@Override
		public void next() {
		}

		@Override
		public String toString() {
			return "DISCONNECTED";
		}
	};

	/**
	 * Generate the samples for this source.
	 *
	 * @param samplesToGenerate how many samples to generate
	 * @param instruction       the instruction if something is playable. This can be null if nothing is currently playing.
	 *                          We might for example stop playing and still have the delay effect running, or we might want
	 *                          to visualize the modulation from an lfo when the synth isn't playing. If the instruction is
	 *                          null, it's perfectly fine, and in some cases (like for example {@link VelocityModule}, for
	 *                          which it is impossible to determine velocity when no key is struck) to return {@code new double[samplesToGenerate]}
	 * @return the samples for this source.
	 */
	public double[] generateSamples(int samplesToGenerate, Instruction instruction);


	void next();
}
