/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.modular;

import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.input.Instruction;

import java.util.Arrays;
import java.util.Objects;

/**
 * A module that takes in a signal and scales is from 1 to -1 depending on the control.
 * Unlike most other {@link Source inputs}, this does not have a configurable input setting.
 * Instances of this class are expected to used as modular cables, in a way, and re-created
 * and garbage collected when replaced.
 *
 * @author markus@jevring.net
 * @created 2020-06-14 00:24
 */
public class AttenuverterModule implements Source {
	private final Control depth;
	private final Source input;

	private volatile double[] samples;

	public AttenuverterModule(Control depth, Source input) {
		this.depth = Objects.requireNonNull(depth);
		this.input = Objects.requireNonNull(input);
	}

	public Control getDepth() {
		return depth;
	}

	@Override
	public double[] generateSamples(int samplesToGenerate, Instruction instruction) {
		if (this.samples == null) {
			double[] samples = input.generateSamples(samplesToGenerate, instruction);
			double depth = this.depth.getCurrentValue(); // -1 to 1
			this.samples = Arrays.stream(samples).map(s -> s * depth).toArray();
		}
		return this.samples;
	}

	@Override
	public void next() {
		input.next();
		this.samples = null;
	}
}
