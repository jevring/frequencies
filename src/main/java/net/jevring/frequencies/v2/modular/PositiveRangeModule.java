/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.modular;

import net.jevring.frequencies.v2.input.Instruction;

import java.util.Arrays;

/**
 * Modifies the -1 to +1 range of another module to only be in the 0 to +1 range.
 *
 * @author markus@jevring.net
 * @created 2020-06-14 00:48
 */
public class PositiveRangeModule implements Source {
	private final Source source;
	private volatile double[] samples;

	public PositiveRangeModule(Source source) {
		this.source = source;
	}

	@Override
	public double[] generateSamples(int samplesToGenerate, Instruction instruction) {
		if (this.samples == null) {
			this.samples = shiftToPositive(source.generateSamples(samplesToGenerate, instruction));
		}
		return this.samples;
	}

	private double[] shiftToPositive(double[] samples) {
		return Arrays.stream(samples).map(sample -> (sample + 1d) / 2).toArray();
	}

	@Override
	public void next() {
		source.next();
		this.samples = null;
	}

	public static void main(String[] args) {
		PositiveRangeModule prm = new PositiveRangeModule(new Source() {
			@Override
			public double[] generateSamples(int samplesToGenerate, Instruction instruction) {
				return new double[]{-1, -0.75, -0.5, -0.25, 0, .25, .75, 1};
			}

			@Override
			public void next() {
			}
		});

		System.out.println("prm.generateSamples(0, null) = " + Arrays.toString(prm.generateSamples(0, null)));
	}
}
