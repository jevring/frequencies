/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.modular;

import net.jevring.frequencies.v2.control.BooleanControl;
import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.engine.Voice;
import net.jevring.frequencies.v2.envelopes.ControlledEnvelope;
import net.jevring.frequencies.v2.envelopes.Envelopes;
import net.jevring.frequencies.v2.filters.ControlledFilter;
import net.jevring.frequencies.v2.filters.Filters;
import net.jevring.frequencies.v2.input.Instruction;
import net.jevring.frequencies.v2.modulation.SampleAndHold;
import net.jevring.frequencies.v2.modulation.matrix.ModulationMatrixVoice;
import net.jevring.frequencies.v2.modulation.matrix.ModulationSource;
import net.jevring.frequencies.v2.modulation.matrix.ModulationTarget;
import net.jevring.frequencies.v2.oscillators.ControlledOscillator;

/**
 * The ability to play a single note through the whole sound path, including filters etc.
 *
 * @author markus@jevring.net
 * @created 2020-06-14 15:54
 */
public class ModularVoice implements Voice {
	private final Source chain;

	public ModularVoice(float sampleRate,
	                    Filters filters,
	                    Envelopes envelopes,
	                    Controls controls,
	                    SampleAndHold sampleAndHold,
	                    ModulationMatrixVoice modulationMatrixVoice) {

		ControlledOscillator primaryOscillator = new ControlledOscillator("primary-oscillator", controls, sampleRate, true);
		ControlledOscillator secondaryOscillator = new ControlledOscillator("secondary-oscillator", controls, sampleRate, true);
		ControlledOscillator lfo1 = new ControlledOscillator("lfo1", controls, sampleRate, false);
		ControlledOscillator lfo2 = new ControlledOscillator("lfo2", controls, sampleRate, false);
		ControlledOscillator lfo3 = new ControlledOscillator("lfo3", controls, sampleRate, false);


		// Modulation source modules
		LFOModule lfo1Module = new LFOModule(lfo1, controls.getControl("lfo1-frequency"), controls.getBooleanControl("lfo1-key-reset"));
		LFOModule lfo2Module = new LFOModule(lfo2, controls.getControl("lfo2-frequency"), controls.getBooleanControl("lfo2-key-reset"));
		LFOModule lfo3Module = new LFOModule(lfo3, controls.getControl("lfo3-frequency"), controls.getBooleanControl("lfo3-key-reset"));

		PositiveRangeModule lfo1PositiveModule = new PositiveRangeModule(lfo1Module);
		PositiveRangeModule lfo2PositiveModule = new PositiveRangeModule(lfo2Module);
		PositiveRangeModule lfo3PositiveModule = new PositiveRangeModule(lfo3Module);

		VelocityModule velocityModule = new VelocityModule();
		KeyboardTrackingModule keyboardTrackingModule = new KeyboardTrackingModule();
		PitchBendModule pitchBendModule = new PitchBendModule(controls.getControl("pitch-bend"));
		SampleAndHoldModule sampleAndHoldModule = new SampleAndHoldModule(sampleAndHold);

		PositiveRangeModule sampleAndHoldPositiveModule = new PositiveRangeModule(sampleAndHoldModule);

		/*
		This is what this voice looks like, at a glance, before any modulation is applied.
		The instruction, which contains frequency etc, is passed in through the graph from right to left <- 
		
		                          volume envelope
		                                |
		noise \                        \/
		osc1  --> mixer -> filter -> scale -> scale -> [output] 
		osc2  /              /\                /\
		 /\                  |                 |
		 |                   |                 |
		pitch bend     filter envelope     velocity (if enabled)   
		
		 */

		// Sound path modules
		EnvelopeModule volumeEnvelope = new EnvelopeModule(new ControlledEnvelope(controls, envelopes, "volume"), sampleRate);
		EnvelopeModule filterEnvelope = new EnvelopeModule(new ControlledEnvelope(controls, envelopes, "filter"), sampleRate);
		EnvelopeModule modulationEnvelope = new EnvelopeModule(new ControlledEnvelope(controls, envelopes, "modulation"), sampleRate);

		NoiseModule noise = new NoiseModule();
		OscillatorModule osc1 = new OscillatorModule(primaryOscillator);
		osc1.getPitchBendModulation().connect(new AttenuverterModule(controls.getControl("primary-oscillator-pitch-bend-depth"), pitchBendModule));

		OscillatorModule osc2 = new OscillatorModule(secondaryOscillator);
		osc2.getPitchBendModulation().connect(new AttenuverterModule(controls.getControl("secondary-oscillator-pitch-bend-depth"), pitchBendModule));

		// Using a ring modulator is similar to basically just messing with the mixer.
		// However, it's another sound source, and we can make interesting things, so why the hell not =)
		ScalingModule ringModulator = new ScalingModule(BooleanControl.alwaysTrue());
		ringModulator.setInput(osc1);
		ringModulator.setScaleInput(osc2);

		ControlledMixer oscillatorMixer = new ControlledMixer(noise,
		                                                      osc1,
		                                                      osc2,
		                                                      ringModulator,
		                                                      controls.getControl("noise-oscillator-volume"),
		                                                      controls.getControl("primary-oscillator-volume"),
		                                                      controls.getControl("secondary-oscillator-volume"),
		                                                      controls.getControl("ring-modulator-volume"));

		// Introducing a pre-filter drive here is very simple with an AttenuverterModule and a control, and while it's possible to drive 
		// the filter, in many cases it doesn't really make it sound much better. Different, yes, but not significantly different from 
		// the overdrive-volume boost which happens *after* the filter.
		FilterModule filter = new FilterModule(new ControlledFilter(null, controls, filters));
		filter.setInput(oscillatorMixer);

		ScalingModule applyVolumeEnvelopeToSound = new ScalingModule(BooleanControl.alwaysTrue());
		applyVolumeEnvelopeToSound.setInput(filter);
		applyVolumeEnvelopeToSound.setScaleInput(volumeEnvelope);

		ScalingModule applyVelocityToVolume = new ScalingModule(controls.getBooleanControl("velocity-sensitive-keys"));
		applyVelocityToVolume.setInput(applyVolumeEnvelopeToSound);
		applyVelocityToVolume.setScaleInput(velocityModule);

		this.chain = applyVelocityToVolume;

		modulationMatrixVoice.registerTarget(ModulationTarget.FILTER_CUTOFF_FREQUENCY, filter.getCutoffFrequencyModulation());
		modulationMatrixVoice.registerTarget(ModulationTarget.FILTER_RESONANCE, filter.getResonanceFrequencyModulation());
		modulationMatrixVoice.registerTarget(ModulationTarget.NOISE_OSCILLATOR_VOLUME, oscillatorMixer.getNoiseModulation());
		modulationMatrixVoice.registerTarget(ModulationTarget.RING_MODULATION_VOLUME, oscillatorMixer.getRingModulatorModulation());


		modulationMatrixVoice.registerTarget(ModulationTarget.PRIMARY_OSCILLATOR_FREQUENCY, osc1.getFrequencyModulation());
		modulationMatrixVoice.registerTarget(ModulationTarget.PRIMARY_OSCILLATOR_WAVE_SHAPE, osc1.getWaveShapeModulation());
		modulationMatrixVoice.registerTarget(ModulationTarget.PRIMARY_OSCILLATOR_PHASE_SHIFT, osc1.getPhaseShiftModulation());
		modulationMatrixVoice.registerTarget(ModulationTarget.PRIMARY_OSCILLATOR_QUANTIZATION_STEPS, osc1.getQuantizationStepModulation());
		modulationMatrixVoice.registerTarget(ModulationTarget.PRIMARY_OSCILLATOR_WAVEFORM_VARIATION, osc1.getWaveformVariationModulation());
		modulationMatrixVoice.registerTarget(ModulationTarget.PRIMARY_OSCILLATOR_DETUNE, osc1.getSemitoneOffsetModulation());
		modulationMatrixVoice.registerTarget(ModulationTarget.PRIMARY_OSCILLATOR_OCTAVE, osc1.getOctaveOffsetModulation());
		modulationMatrixVoice.registerTarget(ModulationTarget.PRIMARY_OSCILLATOR_VOLUME, oscillatorMixer.getOsc1Modulation());

		modulationMatrixVoice.registerTarget(ModulationTarget.SECONDARY_OSCILLATOR_FREQUENCY, osc2.getFrequencyModulation());
		modulationMatrixVoice.registerTarget(ModulationTarget.SECONDARY_OSCILLATOR_WAVE_SHAPE, osc2.getWaveShapeModulation());
		modulationMatrixVoice.registerTarget(ModulationTarget.SECONDARY_OSCILLATOR_PHASE_SHIFT, osc2.getPhaseShiftModulation());
		modulationMatrixVoice.registerTarget(ModulationTarget.SECONDARY_OSCILLATOR_QUANTIZATION_STEPS, osc2.getQuantizationStepModulation());
		modulationMatrixVoice.registerTarget(ModulationTarget.SECONDARY_OSCILLATOR_WAVEFORM_VARIATION, osc2.getWaveformVariationModulation());
		modulationMatrixVoice.registerTarget(ModulationTarget.SECONDARY_OSCILLATOR_DETUNE, osc2.getSemitoneOffsetModulation());
		modulationMatrixVoice.registerTarget(ModulationTarget.SECONDARY_OSCILLATOR_OCTAVE, osc2.getOctaveOffsetModulation());
		modulationMatrixVoice.registerTarget(ModulationTarget.SECONDARY_OSCILLATOR_VOLUME, oscillatorMixer.getOsc2Modulation());


		modulationMatrixVoice.registerSource(ModulationSource.LFO1, lfo1Module);
		modulationMatrixVoice.registerSource(ModulationSource.LFO2, lfo2Module);
		modulationMatrixVoice.registerSource(ModulationSource.LFO3, lfo3Module);
		modulationMatrixVoice.registerSource(ModulationSource.SAMPLE_AND_HOLD, sampleAndHoldModule);
		modulationMatrixVoice.registerSource(ModulationSource.LFO1_POSITIVE, lfo1PositiveModule);
		modulationMatrixVoice.registerSource(ModulationSource.LFO2_POSITIVE, lfo2PositiveModule);
		modulationMatrixVoice.registerSource(ModulationSource.LFO3_POSITIVE, lfo3PositiveModule);
		modulationMatrixVoice.registerSource(ModulationSource.SAMPLE_AND_HOLD_POSITIVE, sampleAndHoldPositiveModule);
		modulationMatrixVoice.registerSource(ModulationSource.FILTER_ENVELOPE, filterEnvelope);
		modulationMatrixVoice.registerSource(ModulationSource.AMPLIFIER_ENVELOPE, volumeEnvelope);
		modulationMatrixVoice.registerSource(ModulationSource.MODULATION_ENVELOPE, modulationEnvelope);
		modulationMatrixVoice.registerSource(ModulationSource.VELOCITY, velocityModule);
		modulationMatrixVoice.registerSource(ModulationSource.RANDOM_NOISE, noise);
		modulationMatrixVoice.registerSource(ModulationSource.KEYBOARD_TRACKING, keyboardTrackingModule);
		modulationMatrixVoice.registerSource(ModulationSource.PITCH_BEND, pitchBendModule);

		modulationMatrixVoice.hardwire(ModulationSource.FILTER_ENVELOPE,
		                               ModulationTarget.FILTER_CUTOFF_FREQUENCY,
		                               controls.getControl("filter-envelope-depth"));
	}


	public double[] samples(Instruction instruction, int samplesToGenerate) {
		return chain.generateSamples(samplesToGenerate, instruction);
	}

	@Override
	public void next() {
		chain.next();
	}
}
