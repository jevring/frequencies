/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.modular;

import net.jevring.frequencies.v2.envelopes.Envelope;
import net.jevring.frequencies.v2.input.Instruction;

/**
 * A module representing an {@link net.jevring.frequencies.v2.envelopes.Envelope}.
 *
 * @author markus@jevring.net
 * @created 2020-06-13 22:34
 */
public class EnvelopeModule implements Source {
	private final Envelope envelope;
	private final double sampleRate;

	private volatile double[] samples;

	public EnvelopeModule(Envelope envelope, double sampleRate) {
		this.envelope = envelope;
		this.sampleRate = sampleRate;
	}

	@Override
	public double[] generateSamples(int samplesToGenerate, Instruction instruction) {
		if (this.samples == null) {
			if (instruction == null) {
				this.samples = new double[samplesToGenerate];
			} else {
				// unlike oscillators and filters, we MUST reset this, because we reset the progress in the envelope
				if (instruction.isNewInstruction()) {
					envelope.reset();
				}
				this.samples = envelope.levels(instruction.getNanosecondsActivated(), instruction.getNanosecondsDeactivated(), samplesToGenerate, sampleRate);
			}
		}
		return this.samples;
	}

	@Override
	public void next() {
		this.samples = null;
	}
}
