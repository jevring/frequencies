/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.modular;

import net.jevring.frequencies.v2.input.Instruction;
import net.jevring.frequencies.v2.oscillators.Oscillator;

/**
 * A module with input connections that represents an oscillator.
 *
 * @author markus@jevring.net
 * @created 2020-06-13 21:16
 */
public class OscillatorModule implements Source {
	private final Oscillator oscillator;
	private volatile double[] samples;
	private final AdditiveMultiSource frequencyModulation = new AdditiveMultiSource();
	private final AdditiveMultiSource waveShapeModulation = new AdditiveMultiSource();
	private final AdditiveMultiSource pitchBendModulation = new AdditiveMultiSource();
	private final AdditiveMultiSource phaseShiftModulation = new AdditiveMultiSource();
	private final AdditiveMultiSource octaveOffsetModulation = new AdditiveMultiSource();
	private final AdditiveMultiSource semitoneOffsetModulation = new AdditiveMultiSource();
	private final AdditiveMultiSource quantizationStepModulation = new AdditiveMultiSource();
	private final AdditiveMultiSource waveformVariationModulation = new AdditiveMultiSource();

	public OscillatorModule(Oscillator oscillator) {
		this.oscillator = oscillator;
	}

	public AdditiveMultiSource getFrequencyModulation() {
		return frequencyModulation;
	}

	public AdditiveMultiSource getWaveShapeModulation() {
		return waveShapeModulation;
	}

	public AdditiveMultiSource getPitchBendModulation() {
		return pitchBendModulation;
	}

	public AdditiveMultiSource getPhaseShiftModulation() {
		return phaseShiftModulation;
	}

	public AdditiveMultiSource getOctaveOffsetModulation() {
		return octaveOffsetModulation;
	}

	public AdditiveMultiSource getSemitoneOffsetModulation() {
		return semitoneOffsetModulation;
	}

	public AdditiveMultiSource getQuantizationStepModulation() {
		return quantizationStepModulation;
	}

	public AdditiveMultiSource getWaveformVariationModulation() {
		return waveformVariationModulation;
	}

	@Override
	public double[] generateSamples(int samplesToGenerate, Instruction instruction) {
		if (this.samples == null) {
			if (instruction == null) {
				this.samples = new double[samplesToGenerate];
			} else {
				/* Disabled resetting, because the crackle that used to be there seems to be gone now
				if (instruction.isNewInstruction()) {
					oscillator.reset();
				}
				 */
				this.samples = oscillator.generateSamples(samplesToGenerate,
				                                          instruction.getFrequency(),
				                                          frequencyModulation.generateSamples(samplesToGenerate, instruction),
				                                          waveShapeModulation.generateSamples(samplesToGenerate, instruction),
				                                          pitchBendModulation.generateSamples(samplesToGenerate, instruction),
				                                          phaseShiftModulation.generateSamples(samplesToGenerate, instruction),
				                                          octaveOffsetModulation.generateSamples(samplesToGenerate, instruction),
				                                          semitoneOffsetModulation.generateSamples(samplesToGenerate, instruction),
				                                          quantizationStepModulation.generateSamples(samplesToGenerate, instruction),
				                                          waveformVariationModulation.generateSamples(samplesToGenerate, instruction));
			}
		}
		return this.samples;
	}

	@Override
	public void next() {
		frequencyModulation.next();
		waveShapeModulation.next();
		pitchBendModulation.next();
		phaseShiftModulation.next();
		octaveOffsetModulation.next();
		semitoneOffsetModulation.next();
		quantizationStepModulation.next();
		waveformVariationModulation.next();
		this.samples = null;
	}
}
