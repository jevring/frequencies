/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.modular;

import net.jevring.frequencies.v2.control.BooleanControl;
import net.jevring.frequencies.v2.input.Instruction;

/**
 * Scales a signal from {@link #input} using the scale values in {@link #scaleInput} if {@link #enabled}.
 *
 * @author markus@jevring.net
 * @created 2020-06-14 14:33
 */
public class ScalingModule implements Source {
	private final BooleanControl enabled;
	private volatile Source input = Source.DISCONNECTED;
	private volatile Source scaleInput = Source.DISCONNECTED;
	private volatile double[] samples;

	public ScalingModule(BooleanControl enabled) {
		this.enabled = enabled;
	}

	public void setInput(Source input) {
		this.input = input;
	}

	public void setScaleInput(Source scaleInput) {
		this.scaleInput = scaleInput;
	}

	@Override
	public double[] generateSamples(int samplesToGenerate, Instruction instruction) {
		if (this.samples == null) {
			double[] samples = input.generateSamples(samplesToGenerate, instruction);
			if (enabled.get()) {
				double[] scaledSamples = scaleInput.generateSamples(samplesToGenerate, instruction);
				double[] output = new double[samplesToGenerate];
				for (int i = 0; i < samplesToGenerate; i++) {
					output[i] = samples[i] * scaledSamples[i];
				}
				this.samples = output;
			} else {
				// bypass
				this.samples = samples;
			}
		}
		return this.samples;
	}

	@Override
	public void next() {
		input.next();
		scaleInput.next();
		this.samples = null;
	}
}
