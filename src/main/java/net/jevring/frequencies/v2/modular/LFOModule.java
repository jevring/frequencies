/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.modular;

import net.jevring.frequencies.v2.control.BooleanControl;
import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.input.Instruction;
import net.jevring.frequencies.v2.oscillators.ControlledOscillator;

/**
 * A module representing an LFO and its frequency setting.
 *
 * @author markus@jevring.net
 * @created 2020-06-14 00:48
 */
public class LFOModule implements Source {
	private final ControlledOscillator lfo;
	private final Control frequency;
	private final BooleanControl retrigger;
	private volatile double[] samples;

	public LFOModule(ControlledOscillator lfo, Control frequency, BooleanControl retrigger) {
		this.lfo = lfo;
		this.frequency = frequency;
		this.retrigger = retrigger;
	}

	@Override
	public double[] generateSamples(int samplesToGenerate, Instruction instruction) {
		if (this.samples == null) {
			if (instruction != null && instruction.isNewInstruction() && retrigger.get()) {
				lfo.reset();
			}
			double[] zeroes = new double[samplesToGenerate];
			// LFOs don't get modulation. Otherwise we run in to a whole mess of a dependency graph
			// with recursive modulation. Let's just keep it simple.
			this.samples = lfo.generateSamples(samplesToGenerate, frequency.getCurrentValue(), zeroes, zeroes, zeroes, zeroes, zeroes, zeroes, zeroes, zeroes);
		}
		return this.samples;
	}

	@Override
	public void next() {
		this.samples = null;
	}
}
