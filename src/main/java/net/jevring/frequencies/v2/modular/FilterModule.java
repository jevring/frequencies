/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.modular;

import net.jevring.frequencies.v2.filters.ControlledFilter;
import net.jevring.frequencies.v2.filters.ModulatedFilter;
import net.jevring.frequencies.v2.input.Instruction;

/**
 * A module representing a selectable {@link ModulatedFilter filter}.
 *
 * @author markus@jevring.net
 * @created 2020-06-13 21:21
 */
public class FilterModule implements Source {
	private final ControlledFilter filter;
	private final AdditiveMultiSource cutoffFrequencyModulation = new AdditiveMultiSource();
	private final AdditiveMultiSource resonanceFrequencyModulation = new AdditiveMultiSource();
	private volatile Source input = Source.DISCONNECTED;
	private volatile double[] samples;

	public FilterModule(ControlledFilter filter) {
		this.filter = filter;
	}

	public AdditiveMultiSource getCutoffFrequencyModulation() {
		return cutoffFrequencyModulation;
	}

	public AdditiveMultiSource getResonanceFrequencyModulation() {
		return resonanceFrequencyModulation;
	}

	public void setInput(Source input) {
		this.input = input;
	}

	@Override
	public double[] generateSamples(int samplesToGenerate, Instruction instruction) {
		if (this.samples == null) {
			/* Disabled resetting, because the crackle that used to be there seems to be gone now
			if (instruction != null && instruction.isNewInstruction()) {
				filter.reset();
			}
			*/
			this.samples = filter.apply(input.generateSamples(samplesToGenerate, instruction),
			                            cutoffFrequencyModulation.generateSamples(samplesToGenerate, instruction),
			                            resonanceFrequencyModulation.generateSamples(samplesToGenerate, instruction));
		}
		return this.samples;
	}

	@Override
	public void next() {
		input.next();
		cutoffFrequencyModulation.next();
		resonanceFrequencyModulation.next();
		this.samples = null;
	}
}
