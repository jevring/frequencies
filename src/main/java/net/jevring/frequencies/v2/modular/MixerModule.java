/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.modular;

import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.input.Instruction;

import java.util.ArrayList;
import java.util.List;

/**
 * A mixer that mixes multiple signals based on a control that specifies how much each source should contribute.
 *
 * @author markus@jevring.net
 * @created 2021-03-09 20:10
 */
public class MixerModule implements Source {
	private final List<ControlledSource> sources = new ArrayList<>();
	private volatile double[] samples;

	public void add(Source source, Control control) {
		this.sources.add(new ControlledSource(source, control));
	}

	@Override
	public double[] generateSamples(int samplesToGenerate, Instruction instruction) {
		if (this.samples == null) {
			double[] output = new double[samplesToGenerate];

			for (ControlledSource controlledSource : sources) {
				double[] samples = controlledSource.source.generateSamples(samplesToGenerate, instruction);
				double controlScale = controlledSource.control.getCurrentValue();
				for (int i = 0; i < samplesToGenerate; i++) {
					output[i] += samples[i] * controlScale;
				}
			}
			this.samples = output;
		}
		return this.samples;
	}

	@Override
	public void next() {
		for (ControlledSource controlledSource : sources) {
			controlledSource.source.next();
		}
		this.samples = null;
	}

	private record ControlledSource(Source source, Control control) {
	}
}
