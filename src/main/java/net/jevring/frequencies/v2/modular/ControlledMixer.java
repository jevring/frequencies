/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.modular;

import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.input.Instruction;

/**
 * Mixer for two oscillators and a noise source. Technically speaking it's a mixer for any 3 inputs
 * with 3 controls and 3 sources of volume modulation, but it might be easier to think of as a noise-osc-osc-mixer.
 *
 * @author markus@jevring.net
 * @created 2020-06-13 23:49
 */
public class ControlledMixer implements Source {
	private final Source noise;
	private final Source osc1;
	private final Source osc2;
	private final Source ringModulator;

	private final Control noiseVolume;
	private final Control osc1Volume;
	private final Control osc2Volume;
	private final Control ringModulatorVolume;

	private final AdditiveMultiSource noiseModulation = new AdditiveMultiSource();
	private final AdditiveMultiSource osc1Modulation = new AdditiveMultiSource();
	private final AdditiveMultiSource osc2Modulation = new AdditiveMultiSource();
	private final AdditiveMultiSource ringModulatorModulation = new AdditiveMultiSource();

	private volatile double[] samples;

	public ControlledMixer(Source noise,
	                       Source osc1,
	                       Source osc2,
	                       Source ringModulator,
	                       Control noiseVolume,
	                       Control osc1Volume,
	                       Control osc2Volume,
	                       Control ringModulatorVolume) {
		this.noise = noise;
		this.osc1 = osc1;
		this.osc2 = osc2;
		this.ringModulator = ringModulator;
		this.noiseVolume = noiseVolume;
		this.osc1Volume = osc1Volume;
		this.osc2Volume = osc2Volume;
		this.ringModulatorVolume = ringModulatorVolume;
	}

	public AdditiveMultiSource getNoiseModulation() {
		return noiseModulation;
	}

	public AdditiveMultiSource getOsc1Modulation() {
		return osc1Modulation;
	}

	public AdditiveMultiSource getOsc2Modulation() {
		return osc2Modulation;
	}

	public AdditiveMultiSource getRingModulatorModulation() {
		return ringModulatorModulation;
	}

	@Override
	public double[] generateSamples(int samplesToGenerate, Instruction instruction) {
		if (this.samples == null) {
			double[] output = new double[samplesToGenerate];

			double[] noiseSamples = noise.generateSamples(samplesToGenerate, instruction);
			double[] osc1Samples = osc1.generateSamples(samplesToGenerate, instruction);
			double[] osc2Samples = osc2.generateSamples(samplesToGenerate, instruction);
			double[] ringModulatorSamples = ringModulator.generateSamples(samplesToGenerate, instruction);

			// Mixer
			double[] osc1VolumeModulation = osc1Modulation.generateSamples(samplesToGenerate, instruction);
			double[] osc2VolumeModulation = osc2Modulation.generateSamples(samplesToGenerate, instruction);
			double[] noiseVolumeModulation = noiseModulation.generateSamples(samplesToGenerate, instruction);
			double[] ringModulatorVolumeModulation = ringModulatorModulation.generateSamples(samplesToGenerate, instruction);

			for (int i = 0; i < samplesToGenerate; i++) {
				double osc1Sample = osc1Samples[i];
				double osc2Sample = osc2Samples[i];
				double noiseSample = noiseSamples[i];
				double ringModulatorSample = ringModulatorSamples[i];
				output[i] = (osc1Sample * (osc1Volume.getCurrentValue() + osc1VolumeModulation[i])) +
				            (osc2Sample * (osc2Volume.getCurrentValue() + osc2VolumeModulation[i])) +
				            (noiseSample * (noiseVolume.getCurrentValue() + noiseVolumeModulation[i])) +
				            (ringModulatorSample * (ringModulatorVolume.getCurrentValue() + ringModulatorVolumeModulation[i]));
			}
			this.samples = output;
		}
		return this.samples;
	}

	@Override
	public void next() {
		osc1.next();
		osc2.next();
		noise.next();
		ringModulator.next();
		osc1Modulation.next();
		osc2Modulation.next();
		noiseModulation.next();
		ringModulatorModulation.next();
		this.samples = null;
	}
}
