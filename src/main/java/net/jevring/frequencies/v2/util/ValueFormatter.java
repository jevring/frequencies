/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.util;

import net.jevring.frequencies.v2.input.midi.MidiNoteNameMapper;

import java.util.Locale;
import java.util.function.DoubleFunction;

/**
 * A formatter turns a double into a string.
 *
 * @author markus@jevring.net
 * @created 2020-05-01 19:47
 */
public enum ValueFormatter {
	PPQN(d -> TempoTools.ppqnName((int) d)),
	MIDI_NOTE_TO_NAME(d -> MidiNoteNameMapper.noteName((int) d)),
	NOTE_TO_NAME(d -> NoteNameMapper.noteName((int) d)),
	INTEGER(d -> String.format(Locale.US, "%d", Math.round(d))),
	INTEGER_16_OFF(d -> d == 16 ? "OFF" : String.format(Locale.US, "%d", Math.round(d))),
	PURE(d -> String.format(Locale.US, "%.2f", d)),
	LOW_FREQUENCY(d -> String.format(Locale.US, "%.2f Hz", d)),
	FREQUENCY(d -> String.format(Locale.US, "%.0f Hz", d)),
	PERCENT(d -> String.format(Locale.US, "%.0f %%", d * 100d)),
	SEMITONES(d -> String.format(Locale.US, "%.2f st", d)),
	SECONDS(d -> String.format(Locale.US, "%.2f s", d)),
	LOW_MILLISECONDS(d -> String.format(Locale.US, "%.2f ms", d)),
	MILLISECONDS(d -> String.format(Locale.US, "%.0f ms", d));

	private final DoubleFunction<String> formatter;

	ValueFormatter(DoubleFunction<String> formatter) {
		this.formatter = formatter;
	}

	public String format(double d) {
		return formatter.apply(d);
	}
}
