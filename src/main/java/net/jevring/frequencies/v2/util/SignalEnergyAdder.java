/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.util;

/**
 * Finally, this is what I was looking for all along: The Bark scale.
 * The relationship between frequency and perceived loudness: https://en.wikipedia.org/wiki/Bark_scale
 * That explains a lot of the issues I've had with mixing.
 * <p>
 * Dividing by the square root normalizes the signal based on energy, not amplitude.
 * https://www.reddit.com/r/DSP/comments/gplogc/relationship_between_frequency_and_either_actual/frms8hm/
 * That said, dividing each signal isn't strictly speaking necessary.
 * We can in general just add them.
 *
 * @author markus@jevring.net
 * @created 2021-02-19 21:01
 */
public class SignalEnergyAdder {
	private final double[] output;
	private final double divisor;

	public SignalEnergyAdder(int voices, int size) {
		this.output = new double[size];
		this.divisor = Math.sqrt(voices);
	}

	public void add(double[] samples) {
		for (int i = 0; i < samples.length; i++) {
			output[i] += samples[i] / divisor;
		}
	}

	public double[] getOutput() {
		return output;
	}
}
