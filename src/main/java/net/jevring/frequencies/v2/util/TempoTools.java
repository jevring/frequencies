/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.util;

import java.util.Map;
import java.util.TreeMap;

/**
 * Tools for dealing with tempo.
 *
 * @author markus@jevring.net
 * @created 2021-02-28 01:53
 */
public class TempoTools {
	private static final TreeMap<Integer, String> ppqn = new TreeMap<>();

	static {
		ppqn.put(24, "1/4");
		ppqn.put(16, "1/4T");
		ppqn.put(12, "1/8");
		ppqn.put(8, "1/8T");
		ppqn.put(6, "1/16");
		ppqn.put(4, "1/16T");
		ppqn.put(3, "1/32");
		ppqn.put(2, "1/32T");
	}

	/**
	 * @see #normalize(int) 
	 */
	public static String ppqnName(int i) {
		Map.Entry<Integer, String> ceilingEntry = ppqn.ceilingEntry(i);
		if (ceilingEntry == null) {
			return "1/4";
		}
		return ceilingEntry.getValue();
	}

	/**
	 * This is counted in ticks. How for how many ticks we should trigger.
	 * <ul>
	 *     <li>24 = 1/4    </li>
	 *     <li>16 = 1/4T   </li>
	 * 	   <li>12 = 1/8    </li>
	 * 	   <li> 8 = 1/8T   </li>
	 * 	   <li>	6 = 1/16   </li>
	 * 	   <li>	4 = 1/16T  </li>
	 * 	   <li>	3 = 1/32   </li>
	 * 	   <li>	2 = 1/32T  </li>
	 * </ul>
	 *
	 */
	public static int normalize(int i) {
		return ppqn.ceilingKey(i);
	}

	public static void main(String[] args) {
		for (int i = 0; i <= 24; i++) {
			System.out.printf("%2d -> %2d%n", i, normalize(i));
		}
	}
}
