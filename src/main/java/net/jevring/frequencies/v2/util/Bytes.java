/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.util;

import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * Converts arrays of signed bytes in big-endian into integers and vice versa.
 * Supports arrays of length 1 to 4.
 *
 * @author markus@jevring.net
 */
public class Bytes {
	public static int convertToInt(byte[] bytes) {
		return convertToInt(bytes, 0, bytes.length);
	}

	public static int convertToInt(byte[] bytes, int start, int length) {
		//return new BigInteger(bytes).intValue();
		switch (length) {
			case 1:
				return bytes[start];
			case 2:
				// why don't we need the &-operation on the first byte? We have it when we create...
				return ((bytes[start]) << 8) | ((bytes[start + 1] & 0xff));
			case 3:
				return ((bytes[start]) << 16) | ((bytes[start + 1] & 0xff) << 8) | ((bytes[start + 2] & 0xff));
			case 4:
				return ((bytes[start]) << 24) | ((bytes[start + 1] & 0xff) << 16) | ((bytes[start + 2] & 0xff) << 8) | ((bytes[start + 3] & 0xff));
			default:
				throw new IllegalArgumentException("Cannot handle byte arrays of size " + length);
		}
	}

	public static byte[] convertToBytes(int value, int sizeOfArray) {
		byte[] bytes = new byte[sizeOfArray];
		switch (sizeOfArray) {
			case 1:
				bytes[0] = (byte) value;
				break;
			case 2:
				bytes[1] = (byte) (value & 0xff); // low byte
				bytes[0] = (byte) ((value >> 8) & 0xff); // high byte
				break;
			case 3:
				bytes[2] = (byte) (value & 0xff);
				bytes[1] = (byte) ((value >> 8) & 0xff);
				bytes[0] = (byte) ((value >> 16) & 0xff);
				break;
			case 4:
				bytes[3] = (byte) (value & 0xff);
				bytes[2] = (byte) ((value >> 8) & 0xff);
				bytes[1] = (byte) ((value >> 16) & 0xff);
				bytes[0] = (byte) ((value >> 24) & 0xff);
				break;
		}
		return bytes;
	}

	public static void writeShort(short value, int index, byte[] bytes) {
		bytes[index + 1] = (byte) (value & 0xff); // low byte
		bytes[index] = (byte) ((value >> 8) & 0xff); // high byte
	}

	public static byte[] convertToBytesViaByteBuffer(int value, int sizeOfArray) {
		// defaults to big endian, which is what we want for our sounds anyway
		return ByteBuffer.allocate(sizeOfArray).putInt(value).array();
	}

	public static byte[] convertToBytesViaByteBuffer(short value, int sizeOfArray) {
		// defaults to big endian, which is what we want for our sounds anyway
		return ByteBuffer.allocate(sizeOfArray).putShort(value).array();
	}

	public static int convertToIntViaByteBuffers(byte[] bytes) {
		return ByteBuffer.wrap(bytes).getInt();
	}

	public static short convertToShortViaByteBuffers(byte[] bytes) {
		return ByteBuffer.wrap(bytes).getShort();
	}

	public static void main(String[] args) {
		test(1, -127, 127);
		test(2, Short.MIN_VALUE, Short.MAX_VALUE);
		test(3, -2048383, 2048383); // this is a weird 24-bit combo, which we need, because sound is occasionally in 24-bit.
		test(4, Integer.MIN_VALUE, Integer.MAX_VALUE);
	}

	private static void test(int size, int start, int end) {
		for (int i = start; i <= end; i++) {
			byte[] bytes = Bytes.convertToBytes(i, size);
			if (size == 4) {
				byte[] bytesVerification = convertToBytesViaByteBuffer(i, size);
				if (!Arrays.equals(bytes, bytesVerification)) {
					System.out.println("We got up to " + (i - 1) + " in size " + size + " before breaking [array]");
					return;
				}
				int s = convertToIntViaByteBuffers(bytes);
				if (s != i) {
					System.out.println("We got up to " + (i - 1) + " in size " + size + " before breaking [bb-int]");
					return;
				}
			}
			if (size == 2) {
				byte[] bytesVerification = convertToBytesViaByteBuffer((short) i, size);
				if (!Arrays.equals(bytes, bytesVerification)) {
					System.out.println("We got up to " + (i - 1) + " in size " + size + " before breaking [array]");
					return;
				}
				short s = convertToShortViaByteBuffers(bytes);
				if (s != i) {
					System.out.println("We got up to " + (i - 1) + " in size " + size + " before breaking [bb-short]");
					return;
				}
			}
			// no need to test bytesVerification conversion to integer, as the arrays are equal to begin with
			int value = Bytes.convertToInt(bytes);
			if (value != i) {
				System.out.println("We got up to " + (i - 1) + " in size " + size + " before breaking [int]");
				return;
			}
		}
		System.out.println("Complete from " + start + " to " + end + " in size " + size);
	}
}
