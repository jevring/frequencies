/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.engine;

import net.jevring.frequencies.v2.effects.EffectsChain;
import net.jevring.frequencies.v2.hooks.Visualizer;
import net.jevring.frequencies.v2.input.Instruction;
import net.jevring.frequencies.v2.input.InstructionInput;
import net.jevring.frequencies.v2.util.SignalEnergyAdder;

import javax.sound.sampled.SourceDataLine;
import java.util.List;
import java.util.Objects;

/**
 * A sound engine that has a main loop that generates sound sample data ~100 times per second.
 *
 * @author markus@jevring.net
 * @created 2020-02-01 09:54
 */
public class MainLoopPullingEngine {
	private static final double ONE_HUNDREDTH_OF_A_SECOND_IN_NANOS = 1e7;
	private static final double NANOS_PER_SECOND = 1e9;
	private final SamplesToBytesConverter samplesToBytesConverter;
	private final InstructionInput instructionInput;
	private final EffectsChain effectsChain;
	private final MasterVolume masterVolume;
	private final float sampleRate;
	private final Voice[] voices;

	private volatile boolean running = false;
	private volatile Visualizer visualizer;

	private SourceDataLine sourceDataLine;
	private Thread thread;

	public MainLoopPullingEngine(SamplesToBytesConverter samplesToBytesConverter,
	                             InstructionInput instructionInput,
	                             EffectsChain effectsChain,
	                             MasterVolume masterVolume,
	                             float sampleRate,
	                             Voice[] voices) {
		this.samplesToBytesConverter = samplesToBytesConverter;
		this.instructionInput = instructionInput;
		this.effectsChain = effectsChain;
		this.masterVolume = masterVolume;
		this.sampleRate = sampleRate;
		this.voices = voices;
	}

	/**
	 * Read input and generate sound ~100 times per second.
	 */
	private void loop() {
		long previousNanos = System.nanoTime();

		while (running) {
			//Thread.onSpinWait();
			try {
				//noinspection BusyWait
				Thread.sleep(1);
			} catch (InterruptedException ignored) {
			}

			try {
				long nowNanos = System.nanoTime();
				// technically this should include the cost of fetching the nano time, but since we don't know when in the call we get the time, there isn't much we can do
				long elapsedNanos = nowNanos - previousNanos;
				if (elapsedNanos > ONE_HUNDREDTH_OF_A_SECOND_IN_NANOS) {
					previousNanos = nowNanos;
					int samplesToGenerate = samplesToGenerate(elapsedNanos);

					List<Instruction> instructions = instructionInput.inputInstructions();
					double[] samples = pullSamplesFromVoices(samplesToGenerate, instructions);
					// The effects chain lives here as it operates on the combined output on all the voices, 
					// rather than on each voice itself
					samples = effectsChain.apply(samples);
					samples = masterVolume.apply(samples);

					byte[] soundOutputBytes = samplesToBytesConverter.convert(samples);
					sourceDataLine.write(soundOutputBytes, 0, soundOutputBytes.length);
					// Now that we have written to the audio buffer, we have some time for visualization
					visualizer.visualizeWaveformChunk(samples);
					visualizer.visualizeModulation(samples.length, instructions);
					double loopTimeInNanoseconds = System.nanoTime() - nowNanos;
					double percentageOfMax = (loopTimeInNanoseconds / ONE_HUNDREDTH_OF_A_SECOND_IN_NANOS) * 100d;
					visualizer.visualizeLoopTime(percentageOfMax);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private double[] pullSamplesFromVoices(int samplesToGenerate, List<Instruction> instructions) {
		SignalEnergyAdder output = new SignalEnergyAdder(voices.length, samplesToGenerate);
		for (int i = 0; i < voices.length; i++) {
			Voice voice = voices[i];
			// Even if there are no instructions, we should advance the state so that modulation is still visible, and free-running LFOs don't stop
			voice.next();
			Instruction instruction = instructions.get(i);
			if (instruction != null) {
				double[] samplesForVoice = voice.samples(instruction, samplesToGenerate);
				output.add(samplesForVoice);
			}
		}
		return output.getOutput();
	}

	private int samplesToGenerate(double elapsedNanos) {
		// This method has gone through some changes, and I'm keeping the old stuff to remind me of why I changed it, 
		// and so that I can avoid making the same mistake again. 
		// What this ultimately lead to is filling up the buffer, which manifested in regular stalls. Those extra samples...
		// It didn't become obvious until the introduction of the sequencer when I was pushing sounds regularly, at which point 
		// the stalls became apparent. 

		double proportionOfASecond = (elapsedNanos / NANOS_PER_SECOND);
		// NOTE: Just casting this to an int is no good. That extra sample we're missing every now and then results in audible clicks
		return (int) Math.round(proportionOfASecond * sampleRate);
/*		
		// to prevent this from getting out of hand if we have a temporary slow-down, 
		// we're going to clamp it to a max which is approximately two normal cycles, 
		// assuming a rate of 100 cycles per second
		// NOTE: This clamping, combined with a buffer that is too small, which causes us to occasionally block and overrun our timeslot
		//  leads to chopped off samples. 
		proportionOfASecond = Math.min(proportionOfASecond, 0.02);
		int samplesToGenerate = (int) (proportionOfASecond * sampleRate);
		// cover any miscalculations. This shouldn't introduce too much latency
		// without these extra samples, there are occasional subtle clicks
		// NOTE: These extra samples, combined with a buffer that is too large, introduces massive delays very quickly. Both for ramp up and ramp down.
		samplesToGenerate += 25;
		return samplesToGenerate;
		
 */
	}

	/**
	 * Start the synthesizer with a specified sound output. The sound output will be closed when {@link #stop()} is called.
	 */
	public synchronized void start(SourceDataLine sourceDataLine) {
		if (thread != null) {
			throw new IllegalStateException("Already running");
		}
		this.sourceDataLine = Objects.requireNonNull(sourceDataLine);

		running = true;
		thread = new Thread(this::loop, "main-loop-pulling-engine");
		thread.start();
	}

	public synchronized void stop() {
		if (thread == null) {
			throw new IllegalStateException("Not running");
		}
		running = false;
		sourceDataLine.flush();
		sourceDataLine.close();
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		thread = null;
	}

	public void setVisualizer(Visualizer visualizer) {
		this.visualizer = Objects.requireNonNull(visualizer);
	}
}
