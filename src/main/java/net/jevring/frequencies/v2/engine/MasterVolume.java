/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.engine;

import net.jevring.frequencies.v2.control.BooleanControl;
import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.math.Clamp;

/**
 * Applies the master volume and handles clipping.
 *
 * @author markus@jevring.net
 * @created 2021-02-24 21:34
 */
public class MasterVolume {
	private final BooleanControl clipControl;
	/**
	 * https://en.wikipedia.org/wiki/Distortion_(music)#/media/File:Clipping_waveform.svg
	 */
	private final Control overdriveControl;

	public MasterVolume(Controls controls) {
		this.overdriveControl = controls.getControl("overdrive-volume");
		this.clipControl = controls.getBooleanControl("clip");
	}

	double[] apply(double[] samples) {
		boolean clip = clipControl.get();
		double overdrive = overdriveControl.getCurrentValue();

		double[] output = new double[samples.length];
		for (int i = 0; i < samples.length; i++) {
			double masterSample = samples[i] * overdrive;
			if (clip) {
				output[i] = Clamp.clamp(masterSample, -1, 1);
			} else {
				output[i] = masterSample;
			}
		}
		return output;
	}
}
