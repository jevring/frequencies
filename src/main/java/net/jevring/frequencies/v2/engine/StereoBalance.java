/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v2.engine;

import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.math.Clamp;
import net.jevring.frequencies.v2.util.Bytes;

/**
 * Converts mono samples into stereo bytes.
 *
 * @author markus@jevring.net
 * @created 2021-02-24 21:37
 */
public class StereoBalance implements SamplesToBytesConverter {
	private final Control balanceControl;

	public StereoBalance(Controls controls) {
		this.balanceControl = controls.getControl("balance");
	}

	@Override
	public byte[] convert(double[] samples) {
		byte[] bytes = new byte[samples.length * Short.BYTES * 2];
		int shortsWritten = 0;
		boolean empty = true;
		for (double sample : samples) {
			if (sample != 0 && empty) {
				// lets us know if we have at least 1 value that isn't 0
				empty = false;
			}
			// -1 is all left, 1 is all right, 0 is both
			double balance = Clamp.clamp(balanceControl.getCurrentValue(), -1, 1);
			double leftAmount = 1;
			double rightAmount = 1;
			// The idea here isn't to just double the amount on one side as we go all the way to that side.
			// The idea is to instead reduce the amount on the *other* side
			if (balance < 0) {
				// We're going to the left. 
				rightAmount += balance;
			} else if (balance > 0) {
				// We're going to the right
				leftAmount = (1d - balance);
			}

			// scale to the bit rate for the sample (8 or 16 or 24 most commonly), in this case 16 bits (which is a short)
			short leftScaledSample = (short) (sample * leftAmount * Short.MAX_VALUE);
			short rightScaledSample = (short) (sample * rightAmount * Short.MAX_VALUE);
			Bytes.writeShort(leftScaledSample, shortsWritten, bytes); // left
			shortsWritten += Short.BYTES;
			Bytes.writeShort(rightScaledSample, shortsWritten, bytes); // right
			shortsWritten += Short.BYTES;
		}
		return bytes;
	}
}
