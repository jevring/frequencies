/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v1.ui;

import net.jevring.frequencies.v2.util.Bytes;
import net.jevring.frequencies.v2.ui.JSkinnable;
import net.jevring.frequencies.v2.ui.Skin;
import net.jevring.frequencies.v2.ui.Skins;

import javax.swing.*;
import java.awt.*;

/**
 * @author markus@jevring.net
 * Time: 2011-05-26 15:44
 */
public class ByteArrayGraph extends JComponent implements JSkinnable {
	private static final boolean ALIGN = true;
	private final int maxXAxis = 600;
	private final int yOffset = 150;
	private final int xOffset = 5;

	private byte[] data = null;
	private int bitsPerSample = 8;
	private volatile Skin skin = Skins.defaultSkin();

	public ByteArrayGraph() {
		Dimension size = new Dimension(maxXAxis + xOffset, 300);
		setPreferredSize(size);
		setSize(size);
		setMaximumSize(size);
		setMinimumSize(size);
		setAlignmentX(LEFT_ALIGNMENT);
	}

	@Override
	public void setSkin(Skin skin) {
		this.skin = skin;
		repaint();
	}

	public void update(byte[] data, int bitsPerSample) {
		this.bitsPerSample = bitsPerSample;
		this.data = data;
		revalidate();
		repaint();
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);


		g.setColor(skin.foreground());
		// todo: the 135 here should really be a scale
		g.drawLine(xOffset, -135 + yOffset, xOffset, 135 + yOffset); // y-axis
		g.drawLine(xOffset, 0 + yOffset, maxXAxis + xOffset, 0 + yOffset); // x-axis

		if (data != null) {
			Polygon p = new Polygon();

			int x = xOffset;
			int bytesPerSample = bitsPerSample / 8;
			int previousValue = Integer.MIN_VALUE;
			boolean startingPointFound = false;
			for (int i = 0; i < data.length; i += bytesPerSample) {
				int value = Bytes.convertToInt(data, i, bytesPerSample);
				//if (Math.signum(previousValue) != Math.signum(value) || value == 0) {
				if (!startingPointFound &&
				    (previousValue != Integer.MIN_VALUE && Math.signum(previousValue) != Math.signum(value) && value - previousValue < 0.1) || value == 0) {
					startingPointFound = true;
				}
				previousValue = value;

				if (startingPointFound || !ALIGN) {

					// this works for 8 and 16 bit, but nothing higher =(
					// maybe the values we stick in are wrong at those levels...
					value = (int) Math.round((value / Math.pow(2, bitsPerSample)) * 255d);

					p.addPoint(x, yOffset - value);
					x++;
				}
				if (x >= maxXAxis) {
					break;
				}
			}
			g.setColor(skin.modulationRange());
			g.drawPolyline(p.xpoints, p.ypoints, p.npoints);
			g.setColor(skin.modulationValue());
			for (int i = 0; i < p.npoints; i++) {
				// we want to draw the dots AFTER the polyline
				g.drawRect(p.xpoints[i], p.ypoints[i], 1, 1);
			}
		}

		// it's probably better to generate an image that this panel consists of, but we can get to that later.
		// it would be fun if we could drag points on the graph to modify the sound.
	}
}
