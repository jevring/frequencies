/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v1.ui;

import net.jevring.frequencies.v2.math.Interpolation;
import net.jevring.frequencies.v2.control.Control;
import net.jevring.frequencies.v2.control.ControlListener;
import net.jevring.frequencies.v2.control.curves.Curve;
import net.jevring.frequencies.v2.control.curves.Exponential;
import net.jevring.frequencies.v2.control.curves.Linear;
import net.jevring.frequencies.v2.control.curves.Logarithmic;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.NumberFormat;
import java.util.Arrays;

import static net.jevring.frequencies.v2.ui.GridBadLayoutUtils.gbc;

/**
 * A slider control that corresponds to knobs and other similar things on a physical device.
 *
 * @author markus@jevring.net
 * @created 2020-02-12 20:03
 */
public class JSliderControl extends JPanel {
	private static final int SLIDER_MIN = 0;
	private static final int SLIDER_MAX = 1_000_000;

	private final JLabel label;
	private final JSlider slider;
	private final JComboBox<Curve> curveSelector;
	private final JFormattedTextField min;
	private final JFormattedTextField max;
	private final JLabel value;
	private volatile Control control;

	public JSliderControl(String label, double min, double current, double max, Class<? extends Curve> curve) {
		super(new GridBagLayout());
		this.label = new JLabel(label, JLabel.CENTER);

		// todo: make changes to these change the control et later.
		Curve[] curves = {new Linear(), new Logarithmic(), new Exponential()};
		this.curveSelector = new JComboBox<>(curves);
		this.curveSelector.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED && control != null) {
					Curve curve = (Curve) e.getItem();
					//control.setCurve(curve);
				}
			}
		});
		this.curveSelector.setSelectedItem(Arrays.stream(curves).filter(c -> c.getClass().equals(curve)).findFirst().orElseThrow());

		this.slider = new JSlider(JSlider.VERTICAL, SLIDER_MIN, SLIDER_MAX, (int) domainToSlider(min, current, max));
		this.slider.setPreferredSize(new Dimension(25, 100));

		this.min = new JFormattedTextField(NumberFormat.getNumberInstance());
		this.min.setHorizontalAlignment(JTextField.CENTER);
		this.min.setValue(min);

		this.max = new JFormattedTextField(NumberFormat.getNumberInstance());
		this.max.setHorizontalAlignment(JTextField.CENTER);
		this.max.setValue(max);


		// todo: it would be nice if this would just scale UP with the text, but not DOWN
		this.value = new JLabel(String.format("%.4f", current));
		this.value.setMinimumSize(new Dimension(25, 0));
		this.value.setText(String.format("%.4f", current));


		this.slider.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				int value = slider.getValue();
				double valueScaledToDomain = sliderToDomain(minValue(), value, maxValue());
				JSliderControl.this.value.setText(String.format("%.4f", valueScaledToDomain));
				if (control != null) {
					control.set(SLIDER_MIN, slider.getValue(), SLIDER_MAX, JSliderControl.this);
				}
			}
		});


		// @formatter:off
		add(this.label,         gbc(0, 0, 2, 1, 0, 0));
		add(this.curveSelector, gbc(0, 1, 2, 1, 0, 0));
		add(this.max,           gbc(0, 2, 2, 1, 0, 0));
		add(this.slider,        gbc(0, 3, 1, 1, 0, 10));
		add(this.value,         gbc(1, 3, 1, 1, 0, 10));
		add(this.min,           gbc(0, 4, 2, 1, 0, 0));
		// @formatter:on
	}

	private double domainToSlider(double min, double value, double max) {
		return Interpolation.linear(min, max, value, SLIDER_MIN, SLIDER_MAX);
	}

	private double sliderToDomain(double min, double value, double max) {
		return Interpolation.linear(SLIDER_MIN, SLIDER_MAX, value, min, max);
	}

	public void attachControl(Control control) {
		this.control = control;
		this.control.addListener(new ControlListener() {
			@Override
			public void valueChanged(double min, double newValue, double max, Object source) {
				//value.setText(String.format("%.4f", LinearInterpolation.lerp(min, max, newValue, minValue(), maxValue())));
				if (source == JSliderControl.this) {
					// If we already came from the slider, there's no need to retrigger the value for the slider.
					// This is to avoid getting into a loop where the values keep changing each other, and never stopping
					return;
				}
				double valueScaledToSlider = domainToSlider(min, newValue, max);
				System.out.printf("slider: %13.5f = %13.5f, %s%n", newValue, valueScaledToSlider, source);
				slider.setValue((int) valueScaledToSlider);
			}
		});
	}

	public double minValue() {
		Number minValue = (Number) this.min.getValue();
		return minValue.doubleValue();
	}

	public double maxValue() {
		Number maxValue = (Number) this.max.getValue();
		System.out.println("maxValue = " + maxValue);
		return maxValue.doubleValue();
	}
}
