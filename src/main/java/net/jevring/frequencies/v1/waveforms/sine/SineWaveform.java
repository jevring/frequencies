/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v1.waveforms.sine;

import net.jevring.frequencies.v1.waveforms.Waveform;

/**
 * https://secure.wikimedia.org/wikipedia/en/wiki/Sine_wave
 * <p/>
 * This generates a byte array representing a sine wave tone.
 *
 * @author markus@jevring.net
 * Time: 2011-05-26 16:15
 */
public class SineWaveform extends Waveform {
	public SineWaveform(int sampleRate, int bitsPerSample, int frequency) {
		super(sampleRate, bitsPerSample, frequency);
	}

	@Override
	public int valueAtStep(int i) {
		int bytesPerSample = bitsPerSample / 8;
		// note: this is taken from the internet. It's clearly some sine wave stuff,
		// but I will admit I'm not well enough versed in trigonometry to know what it
		// all means.
		double angle = i / (sampleRate / frequency) * 2.0 * Math.PI;
		angle = angle / (double) bytesPerSample; // this makes the frequency correct as we scale up the bits

		// how many Sine waves can we fit in to this time?
	        /*
            440 hz -> 440 Sine waves per second

            ahh, this, i / (sampleRate / frequency) = the period we are in!
             */
		//System.out.println(((double)i / ((double)sampleRate / (double)frequency)) + ", " + ((double)i / ((double)sampleRate / (double)frequency)) / (double)bytesPerSample);

		return (int) (Math.sin(angle) * (Math.pow(2, bitsPerSample - 1) - 1));
	}
}
