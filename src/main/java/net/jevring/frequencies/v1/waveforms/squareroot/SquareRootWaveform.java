/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v1.waveforms.squareroot;

import net.jevring.frequencies.v1.waveforms.Waveform;

/**
 * This is like a "rave drum" waveform. We need to compress it a bit to get a higher bpm.
 *
 * @author markus@jevring.net
 * Time: 2011-05-26 16:15
 */
public class SquareRootWaveform extends Waveform {
	public SquareRootWaveform(int sampleRate, int bitsPerSample, int frequency) {
		super(sampleRate, bitsPerSample, frequency);
	}

	@Override
	public int valueAtStep(int i) {
		int bytesPerSample = bitsPerSample / 8;
		// the higher the speed, the most lazer-y it sounds 
		double speed = 2.0;
		double angle = i / (sampleRate / frequency) * speed * Math.PI;
		angle = angle / (double) bytesPerSample; // this makes the frequency correct as we scale up the bits
		// this may look like a sine wave, but the sqrt() here makes all the difference!
		return (int) (Math.sqrt(angle) * (Math.pow(2, bitsPerSample - 1) - 1));
	}
}
