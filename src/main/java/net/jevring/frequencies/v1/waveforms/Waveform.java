/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v1.waveforms;

import javax.sound.sampled.AudioFormat;

/**
 * @author markus@jevring.net
 */
public abstract class Waveform {
	protected final float sampleRate; // if this isn't a float, we can't do sane math with it.
	protected final int bitsPerSample;
	protected final AudioFormat audioFormat;
	protected int frequency;

	protected Waveform(int sampleRate, int bitsPerSample, int frequency) {
		this.sampleRate = sampleRate;
		this.bitsPerSample = bitsPerSample;
		this.audioFormat = new AudioFormat(sampleRate, bitsPerSample, 1, true, true);
		this.frequency = frequency;
	}

	public abstract int valueAtStep(int step);

	// todo: we should allow for a "general mathematical expression" waveform. That'd be neat

	public float getSampleRate() {
		return sampleRate;
	}

	public int getBitsPerSample() {
		return bitsPerSample;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	public AudioFormat getAudioFormat() {
		return audioFormat;
	}
}
