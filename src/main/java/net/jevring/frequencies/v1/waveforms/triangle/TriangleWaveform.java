/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v1.waveforms.triangle;

import net.jevring.frequencies.v1.waveforms.Waveform;

/**
 * https://en.wikipedia.org/wiki/Triangle_wave
 *
 * @author markus@jevring.net
 * Time: 2011-05-26 16:15
 */
public class TriangleWaveform extends Waveform {
	public TriangleWaveform(int sampleRate, int bitsPerSample, int frequency) {
		super(sampleRate, bitsPerSample, frequency);
	}

	@Override
	public int valueAtStep(int i) {
		int bytesPerSample = bitsPerSample / 8;
		double t = i / (sampleRate / frequency); // this is just the number of the period
		t = t / (double) bytesPerSample;
		double d = 2.0 * Math.abs(2.0 * (t - Math.floor(t + 0.5d))) - 1;
		return (int) (d * (Math.pow(2, bitsPerSample - 1) - 1));
	}
}
