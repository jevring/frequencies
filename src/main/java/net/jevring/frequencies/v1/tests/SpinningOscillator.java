/*
 * Copyright 2021 Markus Jevring                                              
 *                                                                            
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *                                                                            
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 *                                                                            
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 *                                                                            
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v1.tests;

import net.jevring.frequencies.v2.waveforms.SineWaveform;
import net.jevring.frequencies.v2.waveforms.Waveform;

import java.util.concurrent.TimeUnit;

/**
 * An oscillator spinning in a loop to generate values according to some wave form and frequency.
 *
 * @author markus@jevring.net
 * @created 2020-02-02 14:41
 */
@Deprecated
public class SpinningOscillator {
	// todo: the default version of this makes the value available to be pulled, but we can also make one version that accepts a a callback for the value to be pushed


	private Thread thread;
	private volatile boolean running = false;

	private volatile double frequency;
	private volatile Waveform waveform;

	private volatile double value = 0;

	public SpinningOscillator(double frequency, Waveform waveform) {
		this.frequency = frequency;
		this.waveform = waveform;
	}

	public static void main(String[] args) {
		SpinningOscillator spinningOscillator = new SpinningOscillator(1, new SineWaveform());
		spinningOscillator.start();
	}

	public double getFrequency() {
		return frequency;
	}

	public void setFrequency(double frequency) {
		this.frequency = frequency;
	}

	public Waveform getWaveform() {
		return waveform;
	}

	public void setWaveform(Waveform waveform) {
		this.waveform = waveform;
	}

	public double currentValue() {
		return value;
	}

	public synchronized void start() {
		if (thread != null) {
			throw new IllegalStateException("Already running");
		}
		running = true;
		thread = new Thread(this::loop);
		thread.start();
	}

	public synchronized void stop() {
		if (thread == null) {
			throw new IllegalStateException("Not running");
		}
		running = false;
		thread.interrupt();
		thread = null;
	}

	/**
	 * Generate new values ~100 times per second
	 */
	private void loop() {
		double nanosecondsPerSecond = TimeUnit.SECONDS.toNanos(1);
		double cycleProgress = 0;
		long previousNanos = System.nanoTime();
		while (running) {
			long nowNanos = System.nanoTime();
			long elapsedNanos = nowNanos - previousNanos;
			if (elapsedNanos > 1e7) {
				previousNanos = nowNanos;

				double nanosecondsPerCycle = nanosecondsPerSecond / frequency;
				double elapsedCycles = elapsedNanos / nanosecondsPerCycle;

				cycleProgress += elapsedCycles;
				cycleProgress = cycleProgress % 1d;

				// get sample for this wave form
				value = waveform.valueAt(cycleProgress, 0, true, 0, 0);
			}
			//Thread.onSpinWait();
			try {
				Thread.sleep(1);
			} catch (InterruptedException ignored) {
			}
		}
	}

}
