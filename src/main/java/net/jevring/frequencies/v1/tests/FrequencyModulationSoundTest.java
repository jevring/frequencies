/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v1.tests;

import net.jevring.frequencies.v1.continuous.signals.FrequencyModulatedSineWaveSignal;
import net.jevring.frequencies.v1.continuous.signals.SineWaveFrequencyModulator;
import net.jevring.frequencies.v1.output.LineOutput;

import javax.sound.sampled.LineUnavailableException;
import java.io.IOException;

/**
 * @author markus@jevring.net
 * Time: 2011-05-30 15:56
 */
public class FrequencyModulationSoundTest {
	public static void main(String[] args) {
		final LineOutput out = new LineOutput();
		//Signal lowFrequencyOscillator = new SineWaveSignal(44100, 16, 20);
		FrequencyModulatedSineWaveSignal a = new FrequencyModulatedSineWaveSignal(44100, 8, 440); // 440 Hz, A
		a.setFrequencyModulator(new SineWaveFrequencyModulator(44100, 8, 18));
		out.connect(a);
		Thread t = new Thread(new Runnable() {
			public void run() {
				try {
					System.out.println("Commencing playback");
					out.play();
				} catch (LineUnavailableException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
					System.exit(1);
				}
			}
		});
		t.start();
		System.out.println("Press any key to stop playback");
		try {
			System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}
		out.stop();
		System.out.println("Playback stopped");


	}
}
