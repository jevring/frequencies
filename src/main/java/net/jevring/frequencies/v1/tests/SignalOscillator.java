/*
 * Copyright 2021 Markus Jevring                                              
 *                                                                            
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *                                                                            
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 *                                                                            
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 *                                                                            
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v1.tests;

import net.jevring.frequencies.v1.tests.MutableDouble;
import net.jevring.frequencies.v2.waveforms.Waveform;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

/**
 * @author markus@jevring.net
 * @created 2019-12-11 19:47
 */
@Deprecated
public class SignalOscillator {
	private final MutableDouble value;
	private volatile Waveform waveform;
	private volatile MutableDouble modulationSize;
	private volatile MutableDouble frequency;

	private volatile boolean running = false;

	public SignalOscillator(MutableDouble value, Waveform waveform, MutableDouble modulationSize, MutableDouble frequency) {
		this.value = value;
		this.waveform = waveform;
		this.modulationSize = modulationSize;
		this.frequency = frequency;
	}

	public void start() {
		// todo: nicer
		running = true;
		new Thread(this::loop).start();
	}

	private void loop() {
		TimeUnit timeUnit = TimeUnit.NANOSECONDS;

		System.out.println("frequency.get() = " + frequency.get());

		// Discrete time steps per second
		double samplesPerSecond = 10;
		double samplesPerPeriod = samplesPerSecond / frequency.get();
		double stepSizeInPercent = 1d / samplesPerPeriod;
		long timeUnitsPerSecond = timeUnit.convert(Duration.ofSeconds(1));
		long pause = Math.round(timeUnitsPerSecond / samplesPerSecond);

		long lastReset = System.currentTimeMillis();
		double cycleProgress = 0;
		long sleepAdjustment = 0;
		while (running) {
			long stepStart = System.nanoTime();
			// A value between 0 and 1. 
			// 0 at the start of the wave form (wave form is at y=0) 
			// 1 at the end of the wave form (the wave having gone through one whole period and arrived back at y=0).
			// This is the same for all wave forms, no matter if they are Sine or sawtooth or what
			cycleProgress += stepSizeInPercent;

			// todo: later we shouldn't write to the same frequency that the actual dial controls

			// todo: should we really put the size in here?
			value.set(waveform.valueAt(cycleProgress, 0, true, 0, 0) * modulationSize.get());


			// todo. this modulo is what we eventually want, but for now, we want to see how often it resets, so let's just track that.
			//cycleProgress = cycleProgress % 1d;

			if (cycleProgress >= 1d) {
				// this works, but we seem to get aliasing where we only change the frequency in steps. It's not continuous
				// that could just be because we're not getting continuous numbers from the slider! It doesn't have a high enough resolution!
				// indeed it was!
				long now = System.currentTimeMillis();
				System.out.println("reset after " + (now - lastReset) + " ms");
				lastReset = now;
				cycleProgress = cycleProgress % 1d;
			}
			// We just run this at some set updating frequency, and we just set the frequency modulation that many times per second!
			// We can start with the assumption that calculating it is effectively instant, but that might change later
			try {
				timeUnit.sleep(pause + sleepAdjustment);
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
			long stepEnd = System.nanoTime();
			long stepTotal = stepEnd - stepStart;
			// todo: adjust the sleep for the next cycle
			long stepTotalInCorrectUnit = timeUnit.convert(stepTotal, TimeUnit.NANOSECONDS);
			sleepAdjustment = pause - stepTotalInCorrectUnit;
			//System.out.println("sleepAdjustment = " + sleepAdjustment);

		}
	}

	// todo: stop;
}
