/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v1.tests;

import net.jevring.frequencies.v1.output.LineOutput;
import net.jevring.frequencies.v1.output.SimplePlayer;
import net.jevring.frequencies.v1.sound.ContinuousSound;
import net.jevring.frequencies.v1.sound.FixedSound;
import net.jevring.frequencies.v1.ui.ByteArrayGraph;
import net.jevring.frequencies.v2.ui.SwingUtils;
import net.jevring.frequencies.v1.waveforms.Waveform;
import net.jevring.frequencies.v1.waveforms.sawtooth.SawtoothWaveform;
import net.jevring.frequencies.v1.waveforms.sine.SineWaveform;
import net.jevring.frequencies.v1.waveforms.square.SquareWaveform;
import net.jevring.frequencies.v1.waveforms.squareroot.SquareRootWaveform;
import net.jevring.frequencies.v1.waveforms.triangle.TriangleWaveform;

import javax.sound.sampled.*;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.atomic.AtomicBoolean;

import static net.jevring.frequencies.v2.ui.GridBadLayoutUtils.gbc;

/**
 * @author markus@jevring.net
 * Time: 2011-05-26 16:46
 */
public class SoundBoard extends JPanel {
	private final LineOutput out = new LineOutput();
	private volatile WaveformCreator waveformCreator = new WaveformCreator(SineWaveform.class);
	private volatile Waveform currentWaveform;
	
	/*
	
	
	UI Ideas:
	* Each sound added to the board has a key associated with it. Holding down that key while twiddling the knobs with the mouse adjusts that sound.
	** Holding down multiple keys adjusts multiple sounds
	* Outside lines in (mic, for instance)
	* Sampler banks
	* MIDI, of course
	* Actual knobs
	* Oscilloscope
	
	 
	 
	 */

	public SoundBoard() {
		setLayout(new GridBagLayout());

		final AtomicBoolean continuous = new AtomicBoolean();
		final ByteArrayGraph graph = new ByteArrayGraph();
		final JSlider frequency = new JSlider(100, 600, 440); // 440Hz, A
		final JSpinner bitRate = new JSpinner(new SpinnerNumberModel(8, 8, 32, 8));
		bitRate.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				changeWaveform(frequency, graph, bitRate);
			}
		});
		final JTextField frequencyValue = new JTextField();
		frequency.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				currentWaveform.setFrequency(frequency.getValue());
				frequencyValue.setText(String.valueOf(frequency.getValue()) + " Hz");
				updateGraph(graph);
				revalidate();
				repaint();
			}
		});
		JButton play = new JButton("Play!");
		play.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					SimplePlayer.play(new FixedSound(currentWaveform, 1000));
				} catch (LineUnavailableException e1) {
					e1.printStackTrace();
				}
			}
		});
		final JButton on = new JButton("Turn on");
		on.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if ("Turn on".equals(on.getText())) {
					continuous.set(true);
					out.connect(new ContinuousSound(currentWaveform));
					Thread t = new Thread(new Runnable() {
						public void run() {
							try {
								System.out.println("Commencing playback");
								out.play();
							} catch (Exception e) {
								e.printStackTrace();
								//System.exit(1);
							}
						}
					});
					t.start();
					on.setText("Turn off");
				} else {
					continuous.set(false);
					out.stop(); // will also kill the thread, as it can return
					on.setText("Turn on");
				}
			}
		});


		MutableComboBoxModel<Class<? extends Waveform>> waveforms = new DefaultComboBoxModel<>();
		waveforms.addElement(SineWaveform.class);
		waveforms.addElement(SawtoothWaveform.class);
		waveforms.addElement(SquareRootWaveform.class);
		waveforms.addElement(SquareWaveform.class);
		waveforms.addElement(TriangleWaveform.class);
		JComboBox<Class<? extends Waveform>> waveform = new JComboBox<>(waveforms);
		// todo: would be nice if we only rendered the simple name
		waveform.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					System.out.println("Setting sound creator for class " + e.getItem());
					waveformCreator = new WaveformCreator((Class<? extends Waveform>) e.getItem());
					changeWaveform(frequency, graph, bitRate);
				}
			}
		});

		add(graph, gbc(0, 0, 4, 1, 1, 1));
		add(frequency, gbc(0, 1, 1, 1, 1, 0));
		add(frequencyValue, gbc(1, 1, 1, 1, 0, 0));
		add(bitRate, gbc(2, 1, 1, 1, 0, 0));
		add(waveform, gbc(0, 2, 4, 1, 0, 0));
		add(play, gbc(0, 3, 4, 1, 0, 0));
		add(on, gbc(0, 4, 4, 1, 0, 0));


		changeWaveform(frequency, graph, bitRate);
		frequencyValue.setText(String.valueOf(frequency.getValue()) + " Hz");
	}

	public static void main(String[] args) {
		// http://www.jsresources.org/faq_audio.html#sr_unspecified
		// todo: see if we can do something sane with this information, like populate the bit rate
		for (Mixer.Info info : AudioSystem.getMixerInfo()) {
			Mixer mixer = AudioSystem.getMixer(info);
			System.out.println("For mixer " + info.getDescription());
			System.out.println("These are the supported formats");
			Line.Info[] sourceLineInfo = mixer.getSourceLineInfo();
			for (Line.Info info2 : sourceLineInfo) {
				if (info2 instanceof DataLine.Info) {
					for (AudioFormat audioFormat : ((DataLine.Info) info2).getFormats()) {
						System.out.println("audioFormat = " + audioFormat);
					}
				}
			}
		}


		SwingUtils.showWindowFor("Sound board", new SoundBoard(), false);
	}

	private void changeWaveform(JSlider frequency, ByteArrayGraph graph, JSpinner bitRate) {
		currentWaveform = waveformCreator.create(frequency.getValue(), (int) bitRate.getValue());
		updateGraph(graph);
		revalidate();
		repaint();
	}

	private void updateGraph(ByteArrayGraph graph) {
		// we need a sound to show in the graph, so we'll have to create a temporary one
		// todo: once we've solved the clicking, we should update the graph whenever the frequency changes as well
		FixedSound sound = new FixedSound(currentWaveform, 50);
		graph.update(sound.getData(), sound.getBitsPerSample()); // this repaints as well
	}

	private static final class WaveformCreator {
		private final Class<? extends Waveform> clazz;

		private WaveformCreator(Class<? extends Waveform> clazz) {
			this.clazz = clazz;
		}

		private Waveform create(int frequency, int bitRate) {
			try {
				Constructor<? extends Waveform> constructor = clazz.getConstructor(int.class, int.class, int.class);
				return constructor.newInstance(44100, bitRate, frequency);
			} catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
				e.printStackTrace();
				// should work, as the 4 classes we use are known to have compatible constructors
				throw new AssertionError(e);
			}
		}
	}
}
