/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v1.tests;


import javax.sound.sampled.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

public class Test {
	public static void xmain(String[] args) {
		int i = -253;
		System.out.println(i);
		System.out.println(i >> 1);
		// this goes to hell when we have signed bytes
		System.out.println(i & getPattern(0));
		System.out.println(i & getPattern(1));
		System.out.println(i & getPattern(2));
		System.out.println(i & getPattern(3));
		System.out.println((byte) 130);


	}

	private static int getPattern(int offset) {
		switch (offset) {
			case 0:
				return 0x000000FF;
			case 1:
				return 0x0000FF00;
			case 2:
				return 0x00FF0000;
			case 3:
				return 0xFF000000;
			default:
				throw new IllegalArgumentException("Max offset = 4 (32 bit integers)");
		}
	}

	public static void xxmain(String[] args) {
		int value = 257;
		byte b1 = (byte) (value & 0xff); // low byte
		byte b2 = (byte) ((value >> 8) & 0xff); // high byte

		int newValue = ((b2 & 0xff) << 8) | ((b1 & 0xff) << 0);

		System.out.println("b1: " + b1);
		System.out.println("b2: " + b2);

		System.out.println("value = " + value);
		System.out.println("newValue = " + newValue);
	}

	public static void main(String[] args) {
		try {
			float frameRate = 44100f; // 44100 samples/s
			int channels = 2;
			double duration = 1.0;
			int sampleBytes = Short.SIZE / 8;
			int frameBytes = sampleBytes * channels;
			AudioFormat format = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, frameRate, Short.SIZE, channels, frameBytes, frameRate, true);
			int nFrames = (int) Math.ceil(frameRate * duration);
			int nSamples = nFrames * channels;
			int nBytes = nSamples * sampleBytes;
			ByteBuffer data = ByteBuffer.allocate(nBytes);
			double freq = 440.0;
// Generate all samples
			for (int i = 0; i < nFrames; ++i) {
				double value = Math.sin((double) i / (double) frameRate * freq * 2 * Math.PI) * (Short.MAX_VALUE);
				for (int c = 0; c < channels; ++c) {
					int index = (i * channels + c) * sampleBytes;
					data.putShort(index, (short) value);
				}
			}

			AudioInputStream stream = new AudioInputStream(new ByteArrayInputStream(data.array()), format, nFrames * 2);
			Clip clip = AudioSystem.getClip();
			clip.open(stream);
			clip.start();
			clip.drain();
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
