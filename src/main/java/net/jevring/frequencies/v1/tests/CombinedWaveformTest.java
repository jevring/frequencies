/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v1.tests;

import net.jevring.frequencies.v1.output.SimplePlayer;
import net.jevring.frequencies.v1.sound.CombinedWaveform;
import net.jevring.frequencies.v1.sound.FixedSound;
import net.jevring.frequencies.v1.ui.ByteArrayGraph;
import net.jevring.frequencies.v2.ui.SwingUtils;
import net.jevring.frequencies.v1.waveforms.sine.SineWaveform;

import javax.sound.sampled.LineUnavailableException;
import javax.swing.*;

/**
*
 * @author markus@jevring.net
 * @created 2019-12-08 20:34
 */
public class CombinedWaveformTest {
	// keep this to experiment with graphs
	public static void main(String[] args) {
		int bitsPerSample = 8;
		final CombinedWaveform sc = new CombinedWaveform(44100, bitsPerSample);
		sc.add(new SineWaveform(44100, bitsPerSample, 100));
		sc.add(new SineWaveform(44100, bitsPerSample, 500));
		//sc.add(new SineWaveform(44100, 8, 1600));
		//sc.add(new SineWaveform(44100, 8, 880));
		//sc.add(new TriangleWaveform(44100, 8, 442));
		//sc.add(new SquareWaveform(44100, 8, 110));
		//sc.add(new SawtoothWaveform(44100, 8, 441));

		final FixedSound sound = new FixedSound(sc, 30000);
		//final FixedSound sound = new FixedSound(new SineWaveform(44100, 8, 440), 30000);

		ByteArrayGraph byteArrayGraph = new ByteArrayGraph();
		SwingUtils.showWindowFor("Byte array graph", byteArrayGraph, false);

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				// todo: make this graph zoomable/panable, etc
				byteArrayGraph.update(sound.getData(), sound.getBitsPerSample());
			}
		});

		try {
			SimplePlayer.play(sound);
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		}
	}
}
