/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v1.tests;

import net.jevring.frequencies.v2.ui.SwingUtils;
import net.jevring.frequencies.v2.util.Bytes;
import net.jevring.frequencies.v2.waveforms.*;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import static net.jevring.frequencies.v2.ui.GridBadLayoutUtils.gbc;

/**
*
 * @author markus@jevring.net
 * @created 2019-12-07 14:22
 */
public class MainLoopOscillator {
	private static final float SAMPLE_RATE = 44100f;
	private static final float SAMPLE_SIZE_IN_BITS = Short.SIZE;
	private Thread thread;
	private volatile boolean running = false;
	private SourceDataLine sourceDataLine;

	private volatile short volume = Short.MAX_VALUE;
	private volatile double frequency = 440;
	private volatile Waveform waveform = new SineWaveform();

	public MainLoopOscillator() {
		JSlider frequency = new JSlider(100, 600, 440); // 440Hz, A
		frequency.setPreferredSize(new Dimension(1000, 25));
		JTextField frequencyValue = new JTextField(frequency.getValue() + " Hz");

		frequency.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				MainLoopOscillator.this.frequency = frequency.getValue();
				frequencyValue.setText(frequency.getValue() + " Hz");
			}
		});
		JComboBox<Waveform> waveforms =
				new JComboBox<>(new Waveform[]{new SineWaveform(), new SawtoothWaveform(true), new SquareWaveform(), new TriangleWaveform()});
		waveforms.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				MainLoopOscillator.this.waveform = (Waveform) waveforms.getSelectedItem();
			}
		});

		JPanel mainPanel = new JPanel();
		mainPanel.add(frequency, gbc(0, 1, 1, 1, 1, 0));
		mainPanel.add(frequencyValue, gbc(1, 1, 1, 1, 0, 0));


		SwingUtils.showWindowFor("Frequency", mainPanel, false);
		//startFrequencyModulator(50);
	}

	public static void main(String[] args) throws LineUnavailableException {
		MainLoopOscillator mainLoopOscillator = new MainLoopOscillator();
		mainLoopOscillator.start();
	}

	private void startFrequencyModulator(int by) {

		SignalOscillator signalOscillator = new SignalOscillator(new MutableDouble() {
			@Override
			public double get() {
				return frequency;
			}

			@Override
			public void set(double d) {
				frequency = 440d + d;
			}
		}, new SineWaveform(), MutableDouble.fixed(by), MutableDouble.fixed(2));
		signalOscillator.start();
	}

	private void loop() {
		double cycleProgress = 0;
		long s = System.currentTimeMillis();
		int i = 0;
		while (running) {
			double samplesPerPeriod = SAMPLE_RATE / frequency;

			// todo: it's not called 'period', it's called 'cycle'

			// A value between 0 and 1. 
			// 0 at the start of the wave form (wave form is at y=0) 
			// 1 at the end of the wave form (the wave having gone through one whole period and arrived back at y=0).
			// This is the same for all wave forms, no matter if they are Sine or sawtooth or what
			double stepSizeInPercent = 1d / samplesPerPeriod;
			cycleProgress += stepSizeInPercent;
			cycleProgress = cycleProgress % 1d;

			// get sample for this wave form
			double waveformValueAtPeriod = waveform.valueAt(cycleProgress, 0, true, 0, 0);

			// todo: we can modulate amplitude by multiplying the signal before creating the sample (to make it larger or smaller, etc).
			// However it must still be in the range of 0..1, so we'd have to clip it

			// scale to the bit rate for the sample (8 or 16 or 24 most commonly), in this case 16 bits (which is a short)
			short sample = (short) (waveformValueAtPeriod * volume);
			// todo: we should just re-use the array here

			// NOTE: This thread is being time-coordinated by how long it takes to write to the source data line.
			// Other threads, like the LFO, for example, won't be.
			// Alternatively this thread is the ONLY thread, and we just sample the whole system at these intervals.
			sourceDataLine.write(Bytes.convertToBytes(sample, Short.BYTES), 0, Short.BYTES);
			i++;
			long now = System.currentTimeMillis();
			if ((now - s) > 1000) {
				System.out.println("i = " + i);
				i = 0;
				s = now;
			}
		}
	}

	private synchronized void start() throws LineUnavailableException {
		if (thread != null) {
			throw new IllegalStateException("Already running");
		}
		running = true;
		thread = new Thread(this::loop);

		AudioFormat format = new AudioFormat(SAMPLE_RATE, (int) SAMPLE_SIZE_IN_BITS, 1, true, true);
		sourceDataLine = AudioSystem.getSourceDataLine(format);
		sourceDataLine.open(format);
		sourceDataLine.start();
		thread.start();
	}

	private synchronized void stop() {
		if (thread == null) {
			throw new IllegalStateException("Not running");
		}
		running = false;
		sourceDataLine.flush();
		sourceDataLine.close();
		thread = null;
	}
}
