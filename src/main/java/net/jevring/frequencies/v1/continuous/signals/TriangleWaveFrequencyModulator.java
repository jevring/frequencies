/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v1.continuous.signals;

import net.jevring.frequencies.v1.continuous.ValueProvider;

/**
 * @author markus@jevring.net
 * Time: 2011-05-30 20:11
 */
public class TriangleWaveFrequencyModulator implements ValueProvider {
	private final float sampleRate;
	private boolean up = true;
	private double modulation = 0.0d;

	public TriangleWaveFrequencyModulator(float sampleRate) {
		this.sampleRate = sampleRate;
	}

	public double getValue() {
		int changeInFrequency = 200; // Hz
		int timeForChange = 500; // milliseconds

		// _todo: we still have the problem of getting a double thing in the beginning
		// it seems like this is a computer issue. it seems that it can't keep up with the initial burst, or something


		// this generates a triangle modulation.
		// it looks fine when graphed, so why do we get the double thing then?

		double modulationAmountPerTick = (double) changeInFrequency / (sampleRate * ((double) timeForChange / 1000.0d));

		if (up) {
			modulation += modulationAmountPerTick;
		} else {
			modulation -= modulationAmountPerTick;
		}

		if (modulation >= changeInFrequency || modulation < 0) {
			up = !up;
		}

		return modulation;
	}
}
