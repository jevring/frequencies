/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v1.continuous.signals;

import net.jevring.frequencies.v1.continuous.ValueProvider;

/**
 * @author markus@jevring.net
 * Time: 2011-05-30 20:33
 */
public class SineWaveFrequencyModulator implements ValueProvider {
	private final float sampleRate;
	private final int frequency;
	private final int bitsPerSample;
	private int step = 0;

	public SineWaveFrequencyModulator(float sampleRate, int bitsPerSample, int frequency) {
		this.sampleRate = sampleRate;
		this.frequency = frequency;
		this.bitsPerSample = bitsPerSample;
	}

	public double getValue() {
		double angle = step++ / (sampleRate / frequency) * 2.0 * Math.PI;

		//int value = (int) (Math.sin(angle) * (Math.pow(2, bitsPerSample - 1) - 1));
		double value = Math.sin(angle) * frequency;
		//System.out.println(value);
		return value;

		// todo: ok, this is clearly not how you actually do frequency modulation. we must find out how. to wikipedia!
	}
}
