/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v1.continuous.signals;

import net.jevring.frequencies.v1.sound.FixedSound;
import net.jevring.frequencies.v1.sound.Signal;
import net.jevring.frequencies.v1.waveforms.sine.SineWaveform;

/**
 * @author markus@jevring.net
 * Time: 2011-05-30 16:00
 */
public class SineWaveSignal extends FixedSound implements Signal {
	public SineWaveSignal(int sampleRate, int bitsPerSample, int frequency) {
		super(new SineWaveform(sampleRate, bitsPerSample, frequency), 0);
	}

	public byte[] getSound() {
		// generate one period of sound per invocation.
		// the reason we don't pre-generate it, is that we want to be able to filter it later.
		// todo: perhaps that's not the best way to do it, but we'll see.

		int bytesPerSample = waveform.getBitsPerSample() / 8;
		byte[] data = new byte[(int) ((double) (waveform.getSampleRate() * bytesPerSample) / (double) waveform.getFrequency())];
		// todo: this chunk size may be too large.
		// perhaps we need to do this byte-by-byte, in the end.
		// that is, set up all signals and filters, and only then generate the bytes

		for (int i = 0; i < data.length; i += bytesPerSample) {
			// note: this is taken from the internet. It's clearly some sine wave stuff,
			// but I will admit I'm not versed enough in trigonometry to know what it
			// all means.
			double angle = i / (waveform.getSampleRate() / waveform.getFrequency()) * 2.0 * Math.PI;
			angle = angle / (double) bytesPerSample; // this makes the frequency correct as we scale up the bits

			int value = (int) (Math.sin(angle) * (Math.pow(2, waveform.getBitsPerSample() - 1) - 1));
			// todo: replace the values with a more generic algorithm: http://social.msdn.microsoft.com/Forums/en/netfxbcl/thread/46272ef0-59e6-47f2-bcce-166947e2cdb5
			if (waveform.getBitsPerSample() == 8) {
				data[i] = (byte) value;
			} else if (waveform.getBitsPerSample() == 16) {
				// every individual block of N bytes adheres to endian-ness.
				// even if the whole stream is still in the "correct" (normal readable) order
				data[i + 1] = (byte) (value & 0xff); // low byte
				data[i] = (byte) ((value >> 8) & 0xff); // high byte
			}
		}
		return data;
	}
}
