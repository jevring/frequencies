/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v1.sound;

import net.jevring.frequencies.v2.util.Bytes;
import net.jevring.frequencies.v1.waveforms.Waveform;

import javax.sound.sampled.AudioFormat;

/**
 * @author markus@jevring.net
 */
public class ContinuousSound implements Signal {
	private final Waveform waveform;
	private final int bytesPerSample;
	private int step = 0;

	public ContinuousSound(Waveform waveform) {
		this.waveform = waveform;
		this.bytesPerSample = waveform.getBitsPerSample() / 8;
	}

	@Override
	public AudioFormat getAudioFormat() {
		return waveform.getAudioFormat();
	}

	@Override
	public byte[] getSound() {
		//return getSoundChunk();
		//return getSoundPiece();
		return getSingleSound();
	}

	private byte[] getSingleSound() {
		// frequency: 440 hz
		// sample rate: 44100 Hz
		// periods per second = 44100 / 440 = 100.227272727272
		// todo: HAH! That seems to have done it! Change when the period hits 0 again!
		// but whenever we get to a number lower than bytesPerSample, it goes quiet
		// todo: why are we even using bytesPerSample to control the step?
		// Also, there is still some clicking, but decidedly less than before

		// todo: with this we get a lot of aliasing. A lot of the frequencies sound the same
		step += bytesPerSample;
		// we want to do this reset where it makes sense from the sound's point of view.
		// using the frequency as a reset point creates some interesting distortion. Can be good for future effects
		if (step > (waveform.getSampleRate() / waveform.getFrequency()) || step < 0) { // wrap around to avoid weirdness
			// if the step gets too high, we run in to strange sounds.
			// I don't (yet) understand why, but as generating chunks that are
			// multiples of the sample rate works, I'll just stick to the sample rate
			// as a wrapping condition.
			// todo: every time we reset, we cause a little click.
			// todo: the higher the bit rate, the bigger the click!

			// perhaps the key is to keep building things up in the buffer so that we never run out
			System.out.println("Reset at " + step);
			step = 0;
		}
		int value = waveform.valueAtStep(step);

		return Bytes.convertToBytes(value, bytesPerSample);
	}

	private byte[] getSoundPiece() {
		// todo: this doesn't work for 16 bit
		int stepsPerChunk = 4;
		byte[] data = new byte[stepsPerChunk * bytesPerSample];
		// todo: even with a stepsPerChunk of 44100, the same as the sample rate, we still get clicks during playback.
		// maybe it's the wrapping condition.
		// when using chunkSound=false, we get some artefacts, but they're more like the computer taking a "breath" for every sample.

		// todo: the longer we let a chunkSound=false run, the more distorted it gets, in small steps that are few and farther and farther between.
		// eventually it sounds like absolute ass
		for (int i = 0; i < stepsPerChunk; i += bytesPerSample) {
			step += bytesPerSample;
			int value = waveform.valueAtStep(step);

			if (waveform.getBitsPerSample() == 8) {
				data[i] = (byte) value;
			} else if (waveform.getBitsPerSample() == 16) {
				data[i + 1] = (byte) (value & 0xff); // low byte
				data[i] = (byte) ((value >> 8) & 0xff); // high byte
			}
		}
		if (step > waveform.getSampleRate() * bytesPerSample * waveform.getFrequency()) { // wrap around to avoid weirdness
			// if the step gets too high, we run in to strange sounds.
			// I don't (yet) understand why, but as generating chunks that are
			// multiples of the sample rate works, I'll just stick to the sample rate
			// as a wrapping condition
			// todo: this results in distortion and clipping etc. wtf is going on?
			// if we reset too early, we get a click. If we reset too late, we get distortion, 
			// likely because 'step' causes the underlying algorithm to wrap and clip
			System.out.println("Reset");
			step = 0;
		}
		return data;
	}

	private byte[] getSoundChunk() {
		// todo: every time we enter here, it's like the computer takes a little breath. Maybe that's the time taken to run through this method.
		System.out.println("ContinuousSound.getSound");
		// generate one period of sound per invocation.

		byte[] data = new byte[(int) (waveform.getSampleRate() * bytesPerSample)];
		// todo: what should the chunk size be? at the current size, we end up here once a second, which is unlikely to be sufficiently often

		for (int i = 0; i < data.length; i += bytesPerSample) {
			int value = waveform.valueAtStep(i);
			// todo: replace the values with a more generic algorithm: http://social.msdn.microsoft.com/Forums/en/netfxbcl/thread/46272ef0-59e6-47f2-bcce-166947e2cdb5
			if (waveform.getBitsPerSample() == 8) {
				data[i] = (byte) value;
			} else if (waveform.getBitsPerSample() == 16) {
//                byteBuffer.putShort(i, value);

				// as nice as ByteBuffer.putShort() is, it doesn't let us do things like 24 bit sound, so we have to
				// stick to "manual" bit twiddling.

				// every individual block of N bytes adheres to endian-ness.
				// even if the whole stream is still in the "correct" (normal readable) order
				data[i + 1] = (byte) (value & 0xff); // low byte
				data[i] = (byte) ((value >> 8) & 0xff); // high byte
//                System.out.println(value + ": [" + (value & 0xff) + ", " + ((value >> 8) & 0xff) + "]");
//                System.out.println(value + ": " + Arrays.toString(new byte[]{data[i],data[i+1]}));
			} // todo: if we choose this approach, we have to support more bitrates. first just get it to work, though
		}
		return data;
	}

	public int getBitsPerSample() {
		return waveform.getBitsPerSample();
	}
}
