/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v1.sound;

import net.jevring.frequencies.v1.waveforms.Waveform;

import javax.sound.sampled.AudioFormat;

/**
 * A sound of a fixed length based on a waveform.
 *
 * @author markus@jevring.net
 * Time: 2011-05-30 11:13
 */
public class FixedSound {
	protected final Waveform waveform;
	private final long milliseconds;
	private final byte[] data;

	public FixedSound(Waveform waveform, long milliseconds) {
		this.waveform = waveform;
		this.milliseconds = milliseconds;
		if (milliseconds < 0) {
			throw new IllegalArgumentException("Duration must be > 0");
		}
		this.data = generateSound();
	}

	public byte[] getData() {
		// todo: document that this isn't a copy. thus, Tones are not immutable
		return data;
	}

	public float getSampleRate() {
		return waveform.getSampleRate();
	}

	public int getBitsPerSample() {
		return waveform.getBitsPerSample();
	}

	public long getMilliseconds() {
		return milliseconds;
	}

	public AudioFormat getAudioFormat() {
		return waveform.getAudioFormat();
	}

	protected byte[] generateSound() {
		int bytesPerSample = getBitsPerSample() / 8;
		byte[] data = new byte[(int) ((getSampleRate() * bytesPerSample * milliseconds) / 1000.0d)];

		//ByteBuffer byteBuffer = ByteBuffer.wrap(data);

		for (int i = 0; i < data.length; i += bytesPerSample) {
			int value = waveform.valueAtStep(i);

			// todo: replace the values with a more generic algorithm: http://social.msdn.microsoft.com/Forums/en/netfxbcl/thread/46272ef0-59e6-47f2-bcce-166947e2cdb5
			if (getBitsPerSample() == 8) {
				data[i] = (byte) value;
			} else if (getBitsPerSample() == 16) {
//                byteBuffer.putShort(i, value);

				// as nice as ByteBuffer.putShort() is, it doesn't let us do things like 24 bit sound, so we have to
				// stick to "manual" bit twiddling.

				// every individual block of N bytes adheres to endian-ness.
				// even if the whole stream is still in the "correct" (normal readable) order
				data[i + 1] = (byte) (value & 0xff); // low byte
				data[i] = (byte) ((value >> 8) & 0xff); // high byte
//                System.out.println(value + ": [" + (value & 0xff) + ", " + ((value >> 8) & 0xff) + "]");
//                System.out.println(value + ": " + Arrays.toString(new byte[]{data[i],data[i+1]}));
			} // todo: if we choose this approach, we have to support more bitrates. first just get it to work, though
		}

		return data;
	}
}
