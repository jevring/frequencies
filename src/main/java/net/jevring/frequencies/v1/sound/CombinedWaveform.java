/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v1.sound;

import net.jevring.frequencies.v1.waveforms.Waveform;

import java.util.ArrayList;
import java.util.List;

/**
 * Combines a set of sounds to produce a new sound wave.
 *
 * @author markus@jevring.net
 */
public class CombinedWaveform extends Waveform {
	private final List<Waveform> waveforms = new ArrayList<>();

	public CombinedWaveform(int sampleRate, int bitsPerSample) {
		super(sampleRate, bitsPerSample, 0);
	}

	@Override
	public int valueAtStep(int i) {
		int s = 0;
		for (Waveform waveform : waveforms) {
			s += waveform.valueAtStep(i) / waveforms.size();
		}
		return s;
	}

	public void add(Waveform waveform) { // todo: add a gain here for mixing "add 10% of this sound"
		// todo: we probably need to stop playing when we do this
		if (waveform.getBitsPerSample() != this.bitsPerSample) {
			throw new IllegalArgumentException("Bits per sample don't match. Got " + waveform.getBitsPerSample() + " bit expected " + this.bitsPerSample);
		}
		if (waveform.getSampleRate() != this.sampleRate) {
			throw new IllegalArgumentException("Sample rate doesn't match. Got " + waveform.getSampleRate() + " bit expected " + this.sampleRate);
		}
		waveforms.add(waveform);
	}

	public void remove(Waveform waveform) {
		waveforms.remove(waveform);
	}
}
