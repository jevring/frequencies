/*
 * Copyright 2020 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.frequencies.v1.output;

import net.jevring.frequencies.v1.sound.Signal;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

/**
 * @author markus@jevring.net
 * Time: 2011-05-30 15:48
 */
public class LineOutput {
	private Signal signal;
	private SourceDataLine sourceDataLine;
	private volatile boolean stopped = false;

	public void connect(Signal signal) {
		this.signal = signal;
	}

	public void play() throws LineUnavailableException {
		AudioFormat format = signal.getAudioFormat();
		sourceDataLine = AudioSystem.getSourceDataLine(format);
		// The default buffer size here is 22050 (presumably sample rate / 2). That's too large to be able to tweak the frequency in a smooth manner.
		// getting it too low, like 4, just results in silence
		sourceDataLine.open(format);
		sourceDataLine.start();
		byte[] sound;
		stopped = false;
		while ((sound = signal.getSound()) != null && !stopped) {
			sourceDataLine.write(sound, 0, sound.length);

		}
		// if we include drain() in the loop, we just get a series of clicks.
		//sourceDataLine.drain();

	}

	public void stop() {
		stopped = true;
		sourceDataLine.flush();
		sourceDataLine.close();
	}
}
