/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.thief;

import net.jevring.frequencies.v2.input.midi.MidiManager;
import net.jevring.frequencies.v2.input.midi.MidiReceiver;
import net.jevring.frequencies.v2.output.Device;
import net.jevring.frequencies.v2.ui.FontSupport;
import net.jevring.frequencies.v2.ui.MidiMapperGlassPane;
import net.jevring.frequencies.v2.ui.Skin;
import net.jevring.frequencies.v2.ui.Skins;

import javax.swing.*;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URI;
import java.net.URL;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Window-level menu bar for thief.
 *
 * @author markus@jevring.net
 * @created 2021-07-06 22:02
 */
public class ThiefUIMenuBar extends JMenuBar {
	private final ThiefUI thiefUI;
	private final Thief thief;

	public ThiefUIMenuBar(ThiefUI thiefUI, Thief thief) {
		this.thiefUI = thiefUI;
		this.thief = thief;
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				JMenu fileMenu = createFileMenu();
				JMenu midiMenu = createMidiMenu();
				JMenu presetsMenu = createPresetsMenu();
				JMenu outputMenu = createOutputMenu();
				JMenu skinMenu = createSkinMenu();

				add(fileMenu);
				add(presetsMenu);
				add(midiMenu);
				add(outputMenu);
				add(skinMenu);
			}
		});
	}

	// region Menu

	private JMenu createSkinMenu() {
		JMenu skinMenu = styleMenuItem(new JMenu("Skin"));
		for (Skin skin : Skins.skins()) {
			skinMenu.add(styleMenuItem(new JMenuItem(skin.name()))).addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					Skins.setSkin(skin, getRootPane());
					if (!skin.opaque()) {
						thiefUI.setBackgroundImage("/club lasers.jpg");
					} else {
						thiefUI.setBackgroundImage(null);
					}
				}
			});
		}
		return skinMenu;
	}

	private JMenu createOutputMenu() {
		JMenu outputMenu = styleMenuItem(new JMenu("Outputs"));
		List<Device> supportedSoundDevices = Device.supportedSoundDevices(Thief.AUDIO_FORMAT);
		for (Device supportedSoundDevice : supportedSoundDevices) {
			outputMenu.add(styleMenuItem(new JMenuItem(supportedSoundDevice.describe()))).addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					thief.switchSoundDevice(supportedSoundDevice);
				}
			});
		}
		return outputMenu;
	}

	private JMenu createPresetsMenu() {
		JMenu presetsMenu = styleMenuItem(new JMenu("Presets"));
		try {
			URL presets = ThiefUI.class.getResource("/presets");
			URI uri = presets.toURI();
			Map<Path, JMenu> parents = new HashMap<>();
			SimpleFileVisitor<Path> addPresetToMenu = new SimpleFileVisitor<>() {
				@Override
				public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
					JMenu menuForParentDirectory = parents.get(dir.getParent());
					if (menuForParentDirectory != null) {
						JMenu menuForThisDirectory = styleMenuItem(new JMenu(dir.getFileName().toString()));
						parents.putIfAbsent(dir, menuForThisDirectory);
						menuForParentDirectory.add(menuForThisDirectory);
					}
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
					String dir = file.getParent().getFileName().toString();
					String name = file.getFileName().toString();
					if (name.endsWith(".conf")) {
						parents.get(file.getParent()).add(styleMenuItem(new JMenuItem(name))).addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								thiefUI.resetModulation();
								thief.getConfiguration().load(getClass().getResourceAsStream("/presets/" + dir + "/" + name));
								thiefUI.resetModulation();
								// todo: when we load a preset or a config, we should change the title. 
								//  If there are any changes after that, we should add a little star to the title, to show that it's been changed
							}
						});
					}
					return FileVisitResult.CONTINUE;
				}
			};
			if ("file".equalsIgnoreCase(uri.getScheme())) {
				parents.put(Path.of(uri), presetsMenu);
				Files.walkFileTree(Path.of(uri), addPresetToMenu);
			} else {
				// This is supposed to work with files as well, but for reasons that seem arbitrary, it doesn't.
				try (FileSystem fileSystem = FileSystems.newFileSystem(uri, Collections.emptyMap())) {
					Path path = fileSystem.getPath("/presets");
					parents.put(path, presetsMenu);
					Files.walkFileTree(path, addPresetToMenu);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return presetsMenu;
	}

	private JMenu createFileMenu() {
		JMenuItem resetMenuItem = styleMenuItem(new JMenuItem("Reset", KeyEvent.VK_R));
		resetMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				thief.getConfiguration().reset();
				thiefUI.resetModulation();
			}
		});

		JMenuItem resetSynthesizerMenuItem = styleMenuItem(new JMenuItem("Reset synthesizer", KeyEvent.VK_R));
		resetSynthesizerMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				thief.getConfiguration().resetSynthesizer();
				thiefUI.resetModulation();
			}
		});

		JMenuItem resetSequencerMenuItem = styleMenuItem(new JMenuItem("Reset sequencer", KeyEvent.VK_R));
		resetSequencerMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				thief.getConfiguration().resetSequencer();
			}
		});

		JMenuItem randomizeMenuItem = styleMenuItem(new JMenuItem("Randomize", KeyEvent.VK_A));
		randomizeMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				thiefUI.resetModulation();
				thief.getConfiguration().randomize();
				thiefUI.resetModulation();
			}
		});

		JMenuItem randomizeSynthesizerMenuItem = styleMenuItem(new JMenuItem("Randomize synthesizer", KeyEvent.VK_A));
		randomizeSynthesizerMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				thiefUI.resetModulation();
				thief.getConfiguration().randomizeSynthesizer();
				thiefUI.resetModulation();
			}
		});
		JMenuItem randomizeSequencerMenuItem = styleMenuItem(new JMenuItem("Randomize sequencer", KeyEvent.VK_A));
		randomizeSequencerMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				thief.getConfiguration().randomizeSequencer();
			}
		});

		JMenuItem saveMenuItem = styleMenuItem(new JMenuItem("Save", KeyEvent.VK_S));
		saveMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				JFileChooser fileChooser = new JFileChooser(thief.getConfiguration().ensureConfigurationDirectoryExists());
				int response = fileChooser.showSaveDialog(getRootPane());
				if (response == JFileChooser.APPROVE_OPTION) {
					thief.getConfiguration().save(fileChooser.getSelectedFile());
				}
			}
		});

		JMenuItem loadMenuItem = styleMenuItem(new JMenuItem("Load", KeyEvent.VK_L));
		loadMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				JFileChooser fileChooser = new JFileChooser(thief.getConfiguration().ensureConfigurationDirectoryExists());
				int response = fileChooser.showOpenDialog(getRootPane());
				if (response == JFileChooser.APPROVE_OPTION) {
					try {
						thief.getConfiguration().load(new FileInputStream(fileChooser.getSelectedFile()));
						thiefUI.resetModulation();

					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
				}
			}
		});

		JMenu fileMenu = styleMenuItem(new JMenu("File"));
		fileMenu.add(resetMenuItem);
		fileMenu.add(resetSynthesizerMenuItem);
		fileMenu.add(resetSequencerMenuItem);
		fileMenu.add(randomizeMenuItem);
		fileMenu.add(randomizeSynthesizerMenuItem);
		fileMenu.add(randomizeSequencerMenuItem);
		fileMenu.add(saveMenuItem);
		fileMenu.add(loadMenuItem);
		return fileMenu;
	}

	private <T extends JMenuItem> T styleMenuItem(T menuItem) {
		// todo: if we change the style, this has to be redone. How can we do that.
		menuItem.setFont(new Font(FontSupport.PREFERRED_FONT, Font.PLAIN, 12));
		menuItem.setForeground(Skins.currentSkin().foreground());
		menuItem.setBackground(Skins.currentSkin().background());
		return menuItem;
	}

	private JMenu createMidiMenu() {
		MidiManager midiManager = thief.getMidiManager();
		JMenu midiDeviceMenu = styleMenuItem(new JMenu("Midi device"));
		midiDeviceMenu.addMenuListener(new MenuListener() {
			@Override
			public void menuSelected(MenuEvent e) {
				midiDeviceMenu.removeAll();

				for (String compatibleDevice : midiManager.getMidiDevices().compatibleDevices()) {
					JCheckBoxMenuItem menuItem = new JCheckBoxMenuItem(compatibleDevice, midiManager.isCurrentDevice(compatibleDevice));
					midiDeviceMenu.add(styleMenuItem(menuItem)).addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							if (midiManager.isCurrentDevice(compatibleDevice)) {
								midiManager.disableMidiDevice(compatibleDevice);
							} else {
								midiManager.enableMidiDevice(compatibleDevice);
							}
						}
					});
				}
			}

			@Override
			public void menuDeselected(MenuEvent e) {
			}

			@Override
			public void menuCanceled(MenuEvent e) {
			}
		});

		JMenu midiMappingsMenu = styleMenuItem(new JMenu("Midi mappings"));
		midiMappingsMenu.add(styleMenuItem(new JMenuItem("Map controls", KeyEvent.VK_C))).addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						MidiMapperGlassPane midiMapperGlassPane = new MidiMapperGlassPane(thiefUI, thief.getMidiManager().getMidiMappings());
						getRootPane().setGlassPane(midiMapperGlassPane);
						midiMapperGlassPane.setVisible(true);
					}
				});
			}
		});
		midiMappingsMenu.add(styleMenuItem(new JMenuItem("Save midi config", KeyEvent.VK_S))).addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				midiManager.writeToDefaultFile();
			}
		});
		midiMappingsMenu.add(styleMenuItem(new JMenuItem("Save midi config as", KeyEvent.VK_A))).addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser(thief.getConfiguration().ensureConfigurationDirectoryExists());
				int response = fileChooser.showSaveDialog(getRootPane());
				if (response == JFileChooser.APPROVE_OPTION) {
					midiManager.writeToFile(fileChooser.getSelectedFile());
				}
			}
		});

		MidiReceiver midiReceiver = midiManager.getReceiver();
		JMenu midiChannelsMenu = styleMenuItem(new JMenu("Midi channels (notes)"));
		midiChannelsMenu.addMenuListener(new MenuListener() {
			@Override
			public void menuSelected(MenuEvent e) {
				midiChannelsMenu.removeAll();

				midiChannelsMenu.add(styleMenuItem(new JCheckBoxMenuItem("All", midiReceiver.listensOnAllChannels()))).addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						midiReceiver.listenOnAllChannels();
						// This menu item can't really be unchecked. 
						// The other menu items will have to be checked instead
					}
				});
				midiChannelsMenu.add(styleMenuItem(new JCheckBoxMenuItem("None", midiReceiver.listensOnNoChannels()))).addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						midiReceiver.listenOnNoChannels();
						// This menu item can't really be unchecked. 
						// The other menu items will have to be checked instead
					}
				});
				for (int i = 1; i <= 16; i++) {
					final int channel = i;
					JCheckBoxMenuItem checkBoxForChannel = new JCheckBoxMenuItem("Channel " + channel, midiReceiver.listensToNotesOnChannel(channel));
					midiChannelsMenu.add(styleMenuItem(checkBoxForChannel)).addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							boolean currentlyListensOnThisChannel = midiReceiver.listensToNotesOnChannel(channel);
							midiReceiver.listenToNotesOnChannel(channel, !currentlyListensOnThisChannel);
						}
					});
				}
			}

			@Override
			public void menuDeselected(MenuEvent e) {
			}

			@Override
			public void menuCanceled(MenuEvent e) {
			}
		});

		JMenu midiMenu = styleMenuItem(new JMenu("Midi"));
		midiMenu.add(midiDeviceMenu);
		midiMenu.add(midiMappingsMenu);
		midiMenu.add(midiChannelsMenu);
		return midiMenu;
	}

	//endregion
}
