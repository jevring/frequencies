/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.thief;

import net.jevring.frequencies.v2.hooks.Visualizer;
import net.jevring.frequencies.v2.input.Instruction;
import net.jevring.frequencies.v2.modulation.SidedDepth;
import net.jevring.frequencies.v2.modulation.matrix.ModulationMatrix;
import net.jevring.frequencies.v2.modulation.matrix.ModulationMatrixVoice;
import net.jevring.frequencies.v2.modulation.matrix.ModulationTarget;

import java.util.List;

/**
 * Visualizes modulation via a {@link net.jevring.frequencies.v2.hooks.Visualizer}.
 *
 * @author markus@jevring.net
 * @created 2021-02-24 19:35
 */
public class ThiefUIVisualizer implements Visualizer {
	private final ModulationMatrix modulationMatrix;
	private final ThiefUI thiefUI;

	public ThiefUIVisualizer(ModulationMatrix modulationMatrix, ThiefUI thiefUI) {
		this.modulationMatrix = modulationMatrix;
		this.thiefUI = thiefUI;
	}

	@Override
	public void visualizeModulation(int samplesToGenerate, List<Instruction> instructions) {
		// Get the voice we're going to use to fetch the visualization from.
		double youngestAge = Long.MAX_VALUE;
		int youngestVoice = 0;
		Instruction youngestInstruction = null;
		for (int i = 0; i < instructions.size(); i++) {
			Instruction instruction = instructions.get(i);
			if (instruction != null && instruction.getAge() < youngestAge) {
				youngestVoice = i;
				youngestAge = instruction.getAge();
				youngestInstruction = instruction;
			}
		}
		ModulationMatrixVoice representativeVoiceForModulation = modulationMatrix.voice(youngestVoice);
		// Once we have the voice, visualize the modulation
		for (ModulationTarget modulationTarget : ModulationTarget.values()) {
			SidedDepth modulationMaxForTarget = representativeVoiceForModulation.getModulationMaxForTarget(modulationTarget);
			if (modulationMaxForTarget != null) {
				// we'll just render the YOUNGEST voice here in the case of polyphony.
				// We don't technically *need* the instruction, as the sources in the voice haven't had next() called on them yet, 
				// so they'll still show cached data, but we may as well send it, because we *must* have it anyway to select the
				// youngest modulation matrix voice to get the correct visualization.
				// Same with samplesToGenerate.
				double representativeModulationValue =
						representativeVoiceForModulation.getModulationForTarget(modulationTarget, samplesToGenerate, youngestInstruction)[0];
				thiefUI.visualizedCurrentModulation(modulationTarget, modulationMaxForTarget, representativeModulationValue);
			}
		}
	}

	@Override
	public void visualizeLoopTime(double percentageOfMax) {
		thiefUI.visualizeLoopTime(percentageOfMax);
	}

	@Override
	public void visualizeWaveformChunk(double[] samples) {
		thiefUI.visualizeNewWaveformChunk(samples);
	}
}
