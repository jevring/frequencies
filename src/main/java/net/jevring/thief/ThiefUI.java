/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.thief;

import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.hooks.EuclideanSequencerVisualizer;
import net.jevring.frequencies.v2.input.sequencer.Sequencer;
import net.jevring.frequencies.v2.modulation.SidedDepth;
import net.jevring.frequencies.v2.modulation.matrix.ModulationTarget;
import net.jevring.frequencies.v2.ui.*;

import javax.imageio.ImageIO;
import javax.management.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * The full UI for {@link Thief}.
 *
 * @author markus@jevring.net
 * @created 2020-02-23 14:25
 */
public class ThiefUI extends JPanel implements EuclideanSequencerVisualizer {
	private static final ScheduledExecutorService SCHEDULER = Executors.newScheduledThreadPool(1, runnable -> {
		Thread thread = new Thread(runnable, "ui-stats-scheduler");
		thread.setDaemon(true);
		return thread;
	});
	private final Map<ModulationTarget, JModulationDisplay> modulationDisplays = new HashMap<>();
	private final JLabel loopTimeLabel = new JLabel("000000");
	private final JLabel cpuLabel = new JLabel("0.00%");
	private final JMiniWaveformDisplay miniWaveformDisplay;
	private final JModulationMatrix jModulationMatrix;
	private final JTempoPanel tempoPanel;
	private final Controls controls;
	private final Thief thief;

	public ThiefUI(Thief thief) {
		this.jModulationMatrix = new JModulationMatrix(thief.getModulationMatrix());
		this.miniWaveformDisplay = new JMiniWaveformDisplay();
		this.controls = thief.getControls();
		this.tempoPanel = new JTempoPanel(controls);
		this.thief = thief;
		createUI();


		SwingUtilities.invokeLater(() -> {
			// this shouldn't really be on the EDT, but it needs to happen *after* the UI has been created, and it should be fast, so I think it's fine
			Map<String, JModulationDisplay> modulationDisplaysByControlName = new HashMap<>();
			enumerateJModulationDisplays(this, modulationDisplaysByControlName);
			modulationDisplays.put(ModulationTarget.FILTER_CUTOFF_FREQUENCY,
			                       Objects.requireNonNull(modulationDisplaysByControlName.get("filter-cutoff-frequency")));
			modulationDisplays.put(ModulationTarget.FILTER_RESONANCE, Objects.requireNonNull(modulationDisplaysByControlName.get("q-resonance-emphasis")));
			modulationDisplays.put(ModulationTarget.PRIMARY_OSCILLATOR_DETUNE,
			                       Objects.requireNonNull(modulationDisplaysByControlName.get("primary-oscillator-detune-semi-tones")));
			modulationDisplays.put(ModulationTarget.PRIMARY_OSCILLATOR_VOLUME,
			                       Objects.requireNonNull(modulationDisplaysByControlName.get("primary-oscillator-volume")));
			modulationDisplays.put(ModulationTarget.PRIMARY_OSCILLATOR_OCTAVE,
			                       Objects.requireNonNull(modulationDisplaysByControlName.get("primary-oscillator-octave-offset")));
			modulationDisplays.put(ModulationTarget.PRIMARY_OSCILLATOR_PHASE_SHIFT,
			                       Objects.requireNonNull(modulationDisplaysByControlName.get("primary-oscillator-phase-shift")));
			modulationDisplays.put(ModulationTarget.PRIMARY_OSCILLATOR_WAVE_SHAPE,
			                       Objects.requireNonNull(modulationDisplaysByControlName.get("primary-oscillator-wave-shape")));
			modulationDisplays.put(ModulationTarget.PRIMARY_OSCILLATOR_QUANTIZATION_STEPS,
			                       Objects.requireNonNull(modulationDisplaysByControlName.get("primary-oscillator-quantization-steps")));
			modulationDisplays.put(ModulationTarget.PRIMARY_OSCILLATOR_WAVEFORM_VARIATION,
			                       Objects.requireNonNull(modulationDisplaysByControlName.get("primary-oscillator-variable-waveform")));
			modulationDisplays.put(ModulationTarget.SECONDARY_OSCILLATOR_DETUNE,
			                       Objects.requireNonNull(modulationDisplaysByControlName.get("secondary-oscillator-detune-semi-tones")));
			modulationDisplays.put(ModulationTarget.SECONDARY_OSCILLATOR_VOLUME,
			                       Objects.requireNonNull(modulationDisplaysByControlName.get("secondary-oscillator-volume")));
			modulationDisplays.put(ModulationTarget.SECONDARY_OSCILLATOR_OCTAVE,
			                       Objects.requireNonNull(modulationDisplaysByControlName.get("secondary-oscillator-octave-offset")));
			modulationDisplays.put(ModulationTarget.SECONDARY_OSCILLATOR_PHASE_SHIFT,
			                       Objects.requireNonNull(modulationDisplaysByControlName.get("secondary-oscillator-phase-shift")));
			modulationDisplays.put(ModulationTarget.SECONDARY_OSCILLATOR_WAVE_SHAPE,
			                       Objects.requireNonNull(modulationDisplaysByControlName.get("secondary-oscillator-wave-shape")));
			modulationDisplays.put(ModulationTarget.SECONDARY_OSCILLATOR_QUANTIZATION_STEPS,
			                       Objects.requireNonNull(modulationDisplaysByControlName.get("secondary-oscillator-quantization-steps")));
			modulationDisplays.put(ModulationTarget.SECONDARY_OSCILLATOR_WAVEFORM_VARIATION,
			                       Objects.requireNonNull(modulationDisplaysByControlName.get("secondary-oscillator-variable-waveform")));
			modulationDisplays.put(ModulationTarget.NOISE_OSCILLATOR_VOLUME,
			                       Objects.requireNonNull(modulationDisplaysByControlName.get("noise-oscillator-volume")));
		});
		SCHEDULER.scheduleAtFixedRate(new Runnable() {
			private final MBeanServer platformMBeanServer = ManagementFactory.getPlatformMBeanServer();

			@Override
			public void run() {
				try {
					ObjectName operatingSystemName = ObjectName.getInstance("java.lang:type=OperatingSystem");
					Double processCpuLoad = (Double) platformMBeanServer.getAttribute(operatingSystemName, "ProcessCpuLoad");
					if (processCpuLoad != null && processCpuLoad > 0) {
						String load = String.format(Locale.US, " cpu: %-3.2f%%", processCpuLoad * 100d);
						SwingUtilities.invokeLater(() -> cpuLabel.setText(load));
					}
				} catch (MBeanException | AttributeNotFoundException | InstanceNotFoundException | ReflectionException | MalformedObjectNameException e) {
					e.printStackTrace();
				}
			}
		}, 1, 1, TimeUnit.SECONDS);

		// Read keyboard keys as inputs to trigger notes
		// https://stackoverflow.com/questions/5344823/how-can-i-listen-for-key-presses-within-java-swing-across-all-components
		KeyboardInput keyboardInput = new KeyboardInput(thief.getKeyTimings());
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {
					@Override
					public boolean dispatchKeyEvent(KeyEvent e) {
						if (e.getKeyChar() != KeyEvent.CHAR_UNDEFINED) {
							if (e.getID() == KeyEvent.KEY_PRESSED) {
								keyboardInput.keyPressed(e);
							} else if (e.getID() == KeyEvent.KEY_RELEASED) {
								keyboardInput.keyReleased(e);
							}
						}
						return false;
					}
				});
			}
		});
	}

	public void modulationMatrixUpdated() {
		jModulationMatrix.modulationMatrixUpdated();
	}

	public void setBackgroundImage(String resourceImagePath) {
		// At least remove the old one, if it exists.
		for (int i = 0; i < getComponentCount(); i++) {
			Component component = getComponent(i);
			if ("Background".equals(component.getName())) {
				remove(i);
				break;
			}
		}
		if (resourceImagePath != null) {
			try {
				InputStream backgroundImageInputStream = ThiefUI.class.getResourceAsStream(resourceImagePath);
				BufferedImage bg = ImageIO.read(backgroundImageInputStream);
				JLabel bgLabel = new JLabel(new ImageIcon(bg));
				bgLabel.setName("Background");
				add(bgLabel).setBounds(0, 0, bg.getWidth(), bg.getHeight());
				setComponentZOrder(bgLabel, getComponentCount() - 1);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void createUI() {
		SwingUtilities.invokeLater(() -> {
			//JPanel bitCrusherPanel = new JBitCrusherPanel(controls);
			//JPanel chorusPanel = new JChorusPanel(controls);

			//region Layout
			setLayout(null);
			add(new LFOPanel(controls, "lfo1")).setBounds(0, 0, 145, 210);
			add(new LFOPanel(controls, "lfo2")).setBounds(145, 0, 145, 210);
			add(new LFOPanel(controls, "lfo3")).setBounds(290, 0, 145, 210);
			add(new SampleAndHoldPanel(controls)).setBounds(435, 0, 130, 210);
			add(jModulationMatrix).setBounds(0, 210, 565, 365);

			add(new JOscillatorPanel(controls, "primary-oscillator")).setBounds(565, 0, 200, 275);
			add(new JOscillatorPanel(controls, "secondary-oscillator")).setBounds(565, 275, 200, 300);

			add(new JMixerPanel(controls)).setBounds(765, 0, 70, 575);

			add(new JEnvelopePanel(controls, thief.getEnvelopes(), "volume")).setBounds(835, 0, 250, 140);
			add(new JEnvelopePanel(controls, thief.getEnvelopes(), "filter")).setBounds(835, 140, 250, 140);
			add(new JEnvelopePanel(controls, thief.getEnvelopes(), "modulation")).setBounds(835, 280, 250, 140);
			add(new JFilterPanel(controls)).setBounds(835, 420, 250, 155);

			add(new JDelayPanel(controls)).setBounds(1085, 0, 230, 110);
			add(new JReverbPanel(controls)).setBounds(1085, 110, 230, 110);
			add(new JInputPanel(controls)).setBounds(1085, 220, 230, 200);
			add(miniWaveformDisplay).setBounds(1085, 440, 230, 135);

			add(tempoPanel).setBounds(0, 572, 1315, 220);

			add(new JKeyboardPanel()).setBounds(2, 792, 1081, 20);
			add(loopTimeLabel).setBounds(1135, 792, 75, 20);
			add(cpuLabel).setBounds(1210, 792, 75, 20);
			//endregion
		});
	}

	// region Modulation
	public void resetModulation() {
		for (JModulationDisplay jModulationDisplay : modulationDisplays.values()) {
			jModulationDisplay.currentModulation(null, 0);
		}
		jModulationMatrix.modulationMatrixUpdated();
	}

	private void enumerateJModulationDisplays(JComponent c, Map<String, JModulationDisplay> modulationDisplays) {
		if (c instanceof JModulationDisplay jmd) {
			modulationDisplays.put(jmd.getControl(), jmd);
		}
		for (int i = 0; i < c.getComponentCount(); i++) {
			Component component = c.getComponent(i);
			if (component instanceof JComponent jComponent) {
				enumerateJModulationDisplays(jComponent, modulationDisplays);
			}
		}
	}
	//endregion

	//region Visualization
	@Override
	public void visualizedSequencerStep(int step, boolean on, boolean[] steps) {
		tempoPanel.step(step, on, steps);
	}

	@Override
	public void visualizedSequencerStep(int step, boolean on) {
		tempoPanel.step(step, on);
	}

	@Override
	public void visualizedSequencerBeat(Sequencer sequencer) {
		tempoPanel.beat(sequencer);
	}

	public void visualizedCurrentModulation(ModulationTarget target, SidedDepth depth, double value) {
		JModulationDisplay jModulationDisplay = modulationDisplays.get(target);
		if (jModulationDisplay != null) {
			jModulationDisplay.currentModulation(depth, value);
		}
	}

	public void visualizeLoopTime(double percentageOfMax) {
		String labelTest = String.format(Locale.US, "Loop: %03.1f%%", percentageOfMax);
		SwingUtilities.invokeLater(() -> loopTimeLabel.setText(labelTest));
	}

	public void visualizeNewWaveformChunk(double[] samples) {
		miniWaveformDisplay.newChunk(samples);
	}
	//endregion
}
