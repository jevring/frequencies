/*
 * Copyright 2021 Markus Jevring
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.thief;

import net.jevring.frequencies.v2.configuration.Configuration;
import net.jevring.frequencies.v2.control.Controls;
import net.jevring.frequencies.v2.control.DiscreteControlListener;
import net.jevring.frequencies.v2.control.curves.Exponential;
import net.jevring.frequencies.v2.control.curves.Linear;
import net.jevring.frequencies.v2.effects.FullEffectsChain;
import net.jevring.frequencies.v2.engine.MainLoopPullingEngine;
import net.jevring.frequencies.v2.engine.MasterVolume;
import net.jevring.frequencies.v2.engine.StereoBalance;
import net.jevring.frequencies.v2.engine.Voice;
import net.jevring.frequencies.v2.envelopes.ControlledEnvelope;
import net.jevring.frequencies.v2.envelopes.Envelopes;
import net.jevring.frequencies.v2.filters.Filters;
import net.jevring.frequencies.v2.hooks.EuclideanSequencerVisualizer;
import net.jevring.frequencies.v2.hooks.SequencerVisualizer;
import net.jevring.frequencies.v2.hooks.Visualizer;
import net.jevring.frequencies.v2.input.*;
import net.jevring.frequencies.v2.input.arpeggiator.Arpeggiator;
import net.jevring.frequencies.v2.input.arpeggiator.Mode;
import net.jevring.frequencies.v2.input.euclidean.EuclideanSequencer;
import net.jevring.frequencies.v2.input.midi.MidiFrequencyMapper;
import net.jevring.frequencies.v2.input.midi.MidiManager;
import net.jevring.frequencies.v2.input.monophonic.MonophonicInput;
import net.jevring.frequencies.v2.input.polyphonic.PolyphonicInput;
import net.jevring.frequencies.v2.input.scale.Scale;
import net.jevring.frequencies.v2.input.sequencer.StepSequencer;
import net.jevring.frequencies.v2.input.tempo.MainLoopTempoSource;
import net.jevring.frequencies.v2.input.tempo.MidiClockTempoSource;
import net.jevring.frequencies.v2.modular.ModularVoice;
import net.jevring.frequencies.v2.modulation.SampleAndHold;
import net.jevring.frequencies.v2.modulation.matrix.ModulationMatrix;
import net.jevring.frequencies.v2.output.Device;
import net.jevring.frequencies.v2.ui.Skins;
import net.jevring.frequencies.v2.ui.SwingUtils;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.LineUnavailableException;
import javax.swing.*;
import java.io.File;
import java.util.Arrays;

/**
 * Thief is a subtractive polyphonic synthesizer.
 *
 * @author markus@jevring.net
 * @created 2020-04-12 18:04
 */
public class Thief {
	private static final int VOICES = 8;
	private static final String DEFAULT_PRESET = "/presets/long/maffigt som FAN.conf";
	private static final float SAMPLE_SIZE_IN_BITS = Short.SIZE;
	private static final float SAMPLE_RATE = 44100f;
	private static final int STEREO = 2;
	public static final AudioFormat AUDIO_FORMAT = new AudioFormat(SAMPLE_RATE, (int) SAMPLE_SIZE_IN_BITS, STEREO, true, true);
	private final KeyTimings keyTimings = new KeyTimings();
	private final Filters filters = new Filters(SAMPLE_RATE);
	private final Controls controls = new Controls();
	private final Envelopes envelopes = new Envelopes();
	private final ModulationMatrix modulationMatrix = new ModulationMatrix(controls, VOICES);
	private final MidiManager midiManager;
	private final MainLoopPullingEngine engine;
	private final StepSequencer stepSequencer;
	private final Arpeggiator arpeggiator;
	private final EuclideanSequencer euclideanSequencer;
	private final Configuration configuration;
	private final MainLoopTempoSource mainLoopTempoSource;
	private final MidiClockTempoSource midiClockTempoSource;

	// todo: bind MIDI program change messages to saved configurations/presets. 

	// todo. if nothing feeds into the envelopes, we can feed them into the LFO rates. 
	//  That'll make things cascading in the modulation section, which will make things harder, but can lead to cool things like this:
	//  https://youtu.be/rB7rDnY-zcY?t=410

	public Thief() {
		createControls();
		createSequencerControls();
		createArpeggiatorControls();
		createEuclideanSequencerControls();
		midiClockTempoSource = new MidiClockTempoSource();
		mainLoopTempoSource = new MainLoopTempoSource(controls);
		File dotConfigDirectory = new File(new File(System.getProperty("user.home")), ".config/thief");
		midiManager = new MidiManager(controls, keyTimings, midiClockTempoSource, dotConfigDirectory);
		midiManager.initializeMidi();
		stepSequencer = new StepSequencer(midiManager.getReceiver(), controls, keyTimings, SequencerVisualizer.noop());
		arpeggiator = new Arpeggiator(midiManager.getReceiver(), controls, keyTimings, SequencerVisualizer.noop());
		euclideanSequencer = new EuclideanSequencer(controls, keyTimings, EuclideanSequencerVisualizer.noop());
		this.configuration = new Configuration(modulationMatrix, controls, dotConfigDirectory);
		configuration.load(Thief.class.getResourceAsStream(DEFAULT_PRESET));

		ControlledEnvelope volumeEnvelope = new ControlledEnvelope(controls, envelopes, "volume");
		InstructionFactory instructionFactory = new InstructionFactory(new MidiFrequencyMapper(),
		                                                               controls.getDiscreteControl("input-scale").mapping(Scale::valueOf),
		                                                               controls.getControl("input-scale-root-note"),
		                                                               volumeEnvelope);
		MonophonicInput monophonicInput = new MonophonicInput(instructionFactory, keyTimings, controls);
		PolyphonicInput polyphonicInput = new PolyphonicInput(instructionFactory, keyTimings, VOICES);
		InstructionInput instructionInput = new SwitchableMonophonicAndPolyphonicInstructionInput(polyphonicInput, monophonicInput, controls, VOICES);

		// All voices should use the same sample-and-hold because this makes sense from a UI point of view.
		SampleAndHold sampleAndHold =
				new SampleAndHold(controls.getControl("sample-and-hold-frequency"), controls.getControl("sample-and-hold-slew"), SAMPLE_RATE);
		Voice[] voices = new Voice[Thief.VOICES];
		for (int i = 0; i < Thief.VOICES; i++) {
			voices[i] = new ModularVoice(SAMPLE_RATE, filters, envelopes, controls, sampleAndHold, modulationMatrix.voice(i));
		}

		engine = new MainLoopPullingEngine(new StereoBalance(controls),
		                                   instructionInput,
		                                   new FullEffectsChain(controls, SAMPLE_RATE),
		                                   new MasterVolume(controls),
		                                   SAMPLE_RATE,
		                                   voices);

		controls.getBooleanControl("receive-midi-clock-active").addListener(new DiscreteControlListener<>() {
			@Override
			public void valueChanged(Boolean value, Object source) {
				selectTempoSource(value);
			}
		});
	}

	public void startEngine(Device device) throws LineUnavailableException {
		engine.start(device.open());
		mainLoopTempoSource.start();
	}

	public void setVisualizers(SequencerVisualizer sequencerVisualizer, Visualizer visualizer, EuclideanSequencerVisualizer euclideanSequencerVisualizer) {
		engine.setVisualizer(visualizer);
		stepSequencer.setSequencerVisualizer(sequencerVisualizer);
		arpeggiator.setSequencerVisualizer(sequencerVisualizer);
		euclideanSequencer.setSequencerVisualizer(euclideanSequencerVisualizer);
	}

	public void stopEngine() {
		mainLoopTempoSource.stop();
		engine.stop();
	}

	private void createEuclideanSequencerControls() {
		controls.createControl("euclidean-sequencer-gate", 0, 1, 0.25, new Linear());
		controls.createControl("euclidean-sequencer-note", 0, 127, 60, new Linear(), true);
		controls.createControl("euclidean-sequencer-rotation", 0, EuclideanSequencer.MAX_STEPS, 0, new Linear(), true);
		controls.createControl("euclidean-sequencer-inner-steps", 0, EuclideanSequencer.MAX_STEPS, 6, new Linear(), true);
		controls.createControl("euclidean-sequencer-outer-steps", 0, EuclideanSequencer.MAX_STEPS, 16, new Linear(), true);
		controls.createControl("euclidean-sequencer-note-variance", 0, 64, 0, new Linear(), true);
		controls.createBooleanControl("euclidean-sequencer-active", false);
	}

	private void createArpeggiatorControls() {
		controls.createControl("arpeggiator-octaves", 1, 4, 1, new Linear(), true);
		controls.createControl("arpeggiator-gate", 0, 1, 0.5, new Linear());
		controls.createBooleanControl("arpeggiator-active", false);
		controls.createDiscreteControl("arpeggiator-mode", Arrays.stream(Mode.values()).map(String::valueOf).toList(), "UP");
	}

	private void createSequencerControls() {
		controls.createControl("sequencer-steps", 1, 16, 16, new Linear(), true);
		controls.createBooleanControl("sequencer-active", false);
		controls.createBooleanControl("sequencer-listen", false);
		for (int step = 0; step < 16; step++) {
			controls.createControl("sequencer-note-" + step, 21, 108, 57, new Linear(), true);
			controls.createControl("sequencer-gate-" + step, 0, 1, 0.75, new Linear());
		}
	}

	private void createControls() {
		controls.createControl("input-scale-root-note", 0, 11, 0, new Linear(), true);
		controls.createDiscreteControl("input-scale", Arrays.stream(Scale.values()).map(Scale::name).toList(), Scale.CHROMATIC.name());

		controls.createControl("primary-oscillator-pitch-bend-depth", -1, 1, 1, new Linear(), false);
		controls.createControl("secondary-oscillator-pitch-bend-depth", -1, 1, 1, new Linear(), false);

		controls.createControl("tempo", 10, 750, 138, new Linear(), true);
		controls.createControl("swing", 0.5, 0.64, 0.5, new Linear(), false);
		controls.createBooleanControl("receive-midi-clock-active", false);
		// See TempoTools.normalize() for a description of the rate
		controls.createControl("rate", 2, 24, 24, new Linear(), true);

		controls.createDiscreteControl("input-mode", Arrays.stream(InputMode.values()).map(Enum::name).toList(), InputMode.MONOPHONIC.name());

		controls.createControl("bit-crusher-downsampling", 1, 128, 1, new Linear(), true);
		controls.createControl("bit-crusher-resolution", 1, 16, 16, new Linear(), true);
		controls.createControl("primary-oscillator-unison-detune-semi-tones", 0, 2, 1, new Exponential());
		controls.createControl("primary-oscillator-unison-voices", 1, 16, 1, new Exponential(), true);
		controls.createControl("secondary-oscillator-unison-detune-semi-tones", 0, 2, 1, new Exponential());
		controls.createControl("secondary-oscillator-unison-voices", 1, 16, 1, new Exponential(), true);
		controls.createControl("sample-and-hold-frequency", 0, 50, 0, new Exponential());
		controls.createControl("sample-and-hold-slew", 0, 1, 0, new Exponential());

		controls.createControl("experimental-value", 0, 2, 0, new Exponential());
		controls.createControl("pitch-bend", -1, 1, 0, new Linear());

		controls.createControl("filter-envelope-depth", -1, 1, 0, new Linear());
		controls.createControl("filter-cutoff-frequency", 20, 20_000, 20_000, new Exponential());
		controls.createControl("q-resonance-emphasis", 0, 1, 1d / Math.sqrt(2), new Exponential());

		controls.createControl("lfo1-frequency", 0, 1300, 0, new Exponential());
		controls.createControl("lfo2-frequency", 0, 20, 0, new Exponential());
		controls.createControl("lfo3-frequency", 0, 20, 0, new Exponential());
		controls.createControl("lfo1-wave-shape", -1, 1, 0, new Linear());
		controls.createControl("lfo2-wave-shape", -1, 1, 0, new Linear());
		controls.createControl("lfo3-wave-shape", -1, 1, 0, new Linear());
		controls.createControl("overdrive-volume", 0, 10, 1, new Exponential());
		controls.createControl("balance", -1, 1, 0, new Linear());


		controls.createControl("volume-envelope-attack", 0, 10_000, 125, new Exponential());
		controls.createControl("volume-envelope-decay", 0, 10_000, 350, new Exponential());
		controls.createControl("volume-envelope-sustain", 0, 1, 0.6, new Linear());
		controls.createControl("volume-envelope-release", 0, 10_000, 550, new Exponential());
		controls.createBooleanControl("volume-envelope-loop", false);

		controls.createControl("filter-envelope-attack", 0, 10_000, 125, new Exponential());
		controls.createControl("filter-envelope-decay", 0, 10_000, 350, new Exponential());
		controls.createControl("filter-envelope-sustain", 0, 1, 0.6, new Linear());
		controls.createControl("filter-envelope-release", 0, 10_000, 550, new Exponential());
		controls.createBooleanControl("filter-envelope-loop", false);

		controls.createControl("modulation-envelope-attack", 0, 10_000, 0, new Exponential());
		controls.createControl("modulation-envelope-decay", 0, 10_000, 800, new Exponential());
		controls.createControl("modulation-envelope-sustain", 0, 1, 0, new Linear());
		controls.createControl("modulation-envelope-release", 0, 10_000, 0, new Exponential());
		controls.createBooleanControl("modulation-envelope-loop", false);

		controls.createControl("primary-oscillator-octave-offset", -4, 4, 0, new Linear(), true);
		controls.createControl("primary-oscillator-detune-semi-tones", -12, 12, 0, new Linear());
		controls.createControl("primary-oscillator-phase-shift", -0.5, 0.5, 0, new Linear());
		controls.createControl("primary-oscillator-wave-shape", -1, 1, 0, new Linear());
		controls.createControl("primary-oscillator-quantization-steps", 1, 16, 16, new Linear());
		controls.createControl("primary-oscillator-variable-waveform", 0, 3, 2, new Linear());
		controls.createControl("secondary-oscillator-octave-offset", -4, 4, 0, new Linear(), true);
		controls.createControl("secondary-oscillator-detune-semi-tones", -12, 12, 0, new Linear());
		controls.createControl("secondary-oscillator-phase-shift", -0.5, 0.5, 0, new Linear());
		controls.createControl("secondary-oscillator-wave-shape", -1, 1, 0, new Linear());
		controls.createControl("secondary-oscillator-quantization-steps", 1, 16, 16, new Linear());
		controls.createControl("secondary-oscillator-variable-waveform", 0, 3, 2, new Linear());
		controls.createControl("noise-oscillator-volume", 0, 1, 0, new Linear());
		controls.createControl("primary-oscillator-volume", 0, 1, 0.5, new Linear());
		controls.createControl("secondary-oscillator-volume", 0, 1, 0.5, new Linear());
		controls.createControl("ring-modulator-volume", 0, 1, 0d, new Linear());
		controls.createControl("glide-length", 0, 1, 0, new Exponential());

		// Quantization is off at 16
		controls.createControl("lfo1-quantization-steps", 1, 16, 16, new Linear());
		controls.createControl("lfo2-quantization-steps", 1, 16, 16, new Linear());
		controls.createControl("lfo3-quantization-steps", 1, 16, 16, new Linear());
		// 2 is saw
		controls.createControl("lfo1-variable-waveform", 0, 3, 2, new Linear());
		controls.createControl("lfo2-variable-waveform", 0, 3, 2, new Linear());
		controls.createControl("lfo3-variable-waveform", 0, 3, 2, new Linear());

		controls.createControl("delay-dry-wet-mix", 0, 1, 1, new Exponential());
		controls.createControl("delay-in-milliseconds", 0, 2500, 0, new Exponential());
		controls.createControl("delay-decay", 0, 1, 0.75, new Exponential());

		controls.createControl("reverb-dry-wet-mix", 0, 1, 1, new Exponential());
		// Anything more than 5 starts producing weird echoes
		controls.createControl("reverb-length", 1, 5, 1, new Linear());

		controls.createControl("chorus-dry-wet-mix", 0, 1, 0.5, new Exponential());
		controls.createControl("chorus-delay-in-milliseconds-1", 0, 20, 5, new Exponential());
		controls.createControl("chorus-delay-in-milliseconds-2", 0, 20, 7, new Exponential());
		controls.createControl("chorus-rate-1", 0, 15, 0.2, new Exponential());
		controls.createControl("chorus-rate-2", 0, 15, 0.5, new Exponential());
		controls.createControl("chorus-depth-1", 0, 7, 0.1, new Exponential());
		controls.createControl("chorus-depth-2", 0, 7, 0.2, new Exponential());

		controls.createBooleanControl("velocity-sensitive-keys", false);
		controls.createBooleanControl("clip", true);

		controls.createDiscreteControl("volume-envelope-type", envelopes.all(), "LinearADSR");
		controls.createDiscreteControl("filter-envelope-type", envelopes.all(), "LinearADSR");
		controls.createDiscreteControl("modulation-envelope-type", envelopes.all(), "ExponentialDecay");

		controls.createBooleanControl("lfo1-key-reset", true);
		controls.createBooleanControl("lfo2-key-reset", true);
		controls.createBooleanControl("lfo3-key-reset", true);

		controls.createBooleanControl("legato", false);

		controls.createDiscreteControl("filter", filters.all(), "Stilson4PoleLadderFilter");
	}

	public Controls getControls() {
		return controls;
	}

	public Envelopes getEnvelopes() {
		return envelopes;
	}

	public KeyTimings getKeyTimings() {
		return keyTimings;
	}

	public ModulationMatrix getModulationMatrix() {
		return modulationMatrix;
	}

	public MidiManager getMidiManager() {
		return midiManager;
	}

	public Configuration getConfiguration() {
		return configuration;
	}

	private void selectTempoSource(boolean midiSyncOn) {
		if (midiSyncOn) {

			midiClockTempoSource.addSequencer(stepSequencer);
			midiClockTempoSource.addSequencer(arpeggiator);
			midiClockTempoSource.addSequencer(euclideanSequencer);


			mainLoopTempoSource.removeSequencer(euclideanSequencer);
			mainLoopTempoSource.removeSequencer(arpeggiator);
			mainLoopTempoSource.removeSequencer(stepSequencer);

		} else {

			mainLoopTempoSource.addSequencer(stepSequencer);
			mainLoopTempoSource.addSequencer(arpeggiator);
			mainLoopTempoSource.addSequencer(euclideanSequencer);

			midiClockTempoSource.removeSequencer(euclideanSequencer);
			midiClockTempoSource.removeSequencer(arpeggiator);
			midiClockTempoSource.removeSequencer(stepSequencer);

		}
	}

	public void switchSoundDevice(Device device) {
		stopEngine();
		try {
			startEngine(device);
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws LineUnavailableException {
		Thief thief = new Thief();
		String headless = System.getProperty("headless");
		if (headless == null || "false".equalsIgnoreCase(headless)) {
			ThiefUI ui = new ThiefUI(thief);
			JFrame window = SwingUtils.showWindowFor("Thief v1.5-SNAPSHOT", ui, true);
			window.setJMenuBar(new ThiefUIMenuBar(ui, thief));
			ui.modulationMatrixUpdated();
			thief.setVisualizers(ui, new ThiefUIVisualizer(thief.getModulationMatrix(), ui), ui);
			thief.euclideanSequencer.initializeSequencerVisualization();
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					Skins.setSkin(Skins.defaultSkin(), window);
				}
			});
		}
		thief.startEngine(Device.defaultDevice(AUDIO_FORMAT));
	}
}
