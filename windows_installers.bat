
jpackage -i target/output --main-class net.jevring.thief.Thief --main-jar frequencies-1.4-thief.jar --win-dir-chooser --win-shortcut --win-menu --win-menu-group Thief --name Thief --app-version 1.4 --description "Thief is a polyphonic subtractive synthesizer" --vendor "Bandit Works" 
jpackage -i target/output --main-class net.jevring.scoundrel.Scoundrel --main-jar frequencies-1.4-scoundrel.jar --win-dir-chooser --win-shortcut --win-menu --win-menu-group Scoundrel --name Scoundrel --app-version 1.0 --description "Scoundrel is a synthesizer drum machine" --vendor "Bandit Works" 
